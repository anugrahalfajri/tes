<?php

namespace App\Imports;

use App\Models\Eform;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Str;

class EformImportExcel implements OnEachRow, WithStartRow
{
    public $target, $title, $form_id, $directory_id;

    public function __construct($target, $title, $form_id, $directory_id)
    {
        $this->target       = $target;
        $this->title        = $title;
        $this->form_id      = $form_id; 
        $this->directory_id = $directory_id;
    }

    public function startRow(): int
    {
        return 2;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function onRow(Row $row)
    {
        if (!isset($row[0]) || !isset($row[1])) {
            return null;
        }

        $rowIndex = $row->getIndex();

        $eform = Eform::create([
            'form_id'       => $this->form_id,
            'target'        => $this->target,
            'title'         => $this->title,
            'question'      => $row[0],
            'answer_type'   => $row[1],
            'slug'          => Str::slug($this->title),
            'created_by'    => auth()->user()->name,
            'id_directory'  => $this->directory_id,
            'question_no'   => $rowIndex - 1,
        ]);
        
        if ($row[1] == 'radio_button' || $row[1] == 'check_box' || $row[1] == 'select_box') {
            $choice = explode(",", $row[2]);

            foreach ($choice ?? [] as $value) {
                $eform->eform_detail()->create([
                    'answer_choice' => $value,
                    'count_answer'  => 0
                ]);
            }
        } else if ($row[1] == 'multi') {
            $eform->eform_detail()->create([
                'answer_choice' => $row[2] ?? 1,
                'count_answer'  => 0
            ]);
        }
    }
}
