<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

use App\Models\EformSubmit;

class EformAnswer implements ToCollection, WithValidation, WithHeadingRow
{
    private $form_id, $user_ip;

    public function __construct($form_id, $user_ip)
    {
        $this->form_id = $form_id;
        $this->user_ip = $user_ip;
    }

    public function rules(): array
    {
        return [
            'answer_number'     => 'required|numeric',
            'question_number'   => 'required|numeric',
            'answer_text'       => 'nullable',
        ];
    }

    public function collection(Collection $rows)
    {
        $rows = $rows->groupBy('answer_number');

        foreach($rows as $row){   
            $answer = $row->mapWithKeys(function($item, $key){
                $question_no = "answer_$item[question_number]";

                return [$question_no => $item["answer_text"]];
            });

            EformSubmit::create([
                'form_id'           => $this->form_id,
                'answer_eform'      => json_encode($answer),
                'user_id'           => auth()->user()->id,
                'user_ip_address'   => $this->user_ip
            ]);
        }
    }
}
