<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\File_Uploads;

class FileUnique implements Rule
{
    private $directory, $filename;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id_directory, $file)
    {
        $this->directory = $id_directory;
        $this->filename  = $file;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $filename = $this->filename;

        $file = File_Uploads::whereRaw("SUBSTRING(nama_file, 12) = '$filename'")->where('id_directory', $this->directory)->where('is_deleted', '0')->count();

        return $file < 1;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Terdapat file dengan nama sama, silahkan gunakan fitur reupload';
    }
}
