<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\ConfigUpload;

class ConfigUploadRule implements Rule
{
    public $modul, $message, $exclude;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($modul, $exclude = [])
    {
        $this->modul   = $modul;
        $this->exclude = $exclude;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {        
        $result = true;
        $config = ConfigUpload::where('modul_name', $this->modul)->first();
        $size   = $value->getSize() / 1024;
        $ext    = $value->getClientOriginalExtension();

        if(!empty($this->exclude)){
            if(in_array($ext, $this->exclude)){
                if($config){
                    $allowed = explode(';', $config->allowed_mime_types);

                    if(in_array($ext, $allowed) && $size > $config->max_file_size){
                        $result = false;
                        $this->message = "File size must be less than $config->max_file_size Kb";
                    }  
                }

                return $result;
            }
        }

        if($config) {
            $allowed = explode(';', $config->allowed_mime_types);
            if($size > $config->max_file_size){
                $result = false;
                $this->message = "File size must be less than $config->max_file_size Kb";
            }

            if(!in_array($ext, $allowed)){
                $result = false;
                $this->message = "File type not allowed"; 
            }
        }

        return $result;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
