<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Models\Directory;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // $directory = \DB::select("SELECT * FROM directory where parent_id is null");
        $directory = Directory::where('parent_id', null)->with('child')->get();
        View::share('root_dir', $directory);
    }
}
