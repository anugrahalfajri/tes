<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Roles;
use App\Models\Properties;
use App\Models\Ext_Properties;
use App\Models\Version_Log;
use App\Models\Directory;
use App\Models\Workflow;
use App\Models\Workflow_Approval;
use App\Models\Workflow_Detail;
use App\Models\WorkflowDetailApproval;
use App\Models\Note_List;
use App\Models\History;
use Illuminate\Support\Facades\DB;
use App\Models\Tag;
use App\Models\File_Uploads;
use App\Models\Notification;
use App\Models\AccessRight;
use App\Models\OrganizationAccess;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Jobs\WorkflowMailJob;

class WorkflowController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!OrganizationAccess::checkPermission("workflow-view", Auth::user()->id)) {
                return abort(401);
            }
    
            return $next($request);
        });
    }
    public function index(){
        $role_id =  Auth::user()->role_id;
        $name =  Auth::user()->name;
        $role = Roles::where('id', $role_id)->first();
        $level = Auth::user()->level;
        $version = Version_Log::leftjoin('file_uploads', 'file_uploads.id', 'version_log.id_file')
        ->select('version_log.*', 'file_uploads.nama_file as file')->get();
        $notelist = Note_List::leftjoin('file_uploads', 'file_uploads.id', 'note_list.id_file')
        ->select('note_list.*','file_uploads.id as id_file', 'file_uploads.nama_file as file')->get();
        $history = History::leftjoin('file_uploads', 'file_uploads.id', 'history.id_file')
        ->leftjoin('users', 'users.id', 'history.id_user')
        ->select('history.*', 'file_uploads.nama_file as file','users.name as user')->get();
        $result = array();
        // $workflow = DB::select("select workflow.*, file_uploads.nama_file, file_uploads.member, file_uploads.id as id_file from workflow LEFT JOIN file_uploads on file_uploads.id = workflow.id_file");
        $workflow = Workflow::select('workflow.id', 'workflow.nama', 'workflow.created_at', 'workflow.created_by', DB::raw("count(workflow_detail.id_workflow) as total_document"), DB::raw("( SELECT count(status) FROM workflow_detail WHERE id_workflow = workflow.id AND status = '0') as total_pending"))
                    ->leftjoin('workflow_detail', 'workflow.id', 'workflow_detail.id_workflow')
                    ->groupBy('workflow.id', 'workflow.nama', 'workflow.created_at', 'workflow.created_by')
                    ->orderBy('workflow.id')
                    ->get();
        return view('workflow.index', [
            'role' => $role,
            'name' => $name,
            'level' => $level,
            'workflow' => $workflow,
            'properties' => Properties::leftjoin('workflow_status', 'workflow_status.id', 'properties.id_status')
            ->leftjoin('version_log', 'version_log.id', 'properties.id_version')
            ->leftjoin('file_uploads', 'file_uploads.id', 'properties.id_file')
            ->select('properties.*', 'version_log.version as version', 'workflow_status.status as status')
            ->first(),
            'log_version' => $version,
            'notelist' => $notelist,
            'history' => $history,
            'tag' => Tag::all(),
            'directory' => DB::select("SELECT * FROM directory where parent_id is not null"),
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3")
        ]);
    }

    function status($status){
        if($status == '0'){
            $text = 'Pending';
        }
        else if($status == '1'){
            $text = 'Approved';
        }
        else if($status == '2'){
            $text = 'Rejected';
        } else {
            $text = '';
        }
        return $text;
    }

    public function create(){
        $role = Roles::where('id', auth()->user()->role_id ?? null)->first();
        if (auth()->user()->role_id == 4) {
            $users = User::has('role')->where('id_organization', auth()->user()->id_organization)->get();
        } else {
            $users = User::has('role')->get();
        }
        
        return view('workflow.create', [
            'role' => $role,
            'users' => $users
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|string',
            'level'=> 'required|array'
        ]);

        try {

            DB::beginTransaction();

            $parent_id = '';
            $directory = Directory::where('nama_folder', 'Workflow')->first();
            if (!$directory) {
                Directory::create([
                    'nama_folder' => 'Workflow',
                ]);
                $parent_id = DB::getPdo()->lastInsertId();
            } else {
                $parent_id = $directory->id;
            }
            
            Directory::create([
                'parent_id' => $parent_id,
                'nama_folder' => $request->nama,
            ]);
            $directory_id = DB::getPdo()->lastInsertId();

            Workflow::create([
                'nama' => $request->nama,
                'description' => $request->description,
                'id_directory' => $directory_id,
                'created_by' => Auth::user()->name
            ]);
            $workflow_id = DB::getPdo()->lastInsertId();
            
            foreach ($request->level as $key => $value) {
                $workflow = New Workflow_Approval();
                $workflow->id_workflow = $workflow_id;
                $workflow->member = $value;
                $workflow->save();
            }

            $email = User::whereIn('name', $request->level)->get(['email'])->toArray();

            Notification::create([
                'title' => 'Approval Request',
                'text'  => "Workflow $request->nama is requesting an approval."
            ]);
            // dispatch(new WorkflowMailJob($workflow_id, $email));
    
            DB::commit();

            return redirect()->route(routePrefix().'workflow')->with('success_alert','Data has been saved');

        } catch (\Throwable $th) {
            DB::rollback();
            report($th);

            return redirect()->back()->with('error_alert', 'Something went wrong!')->withInput();
        }
    }

    public function store_detail(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|file',
            'title'=> 'required|string'
        ]);

        $photos_path = public_path('/documents');
        if (!is_dir($photos_path)) {
            mkdir($photos_path, 0777);
        }
        
        $document = $request->file('file');
        $documentName = date('ymdHi') . '-' . $document->getClientOriginalName();
        $documentSize = $document->getSize();
        $document->move(public_path('documents'), $documentName);

        $up = File_Uploads::create([
                'nama_file' => $documentName,
                'path_file' => '/public/documents/',
                'member' => Auth::user()->name,
                'id_directory' => $request->id_directory,
                'is_deleted' => '0',
                'title' => $request->title,
                'description' => $request->description,
            ]);
        $lastId = DB::getPdo()->lastInsertId();
        
        $prop = Properties::create([
                    'name' => $documentName,
                    'title' => $documentName,
                    'folder' => $this->get_folder_name_for_properties($request->id_directory),
                    'size' => $documentSize,
                    'id_file' => $lastId,
                    'created_by' => Auth::user()->name,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_by' => Auth::user()->name,
                    'updated_at' => date('Y-m-d H:i:s'),
                    'permalink' => '/public/workflow/'.$documentName
                ]);

        $his = History::create([
            'description' => 'Upload Workflow Detail',
            'id_file' => $lastId,
            'id_user' => Auth::user()->id
        ]);

        Workflow_Detail::create([
            'id_file' => $lastId,
            'id_workflow' => $request->id,
            'status' => '0',
            'due_date' => Carbon::parse($request->due_date)->format('Y-m-d'),
            'priority' => $request->priority
        ]);
        $id_workflow_detail = DB::getPdo()->lastInsertId();
        $workflow_approval = Workflow_Approval::where('id_workflow', $request->id)->get();

        foreach ($workflow_approval as $key => $value) {
            WorkflowDetailApproval::create([
                'id_workflow_detail' => $id_workflow_detail,
                'level' => $value->member,
                'status' => '0'
            ]);

            AccessRight::create([
                'file_upload_id' => $lastId,
                'user' => $value->member,
                'read' => 't',
                'create' => 't',
                'edit' => 't',
                'delete' => 't',
                'download' => 't'
            ]);
        }
   
        return redirect()->route(routePrefix().'workflow')->with('success_alert','Data has been saved');
    }

    public function show($id){
        $file = DB::select("select file_uploads.*, workflow_detail.id as id_detail from workflow_detail left join file_uploads on file_uploads.id = workflow_detail.id_file where workflow_detail.id_workflow = $id");
        if(!empty($file)){
            foreach ($file as $row) {
                echo '<tr>'.
                    '<td>'.$row->nama_file.'</td>'.
                    '<td>'.$row->created_at.'</td>'.
                    '<td>'.$row->status.'</td>'.
                    // '<td>'.
                    //     '<a onclick="detail_file('.$row->id.')" class="btn btn-primary">View</a>'.
                    // '</td>'.
                '</tr>';
            }
        }
    }

    public function approve(Request $request){
        $workflow_id = $request->id;
        $pin = $request->pin;
        $workflow = Workflow::where('id', $workflow_id)->first();
        if($workflow->pin == $pin){
            $workflow = New Workflow_Approval();
            $workflow->id_workflow = $workflow_id;
            $workflow->status = $request->status;
            $workflow->member = Auth::user()->name;
            $workflow->save();
            // notification
            $notification = New Notification();
            $notification->title = 'Approve Workflow';
            $notification->text = Auth::user()->name.' has been approve file';
            $notification->save();
            $alert = 'success';
            $message = 'Data has been approved';
        }else{
            $alert = 'error';
            $message = 'Wrong PIN, approve failed';
        }
        return redirect()->route(routePrefix().'workflow')->with($alert.'_alert',$message);
    }

    public function get_folder_name_for_properties($folder_id)
    {
        $folder = Directory::where('id', $folder_id)->first();
        if ($folder) {
            if (!empty($folder->parent_id)) {
                $parent_folder = Directory::where('id', $folder->parent_id)->first();
                $folder_path = $parent_folder->nama_folder . ' > ' . $folder->nama_folder;
            } else {
                $folder_path = $folder->nama_folder;
            }
        } else {
            $folder_path = '-';
        }
        return $folder_path;
    }

    public function detail($id) 
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $workflow = Workflow::find($id);
        $workflow_detail = Workflow_Detail::select('file_uploads.id', 'file_uploads.title', 'file_uploads.nama_file', 'properties.folder', 'workflow_detail.created_at', 'properties.created_by', 'workflow_detail.id AS id_workflow_detail', DB::raw("(CASE WHEN workflow_detail.status = '0' THEN 'Pending' WHEN workflow_detail.status = '1' THEN 'Approved' ELSE 'Rejected' END) AS status"))
                        ->leftJoin('file_uploads', 'workflow_detail.id_file', 'file_uploads.id')
                        ->leftJoin('properties', 'file_uploads.id', 'properties.id_file')
                        ->where('workflow_detail.id_workflow', $id)
                        ->get();
        $directory = Directory::all();

        return view('workflow.detail', [
            'role' => $role,
            'workflow' => $workflow,
            'workflow_detail' => $workflow_detail,
            'directory' => $directory
        ]);
    }

    public function getWorkflowInfo(Request $request)
    {
        $workflow = Workflow::select('workflow.*', 'directory.nama_folder')
                        ->leftJoin('directory', 'workflow.id_directory', 'directory.id')
                        ->with('workflow_approval')
                        ->where('workflow.id', $request->id_workflow)
                        ->first();
        return response()->json(['data' => $workflow]);
    }

    public function getWorkflowDetail(Request $request)
    {
        $file_upload = File_Uploads::select('file_uploads.nama_file', 'file_uploads.title', 'file_uploads.description', 'file_uploads.created_at', 'properties.folder', 'properties.created_by', 'workflow_detail.due_date', 'workflow_detail.priority')
                        ->leftJoin('properties', 'file_uploads.id', 'properties.id_file')
                        ->leftJoin('workflow_detail', 'file_uploads.id', 'workflow_detail.id_file')
                        ->where('file_uploads.id', $request->file_upload_id)
                        ->first();
        return response()->json(['data' => $file_upload]);
    }

    public function getWorkflowDetailStatus(Request $request)
    {
        $file_upload = WorkflowDetailApproval::select('level', DB::raw("(CASE WHEN status = '0' THEN 'Pending' WHEN status = '1' THEN 'Approved' ELSE 'Rejected' END) AS status"))
                        ->where('id_workflow_detail', $request->id_workflow_detail)
                        ->get();
        return response()->json(['data' => $file_upload]);
    }
}
