<?php

namespace App\Http\Controllers\Publics;

require_once public_path('plugins/phpdocx/classes/CreateDocx.php');
require_once public_path('plugins/DocxConversion.php');
// require_once public_path('plugins/tbs/tbs_class.php');
// require_once public_path('plugins/tbs_plugin_opentbs/tbs_plugin_opentbs.php');

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\EformDetail;
use App\Models\EformSubmit;
use App\Models\Eform;
use App\Models\Roles;
use App\Models\Directory;
use App\Models\File_Uploads;
use App\Models\History;
use App\Models\Properties;
use App\Http\Controllers\WorkflowController;
use CreateDocx;
use DocxConversion;
use clsTinyButStrong;
use Auth;
use DB;
use Illuminate\Support\Str;
use App\Models\OrganizationAccess;

class EformController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $formId
     * @return \Illuminate\Http\Response
     */
    public function show_public($slug)
    {
        $eform = Eform::with('eform_detail')->where('slug', $slug)->get();
        $isPaging = $eform->first()->paging_form ?? false;

        if($eform->isEmpty()){
            return view('eform.inactive');
        }

        if($isPaging){
            $eform = $eform->groupBy('paging_form_number');
        }

        return view('eform.show_public', [
            'eform' => $eform,
            'isPaging' => $isPaging
        ]);
    }

    /**
     * Submit form
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function submit_public(Request $request) {
        $validateArr = [];
        foreach ($request->is_mandatory as $d) {
            $validateArr += [$d => 'required'];
        }

        $this->validate($request, $validateArr,['*.required' => "Please fill the required field"]);

        try {
            DB::beginTransaction();

            $isIpTrack  = Eform::isIpTracking($request->form_id); 
            $data       = $this->populateInput($request->directory_id, $request);
            $eform_submit = new EformSubmit;
            $eform_submit->form_id = $request->form_id;
            $eform_submit->answer_eform = json_encode($data);
            $eform_submit->user_id = null;
            $eform_submit->user_ip_address = $request->ip();
            $eform_submit->save();
    
            DB::commit();

            $expire     = 30*24*3600;
            $cookieName = "eform_submission";

            return $eform_submit->eform->action_submit == "thanks_page" ?
                redirect()->route('eform.submit.thanks')->cookie(
                    $cookieName, set_cookie_value($cookieName, $request->form_id), $expire
                ) : redirect()->away($eform_submit->eform->action_submit_url)->cookie(
                    $cookieName, set_cookie_value($cookieName, $request->form_id), $expire
                );


            return redirect()->back()->with('success_alert','Data has been submitted')->cookie(
                $cookieName, set_cookie_value($cookieName, $request->form_id), $expire
            );
    
        } catch (\Throwable $th) {
            DB::rollback();
            dd($th);

            return redirect()->back()->with('failed_msg', 'Something went wrong!')->withInput();
        }
    }

    public function thankspage()
    {
        return view('eform.thankyou');
    }

    public function getLocationByIp($request)
    {
        $ip_address = $request->ip();
        $geoIp = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$ip_address"));

        return (["lat" => $geoIp["geoplugin_latitude"], "lng" => $geoIp["geoplugin_longitude"]]);
    }

    public function populateInput($directory_id, $input)
    {
        $allFiles = $input->allFiles();
        $data     = $input->except('_token', 'form_id', 'directory_id');

        if(!empty($allFiles)){
            $photos_path = public_path('/documents/eform');
            if (!is_dir($photos_path)) {
                mkdir($photos_path, 0777);
            }

            $attachment_dir = Directory::firstOrCreate([
                'parent_id' => $directory_id, 'nama_folder' => 'attachment'
            ],[
                'parent_id' => $directory_id, 'nama_folder' => 'attachment'
            ]);

            $config = ConfigUpload::where('modul_name', 'eform')->first();
            $allowed = [];
            $allowed_size = 1024;

            if($config) {    
                $allowed = explode(';', $config->allowed_mime_types);
                $allowed_size = $config->max_file_size;
            }

            foreach($allFiles ?? [] as $inputname => $file){
                $ext          = $file->getClientOriginalExtension();
                $documentName = date('ymdHi') . '-' . $file->getClientOriginalName();
                $documentSize = $file->getSize();
                $file->move(public_path('documents/eform'), $documentName);

                if($config && !in_array($ext, $allowed)) throw new \Exception("File type not allowed $ext");
                if($config && ($documentSize / 1024) > $allowed_size)  throw new \Exception("File size must be less than $allowed_size Kb");

                $uploaded = File_Uploads::create([
                    'nama_file'     => $documentName,
                    'path_file'     => "/public/documents/eform/",
                    'member'        => auth()->user()->name,
                    'id_directory'  => $attachment_dir->id,
                    'is_deleted'    => '0'
                ]);

                $prop = Properties::create([
                    'name' => $documentName,
                    'title' => $documentName,
                    'folder' => (new WorkflowController)->get_folder_name_for_properties($attachment_dir->id),
                    'size' => $documentSize,
                    'id_file' => $uploaded->id,
                    'created_by' => Auth::user()->name,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_by' => Auth::user()->name,
                    'updated_at' => date('Y-m-d H:i:s'),
                    'permalink' => '/public/documents/eform/'.$documentName,
                ]);

                $his = History::create([
                    'description' => 'Upload Eform File',
                    'id_file' => $uploaded->id,
                    'id_user' => Auth::user()->id
                ]);

                $data[$inputname] = $documentName;
            }            
        }
        return $data;
    }

}
