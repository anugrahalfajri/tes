<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Roles;
use App\Models\Properties;
use App\Models\Ext_Properties;
use App\Models\Version_Log;
use App\Models\Note_List;
use App\Models\History;
use Illuminate\Support\Facades\DB;
use App\Models\Tag;
use App\Models\File_Uploads;
use App\Models\OrganizationAccess;

use Illuminate\Support\Facades\Auth;
use File;

class TrashController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!OrganizationAccess::checkPermission("trash-view", Auth::user()->id)) {
                return abort(401);
            }
    
            return $next($request);
        });
    }
    public function index(){
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $file_upload = File_Uploads::where('is_deleted', '1')->get();
        $properties = Properties::leftjoin('workflow_status', 'workflow_status.id', 'properties.id_status')
        ->leftjoin('version_log', 'version_log.id', 'properties.id_version')
        ->leftjoin('file_uploads', 'file_uploads.id', 'properties.id_file')
        ->select('properties.*', 'version_log.version as version', 'workflow_status.status as status')
        ->first();
        return view('trash/list', [
            'role' => $role,
            'file_upload' => $file_upload,
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3"),
            'properties' => $properties
        ]);
    }

    public function move_to_trash($id) {
        $upload = File_Uploads::where('id', $id)->first();
        if(!empty($upload)){
            $upload->is_deleted = '1';
            $upload->deleted_by = Auth::user()->name;
            $upload->deleted_at = date('Y-m-d H:i:s');
            $upload->save();
        }

        return redirect()->route(routePrefix().'root')->with('success_alert','Data has been deleted');
    }

    public function restore_from_trash($id) {
        $upload = File_Uploads::where('id', $id)->first();
        if(!empty($upload)){
            $upload->is_deleted = '0';
            $upload->save();
        }

        return redirect()->route(routePrefix().'root')->with('success_alert','Data has been restored');
    }

    public function permanent_delete_from_trash($id) {
        $upload = File_Uploads::where('id', $id)->first();
        $properties = Properties::where('id_file', $id);
        $ext_properties = Ext_Properties::where('id_file', $id);
        $version = Version_Log::where('id_file', $id);
        $note_list = Note_List::where('id_file', $id);
        $history = History::where('id_file', $id);
        if(!empty($upload)){
            if (PHP_OS_FAMILY === "Windows") {
                $file_copy = File::delete(public_path("documents\\".$upload->nama_file));
            } else {
                $file_copy = File::delete(public_path("documents/".$upload->nama_file));
            }
            
            $upload->delete();
            $properties->delete();
            $ext_properties->delete();
            $version->delete();
            $note_list->delete();
            $history->delete();
        }

        return redirect()->route(routePrefix().'root')->with('success_alert','Data permanently deleted');
    }
}
