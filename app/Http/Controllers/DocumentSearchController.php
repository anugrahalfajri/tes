<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Roles;
use App\Models\Properties;
use App\Models\Ext_Properties;
use App\Models\Version_Log;
use App\Models\Note_List;
use App\Models\History;
use Illuminate\Support\Facades\DB;
use App\Models\Tag;
use App\Models\File_Uploads;
use App\Models\OrganizationAccess;
use Illuminate\Support\Facades\Auth;

class DocumentSearchController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!OrganizationAccess::checkPermission("document-search-view", Auth::user()->id)) {
                return abort(401);
            }
    
            return $next($request);
        });
    }
    public function index(){
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $file_upload = File_Uploads::where('is_deleted', '0')->get();
        $version = Version_Log::leftjoin('file_uploads', 'file_uploads.id', 'version_log.id_file')
            ->select('version_log.*', 'file_uploads.nama_file as file')->get();
        $notelist = Note_List::leftjoin('file_uploads', 'file_uploads.id', 'note_list.id_file')
            ->select('note_list.*','file_uploads.id as id_file', 'file_uploads.nama_file as file')->get();
        $history = History::leftjoin('file_uploads', 'file_uploads.id', 'history.id_file')
        ->leftjoin('users', 'users.id', 'history.id_user')
        ->select('history.*', 'file_uploads.nama_file as file','users.name as user')->get();
        return view('document-search.list', [
            'role' => $role,
            'properties' => Properties::leftjoin('workflow_status', 'workflow_status.id', 'properties.id_status')
            ->leftjoin('version_log', 'version_log.id', 'properties.id_version')
            ->leftjoin('file_uploads', 'file_uploads.id', 'properties.id_file')
            ->select('properties.*', 'version_log.version as version', 'workflow_status.status as status')
            ->first(),
            'ext_properties' => Ext_Properties::leftjoin('file_uploads', 'file_uploads.id', 'extented_properties.id_file')
            ->select('extented_properties.*','file_uploads.id as id_file','file_uploads.nama_file as file')->first(),
            'log_version' => $version,
            'file_upload' => $file_upload,
            'notelist' => $notelist,
            'history' => $history,
            'tag' => Tag::all(),
            'directory' => DB::select("SELECT * FROM directory where parent_id is not null"),
            'directory_selection' => $this->directory_builder(),
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3")
        ]);
    } 
    public function directory_builder() {
        $directory = DB::select("SELECT * FROM directory WHERE parent_id is null ORDER BY nama_folder");
        $optionstring = "";
        foreach ($directory as $row) {
            $optionstring .= '<option disabled="disabled" value="'.$row->id.'">'.$row->nama_folder.'</option>';
            $subdirectory = DB::select("SELECT * FROM directory WHERE parent_id = $row->id ORDER BY nama_folder");
            foreach ($subdirectory as $subrow) {
                $optionstring .= '<option value="'.$subrow->id.'">-- '.$subrow->nama_folder.'</option>';
            }
        }
        return $optionstring;
    }
    public function store(Request $request){
        $nama_file = $request->nama_file;
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $file_upload = DB::select("SELECT * from file_uploads where nama_file like '%$nama_file%' and is_deleted='0'");
        $version = Version_Log::leftjoin('file_uploads', 'file_uploads.id', 'version_log.id_file')
            ->select('version_log.*', 'file_uploads.nama_file as file')->get();
        $notelist = Note_List::leftjoin('file_uploads', 'file_uploads.id', 'note_list.id_file')
            ->select('note_list.*','file_uploads.id as id_file', 'file_uploads.nama_file as file')->get();
        $history = History::leftjoin('file_uploads', 'file_uploads.id', 'history.id_file')
        ->leftjoin('users', 'users.id', 'history.id_user')
        ->select('history.*', 'file_uploads.nama_file as file','users.name as user')->get();
        return view('document-search/list', [
            'role' => $role,
            'nama_file' => $nama_file,
            'properties' => Properties::leftjoin('workflow_status', 'workflow_status.id', 'properties.id_status')
            ->leftjoin('version_log', 'version_log.id', 'properties.id_version')
            ->leftjoin('file_uploads', 'file_uploads.id', 'properties.id_file')
            ->select('properties.*', 'version_log.version as version', 'workflow_status.status as status')
            ->first(),
            'ext_properties' => Ext_Properties::leftjoin('file_uploads', 'file_uploads.id', 'extented_properties.id_file')
            ->select('extented_properties.*','file_uploads.id as id_file','file_uploads.nama_file as file')->first(),
            'log_version' => $version,
            'file_upload' => $file_upload,
            'notelist' => $notelist,
            'history' => $history,
            'tag' => Tag::all(),
            'directory' => DB::select("SELECT * FROM directory where parent_id is not null"),
            'directory_selection' => $this->directory_builder(),
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3"),

        ]);
    }  

    public function search_detail(Request $request){
        $opsi = $request->opsi;
        $value = $request->value;
        $id_directory = $request->id_directory;
        $role = Roles::where('id', Auth::user()->role_id)->first();
        if($opsi == 'id_directory'){
            $file_upload = DB::select("SELECT file_uploads.* from file_uploads left join extented_properties on extented_properties.id_file = file_uploads.id
            LEFT JOIN properties on properties.id_file = file_uploads.id where id_directory = $id_directory and is_deleted='0'");
        } else {

            $file_upload = DB::select("SELECT file_uploads.* from file_uploads left join extented_properties on extented_properties.id_file = file_uploads.id
            LEFT JOIN properties on properties.id_file = file_uploads.id where $opsi like '%$value%' and is_deleted='0'");
        }
        $version = Version_Log::leftjoin('file_uploads', 'file_uploads.id', 'version_log.id_file')
            ->select('version_log.*', 'file_uploads.nama_file as file')->get();
        $notelist = Note_List::leftjoin('file_uploads', 'file_uploads.id', 'note_list.id_file')
            ->select('note_list.*','file_uploads.id as id_file', 'file_uploads.nama_file as file')->get();
        $history = History::leftjoin('file_uploads', 'file_uploads.id', 'history.id_file')
        ->leftjoin('users', 'users.id', 'history.id_user')
        ->select('history.*', 'file_uploads.nama_file as file','users.name as user')->get();
        return view('document-search/list', [
            'role' => $role,
            'properties' => Properties::leftjoin('workflow_status', 'workflow_status.id', 'properties.id_status')
            ->leftjoin('version_log', 'version_log.id', 'properties.id_version')
            ->leftjoin('file_uploads', 'file_uploads.id', 'properties.id_file')
            ->select('properties.*', 'version_log.version as version', 'workflow_status.status as status')
            ->first(),
            'ext_properties' => Ext_Properties::leftjoin('file_uploads', 'file_uploads.id', 'extented_properties.id_file')
            ->select('extented_properties.*','file_uploads.id as id_file','file_uploads.nama_file as file')->first(),
            'log_version' => $version,
            'file_upload' => $file_upload,
            'notelist' => $notelist,
            'history' => $history,
            'tag' => Tag::all(),
            'directory' => DB::select("SELECT * FROM directory where parent_id is not null"),
            'directory_selection' => $this->directory_builder(),
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3"),
            'tab_detail' => 1
        ]);
    }
}
