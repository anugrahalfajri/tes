<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ResetController extends Controller
{
    public function reset(){
        $user = User::all();
        foreach ($user as $data){
            $data->password = Hash::make("123");
            $data->save();
        }

        return "Sukses";
    }
}
