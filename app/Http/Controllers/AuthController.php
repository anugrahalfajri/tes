<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Organization;
use App\Models\User;
use Carbon\Carbon;

class AuthController extends Controller
{
    //

    public function logout()
    {
        $guard = Auth::getDefaultDriver();
        Auth::guard($guard)->logout(); // menghapus session yang aktif
        return redirect()->route(routePrefix().'login');
    }

    public function showRegisterForm()
    {
        $organizations = Organization::where('status', 1)->get();
        return view('auth.register.form', [
            'organizations' => $organizations
        ]);
    }

    public function registerUser(Request $request)
    {
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->email), // default
            'phone' => $request->phone,
            'id_organization' => $request->id_organization,
            'join_date' => Carbon::now(),
            'status' => 1
        ];
        $user = User::create($data);
        $user->sendEmailVerificationNotificationGuest();

        // \Session::flash('confirm', 'Check your email!'); 
        return redirect()->back()->with('confirm', 'Check your email!');
    }

    public function verify(Request $request)
    {
        try {
            $hasVerifiedEmail = false;
            $isExpired = false;
            $invalidLinkVerification = false;
            $error = false;
            $message = '';

            $user = User::find($request['id']);

            // if (!$user) {
            //     $error = true;
            //     $message = 'Data tidak ditemukan';
            //     return response()->json(['message' => $message]);
            // }

            // if ($user->hasVerifiedEmail()) {
            //     $hasVerifiedEmail = true;
            //     $message = 'Sudah verifikasi';
            //     return response()->json(['message' => $message]);
            // }

            $user->email_verified_at = date('Y-m-d g:i:s');
            $user->save();
            return view('auth.register.verification.form', compact('user'));
        } catch (\Exception $e) {
            // Log::error($e->getMessage());
            abort(500);
        }
    }

    public function updatePassword(Request $request, $id)
    {
        $request->validate([
            'password' => 'required|min:6|regex:/^.*(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[@$!%*#?&]).*$/|confirmed',
            'pin' => 'required|numeric|digits:6|confirmed'
        ],
        [
            'password.regex' => 'Your password must be more than 8 characters long, should contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character.',
        ]);

        $users = User::find($id);
        $users->update([
            'password' => bcrypt($request->password),
            'pin' => bcrypt($request->pin)
        ]);

        Auth::logout();

        \Session::flash('pending_approval', 'User need approval!'); 
        return redirect()->route(routePrefix().'login')->with('success_alert','Password and PIN has been updated');
    }
}
