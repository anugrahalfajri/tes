<?php

namespace App\Http\Controllers;

use App\Models\Message;
use App\Models\Roles;
use App\Models\OrganizationAccess;
use Illuminate\Http\Request;

use DB;
use Auth;

class PrivateMessageController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!OrganizationAccess::checkPermission("private-message-view", Auth::user()->id)) {
                return abort(401);
            }
    
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $inbox = DB::select("SELECT message.*, users.name as users from message left join users on users.id = message.from where message.to = $user_id");
        $outbox = DB::select("SELECT message.*, users.name as users from message left join users on users.id = message.to where message.from = $user_id");
        $role = Roles::where('id', Auth::user()->role_id)->first();
        return view('private-message.index', [
            'inbox' => $inbox,
            'outbox' => $outbox,
            'role' => $role,
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3")
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $user_id = Auth::user()->id;
        $user = DB::select("SELECT * from users where id != $user_id");
        return view('private-message.insert', [
            'role' => $role,
            'user' => $user,
            'action' => 'sent',
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3")
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'to' => 'required',
            'text' => 'required',
        ]);
        $data = [
            'text' => $request->text,
            'from' => Auth::user()->id,
            'to' => $request->to,
        ];
        Message::create($data);
        return redirect()->route(routePrefix().'private-message.index')->with('success_alert','Message has been sent');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $arr_message = array();
        $message = DB::select("SELECT message.*, users.name as users from message left join users on users.id = message.from where message.id = $id");
        $reply = DB::select("SELECT message.*, users.name as users from message left join users on users.id = message.from where message.reply = $id");
        $merge = array_merge((array) array('from' => $message[0]->users, 'message' => $message[0]->text, 'reply' => @$reply[0]->users, 'reply_text' => @$reply[0]->text));
        array_push($arr_message, (object)$merge);
        if(!empty($arr_message)){
            foreach ($arr_message as $row) {
                echo '<div class="float-left">'.
                '<label>'.$row->from.'</label><br>'.
                '<div class="p-3 bg-secondary text-white"">'.
                    $row->message.
                '</div>'.
                '</div>';
                if(!empty($row->reply)){
                    echo '<div class="float-right" style="margin-top:100px;">'.
                    '<label>'.$row->reply.'</label><br>'.
                    '<div class="p-3 bg-success text-white"">'.
                        $row->reply_text.
                    '</div>'.
                    '</div>';
                } else {
                    echo '<div class="float-right" style="margin-top:100px;">'.
                    '</div>';
                }
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $message = Message::find($id);
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $user_id = Auth::user()->id;
        $user = DB::select("SELECT * from users where id = $message->from");
        return view('private-message.insert', [
            'role' => $role,
            'action' => 'reply',
            'user' => $user[0],
            'message' => $message,
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3")
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'text' => 'required',
        ]);
        $data = [
            'text' => $request->text,
            'from' => Auth::user()->id,
            'action' => 'sent',
            'to' => $request->to,
            'reply' => $id,
        ];
        Message::create($data);
        return redirect()->route(routePrefix().'private-message.index')->with('success_alert','Message has been sent');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
