<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Roles;
use App\Models\History;
use App\Models\Properties;
use App\Models\Directory;
use App\Models\Workflow;
use App\Models\Workflow_Approval;
use App\Models\Workflow_Detail;
use App\Models\WorkflowDetailApproval;
use App\Models\OrganizationAccess;
use Illuminate\Support\Facades\DB;
use App\Models\File_Uploads;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use File;

class ApprovalWorkflowController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!OrganizationAccess::checkPermission("approval-workflow-view", Auth::user()->id)) {
                return abort(401);
            }
    
            return $next($request);
        });
    }
    public function index()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $workflow_detail = Workflow_Detail::select('workflow_detail.id', 'file_uploads.id AS file_upload_id', 'file_uploads.title', 'workflow_detail.created_at', 'properties.created_by', 'workflow_detail_approval.id AS id_workflow_detail_approval', 'workflow_detail_approval.status', DB::raw("(CASE WHEN workflow_detail_approval.status = '0' THEN 'Pending' WHEN workflow_detail_approval.status = '1' THEN 'Approved' ELSE 'Rejected' END) AS status_name"))
                            ->join('workflow_detail_approval', 'workflow_detail.id', 'workflow_detail_approval.id_workflow_detail')
                            ->leftJoin('file_uploads', 'workflow_detail.id_file', 'file_uploads.id')
                            ->leftJoin('properties', 'file_uploads.id', 'properties.id_file')
                            ->where('workflow_detail_approval.level', Auth::user()->name)
                            // ->where('workflow_detail_approval.status', '0')
                            ->groupBy('workflow_detail.id', 'file_uploads.id', 'file_uploads.title', 'workflow_detail.created_at', 'properties.created_by', 'workflow_detail_approval.id', 'workflow_detail_approval.status')
                            ->get();
        
        return view('approval-workflow.index', [
            'role' => $role,
            'workflow_detail' => $workflow_detail
        ]);
    }

    public function show(Request $request)
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $workflow_detail = Workflow_Detail::select('workflow_detail.id', 'file_uploads.id AS file_upload_id', 'file_uploads.title', 'workflow_detail.created_at', 'properties.created_by', 'workflow_detail_approval.id AS id_workflow_detail_approval', 'workflow_detail_approval.status', DB::raw("(CASE WHEN workflow_detail_approval.status = '0' THEN 'Pending' WHEN workflow_detail_approval.status = '1' THEN 'Approved' ELSE 'Rejected' END) AS status_name"))
                            ->join('workflow_detail_approval', 'workflow_detail.id', 'workflow_detail_approval.id_workflow_detail')
                            ->leftJoin('file_uploads', 'workflow_detail.id_file', 'file_uploads.id')
                            ->leftJoin('properties', 'file_uploads.id', 'properties.id_file')
                            ->where('workflow_detail_approval.level', Auth::user()->name)
                            ->where('workflow_detail_approval.status', $request->get('status'))
                            ->groupBy('workflow_detail.id', 'file_uploads.id', 'file_uploads.title', 'workflow_detail.created_at', 'properties.created_by', 'workflow_detail_approval.id', 'workflow_detail_approval.status')
                            ->get();
        
        return view('approval-workflow.index', [
            'role' => $role,
            'status' => $request->get('status'),
            'workflow_detail' => $workflow_detail
        ]);
    }

    public function approval(Request $request)
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        try {
            $approval = WorkflowDetailApproval::where('id_workflow_detail', $request->id_workflow_detail)->where('level',  Auth::user()->name)->update([
                'status' => $request->status
            ]);
    
            $check = WorkFlowDetailApproval::where('id_workflow_detail', $request->id_workflow_detail)->where('status', '0')->get();
            if ($check->isEmpty()) {
                Workflow_Detail::where('id', $request->id_workflow_detail)->update([
                    'status' => $request->status
                ]);
            }

            $workflow_detail = Workflow_Detail::where('id', $request->id_workflow_detail)->first();
            History::create([
                'description' => 'Approval File',
                'id_file' => $workflow_detail->id_file,
                'id_user' => Auth::user()->id
            ]);

            return response()->json(['data' => 'Data has been updated']);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function getWorkflowApprovalDetail(Request $request)
    {
        $workflow_approval_detail = Workflow_Detail::select('file_uploads.nama_file', 'file_uploads.title', 'file_uploads.description', 'file_uploads.created_at', 'properties.folder', 'properties.created_by', 'workflow_detail.id', 'workflow_detail.due_date', 'workflow_detail.priority', 'workflow_detail_approval.id AS id_workflow_detail_approval', 'workflow_detail_approval.status')
                                    ->leftJoin('workflow_detail_approval', 'workflow_detail.id', 'workflow_detail_approval.id_workflow_detail')
                                    ->leftJoin('file_uploads', 'workflow_detail.id_file', 'file_uploads.id')
                                    ->leftJoin('properties', 'file_uploads.id', 'properties.id_file')
                                    ->with('workflow_detail_approval')
                                    ->where('workflow_detail_approval.id', $request->id_workflow_detail_approval)
                                    ->first();
        return response()->json(['data' => $workflow_approval_detail]);
    }

    public function getPinUser(Request $request)
    {
        $user = User::where('id', Auth::user()->id)->first();
        $check = Hash::check($request->pin, $user->pin);
        if ($check) {
            return response()->json([
                'status' => 'success',
                'message' => 'PIN OK'
            ], 200);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Unmatch PIN'
            ], 401);
        }        
    }

    public function saveSignature(Request $request)
    {
        try {

            if ($doc = $request->file('file')) {
                $oldFile = public_path('documents') . $request->oldFile;

                // echo $request->file->getClientOriginalName();exit;
                // echo public_path('documents'), '/(NEW)-'.$request->oldFile;exit;
                // $doc->move(public_path('documents'), '/(NEW)-'.$request->oldFile);
                $file = $request->file->storeAs('public/documents', '(signed)'.$request->oldFile);
                $response   = [
                    "status"    => true,
                    "message"   => "save successfully"
                ];

            }else{
                $response   = [
                    "status"    => false,
                    "message"   => "file not found"
                ];
            }

            return response()->json($response, 200);

        } catch (\Throwable $th) {
            report($th);

            return response()->json([
                "status"    => false,
                "message"   =>$th->getMessage()
            ], 500);
        }
    }
}