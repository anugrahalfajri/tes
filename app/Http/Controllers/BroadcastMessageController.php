<?php

namespace App\Http\Controllers;

use App\Models\Message;
use App\Models\Roles;
use App\Models\OrganizationAccess;
use Illuminate\Http\Request;
use App\Models\User;
use DB;
use Auth;

class BroadcastMessageController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!OrganizationAccess::checkPermission("broadcast-message-view", Auth::user()->id)) {
                return abort(401);
            }
    
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $message = DB::select("SELECT message.*, users.name as users, sent_to.name from message left join users on users.id = message.from left join users sent_to on sent_to.id = message.to where message.from = $user_id");
        $role = Roles::where('id', Auth::user()->role_id)->first();
        return view('broadcast-message.index', [
            'message' => $message,
            'role' => $role,
            'action' => 'sent',
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3")
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $user_id = Auth::user()->id;
        $user = DB::select("SELECT * from users where id != $user_id and role_id = 4");
        return view('broadcast-message.insert', [
            'role' => $role,
            'action' => 'sent',
            'user' => $user,
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3")
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'to' => 'required_if:type,selected_admin',
            'text' => 'required',
        ]);
        $data = $this->setAddress($request);
        Message::insert($data);

        return redirect()->route(routePrefix().'broadcast-message.index')->with('success_alert','Message has been sent');
    }

    public function setAddress($request)
    {
        $type = $request->type;
        $data = [];
        $user_role = [
            "user_admin"    => [2,4],
            'admin'         => [4]
        ];

        if($type == 'selected_admin'){
            $to = $request->to;
        }else{
            $to = User::whereIn('role_id', $user_role[$type])->get();
        }

        foreach($to ?? [] as $t){
            $data[] = [
                'to'    => $type == 'selected_admin' ? $t : $t->id,
                'text'  => $request->text,
                'from'  => auth()->user()->id,
                'created_at' => now(),
                'updated_at' => now()
            ];
        }

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
       /**
     * Display the specified resource.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $message = DB::select("SELECT message.*, users.name as users from message left join users on users.id = message.from where message.id = $id");
        $reply = DB::select("SELECT message.*, users.name as users from message left join users on users.id = message.from where message.reply = $id");
        $arr_message = [
            'from' => $message[0]->users, 
            'message' => $message[0]->text,
            'reply' => []
        ];
        foreach ($reply as $key => $value) {
            $merge = [
                'reply' => $value->users, 
                'reply_text' => $value->text
            ];
            array_push($arr_message['reply'], $merge);
        }
        if(!empty($arr_message)){
            echo '<div class="bubble-left">'.
            '<label>'.$arr_message['from'].'</label><br>'.
            '<div class="p-3 bg-secondary text-white"">'.
                $arr_message['message'].
            '</div>'.
            '</div>';
            foreach ($arr_message['reply'] as $row) {
                if(!empty($row['reply'])){
                    echo '<div class="bubble-right">'.
                    '<label>'.$row['reply'].'</label><br>'.
                    '<div class="p-3 bg-success text-white"">'.
                        $row['reply_text'].
                    '</div>'.
                    '</div>';
                } else {
                    echo '<div class="bubble-right">'.
                    '</div>';
                }
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $message = Message::find($id);
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $user_id = Auth::user()->id;
        $user = DB::select("SELECT * from users where id = $message->from");
        return view('broadcast-message.insert', [
            'role' => $role,
            'action' => 'reply',
            'user' => $user[0],
            'message' => $message,
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3")
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'text' => 'required',
        ]);
        $data = [
            'text' => $request->text,
            'from' => Auth::user()->id,
            'to' => $request->to,
            'reply' => $id,
        ];
        Message::create($data);
        return redirect()->route(routePrefix().'broadcast-message.index')->with('success_alert','Message has been sent');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
