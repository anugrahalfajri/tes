<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notification;
use App\Models\Roles;
use DB;
use Auth;

class NotificationController extends Controller
{
    public function index()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        return view('notifications.index', [
            'role' => $role,
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3"),
            'notifications' => DB::select("SELECT * from notifications")
        ]);
    }
}
