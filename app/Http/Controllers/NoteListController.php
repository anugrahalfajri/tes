<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Note_List;
use Illuminate\Support\Facades\Auth;

class NoteListController extends Controller
{
    public function getId(Request $request)
    {

        return response()->json(array('msg' => $request->id_file), 200);
    }

    public function getNoteListById(Request $request)
    {

        $seluruhNotes = Note_List::leftjoin('file_uploads', 'file_uploads.id', 'note_list.id_file')->
        select('note_list.id', 'note_list.writer', 'note_list.notes', 'note_list.created_at')->where('note_list.id_file', $request->id_file)->get();
        return $seluruhNotes->toJson();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Note_List::create([
            'writer' => Auth::user()->name,
            'id_file' => $request->id_file,
            'notes' => $request->notes,
        ]);
        $success_alert = 'Data has been saved';
        $content = $request->id_file;
        return redirect()->route(routePrefix().'document-search')->with(compact('success_alert', 'content'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
