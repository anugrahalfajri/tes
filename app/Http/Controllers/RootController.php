<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Roles;
use App\Models\Properties;
use App\Models\Ext_Properties;
use App\Models\Version_Log;
use App\Models\Note_List;
use App\Models\History;
use App\Models\Workflow;
use App\Models\Workflow_Detail;
use App\Models\WorkflowDetailApproval;
use Illuminate\Support\Facades\DB;
use App\Models\Tag;
use App\Models\File_Uploads;
use Illuminate\Support\Facades\Auth;
use App\Models\Directory;
use App\Models\AccessRight;
use App\Models\DocumentRelation;
use \Illuminate\Support\Str;

class RootController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $file_upload = File_Uploads::all();
        $version = Version_Log::leftjoin('file_uploads', 'file_uploads.id', 'version_log.id_file')
            ->select('version_log.*', 'file_uploads.nama_file as file')->get();
        $notelist = Note_List::leftjoin('file_uploads', 'file_uploads.id', 'note_list.id_file')
            ->select('note_list.*','file_uploads.id as id_file', 'file_uploads.nama_file as file')->get();
        $users = User::all();
        $workflows = Workflow::get();

        return view('root/list', [
            'role' => $role,
            'properties' => Properties::leftjoin('workflow_status', 'workflow_status.id', 'properties.id_status')
            ->leftjoin('version_log', 'version_log.id', 'properties.id_version')
            ->leftjoin('file_uploads', 'file_uploads.id', 'properties.id_file')
            ->select('properties.*', 'version_log.version as version', 'workflow_status.status as status')
            ->first(),
            'log_version' => $version,
            'file_upload' => $file_upload,
            'notelist' => $notelist,
            'tag' => Tag::all(),
            'parent_directory' => DB::select("SELECT * FROM directory where parent_id is null"),
            'directory' => DB::select("SELECT * FROM directory where parent_id IS NULL"),
            'directory_selection' => $this->directory_builder(),
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3"),
            'users' => $users,
            'workflows' => $workflows
        ]);
    }

    public function test() {
        var_dump($this->directory_builder());
    }

    public function directory_builder() {
        $directory = DB::select("SELECT * FROM directory WHERE parent_id is null ORDER BY nama_folder");
        $optionstring = "";
        foreach ($directory as $row) {
            $optionstring .= '<option disabled="disabled" value="'.$row->id.'">'.$row->nama_folder.'</option>';
            $subdirectory = DB::select("SELECT * FROM directory WHERE parent_id = $row->id ORDER BY nama_folder");
            foreach ($subdirectory as $subrow) {
                $optionstring .= '<option value="'.$subrow->id.'">-- '.$subrow->nama_folder.'</option>';
            }
        }
        return $optionstring;
    }

    public function get_file(Request $request)
    {
        $id = $request->rowid;
        $folder = explode(";", $request->folder);
        $fullpath = str_replace(";", " / ", $request->folder);
        $path = count($folder) >= 4 ? $folder[0] . ' / ....  /  ' . $folder[count($folder) - 2] . ' / ' . $folder[count($folder) - 1]
                                    :str_replace(";", " / ", $request->folder);

        $file_upload = File_Uploads::where('id_directory', $id)->where('is_deleted', '0')->get();
        echo    
        "<h4 class='card-title title-hover' data-toggle='popover' data-trigger='focus' data-content='$fullpath'>$path</h4>".
        '<input type="hidden" class="id_directory" name="id_directory" value="'.$id.'">'.
        '<h6 class="card-subtitle"><a href="#modal-upload" class="btn btn-sm btn-primary upload" data-toggle="modal">Upload File</a></h6><hr>'.
        '<table id="example" class="table table-striped" style="width:100%">'.
        '<thead>'.
            '<tr>'.
                '<th>Nama Dokumen</th>'.
            '</tr>'.
        '</thead>'.
        '<tbody>';
        if(!empty($file_upload)){
            foreach($file_upload as $row){
                $filename = Str::limit($row->nama_file, $limit = 30, $end = ' .....');
                $workfow = '';
                if ($row->title == null) {
                    $workflow = 'non-workflow';
                } else {
                    $workflow = 'workflow';
                }
                
                if (auth()->user()->role_id == 4) {
                    echo
                    '<tr>'.
                        '<td data-toggle="popover" data-content="'.$row->nama_file.'">'.$filename.'<br>'.
                            '<button onclick="detail('.$row->id.')" class="btn btn-xs btn-info" data-id="'.$row->id.'" data-status="'.$workflow.'">Info</button>&nbsp;'.
                        '</td>'.
                    '</tr>';
                } else {
                    echo
                    '<tr>'.
                        '<td data-toggle="popover" data-content="'.$row->nama_file.'">'.$filename.'<br><button onclick="detail('.$row->id.')" class="btn btn-xs btn-info" data-id="'.$row->id.'" data-status="'.$workflow.'">Info</button>&nbsp;'.
                            '<button onclick="preview('.$row->id.')" class="btn btn-xs btn-secondary preview">Preview</button>&nbsp;'.
                            '<button onclick="openaction('.$row->id.')" class="btn btn-xs btn-success">Action</button>&nbsp;'.
                        '</td>'.
                    '</tr>';
                }
                
            }
        }
        echo '</tbody>'.
    '</table>';
    }

    public function get_file_home(Request $request)
    {
        $id = $request->rowid;
        $folder = explode(";", $request->folder);
        $fullpath = str_replace(";", " / ", $request->folder);
        $path = count($folder) >= 4 ? $folder[0] . ' / ....  /  ' . $folder[count($folder) - 2] . ' / ' . $folder[count($folder) - 1]
                                    :str_replace(";", " / ", $request->folder);

        echo '<ol class="breadcrumb" data-toggle="popover" data-trigger="focus" data-content="'.$fullpath.'">
        <li class="breadcrumb-item"><a href="javascript:;" id="backToFolders"><i class="mdi mdi-folder"></i>&nbsp; '. $folder[0] .'</a></li>';
        if(count($folder) >= 4){
            echo '<li class="breadcrumb-item active" id="folder-item-active"> .... </li>';
        }
        if(count($folder) > 2){
            echo '<li class="breadcrumb-item active" id="folder-item-active">' . $folder[count($folder) - 2] . '</li>';
        }
        if(count($folder) > 3){
            echo '
            <li class="breadcrumb-item active" id="folder-item-active">' . $folder[count($folder) - 1] . '</li>';
        }
        echo '</ol><div id="scrollGroup">';

        $this->get_sub_home($request);
        $file_upload = File_Uploads::where('id_directory', $id)->where('is_deleted', '0')->get();
        if(!empty($file_upload)){
            echo '<div id="main-files" class="d-flex align-items-stretch flex-wrap">';
            foreach($file_upload as $row){
            $filename = Str::limit($row->nama_file, $limit = 30, $end = ' .....');
            $workfow = '';
            if ($row->title == null) {
                $workflow = 'non-workflow';
            } else {
                $workflow = 'workflow';
            }

            echo '<div class="d-inline-flex">
              <button class="folder-container" data-toggle="popover" data-content="'.$row->nama_file.'">
                <div class="folder-icon">
                  <i class="fa fa-file file-icon-color"></i>
                </div>
                <div class="folder-name file-div" data-fileid="'.$row->id.'">'.$filename.'</div>
              </button>
            </div>';
            }
            echo '</div></div>';
        }
        echo '</tbody>'.
    '</table>';
    }


    public function get_sub(Request $request)
    {
        $id = $request->rowid;
        $folder = $request->folder;
        $no = 1;
        $directory = Directory::where('parent_id', $id)->get();
        echo
        '<ul class="navbar-nav">';
            if(!empty($directory)){
            foreach($directory as $row){
                $path = $folder . ';' . $row->nama_folder;
                
                if($row->hasSubDirectory()){
                    echo "<li class='nav-item dropdown'>
                                <a onclick='root($row->id, `$path`)' class='nav-link button text-left caret mb-2 demo' href='#demo' data-toggle='collapse-$row->id' data-id='$row->id' data-folder='$path' title='$row->nama_folder'><i class='mdi mdi-folder'></i>
                                    $row->nama_folder
                                </a>
                            </li>".
                            "<div id='demo' class='collapse-$row->id'>
                                <div class='sub-list-$row->id' style='padding-left:20px;'></div>
                            </div>";
                }else{
                    echo '<li class="nav-item">'.
                        "<button onclick='root($row->id, `$path`)' id='root' class='button text-left caret mb-2' title='$row->nama_folder'><i class='mdi mdi-folder'></i>".
                            $row->nama_folder.
                        '</button>'.
                    '</li>';
                }
            $no++;
            }
        }
        echo '</ul>';
    }

    public function get_sub_home(Request $request)
    {
        $id = $request->rowid;
        $folder = $request->folder;
        $no = 1;
        $directory = Directory::where('parent_id', $id)->get();

        echo
        '<div id="main-folders" class="d-flex align-items-stretch flex-wrap"><br>';
            if(!empty($directory)){
            foreach($directory as $row){
                $path = $folder . ';' . $row->nama_folder;
                
                echo "<div class='d-inline-flex'>
                        <button class='folder-container'  onclick='openParentFolder($row->id, `$path`)'>
                        <div class='folder-icon'>
                            <i class='fa fa-folder folder-icon-color'></i>
                        </div>
                        <div class='folder-name'>$row->nama_folder</div>
                        </button>
                    </div>";
                
            $no++;
            }
        }
        echo '</div>';
        echo '</ul>';
    }

    public function detail($id)
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        return view('root/detail', [
            'role' => $role,
        ]);
    }

    public function iframe()
    {
        return view('root.iframe', [
            'file' => File_Uploads::where('id', '10')->first()
        ]);
    }

    public function dark()
    {
        return view('dark');
    }

    public function storeAccessRight(Request $request)
    {
        $check = AccessRight::where('file_upload_id', $request->file_upload_id)->where('user', $request->user)->first();
        if ($check) {
            return response()->json([
                'code' => 400,
                'message' => 'Data already exists'
            ], 200);
        } else {
            $access_right = new AccessRight;
            $access_right->file_upload_id = $request->file_upload_id;
            $access_right->user = $request->user;
            $access_right->read = ($request->read == 'true' ? 't' : 'f');
            $access_right->create = ($request->create == 'true' ? 't' : 'f');
            $access_right->edit = ($request->edit == 'true' ? 't' : 'f');
            $access_right->delete = ($request->delete == 'true' ? 't' : 'f');
            $access_right->download = ($request->download == 'true' ? 't' : 'f');
            $access_right->save();
    
            return response()->json([
                'code' => 200,
                'message' => 'Access right saved successfully'
            ], 200);
        }
    }

    public function getAccessRight(Request $request)
    {
        $access_right = AccessRight::where('file_upload_id', $request->file_upload_id)->get();
        return response()->json([
            'access_right' => $access_right
        ]);
    }

    public function getAccessRightDownload(Request $request)
    {
        $access_right = AccessRight::where('file_upload_id', $request->file_upload_id)->where('user', $request->user)->first();
        if ($access_right) {
            return response()->json([
                'access_right' => $access_right->download
            ]);
        } else {
            return response()->json([
                'access_right' => null
            ]);
        }
    }

    public function getDataFile(Request $request)
    {
        $file_upload = File_Uploads::where('id', $request->file_upload_id)->first();
        if ($file_upload) {
            return response()->json([
                'file_upload' => $file_upload
            ], 200);
        }
    }

    public function getHistory(Request $request)
    {
        $history = History::select(DB::raw('DATE_FORMAT(history.created_at, "%d-%m-%Y") as history_date'), 'users.name', 'history.description')
                    ->leftJoin('users', 'history.id_user', 'users.id')
                    ->where('history.id_file', $request->file_upload_id)
                    ->get();
        if ($history) {
            return response()->json([
                'history' => $history
            ], 200);
        }
    }

    public function showRelationTree($file_id)
    {
        $parent = File_Uploads::findOrFail($file_id);
        $trees  = DocumentRelation::where('primary_document', $file_id)->get();

        return view('root.tree', compact('trees', 'parent'));
    }

    public function get_folder_sidebar(Request $request)
    {
        $directory = Directory::where('parent_id', $request->dirId)->get();

        if($directory->isNotempty()){
            echo '<ul style="display: inline-block; padding: 0px 10px;">';
            foreach($directory as $row){
            $id = $row->id;
            $folder = "'" . $row->nama_folder . "'";
            echo
                '<li class="sidebar-item sidebar-custom" id="parent-folder-'.$id.'">'.
                    '<a class="sidebar-link waves-effect waves-dark" href="javascript:;" title="'.$row->nama_folder.'" aria-expanded="false" onclick="openParentFolder('.$id.', '.$folder.')"><i class="mdi mdi-folder"></i><span class="hide-menu">'.$row->nama_folder.'</span></a>'.
                '</li>';
            }
            '</ul>';
        }
    }

    public function get_file_document_explorer(Request $request)
    {
        $directory = Directory::where('id', $request->dirId)->first();
        $file_upload = File_Uploads::where('id_directory', $request->dirId)->get();

        if ($file_upload->isNotEmpty()) {
            echo '<div class="card-body bg-megna">'.
                    '<h4 class="text-white card-title">'.$directory->nama_folder.'</h4>'.
                '</div>'.
                '<div class="card-body">'.
                   '<div class="table-responsive">'.
                        '<table id="example" class="table table-striped" style="width:100%">'.
                            '<thead>'.
                                '<tr>'.
                                    '<th>Nama Dokumen</th>'.
                                '</tr>'.
                            '</thead>'.
                            '<tbody>';
                            if(!empty($file_upload)){
                                foreach($file_upload as $row){
                                echo '<tr>'.
                                        '<td>'.
                                            $row->nama_file.'<br>'.
                                            '<button onclick="detail('.$row->id.')" class="btn btn-xs btn-info info" data-id="'.$row->id.'">Info</button>&nbsp;'.
                                            '<button onclick="preview('.$row->id.')" class="btn btn-xs btn-secondary preview">Preview</button>&nbsp;'.
                                            '<button onclick="openaction('.$row->id.')" class="btn btn-xs btn-success">Action</button>&nbsp;'.
                                        '</td>'.
                                    '</tr>';
                                }
                            }
                            echo '</tbody>'.
                        '</table>'.
                    '</div>'.
                '</div>';
        } else {
            echo '<div class="card-body bg-megna">'.
                    '<h4 class="text-white card-title">'.$directory->nama_folder.'</h4>'.
                '</div>'.
                '<div class="card-body">'.
                    'Empty files'.
                '</div>';
        }
    }
}
