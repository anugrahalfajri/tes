<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ext_Properties;

class ExtentedPropertiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id_file' => 'required|int',
            'relasi_document' => 'required|string'
        ]);

        Ext_Properties::updateOrCreate(
        ['id_file' => $request->id_file],
        [
            'no_document' => $request->no_document,
            'tgl_document' => \Carbon\Carbon::parse($request->tgl_document),
            'relasi_document' => $request->relasi_document,
            'tag' => implode(',',$request->tag),
            'document_expiration_date' => \Carbon\Carbon::parse($request->document_expiration_date),
        ]);

        return redirect()->route(routePrefix().'root')->with('success_alert','Data has been saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
