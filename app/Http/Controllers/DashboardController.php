<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Roles;
use App\Models\Organization;
use App\Models\File_Uploads;
use Illuminate\Support\Facades\Auth;
use DB;

class DashboardController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $organizations = Organization::count();
        $members = User::count();
        $files = File_Uploads::count();
        return view('dashboard', [
            'role' => $role,
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3"),
            'organizations' => $organizations,
            'members' => $members,
            'files' => $files,
            'directory_selection' => app('App\Http\Controllers\RootController')->directory_builder(),
        ]);
    }

    public function dark()
    {
        return view('dark');
    }
}
