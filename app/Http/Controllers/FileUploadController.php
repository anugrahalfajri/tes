<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Models\File_Uploads;
use App\Models\Properties;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use App\Models\History;
use App\Models\Directory;
use App\Models\Version_Log;
use App\Models\DocumentRelation;
use File;
use App\Http\Requests\FileUploadRequest;
 
class FileUploadController extends Controller
{
    public function store(FileUploadRequest $request)
    {
        $photos_path = public_path('/documents');
        if (!is_dir($photos_path)) {
            mkdir($photos_path, 0777);
        }
        
        $document = $request->file('file');
        $documentName = date('ymdHi') . '-' . $document->getClientOriginalName();
        $documentSize = $document->getSize();
        $document->move(public_path('documents'), $documentName);

        $up = File_Uploads::create([
                    'nama_file' => $documentName,
                    'path_file' => '/public/documents/',
                    'member' => Auth::user()->name,
                    'id_directory' => $request->id_directory,
                    'is_deleted' => '0',
                ]);
        $lastId = DB::getPdo()->lastInsertId();
        
        $prop = Properties::create([
                    'name' => $documentName,
                    'title' => $documentName,
                    'folder' => $this->get_folder_name_for_properties($request->id_directory),
                    'size' => $documentSize,
                    'id_file' => $lastId,
                    'created_by' => Auth::user()->name,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_by' => Auth::user()->name,
                    'updated_at' => date('Y-m-d H:i:s'),
                    'permalink' => '/public/documents/'.$documentName,
                    'id_version' => 1,
                    'id_status' => 1
                ]);

        $his = History::create([
                    'description' => 'Upload File',
                    'id_file' => $lastId,
                    'id_user' => Auth::user()->id
                ]);
   
        if ($up && $prop && $his) {
            return Response::json([
                'message' => 'File uploaded successfully'
            ], 200);
        } else {
            return Response::json([
                'message' => 'Failed to upload'
            ], 400);
        }
    }

    public function show_file_info(Request $request)
    {
        $id = $request->id;
        $upload = DB::table('file_uploads')
                    ->select('file_uploads.*', 'properties.*', 'extented_properties.no_document', 'extented_properties.tag',
                    'extented_properties.tgl_document','extented_properties.relasi_document',
                    'extented_properties.document_expiration_date','version_log.version','version_log.created_by','version_log.keterangan'
                    ,'version_log.created_at', 'file_uploads.title AS title_file_upload', 'file_uploads.created_at as file_created')
                    ->leftJoin('properties', 'file_uploads.id', '=', 'properties.id_file')
                    ->leftJoin('extented_properties', 'file_uploads.id', '=', 'extented_properties.id_file')
                    ->leftJoin('version_log', 'file_uploads.id', '=', 'version_log.id_file')
                    ->where('file_uploads.id', $id)
                    ->first();

        return json_encode($upload);
    }
    public function show_workflow_upload_info(Request $request)
    {
        $id = $request->id;
        $upload = DB::table('workflow_detail')
                    ->select('workflow_detail.*', 'properties.*', 'extented_properties.no_document',
                    'extented_properties.tgl_document','extented_properties.relasi_document',
                    'extented_properties.document_expiration_date','version_log.version','version_log.created_by','version_log.keterangan'
                    ,'version_log.created_at')
                    ->leftJoin('properties', 'workflow_detail.id_file', '=', 'properties.id_file')
                    ->leftJoin('extented_properties', 'workflow_detail.id_file', '=', 'extented_properties.id_file')
                    ->leftJoin('version_log', 'workflow_detail.id_file', '=', 'version_log.id_file')
                    ->where('workflow_detail.id', $id)
                    ->first();

        return json_encode($upload);
    }

    public function change_file_directory(Request $request)
    {
        $update = File_Uploads::where('id', $request->id_file)
                            ->update(['id_directory' => $request->new_directory_id]);
        if ($update) {
            return redirect('root')->with('success_msg', 'File Directory Sucessfully Changed');
        } else {
            return redirect('root')->with('fail_msg', 'Failed to Change File Directory');
        }
    }

    public function duplicate_file(Request $request)
    {
        $sourcefile = File_Uploads::where('id', $request->id_file)->first();
        $oldname = $sourcefile->nama_file;
        $explode_filename = explode("-", $oldname);
        $newname = date('ymdHi');
        for ($i=1; $i < count($explode_filename); $i++) { 
            $newname .= "-" . $explode_filename[$i];
        }
        if (PHP_OS_FAMILY === "Windows") {
            $file_copy = File::copy(public_path("documents\\".$oldname), public_path("documents\\".$newname));
        } else {
            $file_copy = File::copy(public_path("documents/".$oldname), public_path("documents/".$newname));
        }
        if ($file_copy) {
            $duplicate = File_Uploads::create([
                            'nama_file' => $newname,
                            'path_file' => '/public/documents/',
                            'member' => Auth::user()->name,
                            'id_directory' => $request->new_directory_id,
                            'is_deleted' => '0'
                        ]);

            $lastId = DB::getPdo()->lastInsertId();
            $oldprop = Properties::where('id_file', '=', $request->id_file)->first();
            $documentSize = $sourcefile->size;
            $prop = Properties::create([
                    'name' => $newname,
                    'title' => $newname,
                    'folder' => $this->get_folder_name_for_properties($request->new_directory_id),
                    'size' => $oldprop != null ? $oldprop->size : 0,
                    'id_version' => 1,
                    'id_status' => 1,
                    'id_file' => $lastId,
                    'created_by' => Auth::user()->name,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_by' => Auth::user()->name,
                    'updated_at' => date('Y-m-d H:i:s'),
                    'permalink' => '/public/documents/'.$newname,
                ]);

            if ($duplicate) {
                return redirect('root')->with('success_msg', 'File Sucessfully Duplicated');
            } else {
                return redirect('root')->with('fail_msg', 'Failed to Duplicate File');
            }
        }
    }

    public function get_folder_name_for_properties($folder_id)
    {
        $folder = Directory::where('id', $folder_id)->first();
        if (!empty($folder->parent_id)) {
            $parent_folder = Directory::where('id', $folder->parent_id)->first();
            $folder_path = $parent_folder->nama_folder . ' > ' . $folder->nama_folder;
        } else {
            $folder_path = $folder->nama_folder;
        }
        return $folder_path;
    }

    public function reUploadDocument(Request $request)
    {
        try {
            DB::beginTransaction();

            $oldFile     = File_Uploads::findOrFail($request->file_id);
            $oldNameFile = $oldFile->nama_file;
            $photos_path = public_path('/documents');
            
            $document = $request->file('file');
            $documentName = date('ymdHi') . '-' . $document->getClientOriginalName();
            $documentSize = $document->getSize();
            $document->move(public_path('documents'), $documentName);
    
            $up = $oldFile->update([
                'nama_file' => $documentName,
                'member'    => Auth::user()->name,
                'updated_at'=> date('Y-m-d H:i:s'),
            ]);

            $version = $oldFile->version()->create([
                'version'   => number_format((float) $oldFile->version()->count() + 1, 1, '.', ''),
                'created_by'=> auth()->user()->name,
                'keterangan'=> $request->keterangan ?? '-',
            ]);
            
            $prop = Properties::where('id_file', $request->file_id)->update([
                'name'          => $documentName,
                'title'         => $documentName,
                'folder'        => $this->get_folder_name_for_properties($oldFile->id_directory),
                'size'          => $documentSize,
                'updated_by'    => Auth::user()->name,
                'updated_at'    => date('Y-m-d H:i:s'),
                'permalink'     => '/public/documents/'.$documentName,
                'id_version'    => $version->id
            ]);
    
            $his = History::create([
                'description' => 'Reupload File',
                'id_file' => $oldFile->id,
                'id_user' => Auth::user()->id
            ]);
       
            File::delete("$photos_path/") . $oldNameFile;

            DB::commit();
            
            return Response::json([
                'message' => 'File uploaded successfully'
            ], 200);
            
        } catch (\Throwable $th) {
            DB::rollback();
            report($th);

            return Response::json([
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function getVersionLog(Request $request)
    {
        $id = $request->id_file;
        $logs = Version_Log::where('id_file', $id)->get();

        return view('root.version-log', compact('logs'));
    }

    public function getRelatedDokumen(Request $request)
    {
        $file_id  = $request->id;
        $primary  = File_Uploads::findOrFail($file_id);
        $files    = File_Uploads::where('is_deleted', '0')->where('id', '<>', $file_id)->get();
        $document = DocumentRelation::where('primary_document', $file_id)->get();

        return view('root.relation', compact('document', 'primary', 'files'));
    }

    public function relationStore(Request $request)
    {
        $relation = DocumentRelation::firstOrCreate(
            ['primary_document' => $request->id, 'foreign_document' => $request->dokumen],
            ['created_by' => auth()->user()->id]
        );

        return $this->getRelatedDokumen($request);
    }

    public function relationDestroy(Request $request)
    {
        DocumentRelation::where('id', $request->relation_id)->delete();

        return $this->getRelatedDokumen($request);
    }
}
