<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Directory;
use App\Models\DirectoryWorkflow;
use App\Models\Workflow_Detail;
use App\Models\OrganizationAccess;
use Auth;

class DirectoryController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!OrganizationAccess::checkPermission("document-explore-view", Auth::user()->id)) {
                return abort(401);
            }
    
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Directory::create([
            'parent_id' => $request->parent_id,
            'nama_folder' => $request->nama_folder,
        ]);

         return redirect()->route(routePrefix().'root')->with('success_alert','Data has been saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function workflow(Request $request)
    {
        $request->validate([
            'fileid' => 'required',
            'assign' => 'nullable',
            'workflows' => 'nullable|array'
        ]);

        DirectoryWorkflow::updateOrCreate(
            ['directory_id' => $request->fileid],
            [
                'workflow_switch' => $request->assign,
                'workflow_list'   => json_encode($request->workflows)
            ]
        );

        foreach ($request->workflows as $key => $value) {
            Workflow_Detail::create([
                'id_file' => $request->fileid,
                'id_workflow' => $value,
                'status' => '0'
            ]);
        }

        return response()->json([
            "success" => true,
            "message" => 'Data saved!'
        ], 200);
    }
}
