<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function sendEmail(Request $request)
    {

        $data = [
            'email_message' => nl2br($request->message),
            'email_to' => $request->email,
            'company_name' => 'PT Anggara Tehnik (Melkhior Teknologi)',
            'company_address' => nl2br("Menara Kebon Jeruk Ruko G Lt. 2, Jl. Arjuna Utara No.16, RT.1/RW.2, Duri Kepa, Kec. Kb. Jeruk, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11510 "),
            'company_tel' => '(021) 56981392',
            'company_web' => 'www.melkhior.id',
        ];
        // var_dump($data); exit;
        Mail::send('email-template', $data, function ($msg) use ($request) {
            $msg->to($request->email);
            $msg->subject($request->subject);
            $msg->from('demo@melkhior.co', 'PT Anggara Tehnik (Melkhior Teknologi)');
        });

        // check for failures
        if (Mail::failures()) {
            return redirect('root')->with('fail_msg', 'Failed to Send Email');
        }
        return redirect('root')->with('success_msg', 'Email Has Been Sent');
    }
}
