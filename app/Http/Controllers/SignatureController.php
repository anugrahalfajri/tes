<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\DigiSignerClient;
use App\Libraries\libs\SignatureRequest;
use App\Libraries\libs\Document;
use App\Libraries\libs\Signer;
use App\Libraries\libs\Field;

class SignatureController extends Controller
{
    public function signature(Request $request)
    {
        $client = new DigiSignerClient('9480b0ca-4b85-4a47-bd32-70e30bafebb2'); 
        $signature = new SignatureRequest;
        
        $document = new Document($request->file);
        $document->setSubject('My email subject');
        $document->setMessage('My email message');
        $signature->addDocument($document);

        $signer = new Signer('harisz.ryadi17@gmail.com');
        $document->addSigner($signer);

        $field = new Field(0, array(0,0,200,100), Field::TYPE_SIGNATURE);
        $signer->addField($field);

        $signatureRequest = $client->sendSignatureRequest($signature);
        if ($signatureRequest) {
            return response()->json([
                'status' => 200,
                'message' => 'document sent successfully'
            ], 200);
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'Error, please contact your adminstrator'
            ], 500);
        }
        
    }
}