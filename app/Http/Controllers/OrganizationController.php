<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OrganizationAccess;
use App\Models\Organization;
use App\Models\Roles;
use App\Models\User;
use App\Models\Master\OrganizationStatusLog as LogModel;
use App\Models\Master\OrganizationModificationLog;
use App\Models\Master\User as UserMaster;

use Auth;
use DB;

class OrganizationController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!OrganizationAccess::checkPermission("organization-view", Auth::user()->id)) {
                return abort(401);
            }
    
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $organizations = '';
        if ($role->name == 'admin organisasi') {
            $organizations = Organization::select('organizations.*', 'users.name AS user_name')
                            ->leftJoin('users', 'organizations.admin', 'users.id')
                            ->where('users.id_organization', Auth::user()->id_organization)
                            ->orderBy('id')
                            ->get();
        } else {
            $organizations = Organization::select('organizations.*', 'users.name AS user_name')
                            ->leftJoin('users', 'organizations.admin', 'users.id')
                            ->orderBy('id')
                            ->get();
        }
        return view('organization.index', [
            'role' => $role,
            'organizations' => $organizations
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $users = User::orderBy('id')->get();
        return view('organization.insert', [
            'role' => $role,
            'users' => $users,
            'action' => 'store'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'        => 'required|string|max:255',
            'description' => 'nullable|string|max:255',
            'status'      => 'required|string|max:255',
            'admin'       => 'required|string|max:255'    
        ]);

        $data = [
            'name' => $request->name,
            'description' => $request->description,
            'status' => $request->status,
            'admin' => $request->admin,
        ];
        Organization::create($data);
        return redirect()->route(routePrefix().'organization.index')->with('success_alert','Data has been saved');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $organization = Organization::find($id);
        $users = User::orderBy('id')->get();
        $logs   = LogModel::where('organization_id', $id)->orderBy('created_at', 'DESC')->get();
        $modif   = OrganizationModificationLog::where('organization_id', $id)->orderBy('created_at', 'DESC')->get();

        return view('organization.insert', [
            'role' => $role,
            'action' => 'update',
            'users' => $users,
            'organization' => $organization,
            'logs'  => $logs,
            'modif'  => $modif
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'        => 'required|string|max:255',
            'description' => 'nullable|string|max:255',
            'admin'       => 'required|string|max:255'    
        ]);
        
        $organization = Organization::find($id);
        $organization->update([
            'name' => $request->name,
            'description' => $request->description,
            'admin' => $request->admin
        ]);

        //run observer App\Observers\OrganizationLogs
        return redirect()->route(routePrefix().'organization.index')->with('success_alert','Data has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $organization = Organization::find($id);
        $organization->delete();
        return redirect()->route(routePrefix().'organization.index')->with('success_alert','Data has been deleted');
    }

    public function userList($id)
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $users = UserMaster::where('id_organization', $id)->get();

        return view('organization.user-list', compact('users', 'role'));
    }
}
