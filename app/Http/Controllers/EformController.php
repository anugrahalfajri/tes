<?php

namespace App\Http\Controllers;

require_once public_path('plugins/phpdocx/classes/CreateDocx.php');
require_once public_path('plugins/DocxConversion.php');

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\EformDetail;
use App\Models\EformSubmit;
use App\Models\Eform;
use App\Models\EformGenerate;
use App\Models\EformSubmitCount;
use App\Models\Roles;
use App\Models\Directory;
use App\Models\File_Uploads;
use App\Models\History;
use App\Models\Properties;
use App\Http\Controllers\WorkflowController;
use CreateDocx;
use DocxConversion;
use clsTinyButStrong;
use Auth;
use DB;
use Illuminate\Support\Str;
use App\Models\OrganizationAccess;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\EformAnswer;
use App\Imports\EformImportExcel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\ConfigUpload;
use App\Rules\ConfigUploadRule;
use File;
use PDF;

class EformController extends Controller
{
    protected $elements = ["text" => 'richText', "text_area" => 'richText', 'select_box'=> 'comboBox', 'check_box' => 'checkbox'];

     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (!OrganizationAccess::checkPermission("eform-view", Auth::user()->id)) {
                return abort(401);
            }
    
            return $next($request);
        });
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $nama = Auth::user()->name;

        $eforms = Eform::addSelect('form_id', 'title', 'target', 'created_at', 'created_by', 'deleted_at', 'slug', 'graph_report')->where("created_by", "=", $nama)->groupBy('form_id', 'title', 'target', 'created_at', 'created_by', 'deleted_at', 'slug', 'graph_report')->withCount('eform_submit')->withTrashed()->get();


        return view('eform.index', [
            'role' => $role,
            'eforms' => $eforms,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        return view('eform.insert', [
            'role' => $role
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'target' => 'required',
            'title' => 'required|unique:eform,title',
            'question' => 'required',
            'answer_type' => 'required',
            'question.*' => 'required',
            'answer_type.*' => 'required',
            'graph_report' => [
                    'nullable',
                    function ($attribute, $value, $fail) use($request){
                        $type = $request->answer_type;

                        if ($value == '1' && !in_array("number", $type)) {
                            $fail("$attribute : pertanyaan bertipe number belum ada.");
                        }
                    },
                ],
            'action' => ['nullable', Rule::in(['thanks_page', 'redirect_to'])],
            'header' => 'nullable|string',
            'description' => 'nullable|string',
            'paging_form' => 'nullable',
            'page'        => 'nullable|numeric',
            'eg_upload_template' => ['nullable', new ConfigUploadRule('eform')]
        ])->validate();

        $formId = null;
        $check = Eform::withTrashed()->get();
        if($check->isEmpty()) {
            $formId = 1;
        } else {
            $eform = Eform::addSelect('form_id')->groupBy('form_id', 'created_at')->withTrashed()->latest()->first();
            $formId = $eform->form_id+1;
        }

        $directory = Directory::firstOrCreate([
            'nama_folder' => 'E-Form'
        ], [
            'parent_id', null
        ]);
        
        $eform_directory = Directory::create([
            'parent_id' => $directory->id,
            'nama_folder' => $request->get('title'),
        ]);

        if(count($request->get("question")) > 0){
            foreach ($request->get("question") as $key => $value) {
                $eForm = new Eform;
                $eForm->form_id = $formId;
                $eForm->target = $request->get('target');
                $eForm->title = $request->get('title');
                $eForm->question = $request->get("question")[$key];
                $eForm->answer_type = $request->get("answer_type")[$key];
                $eForm->slug = Str::slug($request->get("title"));
                $eForm->created_by = auth()->user()->name;
                $eForm->id_directory = $eform_directory->id;
                $eForm->question_no = $request->get('question_no')[$key];
                $eForm->track_ip = $request->get('track_ip') != null ? $request->get('track_ip') : null;
                $eForm->mobile_used = $request->get('mobile_used') != null ? $request->get('mobile_used') : null;
                $eForm->one_time_submit = $request->get('one_time_submit') != null ? $request->get('one_time_submit') : null;
                $eForm->graph_report = $request->get('graph_report') != null ? $request->get('graph_report') : null;    
                $eForm->action_submit = $request->get('action'); 
                $eForm->action_submit_url = $request->get('action_url');
                $eForm->header_page = $request->get('header');
                $eForm->description_page = $request->get('description'); 
                $eForm->paging_form = $request->get('paging_form') ?? false;
                $eForm->paging_form_total = $request->get('page');
                $eForm->short_desc  = $request->get('short_desc')[$key];
                $eForm->paging_form_number = $request->get('page_num')[$key]; 
                $eForm->is_mandatory = $request->get('is_mandatory')[$key];         
                $eForm->save();

                $auto_generate = EformGenerate::create([
                    'eg_form_id'            => $formId,
                    'eg_upload_template'    => isset($request->eg_upload_template) ? $request->file('eg_upload_template')->store('public/eform/generate') : null,
                    'eg_auto_generate'      => isset($request->eg_auto_generate)
                ]);

                if ($request->get("answer_type")[$key] == 'radio_button') {
                    foreach ($request->get("answer_choice_rb".$key) as $value) {
                        $eFormDetail = new EformDetail;
                        $eFormDetail->id_eform = $eForm->id;
                        $eFormDetail->answer_choice = $value;
                        $eFormDetail->count_answer = 0;
                        $eFormDetail->save();
                    }
                } else if ($request->get("answer_type")[$key] == 'check_box') {
                    foreach ($request->get("answer_choice_cb".$key) as $value) {
                        $eFormDetail = new EformDetail;
                        $eFormDetail->id_eform = $eForm->id;
                        $eFormDetail->answer_choice = $value;
                        $eFormDetail->count_answer = 0;
                        $eFormDetail->save();
                    }
                } else if ($request->get("answer_type")[$key] == 'select_box') {
                    foreach ($request->get("answer_choice_sb".$key) as $value) {
                        $eFormDetail = new EformDetail;
                        $eFormDetail->id_eform = $eForm->id;
                        $eFormDetail->answer_choice = $value;
                        $eFormDetail->count_answer = 0;
                        $eFormDetail->save();
                    }
                } else if ($request->get("answer_type")[$key] == 'multi') {
                        $eFormDetail = new EformDetail;
                        $eFormDetail->id_eform = $eForm->id;
                        $eFormDetail->answer_choice = $request->get("kolom".$key);
                        $eFormDetail->count_answer = 0;
                        $eFormDetail->save();
                }
            }
        }

        return redirect()->route(routePrefix().'eform.index')->with('success_alert','Data has been saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $eform = Eform::with('eform_detail')->where('slug', $slug)->get();
        $isPaging = $eform->first()->paging_form ?? false;
        

        if($eform->isEmpty()){
            return view('eform.inactive');
        }

        if($isPaging){
            $eform = $eform->groupBy('paging_form_number');
        }

        return view('eform.show', [
            'eform' => $eform,
            'isPaging' => $isPaging
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($formId)
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $eform = Eform::with('eform_detail')->where('form_id', $formId)->get();
        return view('eform.edit', [
            'role' => $role,
            'eform' => $eform
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $formId)
    {
        if(count($request->get("id")) > 0){
            foreach ($request->get("id") as $key => $value) {
                Eform::where('id', $value)->withTrashed()->update([
                    'target' => $request->get('target'),
                    'title' => $request->get('title'),
                    'question' => $request->get("question")[$key],
                    'slug' => Str::slug($request->get("title")),
                    // 'track_ip' => $request->get('track_ip') != null ? $request->get('track_ip') : null,
                    // 'mobile_used' => $request->get('mobile_used') != null ? $request->get('mobile_used') : null,
                    // 'one_time_submit' => $request->get('one_time_submit') != null ? $request->get('one_time_submit') : null,
                    // 'graph_report' => $request->get('one_time_submit') != null ? $request->get('one_time_submit') : null,
                    'action_submit' => $request->get('action'),
                    'action_submit_url' => $request->get('action_url'), 
                    'header_page' => $request->get('header'),
                    'description_page' => $request->get('description'),     
                    // 'paging_form' => $request->get('paging_form') ?? false,
                    'paging_form_total' => $request->get('page'),
                    'short_desc'  => $request->get('short_desc')[$key],
                    'paging_form_number' => $request->get('page_num')[$key],
                    'is_mandatory' => $request->get('is_mandatory')[$key],                   
                ]);

                // $auto_generate = EformGenerate::updateOrCreate(
                // ['eg_form_id' => $formId],
                // [
                //     'eg_upload_template'    => isset($request->eg_upload_template) ? $request->file('eg_upload_template')->store('public/eform/generate') : null,
                //     'eg_auto_generate'      => isset($request->eg_auto_generate)
                // ]);

                if ($request->has('id_detail_rb')) {
                    if (count($request->get("id_detail_rb")) > 0) {
                        foreach ($request->get("id_detail_rb") as $key => $value) {
                            EformDetail::where('id', $value)->update([
                                'answer_choice' => $request->get("answer_choice_rb")[$key]
                            ]);
                        }
                    }
                }

                if ($request->has('id_detail_cb')) {
                    if (count($request->get("id_detail_cb")) > 0) {
                        foreach ($request->get("id_detail_cb") as $key => $value) {
                            EformDetail::where('id', $value)->update([
                                'answer_choice' => $request->get("answer_choice_cb")[$key]
                            ]);
                        }
                    }
                }
                
                if ($request->has('id_detail_sb')) {
                    if (count($request->get("id_detail_sb")) > 0) {
                        foreach ($request->get("id_detail_sb") as $key => $value) {
                            EformDetail::where('id', $value)->update([
                                'answer_choice' => $request->get("answer_choice_sb")[$key]
                            ]);
                        }
                    }
                }
            }
        }
        return redirect()->route(routePrefix().'eform.index')->with('success_alert','Data has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($formId)
    {
        $eform = Eform::where('form_id', $formId)->withTrashed()->delete();
        $eform_submit = EformSubmit::where('form_id', $formId)->delete();
        return redirect()->route(routePrefix().'eform.index')->with('success_alert','Data has been deleted');
    }

    /**
     * Submit form
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function submit(Request $request) {
        $validateArr = [];
        foreach ($request->is_mandatory as $d) {
            $validateArr += [$d => 'required'];
        }

        $this->validate($request, $validateArr,['*.required' => "Please fill the required field"]);
        
        try {
            DB::beginTransaction();

            $isIpTrack  = Eform::isIpTracking($request->form_id); 
            $data       = $this->populateInput($request->directory_id, $request);
            $eform_submit = new EformSubmit;
            $eform_submit->form_id = $request->form_id;
            $eform_submit->answer_eform = json_encode($data);
            $eform_submit->user_id = Auth::user()->id;
            $eform_submit->user_ip_address = $request->ip();
            $eform_submit->save();

            if ($eform_submit) {
                foreach ($data as $key => $value) {
                    $k = substr($key, strlen( 'answer_' ));
                    $eform = Eform::where('form_id', $eform_submit->form_id)->where('question_no', $k)->withTrashed()->first();
                    if ($eform->answer_type == 'radio_button' || $eform->answer_type == 'select_box') {
                        $eform_detail = EformDetail::where('answer_choice', $value)->update([
                            'count_answer' => DB::raw('count_answer+1')
                        ]);
                    } else if ($eform->answer_type == 'check_box') {
                        foreach ($value as $item) {
                            EformDetail::where('answer_choice', $item)->update([
                                'count_answer' => DB::raw('count_answer+1')
                            ]);
                        }
                    } else if ($eform->answer_type == 'text' || $eform->answer_type == 'text_area') {
                        $eform_submit_count = EformSubmitCount::where('key', $eform->slug)->where('value', $value)->first();
                        if (!$eform_submit_count) {
                            EformSubmitCount::create([
                                'eform_submit_id' => $eform_submit->id,
                                'eform_id' => $eform->id,
                                'key' => $eform->slug,
                                'value' => $value,
                                'count' => 1
                            ]);
                        } else {
                            EformSubmitCount::where('id', $eform_submit_count->id)->update([
                                'count' => DB::raw('count+1')
                            ]);
                        }
                        
                    }
                }
            }
        
            DB::commit();

            // genrate PDF
            $temp = [];
            $question = Eform::select('question')->where('form_id', $request->form_id)->get();
            $answer = $request->except('_token', 'form_id', 'directory_id', 'is_mandatory');
            
            foreach ($question as $key => $q) {
                array_push($temp, [$q->question => $answer['answer_'.($key+1)]]);
            }
            $pdf = PDF::loadView('eform.pdf', ['data' => $temp]);
            // $content = $pdf->download()->getOriginalContent();
            $pdf->save(public_path('/documents/'.date('ymdHi').'.pdf'));
            // \Storage::put('public/eform/submit/'.time().'.pdf', $content);
            // return $pdf->download('sample.pdf');

            File_Uploads::create([
                'nama_file'     => date('ymdHi').'.pdf',
                'path_file'     => "/public/documents/",
                'member'        => auth()->user()->name,
                'id_directory'  => $request->directory_id,
                'is_deleted'    => '0'
            ]);

            return $eform_submit->eform->action_submit == "thanks_page" ?
                        view('eform.thankyou', ["eform" => $eform_submit->eform]) : redirect()->away($eform_submit->eform->action_submit_url);

        } catch (\Throwable $th) {
            DB::rollback();
            report($th);

            return redirect()->back()->with('error_alert', $th->getMessage())->withInput();
        }
    }
    
    public function getLocationByIp($request)
    {
        $ip_address = $request->ip();
        $geoIp = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$ip_address"));

        return (["lat" => $geoIp["geoplugin_latitude"], "lng" => $geoIp["geoplugin_longitude"], "address" => $geoIp["geoplugin_regionName"] . ', ' . $geoIp["geoplugin_city"]]);
    }

    public function populateInput($directory_id, $input)
    {
        $allFiles = $input->allFiles();
        $data     = $input->except('_token', 'form_id', 'directory_id', 'is_mandatory');

        if(!empty($allFiles)){
            $photos_path = public_path('/documents/eform');
            if (!is_dir($photos_path)) {
                mkdir($photos_path, 0777);
            }

            $attachment_dir = Directory::firstOrCreate([
                'parent_id' => $directory_id, 'nama_folder' => 'attachment'
            ],[
                'parent_id' => $directory_id, 'nama_folder' => 'attachment'
            ]);

            $config = ConfigUpload::where('modul_name', 'eform')->first();
            $allowed = [];
            $allowed_size = 1024;

            if($config) {    
                $allowed = explode(';', $config->allowed_mime_types);
                $allowed_size = $config->max_file_size;
            }

            foreach($allFiles ?? [] as $inputname => $file){
                $ext          = $file->getClientOriginalExtension();
                $documentName = date('ymdHi') . '-' . $file->getClientOriginalName();
                $documentSize = $file->getSize();

                if($config && !in_array($ext, $allowed)) throw new \Exception("File type not allowed $ext");
                if($config && ($documentSize / 1024) > $allowed_size)  throw new \Exception("File size must be less than $allowed_size Kb");

                $file->move(public_path('documents/eform'), $documentName);

                $uploaded = File_Uploads::create([
                    'nama_file'     => $documentName,
                    'path_file'     => "/public/documents/eform/",
                    'member'        => auth()->user()->name,
                    'id_directory'  => $attachment_dir->id,
                    'is_deleted'    => '0'
                ]);

                $prop = Properties::create([
                    'name' => $documentName,
                    'title' => $documentName,
                    'folder' => (new WorkflowController)->get_folder_name_for_properties($attachment_dir->id),
                    'size' => $documentSize,
                    'id_file' => $uploaded->id,
                    'created_by' => Auth::user()->name,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_by' => Auth::user()->name,
                    'updated_at' => date('Y-m-d H:i:s'),
                    'permalink' => '/public/documents/eform/'.$documentName,
                ]);

                $his = History::create([
                    'description' => 'Upload Eform File',
                    'id_file' => $uploaded->id,
                    'id_user' => Auth::user()->id
                ]);

                $data[$inputname] = $documentName;
            }            
        }
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $formId
     * @return \Illuminate\Http\Response
     */
    public function answer($formId) {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $question = Eform::addSelect('question')->where('form_id', $formId)->withTrashed()->get();
        $answer = EformSubmit::where('form_id', $formId)->get();
        $isIpTracking = Eform::isIpTracking($formId);
        $isOneTimeSubmit = Eform::isOTS($formId);
        $isotsip    = Eform::isIPOTS($form_id);
        
        return view('eform.answer', [
            'isIpTracking' => $isIpTracking,
            'role' => $role,
            'question' => $question,
            'answer' => $answer,
            'isOneTimeSubmit' => $isOneTimeSubmit,
            'isotsip'   => $isotsip,
            'form_id'    => $formId
        ]);
    }

    public function generateDocx($id, $answer_id = null)
    {
        $form = Eform::where('form_id', $id)->withTrashed()->get()->toArray();
        $docx = new CreateDocx();
        $title = $form[0]["title"];

        $docx->addText("E-Form : $title", array('bold' => true));

        foreach($form as $no => $f){
            $tag        = $this->elements[$f['answer_type']] ?? null;
            $options    =  array('placeholderText' => $f['question']);

            if(isset($answer_id)){
                $submit = EformSubmit::find($answer_id);
                $answer_json = json_decode($submit->answer_eform, true);

                // print_r($answer_json);exit; 

                $options    =  array('placeholderText' => $answer_json[Str::slug($f['question'], '_')]);
            }

            if($f['answer_type'] == 'select_box'){
                $new_option = [];
                $detail = EformDetail::where('id_eform', $f['id'])->get();
                foreach($detail ?? [] as $d){
                    $new_option[] = array($d->answer_choice, $d->id);
                }    

                $options['listItems'] = $new_option;
            }

            $docx->addText("$f[question] : ", array('bold' => true));

            if(isset($tag)){
                $docx->addStructuredDocumentTag($tag, $options);    
            }else{
                $docx->addText("-");
            }
        }

        $docx->createDocxAndDownload(public_path('documents/eform'));
    }

    public function importToDB(Request $request)
    {
        $this->validate($request, [
            'eform_file' => ['required', new ConfigUploadRule('eform', ['xlsx', 'xls', 'csv'])],
            'target' => 'required',
            'title' => 'required|unique:eform,title',
            'source'=> ['required', Rule::in(['word', 'excel', 'txt', 'csv'])]
        ]);    

        if($request->source == "word"){
            return $this->docxToDB($request);
        }elseif($request->source == "txt"){
            return $this->txtToDB($request);
        }else{
            return $this->xlsxToDB($request);
        }
    }

    public function txtToDB($request)
    {
        try {
            DB::beginTransaction();

            $url  = $request->file('eform_file')->store('public/eform/import');
            $file = fopen(storage_path('app/'.$url), "r");
    
            $formId = null;
            $check = Eform::withTrashed()->get();
            if($check->isEmpty()) {
                $formId = 1;
            } else {
                $eform = Eform::addSelect('form_id')->groupBy('form_id')->withTrashed()->latest()->first();
                $formId = $eform->form_id+1;
            }
    
            $directory = Directory::firstOrCreate([
                'nama_folder' => 'E-Form'
            ], [
                'parent_id', null
            ]);
    
            $eform_directory = Directory::create([
                'parent_id' => $directory->id,
                'nama_folder' => $request->title,
            ]);

            while(!feof($file)) {
                $text = explode("=", fgets($file));

                if(!empty($text)){
                    $eForm = new Eform;
                    $eForm->form_id = $formId;
                    $eForm->target = $request->target;
                    $eForm->title = $request->title;
                    $eForm->created_by = auth()->user()->name;
                    $eForm->id_directory = $eform_directory->id;
                    $eForm->question = $text[0];
                    $eForm->slug = Str::slug($request->title);

                    $type = strtolower($text[1]);
                    if(trim($type)== 'text' || trim($type) == 'text_area' || trim($type) == 'url' || trim($type) == 'date' || trim($type) == 'time' || trim($type) == 'image' || trim($type) == 'file' || trim($type) == 'multi'){
                        $eForm->answer_type = trim($type);
                        $eForm->save();

                    } else {
                        $typeOptions = explode(":", $text[1]);

                        if(trim($typeOptions[0]) == 'radio_button' || trim($typeOptions[0]) == 'select_box' || trim($typeOptions[0]) == 'check_box'){
                            $eForm->answer_type = trim($typeOptions[0]);
                            $eForm->save();

                            $options = explode(",", $typeOptions[1]);

                            foreach ($options as $value) {
                                $eFormDetail = new EformDetail;
                                $eFormDetail->id_eform = $eForm->id;
                                $eFormDetail->answer_choice = $value;
                                $eFormDetail->save();
                            }

                        }
                    }
                }
            }
                
            DB::commit();
            fclose($file);

            return redirect()->route(routePrefix().'eform.index')->with('success_alert','Data has been imported');

        } catch (\Throwable $th) {
            DB::rollback();
            report($th);

            return redirect()->back()->with('error_alert', $th->getMessage())->withInput();
        }
    }

    public function docxToDB($request)
    {
        try {
            DB::beginTransaction();

            $url = $request->file('eform_file')->store('public/eform/import');
            $docObj = new DocxConversion(storage_path('app/'.$url));
            $docText= explode("#", $docObj->convertToText());
    
            $formId = null;
            $check = Eform::withTrashed()->get();
            if($check->isEmpty()) {
                $formId = 1;
            } else {
                $eform = Eform::addSelect('form_id')->groupBy('form_id')->withTrashed()->latest()->first();
                $formId = $eform->form_id+1;
            }
    
            $directory = Directory::firstOrCreate([
                'nama_folder' => 'E-Form'
            ], [
                'parent_id', null
            ]);
    
            $eform_directory = Directory::create([
                'parent_id' => $directory->id,
                'nama_folder' => $request->title,
            ]);
    
            foreach($docText ?? [] as $text){
    
                $eForm = new Eform;
                $eForm->form_id = $formId;
                $eForm->target = $request->target;
                $eForm->title = $request->title;
                $eForm->created_by = auth()->user()->name;
                $eForm->id_directory = $eform_directory->id;
    
                // ignore blank spaces
                if (!ctype_space($text)){
                    $inputType = explode("=", $text);

                    //input question
                    $eForm->question = $inputType[0];
                    $eForm->slug = Str::slug($request->title);
                    
                    if(isset($inputType[1])){
                        $type = strtolower($inputType[1]);
                        if(trim($type)== 'text' || trim($type) == 'text_area' || trim($type) == 'url' || trim($type) == 'date' || trim($type) == 'time' || trim($type) == 'image' || trim($type) == 'file'){
                            //input answer type [text, text_area, dll]
                            $eForm->answer_type = trim($type);
                            $eForm->save();
    
                        } else {
                            $typeOptions = explode(":", $inputType[1]);
                            if(trim($typeOptions[0]) == 'radio_button' || trim($typeOptions[0]) == 'select_box' || trim($typeOptions[0]) == 'check_box'){
                                $eForm->answer_type = trim($typeOptions[0]);
                                $eForm->save();
    
                                $options = explode(",", $typeOptions[1]);
    
                                foreach ($options as $value) {
                                    $eFormDetail = new EformDetail;
                                    $eFormDetail->id_eform = $eForm->id;
                                    $eFormDetail->answer_choice = $value;
                                    $eFormDetail->save();
                                }
    
                            }else{
                                throw new \Exception("Tidak ada parameter tipe input dan options");
                            }
                        }
                    }else{
                        throw new \Exception("Tidak ada parameter tipe input");
                    }
                }
            }
    
            DB::commit();

            return redirect()->route(routePrefix().'eform.index')->with('success_alert','Data has been imported');

        } catch (\Throwable $th) {
            DB::rollback();
            report($th);

            return redirect()->back()->with('error_alert', $th->getMessage())->withInput();
        }
    }

    public function answerToDocx($type = 'docx', $answer_id){
        $answer_form = EformSubmit::findOrFail($answer_id);
        $eform       = Eform::where('form_id', $answer_form->form_id)->where('answer_type', 'check_box')->withTrashed()->get();
        $eform_block = Eform::where('form_id', $answer_form->form_id)->where('answer_type', 'multi')->withTrashed()->get();
        $template    = EformGenerate::where('eg_form_id', $answer_form->form_id)->firstOrFail();
        $answer      = json_decode($answer_form->answer_eform, true);

        // print_r($answer['answer_1']);exit;

        if($eform){
            foreach($eform as $e){
                $answer['answer_'.$e->question_no] = implode(', ', $answer['answer_'.$e->question_no]);
            }
        }
        
        if (\Storage::exists($template->eg_upload_template)) {
            $pathfile = public_path('documents/eform/result_word/answer.docx');
            $resultpath = public_path('documents/eform/result_pdf/');    
            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('app/'.$template->eg_upload_template));

            foreach($eform_block ?? [] as $eb){
                $templateProcessor->cloneBlock('answer_'.$eb->question_no, 0, true, false, $answer['answer_'.$eb->question_no]);
                unset($answer['answer_'.$eb->question_no]);
            }

            $templateProcessor->setValues($answer);

            if($type == 'docx'){
                header("Content-Disposition: attachment; filename=answer.docx");
                $templateProcessor->saveAs('php://output');            
            }else{
                $templateProcessor->saveAs($pathfile);

                /**
                 * ini khusus untuk Mac OsX
                 */
                // shell_exec("/Applications/LibreOffice.app/Contents/MacOS/soffice --headless --convert-to pdf $pathfile --outdir $resultpath");

                /**
                 * Ini khusus untuk linux
                 */
                shell_exec("export HOME=$resultpath && libreoffice --headless --convert-to pdf $pathfile --outdir $resultpath");

                return redirect(url('/documents/eform/result_pdf/').'/answer.pdf');
            }
        }

        return response()->json("File Not Found", 404);
    }

    public function dashboard($form_id)
    {
        $eform = Eform::where('form_id', $form_id)->withTrashed()->get();
        $eform_submit = EformSubmit::where('form_id', $form_id)->get();
        $arrEformId1 = [];
        $arrEformId2 = [];
        $arrEformDashboard1 = [];
        $arrEformDashboard2 = [];
        $arrSlug = [];

        foreach ($eform as $x) {
            if ($x->answer_type == 'radio_button' || $x->answer_type == 'select_box' || $x->answer_type == 'check_box') {
                array_push($arrEformId1, $x->id);
            } else if ($x->answer_type == 'text' || $x->answer_type == 'text_area' || $x->answer_type == 'image') {
                array_push($arrEformId2, $x->id);
            } 
        }

        foreach ($arrEformId1 as $i) {
            $eform_detail = EformDetail::where('id_eform', $i)->get()->toArray();
            $eform = Eform::where('id', $i)->withTrashed()->first();
            array_push($arrEformDashboard1, [
                'question' => $eform->question,
                'answer_type' => $eform->answer_type,
                'detail' => $eform_detail
            ]);
        }

        foreach ($arrEformId2 as $j) {
            $eform = Eform::where('id', $j)->withTrashed()->first();
            array_push($arrEformDashboard2, [
                'question' => $eform->question,
                'answer_type' => $eform->answer_type,
                'slug' => $eform->slug,
                'detail' => []
            ]);

            array_push($arrSlug, [$eform->slug]);
        }
        
        foreach ($eform_submit as $k) {
            $decode = json_decode($k->answer_eform, true);

            if($decode){
                foreach ($decode ?? [] as $d => $dec) {
                    $q = substr($d, strlen( 'answer_' ));
                    $new_key = Eform::where('form_id', $form_id)->where('question_no', $q)->withTrashed()->first();
    
                    $decode[$new_key->slug] = $dec;
                    unset($decode[$d]);
                }
    
                foreach ($arrSlug as $key => $value) {
                    $slug = $value;
                    $filtered = array_filter(
                        $decode,
                        function ($key) use ($slug) {
                            return in_array($key, $slug);
                        },
                        ARRAY_FILTER_USE_KEY
                    );
                    
                    foreach ($arrEformDashboard2 as $i => $v) {
                        // return dd(array_keys($filtered)[0]);
                        if (!empty($filtered)) {
                            if ($v['slug'] == array_keys($filtered)[0]) {
                                array_push($arrEformDashboard2[$i]['detail'], $filtered);
                            }
                        }
                    }
                }
            }
        }
        
        return view('eform.dashboard', compact('arrEformDashboard1', 'arrEformDashboard2'));
    }

    public function inactive(Request $request)
    {
        $eform = Eform::withTrashed()->where('form_id', $request->form_id);

        if($request->status == 'active'){
            return $eform->restore();
        }else{
            return $eform->delete();
        }
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file_import' => 'required|mimes:xlsx',
            'form_number' => 'required|int',
        ]);    

        Excel::import(new EformAnswer($request->form_number, $request->ip()), request()->file('file_import'));

        return redirect()->route(routePrefix().'eform.index')->with('success_alert', 'File berhasil diimport.');
    }

    public function xlsxToDB($request)
    {
        $formId = null;
        $check = Eform::withTrashed()->get();
        if($check->isEmpty()) {
            $formId = 1;
        } else {
            $eform = Eform::addSelect('form_id')->groupBy('form_id', 'created_at')->withTrashed()->latest()->first();
            $formId = $eform->form_id+1;
        }

        $directory = Directory::firstOrCreate([
            'nama_folder' => 'E-Form'
        ], [
            'parent_id', null
        ]);

        $eform_directory = Directory::create([
            'parent_id' => $directory->id,
            'nama_folder' => $request->get('title'),
        ]);

        Excel::import(new EformImportExcel($request->target, $request->title, $formId, $eform_directory->id), request()->file('eform_file'));

        return redirect()->route(routePrefix().'eform.index')->with('success_alert', 'File berhasil diimport.');
    }

    public function otsIp($form_id)
    {
        $otsIp = Eform::where('form_id', $form_id);
        $oti   = $otsIp->first()->one_time_ip ?? false;
        
        $otsIp->update([
            'one_time_ip' => !$oti
        ]);
    }

    public function fileUpload(Request $request)
    {
        Validator::validate($request->all(), [
            'file' => 'required|mimes:png,jpg,jpeg,gif,bmp',
        ]);

        File::ensureDirectoryExists(storage_path('app/public/editor'));
        $path = $request->file('file')->store('public/editor');

        return url(\Storage::url($path));
    }

    public function duplicate($slug)
    {
        $eforms = Eform::where('slug', $slug)->get();
        $formId = null;
        $check = Eform::withTrashed()->get();
        if($check->isEmpty()) {
            $formId = 1;
        } else {
            $eform = Eform::addSelect('form_id')->groupBy('form_id', 'created_at')->withTrashed()->latest()->first();
            $formId = $eform->form_id+1;
        }

        foreach ($eforms as $e) {
            $copy1 = $e->replicate()->fill(
                [
                    'form_id' => $formId,
                    'title' => $e->title . "(1)",
                    'slug' => $e->slug . "(1)"
                ]
            );
            $copy1->save();

            if ($e->eform_detail()->exists()) {
                $detail = EformDetail::where('id_eform', $e->id)->get();
                foreach ($detail as $d) {
                    $copy2 = $d->replicate()->fill(
                        [
                            'id_eform' => $copy1->id
                        ]
                    );
                    $copy2->save();
                }
            } 
        }
        return redirect()->back()->with('success_alert', 'Data has been duplicated.');
    }
}
