<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Organization;
use App\Models\Roles;
use App\Models\User;
use App\Models\OrganizationAccess;
use Auth;
use DB;

class PendingUserController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!OrganizationAccess::checkPermission("pending-user-view", Auth::user()->id)) {
                return abort(401);
            }
    
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $pending_users = User::select('users.*', 'organizations.name As organization_name')
                        ->leftJoin('organizations', 'organizations.id', 'users.id_organization')
                        ->where('users.status', 1)
                        ->where('users.id_organization', Auth::user()->id_organization)
                        ->orderBy('users.id')
                        ->get();
        return view('pending_user.index', [
            'role' => $role,
            'pending_users' => $pending_users
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $organizations = Organization::where('status', 1)->get();
        $roles = Roles::all();
        $parents = User::all();
        $users = User::find($id);
        return view('pending_user.form', [
            'role' => $role,
            'users' => $users,
            'roles' => $roles,
            'parents' => $parents,
            'organizations' => $organizations
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->update([
            'role_id' => $request->role_id,
            'level' => $request->level,
            'parent' => $request->parent,
            'status' => $request->status
        ]);
        return redirect()->route(routePrefix().'pending_user.index')->with('success_alert','Data has been updated');
    }
}
