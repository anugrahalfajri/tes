<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Organization;
use App\Models\Master\OrganizationStatusLog as LogModel;
use Illuminate\Http\Request;

use App\Http\Requests\Master\OrganizationStatusLog;
use DB;

class OrganizationController extends Controller
{
    public function statusLog(OrganizationStatusLog $request)
    {
        try {
            DB::beginTransaction();

            $active       = $request->status === 'true' ? 'Active' : 'Inactive';
            $name         = auth()->user()->name;
            $desc         = "$name telah merubah status menjadi $active";
            $organization = Organization::where('id', $request->organization_id)->update([
                'status'    => $request->status == 'true' ? 1 : 0
            ]);

            LogModel::create([
                'organization_id'   => $request->organization_id,
                'user_id'           => auth()->user()->id,
                'description'       => $desc
            ]);

            DB::commit();

            return response()->json([
                "status"  => true,
                "message" => "berhasil update status"
            ], 200);

        } catch (\Throwable $th) {
            DB::rollback();

            report($th);

            return response()->json([
                "status"  => false,
                "message" => $th->getMessage()
            ], 200);
        }
    }
}
