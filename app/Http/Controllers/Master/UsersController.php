<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Organization;
use App\Models\Roles;
use App\Models\Level;
use App\Models\Master\User;
use App\Models\User as UserDefault;
use App\Models\Department;
use App\Models\OrganizationAccess;
use Carbon\Carbon;
use Validator;
use Auth;
use DB;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!OrganizationAccess::checkPermission("user-management-view", Auth::user()->id)) {
                return abort(401);
            }
    
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $id_organization = Auth::user()->id_organization;
        $users = User::all();
        return view('master.users.index', [
            'users' => $users,
            'role' => $role,
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3")
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $roles = Roles::all();
        $level = Level::where('status', '1')->get();
        $parents = User::all();
        $organizations = Organization::where('status', 1)->get();
        return view('master.users.insert', [
            'role' => $role,
            'level' => $level,
            'action' => 'store',
            'roles' => $roles,
            'parents' => $parents,
            'organizations' => $organizations,
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3")
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->email), // default
            'level' => $request->level,
            'created_at' => $request->created_at,
            'updated_at' => $request->updated_at,
            'status' => $request->status,
            'join_date' => Carbon::parse($request->join_date)->format('Y-m-d'),
            'phone' => $request->phone,
        ];
        $user = User::create($data);
        // $user->sendEmailVerificationNotificationAdmin();

        // \Session::flash('email', 'Check your email!'); 
        return redirect()->route(routePrefix().'master_users.index')->with('success_alert','Data has been saved');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::find($id);
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $roles = Roles::all();
        $level = Level::where('status', '1')->get();
        $parents = User::all();
        $organizations = Organization::where('status', 1)->get();
        return view('master.users.insert', [
            'role' => $role,
            'action' => 'update',
            'level' => $level,
            'users' => $users,
            'roles' => $roles,
            'parents' => $parents,
            'organizations' => $organizations,
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3")
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $users = User::find($id);
        $password_old = $users->password;
        $new_password = bcrypt($request->password);
        if($new_password == $password_old){
            $password = $password_old;
        } else {
            $password = $new_password;
        }
        $users->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $password,
            'level' => $request->level,
            'created_at' => $request->created_at,
            'updated_at' => $request->updated_at,
            'status' => $request->status,
            'join_date' => Carbon::parse($request->join_date)->format('Y-m-d'),
            'phone' => $request->phone,
        ]);
        return redirect()->route(routePrefix().'master_users.index')->with('success_alert','Data has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active_inactive($id)
    {
        $users = User::find($id);
        $users->update([
            'status' => $users->status == '1' ? '2' :'1'
        ]);
        return redirect()->route(routePrefix().'master_users.index')->with('success_alert','Data has been updated');
    }

    public function delete($id)
    {
        $users = User::find($id);
        $users->delete();
        return redirect()->route(routePrefix().'master_users.index')->with('success_alert','Data has been deleted');
    }

    public function verify(Request $request)
    {
        try {
            $hasVerifiedEmail = false;
            $isExpired = false;
            $invalidLinkVerification = false;
            $error = false;
            $message = '';

            $user = User::find($request['id']);

            // if (!$user) {
            //     $error = true;
            //     $message = 'Data tidak ditemukan';
            //     return response()->json(['message' => $message]);
            // }

            // if ($user->hasVerifiedEmail()) {
            //     $hasVerifiedEmail = true;
            //     $message = 'Sudah verifikasi';
            //     return response()->json(['message' => $message]);
            // }

            $user->email_verified_at = date('Y-m-d g:i:s');
            $user->save();
            return view('verification.form', compact('user'));
        } catch (\Exception $e) {
            // Log::error($e->getMessage());
            abort(500);
        }
    }

    public function updatePassword(Request $request, $id)
    {
        $request->validate([
            'password' => 'required|min:6|regex:/^.*(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[@$!%*#?&]).*$/|confirmed',
            'pin' => 'required|numeric|digits:6|confirmed'
        ],
        [
            'password.regex' => 'Your password must be more than 8 characters long, should contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character.',
        ]);

        $users = User::find($id);
        $users->update([
            'password' => bcrypt($request->password),
            'pin' => bcrypt($request->pin),
            'status' => 2
        ]);

        Auth::logout();

        \Session::flash('email_active', 'User active!'); 
        return redirect()->route(routePrefix().'login')->with('success_alert','Password and PIN has been updated');
    }

    public function profile()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();

        return view('users.profile', compact('role'));
    }

    public function changeProfile($id, Request $request)
    {
        $request->validate([
            'password' => 'nullable|regex:/^.*(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[@$!%*#?&]).*$/|confirmed',
        ],
        [
            'password.regex' => 'Your password must be more than 8 characters long, should contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character.',
        ]);

        $user = [
            'name'  => $request->name,
            'email' => $request->email,
        ];

        if(isset($request->password)) $user["password"] = bcrypt($request->password);

        User::where('id', $id)->update($user);

        return redirect()->back()->with('success_msg', "berhasil update Profile!");
    }

    public function setting($id)
    {
        $user           = User::findOrFail($id);
        $role           = $user->role ?? abort(404);
        $roles          = Roles::all();
        $departments    = Department::all();
        $organizations  = Organization::where('status', 1)->get();

        return view('users.account', compact('user', 'roles', 'departments', 'organizations', 'role'));
    }

    public function accountSetting($id, Request $request)
    {
        $request->validate([
            'pin_account' => 'nullable|min:6|confirmed',
        ]);

        $user   = [
            'id_organization'   => $request->organization,
            'role_id'           => $request->role,
            'department_id'     => $request->department
        ];
        if(isset($request->pin_account)) $user["pin"] = bcrypt($request->pin_account);

        User::where('id', $id)->update($user);

        return redirect()->back()->with('success_msg', "berhasil update account!");
    }

    public function statusLog(Request $request)
    {
        try {
            DB::beginTransaction();

            $active       = $request->status === 'true' ? 'Active' : 'Inactive';
            $name         = auth()->user()->name;
            $desc         = "$name telah merubah status menjadi $active";

            if(\Auth::getDefaultDriver() == 'web'){
                $user = UserDefault::findOrFail($request->user_id);
            }else{
                $user = User::findOrFail($request->user_id);
            }

            $user->update([
                'status'    => $request->status == 'true' ? 1 : 0

            ]);

            DB::commit();

            return response()->json([
                "status"  => true,
                "message" => "berhasil update status"
            ], 200);

        } catch (\Throwable $th) {
            DB::rollback();

            report($th);

            return response()->json([
                "status"  => false,
                "message" => $th->getMessage()
            ], 500);
        }
    }
}
