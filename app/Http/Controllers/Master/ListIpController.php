<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\ListIp;
use App\Models\Roles;
use Illuminate\Http\Request;
use DB;
use Auth;

class ListIpController extends Controller{
    public function index()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        return view('master.list_ip.index',[
            'list_ip' => ListIp::all(),
            'role' => $role
        ]);
    }

    public function create()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        return view('master.list_ip.form',[
            'act' => 'tambah',
            'role' => $role
        ]);
    }

    public function store(Request $request)
    {
        ListIp::updateOrCreate(['id' => $request->id], ['ip_address' => $request->ip_address]);
        return redirect()->route(routePrefix().'list_ip.index')->with('success_alert', 'Data has been saved');
    }

    public function edit($id)
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        return view('master.list_ip.form',[
            'act' => 'edit',
            'list_ip' => ListIp::find($id),
            'role' => $role
        ]);
    }

    public function delete($id)
    {
        $list_ip = ListIp::find($id);
        $list_ip->delete();
        return redirect()->route(routePrefix().'list_ip.index')->with('success_alert', 'Data has been delete');
    }
}