<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OrganizationAccess;
use App\Models\SubOrganization;
use App\Models\Organization;
use App\Models\Roles;
use App\Models\User;

use Auth;
use DB;

class SubOrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!OrganizationAccess::checkPermission("sub-organization-view", Auth::user()->id)) {
                return abort(401);
            }
    
            return $next($request);
        });
    }
    public function index()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $sub_organizations = '';
        if ($role->name == 'admin organisasi') {
            $sub_organizations = SubOrganization::select('sub_organizations.*', 'organizations.id AS organization_id', 'organizations.admin')
                                ->leftJoin('organizations', 'sub_organizations.id_organization', 'organizations.id')
                                ->leftJoin('users', 'organizations.admin', 'users.id')
                                ->where('users.id_organization', Auth::user()->id_organization)
                                ->orderBy('id')
                                ->get();
        } else {
            $sub_organizations = SubOrganization::select('sub_organizations.*', 'organizations.id AS organization_id', 'organizations.admin')
                                ->leftJoin('organizations', 'sub_organizations.id_organization', 'organizations.id')
                                ->leftJoin('users', 'organizations.admin', 'users.id')
                                ->orderBy('id')
                                ->get();
        }
        return view('sub_organization.index', [
            'role' => $role,
            'sub_organizations' => $sub_organizations
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $organizations = Organization::where('status', 1)->orderBy('id')->get();
        return view('sub_organization.insert', [
            'role' => $role,
            'organizations' => $organizations,
            'action' => 'store'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id_organization' => 'required|int',
            'name'            => 'required|string|max:255',
            'status'          => 'required|string|max:255',
            'description'     => 'nullable|string|max:255'
        ]);

        $data = [
            'name' => $request->name,
            'description' => $request->description,
            'status' => $request->status,
            'id_organization' => $request->id_organization,
        ];
        SubOrganization::create($data);
        return redirect()->route(routePrefix().'sub_organization.index')->with('success_alert','Data has been saved');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $sub_organization = SubOrganization::find($id);
        $organizations = Organization::where('status', 1)->orderBy('id')->get();
        return view('sub_organization.insert', [
            'role' => $role,
            'action' => 'update',
            'organizations' => $organizations,
            'sub_organization' => $sub_organization
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'id_organization' => 'required|int',
            'name'            => 'required|string|max:255',
            'status'          => 'required|string|max:255',
            'description'     => 'nullable|string|max:255'
        ]);
        
        $sub_organization = SubOrganization::find($id);
        $sub_organization->update([
            'name' => $request->name,
            'description' => $request->description,
            'status' => $request->status,
            'id_organization' => $request->id_organization
        ]);
        return redirect()->route(routePrefix().'sub_organization.index')->with('success_alert','Data has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $sub_organization = SubOrganization::find($id);
        $sub_organization->delete();
        return redirect()->route(routePrefix().'sub_organization.index')->with('success_alert','Data has been deleted');
    }
}
