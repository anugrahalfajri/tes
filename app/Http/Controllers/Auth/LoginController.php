<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */

    public function showLoginForm(){
        return view('auth.login');
    }
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $check = User::where('email', $credentials['email'])->first();
        $prefix = $request->route()->getPrefix() == '' ? '/' : '/ma-mlk/dashboard';
        $guard = $request->route()->getPrefix() == '' ? 'web' : 'master';

        if (Auth::guard($guard)->attempt($credentials)) {
            if (Auth::guard($guard)->user()->status == 1) { 
                return back()->with('error_alert', 'Users not active');
            } else {
                $request->session()->regenerate();
                if ($check->role_id == 4) {
                    return redirect()->intended(route('root'));
                } else {
                    return redirect()->intended($prefix);
                }
            }
        } else {
            return back()->with('error_alert', 'Users not exist');
        }
    }
}