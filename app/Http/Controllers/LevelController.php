<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Permission;
use App\Models\Roles;
use App\Models\Level;
use Auth;

class LevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $levels = Level::orderBy('id')->get();

        return view('level.index', [
            'role' => $role,
            'levels' => $levels
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $permissions = Permission::orderBy('id')->get();

        return view('level.insert', [
            'role' => $role,
            'permissions' => $permissions
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'permission'    => "required|array|min:1",
            'permission.*' => 'required'
        ]);

        $data = [
            'name' => $request->name,
            'description' => $request->description,
            'status' => $request->status,
            'permission' => implode(',', $request->permission)
        ];
        Level::create($data);

        return redirect()->route(routePrefix().'level.index')->with('success_alert','Data has been saved');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $level = Level::where('id', $id)->first();
        $permissions = Permission::orderBy('id')->get();

        return view('level.edit', [
            'role' => $role,
            'level' => $level,
            'permissions' => $permissions
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'permission'    => "required|array|min:1",
            'permission.*' => 'required'
        ]);

        $level = Level::find($id);
        $level->update([
            'name' => $request->name,
            'description' => $request->description,
            'status' => $request->status,
            'permission' => implode(',', $request->permission)
        ]);

        return redirect()->route(routePrefix().'level.index')->with('success_alert','Data has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
    }
}
