<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AccessRight;
use App\Models\Directory;
use App\Models\Eform;
use App\Models\Ext_Properties;
use App\Models\File_Uploads;
use App\Models\History;
use App\Models\Message;
use App\Models\Note_List;
use App\Models\Organization;
use App\Models\Roles;
use App\Models\User;
use App\Models\Workflow;
use App\Models\Workflow_Detail;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAccessRight()
    {
        $access_right = AccessRight::all();
        if (empty($access_right)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $access_right
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getAccessRightById($id)
    {
        $access_right = AccessRight::where('id', $id)->first();
        if (empty($access_right)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $access_right
        ], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDirectory()
    {
        $directory = Directory::all();
        if (empty($directory)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $directory
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDirectoryById($id)
    {
        $directory = Directory::where('id', $id)->first();
        if (empty($directory)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $directory
        ], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEform()
    {
        $eform = Eform::with('eform_detail')->get();
        if (empty($eform)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $eform
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEformById($id)
    {
        $eform = Eform::with('eform_detail')->where('id', $id)->first();
        if (empty($eform)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $eform
        ], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getExtendProperties()
    {
        $ext_properties = Ext_Properties::all();
        if (empty($ext_properties)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $ext_properties
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getExtendPropertiesById($id)
    {
        $ext_properties = Ext_Properties::where('id', $id)->first();
        if (empty($ext_properties)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $ext_properties
        ], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getFileUploads()
    {
        $file_uploads = File_Uploads::with('version', 'properties', 'lastVersion', 'directory')->get();
        if (empty($file_uploads)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $file_uploads
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getFileUploadsById($id)
    {
        $file_uploads = Message::where('id', $id)->with('version', 'properties', 'lastVersion', 'directory')->first();
        if (empty($file_uploads)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $file_uploads
        ], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getHistory()
    {
        $history = History::all();
        if (empty($history)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $history
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getHistoryById($id)
    {
        $history = History::where('id', $id)->first();
        if (empty($history)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $history
        ], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getMessage()
    {
        $message = Message::all();
        if (empty($message)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $message
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getMessageById($id)
    {
        $message = Message::where('id', $id)->first();
        if (empty($message)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $message
        ], 200);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getNoteList()
    {
        $note_list = Note_List::all();
        if (empty($note_list)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $note_list
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getNoteListById($id)
    {
        $note_list = Note_List::where('id', $id)->first();
        if (empty($note_list)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $note_list
        ], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getOrganization()
    {
        $organization = Organization::with('sub_organization')->get();
        if (empty($organization)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $organization
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getOrganizationById($id)
    {
        $organization = Organization::with('sub_organization')->where('id', $id)->first();
        if (empty($organization)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $organization
        ], 200);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRole()
    {
        $role = Roles::all();
        if (empty($role)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $role
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getRoleById($id)
    {
        $role = Roles::where('id', $id)->first();
        if (empty($role)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $role
        ], 200);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUser()
    {
        $user = User::with('role')->get();
        if (empty($user)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $user
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getUserById($id)
    {
        $user = User::with('role')->where('id', $id)->first();
        if (empty($user)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $user
        ], 200);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getWorkflow()
    {
        $workflow = Workflow::with('workflow_approval')->get();
        if (empty($workflow)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $workflow
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getWorkflowById($id)
    {
        $workflow = Workflow::with('workflow_approval')->where('id', $id)->first();
        if (empty($workflow)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $workflow
        ], 200);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getWorkflowDetail()
    {
        $workflow_detail = Workflow_Detail::with('workflow_detail_approval')->get();
        if (empty($workflow_detail)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $workflow_detail
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getWorkflowDetailById($id)
    {
        $workflow_detail = Workflow_Detail::with('workflow_detail_approval')->where('id', $id)->first();
        if (empty($workflow_detail)) {
            return response()->json([
                "success" => true,
                "message" => "Data not found"
            ], 404);
        }

        return response()->json([
            "success" => true,
            "message" => "Data found",
            "data" => $workflow_detail
        ], 200);
    }
}
