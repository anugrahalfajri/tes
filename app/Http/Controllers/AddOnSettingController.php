<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Roles;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Models\Permission;
use App\Models\Organization;
use App\Models\OrganizationAccess;

class AddOnSettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware(function ($request, $next) {
        //     if (!OrganizationAccess::checkPermission("add-on-setting-view", Auth::user()->id)) {
        //         return abort(401);
        //     }
    
        //     return $next($request);
        // });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $permission = Permission::where('status', 'Y')->get();
        return view('permission.index', [
            'role' => $role,
            'permission' => $permission,
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3")
        ]);
    }

    public function create()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        return view('permission.form', [
            'role' => $role,
            'act' => 'tambah',
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3")
        ]);
    }

    public function store(Request $request)
    {
        if(!empty($request->id)){
            $permission = Permission::find($request->id);
            $permission->update([
                'menu_name' => $request->menu_name,
                'function' => $request->function,
                'system_name' => $request->system_name,
            ]);
        }else {
            Permission::create([
                'menu_name' => $request->menu_name,
                'function' => $request->function,
                'system_name' => $request->system_name,
            ]);
        }

         return redirect()->route(routePrefix().'add_on_setting.index')->with('success_alert','Data has been saved');
    }

    public function edit($id)
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $permission = Permission::find($id);
        return view('permission.form', [
            'role' => $role,
            'act' => 'edit',
            'permission' => $permission,
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3")
        ]);
    }

    public function delete($id)
    {
        $permission = Permission::find($id);
        $permission->delete();
        return redirect()->route(routePrefix().'add_on_setting.index')->with('success_alert','Data has been saved');
    }


    public function organization_access()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $organization = Organization::all();
        return view('add-on-setting.index', [
            'role' => $role,
            'organizations' => $organization,
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3")
        ]);
    }

    public function set_organization_access($id)
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $organization = Organization::find($id);
        $organization_access = OrganizationAccess::where('id_organization', $id)->get();
        $permission = Permission::where('function', 'view')->get();
        return view('add-on-setting.access', [
            'role' => $role,
            'organization' => $organization,
            'organization_access' => $organization_access,
            'permission' => $permission,
            'notification' => DB::select("SELECT * from notifications order by updated_at desc limit 3")
        ]);
    }
    public function store_organization_access(Request $request)
    {
        $organization = OrganizationAccess::where('id_organization', $request->organization_id)->get();
        // dd($organization);
        if(count($organization) > 0){
            foreach ($organization as $key => $value) {
                $value->delete();
            }
            if(!empty($request->permissions)){
                foreach ($request->permissions as $key => $value) {
                    OrganizationAccess::create([
                        'id_organization' => $request->organization_id,
                        'id_permission' => $value
                    ]);
                }
            }
        }else {
            if(!empty($request->permissions)){
                foreach ($request->permissions as $key => $value) {
                    OrganizationAccess::create([
                        'id_organization' => $request->organization_id,
                        'id_permission' => $value
                    ]);
                }
            }
        }

         return redirect()->route(routePrefix().'add_on_setting.organization_access')->with('success_alert','Data has been saved');
    }

}
