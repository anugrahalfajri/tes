<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\TroubleTicketList;
use App\Models\User;
use App\Models\Roles;
use Illuminate\Http\Request;
use DB;
use Auth;

class TroubleTicketListController extends Controller{
    public function index()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        return view('trouble_ticket_list.index',[
            'trouble_ticket_list' => TroubleTicketList::where('id_user', Auth::user()->id)->orWhere('created_by', Auth::user()->id)->get(),
            'role' => $role
        ]);
    }

    public function create()
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $user_id = Auth::user()->id;
        $user = DB::select("SELECT * from users where id != $user_id");
        return view('trouble_ticket_list.form',[
            'act' => 'tambah',
            'role' => $role,
            'users' => $user
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'date_time_delivery' => 'required',
            'title' => 'required',
            'description' => 'required',
            'status' => 'required',
            'conversation' => 'required'
        ]);

        if(!empty($request->id)){
            $trouble = TroubleTicketList::find($request->id);
            $trouble->update([
                'id_user' => $request->id_user, 
                'title' => $request->title, 
                'description' => $request->description,
                'status' => $request->status,
                'conversation' => $request->conversation,
                'date_time_delivery' => $request->date_time_delivery,
            ]);
        }else {
            TroubleTicketList::create([
                'id_user' => $request->id_user, 
                'title' => $request->title, 
                'description' => $request->description,
                'status' => $request->status,
                'conversation' => $request->conversation,
                'date_time_delivery' => $request->date_time_delivery,
                'created_by' => Auth::user()->id
            ]);
        }
        return redirect()->route(routePrefix().'trouble_ticket_list.index')->with('success_alert', 'Data has been saved');
    }

    public function edit($id)
    {
        $role = Roles::where('id', Auth::user()->role_id)->first();
        $user_id = Auth::user()->id;
        $user = DB::select("SELECT * from users where id != $user_id");
        return view('trouble_ticket_list.form',[
            'act' => 'edit',
            'trouble_ticket_list' => TroubleTicketList::find($id),
            'role' => $role,
            'users' => $user
        ]);
    }

    public function delete($id)
    {
        $trouble = TroubleTicketList::find($id);
        $trouble->delete();
        return redirect()->route(routePrefix().'trouble_ticket_list.index')->with('success_alert', 'Data has been delete');
    }
}