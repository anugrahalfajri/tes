<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ConfigUpload;

class ConfigUploadController extends Controller
{
    public $moduls      = [
        'eform' => 'eForm', 'workflow' => 'Workflow', 'explore' => 'Document Explore'
    ]; 

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = \App\Models\Roles::findOrFail(auth()->user()->role_id);
        $configs = ConfigUpload::get();
        $moduls = $this->moduls;

        return view('konfigurasi.index', compact('configs', 'role', 'moduls'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $moduls = $this->moduls;
        $label  = "Create";

        return view('konfigurasi.create', compact('moduls', 'label'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tipe_modul' => 'required|unique:config_uploads,modul_name,'.@$request->config_id.',id',
        ]); 

        ConfigUpload::updateOrCreate(
            ['id' => @$request->config_id],
            [
                'modul_name'            => $request->tipe_modul,
                'allowed_mime_types'    => implode(";", $request->tipe_file),
                'max_file_size'         => $request->file_size
            ]
        );

        return redirect()->route('config.index')->with('success_alert', 'Berhasil menyimpan konfigurasi.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $config = ConfigUpload::findOrFail($id);
        $moduls = $this->moduls;
        $label  = "Edit";

        return view('konfigurasi.create', compact('config', 'moduls', 'label'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ConfigUpload::where('id', $id)->delete();

        return redirect()->route('config.index')->with('success_alert', 'berhasil menghapus konfigurasi.');
    }
}
