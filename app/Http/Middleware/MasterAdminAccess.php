<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\MasterAdminIpAccess;
use Auth;

class MasterAdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $ipaccess = MasterAdminIpAccess::where('ip_address', $request->ip())->first();
        
        if(!$ipaccess && !$this->isAdminMaster()) abort(401);

        return $next($request);
    }

    private function isAdminMaster()
    {
        $check = Auth::check() ? Auth::user()->role_id : true;

        return $check ? $check === 3 : $check;
    }
}
