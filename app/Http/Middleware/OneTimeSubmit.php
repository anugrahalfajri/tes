<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Eform;

class OneTimeSubmit
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $isOTS = Eform::with(['eform_submit'])->where('form_id', $request->form_id)->first();
        $ots   = false;

        if(@$isOTS->one_time_submit){
            if($isOTS->target == "login"){
                $submission = $isOTS->eform_submit()->where('user_id', auth()->user()->id)->count();

                $ots = $submission > 0;
            }else{
                if($isOTS->one_time_ip){
                    $submission = $isOTS->eform_submit()->where('user_ip_address', $request->ip())->count();
                    
                    $ots = $submission > 0;
                }else{
                    $cookie = json_decode($request->cookie("eform_submission"), true);
                    $cookie = $cookie ?? [];
                    $ots    = in_array($request->form_id, $cookie);
                }
            }

            if($ots) return redirect()->back()->with("error_alert", "Form ini sudah pernah anda isi.")->withInput();
        }

        return $next($request);
    }
}
