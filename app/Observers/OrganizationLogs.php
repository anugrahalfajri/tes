<?php

namespace App\Observers;

use App\Models\Organization;
use App\Models\Master\OrganizationModificationLog;

class OrganizationLogs
{
    /**
     * Handle the Organization "updated" event.
     *
     * @param  \App\Models\Organization  $organization
     * @return void
     */
    public function updated(Organization $organization)
    {
        $changes    = $organization->getChanges();
        unset($changes["updated_at"]);

        foreach($changes ?? [] as $column => $c){
            $column_name  = str_replace('_', '', $column);
            $name         = auth()->user()->name;
            if($column == 'admin') $c = $this->findAdmin($c, $organization);
            $desc         = "<b>$name</b> telah merubah $column_name menjadi <b>$c</b>";

            OrganizationModificationLog::create([
                'organization_id'   => $organization->id,
                'user_id'           => auth()->user()->id,
                'description'       => $desc
            ]);
        }
    }

    private function findAdmin($id, $model)
    {
        return $model->admin_user->name ?? 'Unknown User';
    }
}
