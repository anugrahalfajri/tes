<?php

function buildRelationStructure($trees, $structure = '')
{
    $structure .= "<ul>";

    foreach($trees ?? [] as $tree){
        $nama_file = $tree->foreign->nama_file ?? "Unknown";

        $structure .= "<li><a href='#'>$nama_file</a>";
        if($tree->foreign->relations->count() > 0){
            $structure = buildRelationStructure($tree->foreign->relations, $structure);
        }
        $structure .= "</li>";
    }

    $structure .= "</ul>";

    return $structure;
}

function urlWithPrefix($url, $params = null)
{
    $prefix = Auth::getDefaultDriver() == 'web' ? '' : '/ma-mlk';

    return url($prefix.$url, $params);
}

function routePrefix()
{
    return Auth::getDefaultDriver() == 'web' ? '' : 'ma-mlk.';  
}

function set_cookie_value($name, $value)
{
    $newCookie  = []; 
    $cookie     = \Illuminate\Support\Facades\Cookie::get($name);

    if(isset($cookie)) $newCookie = json_decode($cookie, true);

    $newCookie[] = $value;

    return json_encode($newCookie);
}