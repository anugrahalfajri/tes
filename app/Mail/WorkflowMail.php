<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Models\Workflow;

class WorkflowMail extends Mailable
{
    use Queueable, SerializesModels;

    private $id;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $workflow = Workflow::findOrFail($this->id);

        return $this->from(env('MAIL_FROM_ADDRESS'), env('APP_NAME'))
                    ->subject('DMS | Approval Notification')
                    ->view('workflow.mail', compact('workflow'));
    }
}
