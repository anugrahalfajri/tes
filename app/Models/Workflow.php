<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Workflow extends Model
{
    use HasFactory;

    public $table = 'workflow';

    protected $fillable = ['id_file','description','nama','pin', 'id_directory', 'created_by'];

    protected $primarykey = 'id';

    public function workflow_approval()
    {
        return $this->hasMany(Workflow_Approval::class, 'id_workflow');
    }
}
