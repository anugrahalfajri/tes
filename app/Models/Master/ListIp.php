<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListIp extends Model
{
    use HasFactory;
    public $table = 'master_admin_ip_access';

    protected $fillable = ['ip_address'];
}
