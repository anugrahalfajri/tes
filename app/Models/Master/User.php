<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Level;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\VerifyEmailAdmin;
use App\Notifications\VerifyEmailGuest;


class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;
    protected $table    = 'master_users';
    protected $guarded = ['master'];
    protected $guard = 'master';

     public function levels()
    {
        return $this->hasOne(Level::class, 'id', 'level');
    }

        /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendEmailVerificationNotificationAdmin()
    {
        $this->notify(new VerifyEmailAdmin);
    }

    public function sendEmailVerificationNotificationGuest()
    {
        $this->notify(new VerifyEmailGuest);
    }

    public function role()
    {
        return $this->belongsTo(\App\Models\Roles::class, 'role_id');
    }
}
