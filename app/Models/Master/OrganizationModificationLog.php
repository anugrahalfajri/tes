<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrganizationModificationLog extends Model
{
    use HasFactory;
    protected $table    = 'organization_modification_logs';
    protected $fillable =  [
        'organization_id', 'user_id', 'description'
    ];

    public function user()
    {
        $model = \Auth::getDefaultDriver() == 'web' ? '\App\Models\User' : '\App\Models\Master\User';

        return $this->belongsTo($model, 'user_id');
    }
}
