<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Purchase_Uploads_Approval extends Model
{
    use HasFactory;
    
    
    protected $table = 'purchase_uploads_approval';

    public $timestamps = 'false';

    protected $fillable = ['id_file','member','date','status'];

    protected $primarykey = 'id';
    
}
