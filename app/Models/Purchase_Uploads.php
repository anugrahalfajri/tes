<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Purchase_Uploads extends Model
{
    use HasFactory;

    public $table = 'purchase_uploads';

    protected $fillable = ['nama_file','path_file','member','id_directory','is_deleted','description'];

    protected $primarykey = 'id';
}
