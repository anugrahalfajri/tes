<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EformDetail extends Model
{
    use HasFactory;

    public $table = 'eform_detail';

    protected $guarded;

    public function eform()
    {
        return $this->belongsTo(Eform::class, 'id_eform');
    }
}
