<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocumentRelation extends Model
{
    use HasFactory;

    protected $table    = 'document_relations';
    protected $fillable = [
        'primary_document', 'foreign_document', 'created_by'
    ];

    public function primary()
    {
        return $this->belongsTo(File_Uploads::class, 'primary_document');
    }

    public function foreign()
    {
        return $this->belongsTo(File_Uploads::class, 'foreign_document');
    }
}
