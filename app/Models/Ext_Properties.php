<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ext_Properties extends Model
{
    use HasFactory;

    public $table = 'extented_properties';

    protected $fillable = ['id_file', 'no_document', 'tgl_document', 'relasi_document', 'document_expiration_date', 'tag'];

    protected $primarykey = 'id';
}
