<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DirectoryWorkflow extends Model
{
    use HasFactory;
    protected $table = 'directory_workflows';
    protected $fillable = [
        'directory_id', 'workflow_switch', 'workflow_list'
    ];
}
