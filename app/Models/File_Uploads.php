<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File_Uploads extends Model
{
    use HasFactory;

    public $table = 'file_uploads';

    protected $fillable = ['nama_file','path_file','member','id_directory','is_deleted','deleted_at','deleted_by', 'title', 'description'];

    protected $primarykey = 'id';

    public function version()
    {
        return $this->hasMany(Version_Log::class, 'id_file');
    }

    public function properties()
    {
        return $this->hasOne(Properties::class, 'id_file');
    }

    public function lastVersion()
    {
        return $this->hasOne(Version_Log::class, 'id_file')->latest('id');
    }

    public function directory()
    {
        return $this->belongsTo(Directory::class, 'id_directory');
    }

    public function relations()
    {
        return $this->hasMany(DocumentRelation::class, 'primary_document');
    }
}
