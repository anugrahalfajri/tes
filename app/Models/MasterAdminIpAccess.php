<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterAdminIpAccess extends Model
{
    use HasFactory;
    protected $table = 'master_admin_ip_access';
}
