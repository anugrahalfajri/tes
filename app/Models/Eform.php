<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Eform extends Model
{
    use HasFactory, SoftDeletes;

    public $table = 'eform';

    protected $guarded;

    public function eform_detail()
    {
        return $this->hasMany(EformDetail::class, 'id_eform');
    }

    public function eform_generate()
    {
        return $this->hasOne(EformGenerate::class, 'eg_form_id', 'form_id');
    }

    public function eform_submit_count()
    {
        return $this->hasMany(EformSubmitCount::class);
    }

    public function eform_submit()
    {
        return $this->hasMany(EformSubmit::class, 'form_id', 'form_id');
    }

    public function scopeIsIpTracking($query, $form_id)
    {
        $record = $query->where('form_id', $form_id)->first();

        return $record->track_ip ?? false;
    }

    public function scopeIsOTS($query, $form_id)
    {
        $record = $query->where('form_id', $form_id)->first();

        return $record->one_time_submit ?? false;
    }

    public function scopeIsIPOTS($query, $form_id)
    {
        $record = $query->where('form_id', $form_id)->first();

        return $record->one_time_ip;
    }
}
