<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Note_List extends Model
{
    use HasFactory;

    public $table = 'note_list';

    protected $fillable = ['id_file', 'writer', 'notes'];

    protected $primarykey = 'id';
}
