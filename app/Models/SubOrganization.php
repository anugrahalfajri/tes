<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubOrganization extends Model
{
    use HasFactory;
    
    protected $fillable = ['name','description','status','id_organization'];

    /**
     * Get the organizations that owns the sub organizations.
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class, 'id_organization')->withTrashed();
    }
}
