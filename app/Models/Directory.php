<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Directory extends Model
{
    use HasFactory;

    public $table = 'directory';

    protected $fillable = ['parent_id', 'nama_folder'];

    protected $primarykey = 'id';

    public function hasSubDirectory()
    {
        return $this->hasMany(Directory::class, 'parent_id', 'id')->count();
    }

    function child(){
        return $this->hasMany(Directory::class, 'parent_id');
    }
}
