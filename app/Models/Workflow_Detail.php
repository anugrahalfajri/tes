<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Workflow_Detail extends Model
{
    use HasFactory;

    public $table = 'workflow_detail';

    protected $fillable = ['id_file','id_workflow', 'status', 'due_date', 'priority'];

    protected $primarykey = 'id';

    public function workflow_detail_approval()
    {
        return $this->hasMany(WorkflowDetailApproval::class, 'id_workflow_detail');
    }
}
