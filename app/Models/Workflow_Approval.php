<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Workflow_Approval extends Model
{
    use HasFactory;
    
    
    protected $table = 'workflow_approval';

    protected $fillable = ['id_workflow','member','status'];

    protected $primarykey = 'id';
    
    public function workflow()
    {
        return $this->belongsTo(Workflow::class, 'id_workflow');
    }
}
