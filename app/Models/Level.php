<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    use HasFactory;

    public $guarded = [];

    public static function checkLevel($system_name, $level)
    {
        $permission = Permission::where('system_name', $system_name)->first();
        $level = Level::where('id', $level)->first();
        $explode = explode(',', $level->permission ?? '');
        $check = in_array($permission->id, $explode);
        if($check == true){
            return true;
        }else {
            return false;
        }
    }
}
