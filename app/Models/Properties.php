<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Properties extends Model
{
    use HasFactory;

    public $table = 'properties';

    protected $fillable = ['name','title','folder','size','id_file', 'id_version', 'id_status', 'created_by', 'created_at', 'updated_by', 'updated_by', 'permalink'];

    protected $primarykey = 'id';

    public function version()
    {
        return $this->belongsTo(Version_Log::class, 'id_version');
    }
}
