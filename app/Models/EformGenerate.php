<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EformGenerate extends Model
{
    use HasFactory;
    protected $table    = 'eform_generates';
    protected $fillable = [
        'eg_form_id', 'eg_upload_template', 'eg_auto_generate'
    ];
}
