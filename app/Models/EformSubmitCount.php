<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EformSubmitCount extends Model
{
    use HasFactory;

    public $table = 'eform_submit_count';

    protected $guarded;

    public function eform()
    {
        return $this->belongsTo(Eform::class, 'eform_id');
    }
}
