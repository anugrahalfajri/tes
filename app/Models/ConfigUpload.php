<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConfigUpload extends Model
{
    use HasFactory;
    protected $fillable = [
        'allowed_mime_types', 'max_file_size', 'modul_name'
    ];
}
