<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EformSubmit extends Model
{
    use HasFactory;

    public $table = 'eform_submit';

    protected $guarded;

    public function auto_generate()
    {
        return $this->hasOne(EformGenerate::class, 'eg_form_id', 'form_id');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function eform()
    {
        return $this->belongsTo(Eform::class, 'form_id', 'form_id');
    }
}
