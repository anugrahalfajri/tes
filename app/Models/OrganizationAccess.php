<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Permission;

class OrganizationAccess extends Model
{
    use HasFactory;

    protected $guarded = [];


    public static function checkPermission($system_name, $id)
    {
        $user = User::where('id', $id)->first();
        $permission = Permission::where('system_name', $system_name)->first();
        $access = OrganizationAccess::where('id_organization', $user->id_organization)->where('id_permission', $permission->id ?? null)->first();
        if(!empty($access)){
            return true;
        }else {
            return false;
        }
    }
}
