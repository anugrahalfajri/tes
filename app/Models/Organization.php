<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Organization extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['name','description','status','admin'];

    /**
     * Get the sub organizations for the organizations.
     */
    public function sub_organization()
    {
        return $this->hasMany(SubOrganization::class);
    }

    public function admin_user()
    {
        return $this->belongsTo('\App\Models\Master\User', 'admin');
    }
}
