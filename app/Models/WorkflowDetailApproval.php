<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkflowDetailApproval extends Model
{
    use HasFactory;

    public $table = 'workflow_detail_approval';

    protected $fillable = ['id_workflow_detail', 'level', 'status'];

    protected $primarykey = 'id';

    public function workflow_detail()
    {
        return $this->belongsTo(Workflow_Detail::class, 'id_workflow_detail');
    }
}
