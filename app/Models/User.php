<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\VerifyEmailAdmin;
use App\Notifications\VerifyEmailGuest;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    public $table= 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id',
        'level',
        'phone',
        'id_organization',
        'parent',
        'join_date',
        'status',
        'pin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendEmailVerificationNotificationAdmin()
    {
        $this->notify(new VerifyEmailAdmin);
    }

    public function sendEmailVerificationNotificationGuest()
    {
        $this->notify(new VerifyEmailGuest);
    }

    public function role()
    {
        return $this->belongsTo(Roles::class, 'role_id');
    }
}
