<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;
    public $table = 'message';
    protected $fillable = ['text','from','to','reply'];

    public function sent_to()
    {
        return $this->belongsTo(User::class, 'to');
    }
}
