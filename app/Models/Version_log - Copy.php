<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Version_Log extends Model
{
    use HasFactory;

    public $table = 'version_log';
    protected $fillable = [
        'version', 'created_by', 'keterangan', 'id_file'
    ];

    public function uploaded_file()
    {
        return $this->belongsTo(File_Uploads::class, 'id_file');
    }
}
