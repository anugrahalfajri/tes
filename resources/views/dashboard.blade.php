@extends('layouts.app')

@section('content')
 <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <!-- <div class="container-fluid ml-5">
        <a class="has-arrow waves-effect waves-dark" href="#collapse" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">My folders</a>
        <ul aria-expanded="false" id="collapse" class="collapse">
            <li>Test</li>
        </ul>
    </div> -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 col-8 align-self-center">
                    <h3 class="text-themecolor">Home</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                    </ol>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <!-- Row -->

            <div class="row">
            @if(auth()->user()->role->name ?? '' != 'admin organisasi')
              <!-- Column -->
              <div class="col-lg-4 col-md-4">
                  <div class="card card-inverse card-primary">
                      <div class="card-body">
                          <div class="d-flex">
                              <div class="m-r-20 align-self-center">
                                  <h1 class="text-white"><i class="icon-people"></i></h1></div>
                              <div>
                                  <h3 class="card-title">Organizations</h3>
                                  <h6 class="card-subtitle"></h6> 
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-4 align-self-center">
                                  <h2 class="font-light text-white">{{$organizations}}</h2>
                              </div>
                              <div class="col-8 p-t-10 p-b-20 align-self-center">
                                  <div class="usage chartist-chart" style="height:65px"></div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              @endif
              <!-- Column -->
              <!-- Column -->
              <div class="col-lg-4 col-md-4">
                  <div class="card card-inverse card-success">
                      <div class="card-body">
                          <div class="d-flex">
                              <div class="m-r-20 align-self-center">
                                  <h1 class="text-white"><i class="icon-user"></i></h1></div>
                              <div>
                                  <h3 class="card-title">Members</h3>
                                  <h6 class="card-subtitle"></h6> 
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-4 align-self-center">
                                  <h2 class="font-light text-white">{{$members}}</h2>
                              </div>
                              <div class="col-8 p-t-10 p-b-20 text-right">
                                  <div class="spark-count" style="height:65px"></div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <!-- Column -->
              <!-- Column -->
              <div class="col-lg-4 col-md-4">
                <div class="card card-inverse card-danger">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="m-r-20 align-self-center">
                                <h1 class="text-white"><i class="mdi mdi-file"></i></h1></div>
                            <div>
                                <h3 class="card-title">Files</h3>
                                <h6 class="card-subtitle"></h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4 align-self-center">
                                <h2 class="font-light text-white">{{$files}}</h2>
                            </div>
                            <div class="col-8 p-t-10 p-b-20 text-right">
                                <div class="spark-count" style="height:65px"></div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <!-- Column -->
          </div>
            <!-- Row -->
            <!-- ============================================================== -->
            <!-- End PAge Content -->
@endsection
