@extends('layouts.app')

@section('content')
<link href="{{url('plugins/summernote/dist/summernote.css')}}" rel="stylesheet">
<style>
    [type="checkbox"]:not(:checked):disabled + label:before {
        border: 1px solid rgba(0, 0, 0, 0.26);
        background-color: white;
    }
    .margin-top-delete{
        margin-top: 2.1rem !important
    }
    .ui-widget-content {
        display: none !important;
    }
</style>
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">E-form</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{urlWithPrefix('/')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{urlWithPrefix('/eform')}}">E-form</a></li>
                    <li class="breadcrumb-item active">Create</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="d-flex justify-content-between">
                            <h4 class="text-white card-title">E-form - Create</h4>          
                        </div>
                    </div>
                    <div class="card-body">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li><span class="text-danger">{{ $error }}</span></li>
                            @endforeach
                        </ul>
                        <form id="blbl" action="{{urlWithPrefix('/eform/store')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            {{-- <div class="row mb-5">
                                <div class="col-md-12">
                                    <select name="target" id="target" class="form-control" required>
                                        <option value="" selected disabled>Select Target</option>
                                        <option value="login">Private</option>
                                        <option value="public">Public</option>
                                    </select>
                                </div>
                            </div> --}}
                            <div class="row mb-2">
                                <div class="col-md-12">
                                    <input class="form-check-input" type="checkbox" id="track_ip" name="track_ip" value="1" {{ old('track_ip') == "1" ? "checked" : '' }}>
                                    <label class="form-check-label mr-3" for="track_ip">
                                        Track IP 
                                    </label>
                                    <input class="form-check-input" type="checkbox" id="mobile_used" name="mobile_used" value="1" {{ old('mobile_used') == "1" ? "checked" : '' }}>
                                    <label class="form-check-label mr-3" for="mobile_used">
                                        Mobile Used 
                                    </label>
                                    <input class="form-check-input" type="checkbox" id="one_time_submit" name="one_time_submit" value="1" {{ old('one_time_submit') == "1" ? "checked" : '' }}>
                                    <label class="form-check-label mr-3" for="one_time_submit">
                                        One Time Submit 
                                    </label>
                                    <input class="form-check-input" type="checkbox" id="graph_report" name="graph_report" value="1" {{ old('graph_report') == "1" ? "checked" : '' }}>
                                    <label class="form-check-label mr-3" for="graph_report">
                                        Graph Report
                                    </label>
                                    <input class="form-check-input" type="checkbox" id="generate_box" name="eg_auto_generate" value="Y" {{ old('generate_box') == "Y" ? "checked" : '' }}>
                                    <label class="form-check-label mr-3" for="generate_box">
                                        Aktifkan auto generate 
                                    </label>

                                    <input class="form-check-input" type="checkbox" id="paging_form" name="paging_form" value="1" {{ old('paging_form') == "1" ? "checked" : '' }}>
                                    <label class="form-check-label mr-3" for="paging_form">
                                        Paging Form
                                    </label>
                                </div>
                            </div>
                            <br>
                            <div class="row mb-3">
                                <div class="col-md-6 template_upload" style="display: none;">
                                    <label><b>Generate upload</b></label>
                                    <label class="custom-file d-block">
                                        <input type="file" id="file" name="eg_upload_template" class="custom-file-input" accept=".docx" value="{{ old('eg_upload_template') }}">
                                        <span class="custom-file-control"></span>
                                    </label><br>
                                    <a class="btn btn-info" data-toggle="collapse" href="#guide" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    Get started
                                    </a>
                                    <div class="collapse" id="guide">
                                        <div class="card card-body">
                                            <ul style="font-size: 12px;">
                                            <li><i>1. Lihat nomor pertanyaan pada form yang anda buat <b>e.g Question-1</b>. Berarti ini nomor 1</i></li>
                                            <li><i>2. Variabel akan sesuai dengan nomor yang ada pada <b>langkah 1</b></i></li>
                                            <li><i>3. Penulisan Variabel di awali dengan simbol <code>${</code> di ikuti dengan keyword <code>answer_(nomor_pada_langkah_1)</code> dan di akhiri dengan simbol <code>}</code></i></li>
                                            <li><i>4. Sehingga variabel lengkapnya menjadi <code>${answer_1}</code></i></li>
                                            <li><i>5. Khusus untuk pilihan <i>Multiple Answer</i> harus diapit dalam satu block dengan keyword <code>${value}</code> menjadi <code>${answer_1} ${value} ${/answer_1}</code>. Untuk lebih lengkap dapat dilihat pada file contoh.</i></li>
                                            <li><strong>Contoh Penggunaan dapat dilihat pada file berikut ini : <a style="font-size: 14px;" href="{{ url('eform_example_answer.docx') }}">Download Contoh</a></strong></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 paging_col" style="display: none;">
                                    <label><b>Total Page</b></label>
                                    <input type="number" min="1" name="page" id="page" onchange="check_page($(this).val())" class="form-control" placeholder="Total Page" value="{{ old('page') }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-12">
                                    <input type="text" name="title" id="title" class="form-control" placeholder="Judul" value="{{ old('title') }}" required>
                                </div>
                            </div>

                            <div class="container-add-form">
 
                            </div>

                            <br>
                            <div align="left">
                                <button type="button" class="btn btn-primary add-form"><i class="mdi mdi-plus"></i></button>
                            </div>

                            <div class="row mt-5">
                                <div class="col-md-12">
                                    <label for=""><b>Select Target</b></label><br>
                                    <input class="form-check-input" type="radio" id="login" name="target" value="login" {{ @old('target') ? old('target') == "login" ? "checked" : "" : "checked" }}>
                                    <label class="form-check-label mr-3" for="login">
                                        Private
                                    </label>
                                    <input class="form-check-input" type="radio" id="public" name="target" value="public" {{ old('target') == "public" ? "checked" : "" }}>
                                    <label class="form-check-label mr-3" for="public">
                                        Public
                                    </label>
                                </div>
                            </div>

                            <div class="row mt-5">
                                <div class="col-md-12">
                                    <label for=""><b>After submitting form, take these action. </b></label><br>
                                    <input class="form-check-input action_sel" type="radio" id="thanks" name="action" value="thanks_page"  {{ @old('action') ? old('action') == "thanks_page" ? "checked" : "" : "checked" }}>
                                    <label class="form-check-label mr-3" for="thanks">
                                        Thankyou page
                                    </label>
                                    <input class="form-check-input action_sel" type="radio" id="redirect" name="action" value="redirect_to"  {{ old('action') == "redirect_to" ? "checked" : "" }}>
                                    <label class="form-check-label mr-3" for="redirect">
                                        Redirect to url
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="action_url" id="action_url" class="form-control"  style="display:  {{ old('action') == "thanks_page" ? "none" : 'block' }};" placeholder="http://example.com/thankyou" value="{{ old('action_url') }}">
                                </div>
                            </div>

                            <div class="row mt-lg-5" style="display: {{ old('action') == "thanks_page" ? "block" : 'none' }};" id="thankyou-row">
                                <div class="col-lg-6 mt-md-4 mt-sm-4">
                                    <label><b>Header Page</b></label>
                                    <textarea id="header" name="header">{{ old('header') ?? view('eform.default_header') }}</textarea>
                                </div>
                                <div class="col-lg-6 mt-md-4 mt-sm-4">
                                    <label><b>Description Page</b></label>
                                    <textarea id="description" name="description">{{ old('description') ?? view('eform.default_desc') }}</textarea>
                                </div>
                            </div>

                            <br>
                            <div align="right">
                                <button type="submit" class="btn btn-primary">Submit</a>
                                <button type="reset" class="btn btn-danger  ml-5">Reset</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{url('plugins/summernote/dist/summernote.min.js')}}"></script>
<script>
    $('#header').summernote({
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear', 'fontname', 'fontsize', 'color', 'forecolor', 'backcolor']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['para', ['ul', 'ol', 'paragraph', 'style']],
            ['height', ['height']],
            ['insert', ['picture', 'link', 'video', 'hr']],
            ['misc', ['undo', 'redo', 'fullscreen']],
        ],
        placeholder: 'Header Page',
        tabsize: 2,
        height: 350,
        callbacks: {
            onImageUpload : function(files, editor, welEditable) {
                for(var i = files.length - 1; i >= 0; i--) {
                        sendFile(files[i], this);
                }
            }
        }
    });

    $('#description').summernote({
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear', 'fontname', 'fontsize', 'color', 'forecolor', 'backcolor']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['para', ['ul', 'ol', 'paragraph', 'style']],
            ['height', ['height']],
            ['insert', ['picture', 'link', 'video', 'hr']],
            ['misc', ['undo', 'redo', 'fullscreen']],
        ],
        placeholder: 'Description Page',
        tabsize: 2,
        height: 350,
        callbacks: {
            onImageUpload : function(files, editor, welEditable) {

                for(var i = files.length - 1; i >= 0; i--) {
                        sendFile(files[i], this);
                }
            }
        }
    });

    function check_page(value) {
        if (isNumber(value.trim()) == true) {
            $('.page-num').each (function () {
                var valueCurrent = $(this).val();
                if (parseInt(valueCurrent) > parseInt(value)) {
                    $(this).val(value)
                }
            });
            $('.page-num').attr('max', value)
        }
        if (value.trim() < 1) {
            $('#page').val(1)
        }
    }

    function set_max(elem) {
        var value = elem.val()
        var valueMax = $(`#page`).val() 
        if (parseInt(value) > parseInt(valueMax)) {
            elem.val(valueMax) 
        } else if (parseInt(value) < 1){
            elem.val(1) 
        }
        $(`.page-num`).attr(`max`, valueMax)
    }

    function isNumber(evt) {
    var pattern = /^\d+\.?\d*$/
    return pattern.test(evt)
    }

    function sendFile(file, el) {
        var form_data = new FormData();
        form_data.append('file', file);
        $.ajax({
            data: form_data,
            type: "POST",
            url: '{{ route("eform.editor.image") }}',
            cache: false,
            contentType: false,
            processData: false,
            headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function(url) {
                $(el).summernote('editor.insertImage', url);
            }
        });
    }

    var arrayRowForm = [];
    var count = 0;
    inputChange();

    $('.action_sel').change(function(){
        inputChange();
    });

    function inputChange(){
        var sel = $('input[name="action"]:checked').val();

        if(sel == "redirect_to"){
            $('#action_url').show();
            $('#thankyou-row').hide();
        }else{
            $('#thankyou-row').show();
            $('#action_url').hide();
            $('#action_url').val("");
        }
    }

    $('.add-form').on('click', function () {    
        arrayRowForm.push(count);
        var isPaging = $('#paging_form').is(":checked") ? 'block' : 'none';
        $('.container-add-form').append('<div class="row row-form mb-5">'
                                        + '<input type="hidden" name="question_no[]" value="'+parseInt(count+1)+'">'
                                        +  '<div class="col-lg-4 col-md-6">'
                                        +      '<label><b>Judul</b></label>'
                                        +      '<input type="text" name="question[]" id="question" class="form-control question-form" placeholder="Judul">'
                                        +  '</div>'
                                        +  '<div class="col-lg-3 col-md-6">'
                                        +      '<label><b>Description</b></label>'
                                        +      '<textarea name="short_desc[]" class="form-control description-form" col="5" row="3"></textarea>'
                                        +  '</div>'
                                        +  '<div class="col-lg-2 col-md-6 col-sm-6">'
                                        +      '<label><b>Answer Mode</b></label>'
                                        +      '<select name="answer_type[]" id="answer_type'+ count +'" class="form-control answer-type-form">'
                                        +          '<option value="" selected disabled>Answer Mode</option>'
                                        +          '<option value="text">Text</option>'
                                        +          '<option value="number">Number</option>'
                                        +          '<option value="text_area">Text Area</option>'
                                        +          '<option value="radio_button">Radio Button</option>'
                                        +          '<option value="check_box">Check Box</option>'
                                        +          '<option value="select_box">Select Box</option>'
                                        +          '<option value="url">URL</option>'
                                        +          '<option value="date">Date</option>'
                                        +          '<option value="time">Time</option>'
                                        +          '<option value="image">Image</option>'
                                        +          '<option value="file">File</option>'
                                        +          '<option value="multi">Multiple Answers</option>'
                                        +      '</select>'
                                        +  '</div>'
                                        +  '<div class="col-lg-1 col-md-6 col-sm-6 page_num" style="display:'+isPaging+';">'
                                        +      '<label><b>Page</b></label>'
                                        +      '<input type="number" name="page_num[]" class="form-control page-num" onchange="set_max($(this))" min="1" value="1">'
                                        +  '</div>'
                                        +  '<div class="col-lg-1 col-md-6 col-sm-6">'
                                        +  '<label><b>Mandatory</b></label>'
                                        +  '<select name="is_mandatory[]" class="form-control is-mandatory-form" required>'
                                        +          '<option value="1">Yes</option>'
                                        +          '<option value="0" selected>No</option>'
                                        +  '</select>'
                                        +   '</div>'
                                        +  '<div class="col-lg-2 col-md-6 col-sm-6 margin-top-delete mt-sm-5">'
                                        +           '<button type="button" class="btn btn-danger remove-form"><i class="mdi mdi-delete"></i></button>&nbsp;'
                                        +           '<button type="button" class="btn btn-info copy-form"><i class="mdi mdi-content-copy"></i></button>'
                                        +   '</div>'
                                        +  '<div class="col-lg-12 form-dynamic" id="form-dynamic'+ count +'"></div>'
                                        +'</div>');
        
        count++;

        $.each(arrayRowForm, function (i, val) {        
            $('#answer_type'+val).on('change', function () {
                if ($(this).val() == 'radio_button') {
                    $('#form-dynamic'+val).html('<div class="row-rb'+val+'">'
                                                +  '<div class="row-rb-append'+val+'">' 
                                                +     '<input class="form-check-input" type="radio" id="radio" disabled>'
                                                +     '<label class="form-check-label" for="radio">'
                                                +         '<input type="text" name="answer_choice_rb'+val+'[]" class="form-control answer-choice-rb-form" maxlength="255"/>'
                                                +     '</label>'
                                                +     '<button type="button" class="btn btn-sm btn-success ml-2 add-rb'+val+'"><i class="mdi mdi-plus"></i></button>'
                                                +  '</div>'
                                                +'</div><br>')

                    $(document).on('click', '.add-rb'+val, function () {
                        $('.row-rb'+val).append('<div class="row-rb-append'+val+'">'
                                            +  '<input class="form-check-input" type="radio" id="radio" disabled>'
                                            +  '<label class="form-check-label mt-3" for="radio">'
                                            +     '<input type="text" name="answer_choice_rb'+val+'[]" class="form-control answer-choice-rb-form"  maxlength="255"/>'
                                            +  '</label>'
                                            +  '<button type="button" class="btn btn-sm btn-success ml-2 add-rb'+val+'"><i class="mdi mdi-plus"></i></button>'
                                            +  '<button type="button" class="btn btn-sm btn-danger ml-2 remove-rb'+val+'"><i class="mdi mdi-minus"></i></button>'
                                            +'</div>')
                    })

                    $(document).on('click', '.remove-rb'+val, function () {
                        $('.row-rb-append'+val).last().remove();
                    })

                } else if ($(this).val() == 'check_box') {
                    $('#form-dynamic'+val).html('<div class="row-cb'+val+'">'
                                                +  '<div class="row-cb-append">'  
                                                +     '<input class="form-check-input" type="checkbox" id="checkbox" disabled>'
                                                +     '<label class="form-check-label" for="checkbox">'
                                                +         '<input type="text" name="answer_choice_cb'+val+'[]"  class="form-control answer-choice-cb-form" maxlength="255"/>'
                                                +     '</label>'
                                                +     '<button type="button" class="btn btn-sm btn-success ml-2 add-cb'+val+'"><i class="mdi mdi-plus"></i></button>'
                                                +  '</div>'
                                                +'</div><br>')

                    $(document).on('click', '.add-cb'+val, function () {
                        $('.row-cb'+val).append('<div class="row-cb-append'+val+'">'
                                            +  '<input class="form-check-input" type="checkbox" id="checkbox" disabled>'
                                            +  '<label class="form-check-label mt-3" for="checkbox">'
                                            +     '<input type="text" name="answer_choice_cb'+val+'[]" class="form-control answer-choice-cb-form" maxlength="255"/>'
                                            +  '</label>'
                                            +  '<button type="button" class="btn btn-sm btn-success ml-2 add-cb'+val+'"><i class="mdi mdi-plus"></i></button>'
                                            +  '<button type="button" class="btn btn-sm btn-danger ml-2 remove-cb'+val+'"><i class="mdi mdi-minus"></i></button>'
                                            +'</div>')
                    })

                    $(document).on('click', '.remove-cb'+val, function () {
                        $('.row-cb-append'+val).last().remove();
                    })

                } else if ($(this).val() == 'select_box') {
                    var number = 1;

                    $('#form-dynamic'+val).html('<div class="row-sb'+val+'">'
                                                +  '<div class="row-sb-append">' 
                                                +     number
                                                +     '<label class="form-check-label">'
                                                +       '<input type="text" name="answer_choice_sb'+val+'[]" class="form-control answer-choice-sb-form" maxlength="255"/>'
                                                +     '</label>'
                                                +     '<button type="button" class="btn btn-sm btn-success ml-2 add-sb'+val+'"><i class="mdi mdi-plus"></i></button>'
                                                +  '</div>'
                                                +'</div><br>')

                    $(document).on('click', '.add-sb'+val, function () {
                        number++;
                        $('.row-sb'+val).append('<div class="row-sb-append'+val+'">' 
                                            +     number
                                            +     '<label class="form-check-label mt-3">'
                                            +       '<input type="text" name="answer_choice_sb'+val+'[]" class="form-control answer-choice-sb-form" maxlength="255"/>'
                                            +     '</label>'
                                            +     '<button type="button" class="btn btn-sm btn-success ml-2 add-sb'+val+'"><i class="mdi mdi-plus"></i></button>'
                                            +     '<button type="button" class="btn btn-sm btn-danger ml-2 remove-sb'+val+'"><i class="mdi mdi-minus"></i></button>'
                                            +  '</div>'
                                            +'</div>')
                    })

                    $(document).on('click', '.remove-sb'+val, function () {
                        $('.row-sb-append'+val).last().remove();
                    })

                } else if ($(this).val() == 'multi') {
                    $('#form-dynamic'+val).html('<div class="row-sb'+val+'">'
                                                +  '<div class="row-sb-append">' 
                                                +     'Jumlah Kolom'
                                                +     '<label class="form-check-label">'
                                                +       '<input type="number" min="1" value="1" name="kolom'+val+'" class="form-control"/>'
                                                +     '</label>'
                                                +  '</div>'
                                                +'</div><br>')
                } else {
                    $('#form-dynamic'+val).empty();
                }
            })
        })
    })

    $(document).on('click', '.remove-form', function () {
        $(this).parent().closest('.row-form').remove();
    });

    $(document).on('input', '.question-form', function () {
        $(this).parent().closest('.row-form').find('.copy-form').attr('data-question', $(this).val());
    })
    
    $(document).on('input', '.description-form', function () {
        $(this).parent().closest('.row-form').find('.copy-form').attr('data-description', $(this).val());
    })

    $(document).on('change', '.answer-type-form', function () {
        $(this).parent().closest('.row-form').find('.copy-form').attr('data-answer-type', $(this).val());
    })
    
    $(document).on('change', '.is-mandatory-form', function () {
        $(this).parent().closest('.row-form').find('.copy-form').attr('data-is-mandatory', $(this).val());
    })

    $(document).on('click', '.copy-form', function () {
        var thisRow = $(this).parent().closest('.row-form').clone();
        var counter = $('.form-dynamic').length;
        var question = $(this).attr('data-question');
        var description = $(this).attr('data-description');
        var answer = $(this).attr('data-answer-type');
        var mandatory = $(this).attr('data-is-mandatory')

        $('.container-add-form').append(thisRow);
        thisRow.find('input:hidden').val(parseInt(count+1));
        thisRow.find('.form-dynamic').attr('id', 'form-dynamic'+parseInt(counter));
        thisRow.find('.answer-choice-rb-form').attr('name', 'answer_choice_rb'+parseInt(counter)+'[]');
        thisRow.find('.answer-choice-cb-form').attr('name', 'answer_choice_cb'+parseInt(counter)+'[]');
        thisRow.find('.answer-choice-sb-form').attr('name', 'answer_choice_sb'+parseInt(counter)+'[]');
        thisRow.find('.question-form').val(question);
        thisRow.find('.description-form').text(description);
        thisRow.find('.answer-type-form').val(answer);
        if (mandatory == undefined) {
            thisRow.find('.is-mandatory-form').val("0").change();
        } else {
            thisRow.find('.is-mandatory-form').val(mandatory);
        }

        count++;
    });

    $("#generate_box").change(function() {
        if(this.checked) {
           $('.template_upload').show();
        }else{
            $('.template_upload').hide();
        }
    });

    $("#paging_form").change(function() {
        if(this.checked) {
           $('.paging_col').show();
           $('.page_num').show();
        }else{
            $('.paging_col').hide();
            $('.page_num').hide();
        }
    });

    function getMaxValueForRequest() {
        var requester = $(".page_num_col");
        var currentItemAmount = parseInt(document.getElementById("page").value);
        requester.attr('max', currentItemAmount);
    }

    $(".page_num_col").change(function(){
        var currentItemAmount = parseInt(document.getElementById("page").value);
        var value = $(this).val();

        console.log("value")
        if(value > currentItemAmount){
            $(this).val(currentItemAmount);
        }

    });
</script>
@endsection