@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">E-form</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{urlWithPrefix('/')}}">Home</a></li>
                    <li class="breadcrumb-item active">E-form</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')

        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="d-flex justify-content-between">
                            <h4 class="text-white card-title">E-form</h4>
                            <div class="btn-group" align="right">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Add New
                                </button>
                                <div class="dropdown-menu">
                                  <a class="dropdown-item" href="{{urlWithPrefix('/eform/create')}}">Add E-form</a>
                                  <div class="dropdown-divider"></div>
                                  <a class="dropdown-item" href="javascript:;"  data-toggle="modal" data-target="#upload-eform-modal">Import E-Form</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li><span class="text-danger">{{ $error }}</span></li>
                            @endforeach
                        </ul>
                        
                        <div class="table-responsive">
                            <table id="eform_table" class="table table-striped table-responsive-lg" style="width:100%">
                                <thead>
                                    <tr>
                                        @if (auth()->user()->role->name == 'user')
                                            <th width="10%">Form ID</th>
                                            <th width="20%">Title</th>
                                            <th width="15%">Created By</th>
                                            <th width="15%">Created At</th>
                                            <th width="40%">Action</th>
                                        @else
                                            <th width="10%">Form ID</th>
                                            <th width="30%">Title</th>
                                            <th width="15%">Created By</th>
                                            <th width="15%">Created At</th>
                                            <th width="30%">Action</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($eforms))
                                    @foreach($eforms as $row)
                                    {{-- {{ print_r($row->toArray()) }}
                                    @php
                                     exit;   
                                    @endphp --}}
                                    <tr>
                                        <td>{{$row->form_id}}</td>
                                        <td>{{$row->title}}</td>
                                        <td>{{ $row->created_by ?? '-' }}</td>
                                        <td>{{$row->created_at}}</td>
                                        <td>
                                            <button type="button" class="btn btn-sm  btn-danger" onclick="deleting({{ $row->form_id }}, '{{ isset($row->deleted_at) ? 'active' : 'inactive' }}')" data-toggle="button" aria-pressed="false">
                                                <i class="ti-check text{{ isset($row->deleted_at) ? '-active' : '' }}" aria-hidden="true"></i>
                                                <span class="text{{ isset($row->deleted_at) ? '-active' : '' }}">Active</span>
                                                <i class="ti-close text{{ isset($row->deleted_at) ? '' : '-active' }}" aria-hidden="true"></i>
                                                <span class="text{{ isset($row->deleted_at) ? '' : '-active' }}">Inactive</span>
                                            </button>
                                            <button type="button" class="btn btn-secondary btn-sm btn-copy" data-url="{{$row->target == 'login' ? urlWithPrefix('/eform/show', $row->slug) : urlWithPrefix('/eform/show/public', $row->slug)}}">Copy Link</button>&nbsp;
                                            <a href="{{urlWithPrefix('/eform/show', $row->slug)}}" class="btn btn-primary btn-sm">Show</a>&nbsp;
                                            <a href="{{  $row->eform_submit_count > 0 ? 'javascript:;' : urlWithPrefix('/eform/edit', $row->form_id)}}" class="btn btn-warning btn-sm {{ $row->eform_submit_count > 0 ? 'disabled' : '' }}">Edit</a>&nbsp;
                                            <a href="{{urlWithPrefix('/eform/answer', $row->form_id)}}" class="btn btn-success btn-sm">List</a>&nbsp;
                                            <a href="{{ urlWithPrefix('/eform/duplicate', $row->slug) }}" class="btn btn-success btn-sm" style="background-color: #28a745; border-color: #28a745;">Duplicate</a>&nbsp;
                                            {{-- <a href="{{ route(routePrefix().'eform.generate', $row->form_id) }}" class="btn btn-secondary btn-sm">Generate</a>&nbsp; --}}
                                            @isset ($row->graph_report)
                                                <a href="{{urlWithPrefix('/eform/dashboard', $row->form_id)}}" class="btn btn-dark btn-sm">Output</a>&nbsp;
                                            @endisset
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="upload-eform-modal" tabindex="-1" role="dialog" aria-labelledby="XlModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Import File</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <form action="{{ route(routePrefix().'eform.import') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Title:</label>
                    <input type="text" class="form-control" name="title" required>
                </div>
                <div class="form-group">
                    <label for="message-text" class="col-form-label">Target:</label>
                    <select name="target" id="target" class="form-control" required>
                        <option value="" selected disabled>Select Target</option>
                        <option value="login">Private</option>
                        <option value="public">Public</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="message-text" class="col-form-label">Tipe Import:</label>
                    <select name="source" id="source" onchange="changeType()" class="form-control" required>
                        <option value="" selected disabled>Select Type</option>
                        <option value="word">Word</option>
                        <option value="excel">Excel</option>
                        <option value="txt">Plain Text</option>
                        <option value="csv">Csv</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="message-text" class="col-form-label">File:</label>
                    <input type="file" name="eform_file" id="eform_file" required accept=".doc,.docx">
                </div>
                <hr>
                <a class="btn btn-info" data-toggle="collapse" href="#guide" role="button" aria-expanded="false" aria-controls="collapseExample">
                Get started
                </a><br><br>
                <div class="collapse" id="guide">
                    <div class="card card-body">
                      <ul style="font-size: 12px;">
                        <li><i>1. Setiap kali membuat kolom <b>"pertanyaan"</b> baru di mulai dengan simbol <code>#</code></i></li>
                        <li><i>2. Kalimat pertanyaan/label di akhiri dengan simbol <code>=</code> kemudian dilanjutkan dengan tipe inputan</i></li>
                        <li><i>3. Terdapat beberapa tipe inputan diantaranya <code>text</code>, <code>text_area</code>, <code>radio_button</code>,
                            <code>check_box</code>, <code>select_box</code>, <code>url</code>, <code>date</code>, <code>time</code>, <code>image</code> dan <code>file</code> </i></li>
                        <li><i>4. Untuk tipe <code>radio_button</code>, <code>check_box</code>, dan <code>select_box</code> terdapat option yang harus didefinisikan. Option tersebut dapat di definisikan dengan awalan simbol <code>:</code> setelah tipe inputan dan tanpa spasi.</i></li>
                        <li><i>5. Masing-masing <b>"pertanyaan"</b> dapat dipisahkan dengan enter.</i></li>
                        <li><strong>Contoh Penggunaan dapat dilihat pada file berikut ini : <br>
                             <a style="font-size: 14px;" href="{{ url('example_eform.docx') }}">Download Contoh Word</a><br>
                             <a style="font-size: 14px;" href="{{ url('Import_eform_xlsx.xlsx') }}">Download Contoh Excel</a><br>
                            </strong>
                        </li>

                      </ul>
                    </div>
                </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    $('#eform_table').DataTable({
        order: [[3, 'desc']],
    });

    $(".btn-copy").click(function (e) {
        e.preventDefault();
        var copyText = $(this).attr('data-url');
        var el = $('<input style="position: absolute; bottom: -120%" type="text" value="'+copyText+'"/>').appendTo('body'); 
        el[0].select();
        document.execCommand("copy");
        el.remove();
        Swal.fire('Copy to clipboard')
    });

    function deleting(formid, status){
        if (confirm('Are you sure?')) {
            $.ajax({
                type: "POST",
                url: "{{ route('eform.inactive') }}",
                data: {form_id: formid, status:status},
                headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                success: function (response) {
                    console.log("active/inactive successfully");
                    window.location.reload();
                }, error: function(data){
                    alert("terjadi kesalahan saat active/inactive eform.")
                }
            });
        }else{
            window.location.reload();
        }
    }

    function changeType(){
        const sourceType = $('#source').find(":selected").val();
        const mappingAllowedTypes = {
            word: '.doc, .docx',
            excel: '.xlsx, .xls',
            txt: '.txt',
            csv: '.csv'
        }
        $('#eform_file').attr("accept", mappingAllowedTypes[sourceType])
    }
</script>
@endsection