@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">E-form</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{urlWithPrefix('/')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{urlWithPrefix('/eform')}}">E-form</a></li>
                    <li class="breadcrumb-item active">Show</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna {{ $isPaging ? 'wizard-content' : '' }}">
                        <div class="d-flex justify-content-between">
                            <h4 class="text-white card-title">{{$eform[0]->title}}</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fa fa-ban"></i> incomplete submission</h5>
                            <ul>
                                    <li><span class="text-danger">{{ $errors->first() }}</span></li>
                            </ul>
                        </div>
                        @endif

                        <div class="btn-group pull-right" align="right">
                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Bulk Insert
                            </button>
                            <div class="dropdown-menu">
                              <a class="dropdown-item" href="{{urlWithPrefix('/documents/eform/answer_import.xlsx')}}">Download Template</a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="javascript:;"  data-toggle="modal" data-target="#upload-eform-modal">Import</a>
                            </div>
                        </div>
                        <br><br>
                        @include('include.material.alert')
                        <br>
                        
                        <form action="{{urlWithPrefix('/eform/submit')}}" class="form-wizard-submit {{ $isPaging ? 'validation-wizard vertical wizard-circle' : '' }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @if ($isPaging)
                                @foreach($eform as $page_number => $ef)
                                <h6>Page {{ $page_number }}</h6>
                                <section>
                                    @foreach ($ef as $item)
                                        <div class="row">
                                            <input type="hidden" name="form_id" value="{{$item->form_id}}">
                                            <input type="hidden" name="directory_id" value="{{$item->id_directory}}">

                                            <div class="col-md-5 {{ $item->is_mandatory ? 'mandatory' : '' }}"><b>{{ $item->question_no }}</b>. {{ $item->question }}</div>
                                            <div class="col-md-7">
                                            @if ($item->answer_type == 'text')
                                                <input type="text" class="form-control" name="answer_{{$item->question_no}}" {{ $item->is_mandatory ? 'required' : '' }}>
                                            @elseif ($item->answer_type == 'number')
                                                <input class="form-control onlyNumber" name="answer_{{$item->question_no}}" {{ $item->is_mandatory ? 'required' : '' }}>
                                            @elseif ($item->answer_type == 'text_area')
                                                <textarea class="form-control" cols="30" rows="5" name="answer_{{$item->question_no}}" {{ $item->is_mandatory ? 'required' : '' }}></textarea>
                                            @elseif ($item->answer_type == 'image')
                                                <input type="file" name="answer_{{ $item->question_no }}" class="form-control" accept="image/*" {{ $item->is_mandatory ? 'required' : '' }}>
                                            @elseif ($item->answer_type == 'file')
                                                <input type="file" name="answer_{{ $item->question_no }}" class="form-control" accept=".pdf,.doc,.docx,.xls,.xlsx,.ppt,.odt,.rar,.zip,.tar.gz" {{ $item->is_mandatory ? 'required' : '' }}>
                                            @elseif ($item->answer_type == 'url')
                                                <input type="url" class="form-control" name="answer_{{$item->question_no}}" {{ $item->is_mandatory ? 'required' : '' }}>
                                            @elseif ($item->answer_type == 'date')
                                                <input type="date" class="form-control" name="answer_{{$item->question_no}}" {{ $item->is_mandatory ? 'required' : '' }}>
                                            @elseif ($item->answer_type == 'time')
                                                <input type="time" class="form-control" name="answer_{{$item->question_no}}" {{ $item->is_mandatory ? 'required' : '' }}>
                                            @elseif ($item->answer_type == 'radio_button')
                                                @foreach ($item->eform_detail as $value)
                                                    <input class="form-check-input" type="radio" id="{{$value->answer_choice.'_'.$index}}" name="answer_{{$item->question_no}}" value="{{$value->answer_choice}}" {{ old('answer_'.$item->question_no) == $value->answer_choice ? 'checked' : '' }}>
                                                    <label class="form-check-label mr-3" for="{{$value->answer_choice.'_'.$index}}">
                                                        {{$value->answer_choice}}
                                                    </label>
                                                @endforeach
                                            @elseif ($item->answer_type == 'check_box')
                                                @foreach ($item->eform_detail as $value)
                                                    <input class="form-check-input" type="checkbox" id="{{$value->answer_choice.'_'.$index}}" name="answer_{{$item->question_no}}[]" value="{{$value->answer_choice}}">
                                                    <label class="form-check-label mr-3" for="{{$value->answer_choice.'_'.$index}}">
                                                        {{$value->answer_choice}}
                                                    </label>
                                                @endforeach
                                            @elseif ($item->answer_type == 'multi')
                                                @php
                                                    $kolom = $item->eform_detail->first()->answer_choice ?? 1;   
                                                @endphp
                                                <div class="row-sb-{{$item->question_no}}">
                                                    <div class="row-sb-append-1">
                                                        @for ($i = 0; $i < $kolom; $i++)
                                                            <label class="form-check-label">
                                                            <input type="text" name="answer_{{$item->question_no}}[][value]" class="form-control" {{ $item->is_mandatory ? 'required' : '' }}/>
                                                            </label>
                                                        @endfor
                                                    <button type="button" class="btn btn-sm btn-success ml-2 add-sb" data-number="{{$item->question_no}}" data-kolom="{{ $kolom }}"><i class="mdi mdi-plus"></i></button>
                                                    </div>
                                                </div><br>
                                            @else
                                                <select class="form-control" name="answer_{{$item->question_no}}" {{ $item->is_mandatory ? 'required' : '' }}>
                                                    @foreach ($item->eform_detail as $value)
                                                        <option value="{{$value->answer_choice}}">{{$value->answer_choice}}</option>
                                                    @endforeach
                                                </select>
                                            @endif
                                            <small class="form-control-feedback"> {{$item->short_desc ?? ''}} </small>
                                            </div>
                                        </div>
                                        <br>
                                    @endforeach
                                </section>
                                @endforeach
                            @else
                                @foreach ($eform as $index => $item)
                                    <div class="row">
                                        <input type="hidden" name="form_id" value="{{$item->form_id}}">
                                        <input type="hidden" name="directory_id" value="{{$item->id_directory}}">
                                        <input type="hidden" name="is_mandatory[]" value="answer_{{$item->question_no}}">

                                        <div class="col-md-5 {{ $item->is_mandatory ? 'mandatory' : '' }}"><b>{{ $index + 1 }}</b>. {{ $item->question }}</div>
                                        <div class="col-md-7">
                                        @if ($item->answer_type == 'text')
                                            <input type="text" class="form-control" name="answer_{{$item->question_no}}" {{ $item->is_mandatory ? 'required' : '' }}>
                                        @elseif ($item->answer_type == 'number')
                                            <input type="number" class="form-control onlyNumber" name="answer_{{$item->question_no}}" {{ $item->is_mandatory ? 'required' : '' }}>
                                        @elseif ($item->answer_type == 'text_area')
                                            <textarea class="form-control" cols="30" rows="5" name="answer_{{$item->question_no}}" {{ $item->is_mandatory ? 'required' : '' }}></textarea>
                                        @elseif ($item->answer_type == 'image')
                                            <input type="file" name="answer_{{ $item->question_no }}" class="form-control" accept="image/*" {{ $item->is_mandatory ? 'required' : '' }}>
                                        @elseif ($item->answer_type == 'file')
                                            <input type="file" name="answer_{{ $item->question_no }}" class="form-control" accept=".pdf,.doc,.docx,.xls,.xlsx,.ppt,.odt,.rar,.zip,.tar.gz" {{ $item->is_mandatory ? 'required' : '' }}>
                                        @elseif ($item->answer_type == 'url')
                                            <input type="url" class="form-control" name="answer_{{$item->question_no}}" {{ $item->is_mandatory ? 'required' : '' }}>
                                        @elseif ($item->answer_type == 'date')
                                            <input type="date" class="form-control" name="answer_{{$item->question_no}}" {{ $item->is_mandatory ? 'required' : '' }}>
                                        @elseif ($item->answer_type == 'time')
                                            <input type="time" class="form-control" name="answer_{{$item->question_no}}" {{ $item->is_mandatory ? 'required' : '' }}>
                                        @elseif ($item->answer_type == 'radio_button')
                                        <div class="form-group">
                                            <div class="form-check">
                                            @foreach ($item->eform_detail as $key => $value)
                                                <label class="custom-control custom-radio">
                                                    <input id="{{$value->answer_choice.'_'.$index}}" name="answer_{{$item->question_no}}" type="radio" value="{{$value->answer_choice}}" class="custom-control-input" {{ old('answer_'.$item->question_no) == $value->answer_choice ? 'checked' : '' }}>
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">{{$value->answer_choice}}</span>
                                                </label>
                                            @endforeach
                                            </div>
                                        </div>
                                        @elseif ($item->answer_type == 'check_box')
                                        <div class="form-group">
                                            @foreach ($item->eform_detail as $value)
                                                <div class="checkbox checkbox-success">
                                                    <input type="checkbox" id="{{$value->answer_choice.'_'.$index}}" name="answer_{{$item->question_no}}[]" value="{{$value->answer_choice}}">
                                                    <label for="{{$value->answer_choice.'_'.$index}}">
                                                        {{$value->answer_choice}}
                                                    </label>
                                                </div><br>
                                            @endforeach
                                        </div>
                                        @elseif ($item->answer_type == 'multi')
                                            @php
                                                $kolom = $item->eform_detail->first()->answer_choice ?? 1;   
                                            @endphp
                                            <div class="row-sb-{{$item->question_no}}">
                                                <div class="row-sb-append-1">
                                                    @for ($i = 0; $i < $kolom; $i++)
                                                        <label class="form-check-label">
                                                        <input type="text" name="answer_{{$item->question_no}}[][value]" class="form-control" {{ $item->is_mandatory ? 'required' : '' }}/>
                                                        </label>
                                                    @endfor
                                                <button type="button" class="btn btn-sm btn-success ml-2 add-sb" data-number="{{$item->question_no}}" data-kolom="{{ $kolom }}"><i class="mdi mdi-plus"></i></button>
                                                </div>
                                            </div><br>
                                        @else
                                            <select class="form-control" name="answer_{{$item->question_no}}" {{ $item->is_mandatory ? 'required' : '' }}>
                                                @foreach ($item->eform_detail as $value)
                                                    <option value="{{$value->answer_choice}}">{{$value->answer_choice}}</option>
                                                @endforeach
                                            </select>
                                        @endif
                                        <small class="form-control-feedback"> {{$item->short_desc ?? ''}} </small>
                                        </div>
                                    </div>
                                    <br>
                                @endforeach
                                <div align="right">
                                    <button type="submit" class="btn btn-primary">Submit</a>
                                    <button type="reset" class="btn btn-danger  ml-5">Reset</a>
                                </div>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<style>
.mandatory:after {
    color:red;
    content: ' *';
}
.input-error{
    outline: 1px solid red;
}
.steps {
    width: 20% !important;
}
.steps .done a{
    background: #00897b !important;
    opacity: 0.5;
}
.steps .current a{
    background: #00897b !important
}
.content{
    background: #fff !important;
    border-left: 2px solid #f8f8f8;
}
</style>
<link href="{{url('material/wizard/steps.css')}}" rel="stylesheet">
<link href="{{url('material/wizard/jquery.steps.css')}}" rel="stylesheet">

@section('scripts')
<script src="{{url('material/wizard/jquery.steps.min.js')}}"></script>
<script src="{{url('material/wizard/jquery.validate.min.js')}}"></script>
{{-- <script src="{{url('material/wizard/steps.js')}}"></script> --}}

<script>
var number = 1;
$(document).on('click', '.add-sb', function () {
    var no_question = $(this).data('number');
    var kolom = $(this).data('kolom');

    number++;
    var element = `<div class="row-sb-append-${number}">`;
        for (let i = 0; i < kolom; i++) {
            element += `<label class="form-check-label mt-3">
                            <input type="text" name="answer_${no_question}[][value]" class="form-control" {{ $item->is_mandatory ? 'required' : '' }}/>
                        </label>`; 
        }
        element += `<button type="button" class="btn btn-sm btn-danger ml-2 remove-sb" data-number="${number}"><i class="mdi mdi-minus"></i></button></div></div>`;
    $('.row-sb-'+no_question).append(element);
})

$(document).on('click', '.remove-sb', function () {
    var no = $(this).data('number');

    $('.row-sb-append-'+no).remove();
})

$(".onlyNumber").inputFilter(function(value) {
    return jQuery.isNumeric(value)
 }, "Must be a Number");

var form = $(".validation-wizard").show();

$(".validation-wizard").steps({
    headerTag: "h6"
    , bodyTag: "section"
    , transitionEffect: "fade"
    , titleTemplate: '<span class="step">#index#</span> #title#'
    , labels: {
        finish: "Submit"
    }
    , onStepChanging: function (event, currentIndex, newIndex) {
        return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
    }
    , onFinishing: function (event, currentIndex) {
        return form.validate().settings.ignore = ":disabled", form.valid()
    }
    , onFinished: function (event, currentIndex) {
        $('.form-wizard-submit').submit();
    }
}), $(".validation-wizard").validate({
    ignore: "input[type=hidden]"
    , errorClass: "text-danger"
    , successClass: "text-success"
    , highlight: function (element, errorClass) {
        $(element).removeClass(errorClass)
    }
    , unhighlight: function (element, errorClass) {
        $(element).removeClass(errorClass)
    }
    , errorPlacement: function (error, element) {
        error.insertAfter(element)
    }
    , rules: {
        @foreach($eform->where('is_mandatory', 1) as $ev)
            answer_{{ $ev->question_no }}: {
                required: !0
            }
        @endforeach
    }
})
</script>
@endsection