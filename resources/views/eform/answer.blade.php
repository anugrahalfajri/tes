@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">E-form</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{urlWithPrefix('/')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{urlWithPrefix('/eform')}}">E-form</a></li>
                    <li class="breadcrumb-item active">List</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body">

                        <div style="display:{{ $isOneTimeSubmit ? 'block' : 'none' }}">
                            <input type="checkbox" id="ots_ip" class="filled-in" {{ $isotsip ? 'checked' : '' }}/>
                            <label for="ots_ip">OTS with IP Address</label>    
                        </div>

                        <div class="message-box contact-box table-responsive">
                            <table id="example" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>User {{ $question->first()->track_ip  }}</th>
                                        <th>Created At</th>
                                        @if ($isIpTracking)
                                            <th>IP Address</th>
                                        @endif
                                        @foreach ($question as $item)
                                            <th>{{$item->question}}</th>
                                        @endforeach
                                        <th>Generate</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($answer))
                                    @foreach($answer as $row)
                                    <tr>
                                        <td>{{$row->users->name ?? '-'}}</td>
                                        <td>{{$row->created_at}}</td>
                                        @if ($isIpTracking)
                                            <td>{{ $row->user_ip_address }}</td>
                                        @endif
                                        @php
                                            $answer_eform = json_decode($row->answer_eform, true);
                                        @endphp
                                        @foreach ($answer_eform ?? [] as $key => $item)
                                            @php
                                                $kolom = $question->first()->eform_detail->first()->answer_choice ?? 1;
                                            @endphp
                                            @if (is_array($item))
                                                <td>
                                                    @foreach ($item as $no => $i)
                                                    @php
                                                        ++$no;
                                                    @endphp
                                                        @if (is_array($i))
                                                            {{ implode(',', $i) }} 

                                                            @if ($no % 3 === 0)
                                                                <br>
                                                            @else
                                                                |
                                                            @endif

                                                        @else
                                                            {{ $i }},
                                                        @endif
                                                    @endforeach
                                                </td>
                                            @else
                                                <td>{{$item}}</td>
                                            @endif
                                        @endforeach
                                        @empty($answer_eform)
                                            @foreach ($question as $item)
                                                <td></td>
                                            @endforeach
                                        @endempty
                                        <td> 
                                            @if ($row->auto_generate->eg_auto_generate ?? false)
                                                <a href="{{ route(routePrefix().'eform.generate', ['id' => $row->form_id, 'type' => 'docx', 'answer_id' => $row->id]) }}" class="btn btn-secondary btn-sm">Generate</a> 
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="mapsModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Modal Heading</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <div id="googleMap" style="width:auto;height:400px;"></div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAoKj8ZmlRkyRjitCBSj-r7ouJ7TgKbiQk">
</script>

<script>
    $('#example').DataTable({
        // scrollX: true,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                exportOptions: {
                    columns: 'th:not(:last-child)'
                }
            }
        ]
    } );

$('.btn-maps').click(function(){
    var latt = $(this).data("lat");
    var lon = $(this).data("lng");

    var myLatLng = {lat: latt, lng: lon};

    var map = new google.maps.Map(document.getElementById('googleMap'), {
    zoom: 4,
    center: myLatLng
    });

    var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Eform'
    });

    $('#mapsModal').modal('show');
})

$('#ots_ip').change(function(){

    $.ajax({
        type: "POST",
        url: "{{ route('eform.answer.ots-ip', $form_id) }}",
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        success: function (response) {
           console.log("change one time submit with Ip") 
        },
        error: function(data){
            alert("Oops, something went wrong!")
        }
    });
});
</script>
@endsection