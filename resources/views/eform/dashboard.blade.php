@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{asset('jqcloud/jqcloud.css')}}">
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">E-form</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{urlWithPrefix('/eform')}}">Home</a></li>
                    <li class="breadcrumb-item active">E-form</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                @foreach ($arrEformDashboard1 as $key => $item)
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">{{$item['question']}}</h4>
                            <canvas id="chart{{$key}}" height="100"></canvas>
                        </div>
                    </div>
                @endforeach

                @foreach ($arrEformDashboard2 as $key => $item)
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">{{$item['question']}}</h4>
                            <div id="tag-cloud{{$key}}" style="width: 100%; height: 350px;"></div>
                            <div id="image{{$key}}"></div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="{{asset('jqcloud/jqcloud.js')}}"></script>
<script>
    var arrEformDashboard1 = {!! json_encode($arrEformDashboard1) !!};
    var arrEformDashboard2 = {!! json_encode($arrEformDashboard2) !!};
    // console.log(arrEformDashboard2);
    $.each(arrEformDashboard1, function(i, v) {
        var labels = [];
        var count = [];
        var color = [];
        $.each(v.detail, function (x, y) {
            labels.push(y.answer_choice);
            count.push(y.count_answer);
            const randomNum = () => Math.floor(Math.random() * (235 - 52 + 1) + 52);
            const randomRGB = () => `rgb(${randomNum()}, ${randomNum()}, ${randomNum()})`;
            color.push(randomRGB());
        })

        const data = {
            labels: labels,
            datasets: [{
                label: '# of Votes',
                data: count,
                backgroundColor: color,
                borderWidth: 1
            }]
        };
    
        // config 
        const config = {
            type: 'bar',
            data,
            options: {
                plugins: {
                    legend: {
                        onClick: (evt, legendItem, legend) => {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels: {
                            generateLabels: (chart) => {
                                // console.log(chart)
                                return chart.data.labels.map(
                                    (label, index) => ({
                                        text: label,
                                        strokeStyle: chart.data.datasets[0].backgroundColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: false
                                    })
                                )
                            },
                            font: {
                                size: 17
                            },
                            color: "black",
                        },
                        position: 'bottom'
                    }
                },
                indexAxis : 'y',
                scales: {
                    y: {
                        display: false,
                        beginAtZero: true,
                        ticks: {
                            font: {
                                size: 17,
                            }
                        }
                    }
                }
            }
        };
    
        // render init block
        const myChart = new Chart(
            document.getElementById('chart'+i),
            config
        );
    });
    

    $.each(arrEformDashboard2, function(i, v) {
        console.log(v);
        if (v.answer_type == 'image') {
            $.each(v.detail, function (index, item) {
                // console.log(item[Object.keys(item)[0]]);
                $('#tag-cloud'+i).attr('hidden', true);
                $('#images'+i).attr('hidden', false);
                var image = '{{ asset("documents/eform") }}'+ '/' +item[Object.keys(item)[0]];
                $('#image'+i).append('<img src="'+image+'" width="250" height="250" style="margin: 20px">')
            })
        } else {
            var check = [];
            var words = [];
            $.each(v.detail, function(index, item) {
                // console.log(item[Object.keys(item)[0]]);
                $('#tag-cloud'+i).attr('hidden', false);
                $('#images'+i).attr('hidden', true);
                const rndInt = Math.floor(Math.random() * 100) + 1
                
                var obj = {text: item[Object.keys(item)[0]], weight: rndInt};
                check.push(item[Object.keys(item)[0]])
            })

            const counts = {};
            check.forEach(function (x) { 
                counts[x] = (counts[x] || 0) + 1;
            });
            
            // console.log(counts)

            const sortable = Object.entries(counts)
                    .sort(([,a],[,b]) => a-b)
                    .reduce((r, [k, v]) => ({ ...r, [k]: v }), {});
            
            var ww = 0;
            $.each(sortable, function(itm, idx) {
                var obj = {text: itm, weight: 7+ww};
                words.push(obj);
                ww+=2;
            })
            // console.log(words)

            $('#tag-cloud'+i).jQCloud(words);
        }
    });
</script>    
@endsection