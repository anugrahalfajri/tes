@extends('layouts.app')

@section('content')
<link href="{{url('plugins/summernote/dist/summernote.css')}}" rel="stylesheet">
<style>
    .ui-widget-content {
        display: none !important;
    }
</style>
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">E-form</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{urlWithPrefix('/')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{urlWithPrefix('/eform')}}">E-form</a></li>
                    <li class="breadcrumb-item active">Edit</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="d-flex justify-content-between">
                            <h4 class="text-white card-title">E-form - Edit</h4>          
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{urlWithPrefix('/eform/update', $eform[0]->form_id)}}" method="post" enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            <input type="hidden" name="form_id" value="{{$eform[0]->form_id}}">
                            {{-- <div class="row mb-5">
                                <div class="col-md-12">
                                    <select name="target" id="target" class="form-control">
                                        <option value="" selected disabled>Select Target</option>
                                        <option value="login" {{$eform[0]->target == 'login' ? 'selected' : ''}}>Private</option>
                                        <option value="public" {{$eform[0]->target == 'public' ? 'selected' : ''}}>Public</option>
                                    </select>
                                </div>
                            </div> --}}
                            <div class="row mb-3">
                                <div class="col-md-12">
                                    <input class="form-check-input" type="checkbox" id="track_ip" name="track_ip" value="1" {{ $eform[0]->track_ip == '1' ? 'checked disabled' : 'disabled' }}>
                                    <label class="form-check-label mr-3" for="track_ip">
                                        Track IP 
                                    </label>
                                    <input class="form-check-input" type="checkbox" id="mobile_used" name="mobile_used" value="1" {{ $eform[0]->mobile_used == '1' ? 'checked disabled' : 'disabled' }}>
                                    <label class="form-check-label mr-3" for="mobile_used">
                                        Mobile Used 
                                    </label>
                                    <input class="form-check-input" type="checkbox" id="one_time_submit" name="one_time_submit" value="1" {{ $eform[0]->one_time_submit == '1' ? 'checked disabled' : 'disabled' }}>
                                    <label class="form-check-label mr-3" for="one_time_submit">
                                        One Time Submit 
                                    </label>
                                    <input class="form-check-input" type="checkbox" id="graph_report" name="graph_report" value="1" {{ $eform[0]->graph_report == "1" ? "checked disabled" : 'disabled' }}>
                                    <label class="form-check-label mr-3" for="graph_report">
                                        Graph Report
                                    </label>
                                    <input class="form-check-input" type="checkbox" id="generate_box" name="eg_auto_generate" value="Y" {{ isset($eform[0]->eform_generate->eg_auto_generate) ? $eform[0]->eform_generate->eg_auto_generate ? 'checked disabled' : 'disabled' : '' }}>
                                    <label class="form-check-label mr-3" for="generate_box">
                                        Aktifkan auto generate 
                                    </label>

                                    <input class="form-check-input" type="checkbox" id="paging_form" name="paging_form" value="1" {{ $eform[0]->paging_form == "1" ? "checked disabled" : 'disabled' }}>
                                    <label class="form-check-label mr-3" for="paging_form">
                                        Paging Form
                                    </label>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6 template_upload" style="display: none;">
                                    <label>Generate upload</label>
                                    <label class="custom-file d-block">
                                        <input type="file" id="file" name="eg_upload_template" class="custom-file-input" accept=".docx" value="{{ old('eg_upload_template') }}">
                                        <span class="custom-file-control"></span>
                                    </label><br>
                                    <a class="btn btn-info" data-toggle="collapse" href="#guide" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    Get started
                                    </a>
                                    <div class="collapse" id="guide">
                                        <div class="card card-body">
                                            <ul style="font-size: 12px;">
                                            <li><i>1. Lihat nomor pertanyaan pada form yang anda buat <b>e.g Question-1</b>. Berarti ini nomor 1</i></li>
                                            <li><i>2. Variabel akan sesuai dengan nomor yang ada pada <b>langkah 1</b></i></li>
                                            <li><i>3. Penulisan Variabel di awali dengan simbol <code>${</code> di ikuti dengan keyword <code>answer_(nomor_pada_langkah_1)</code> dan di akhiri dengan simbol <code>}</code></i></li>
                                            <li><i>4. Sehingga variabel lengkapnya menjadi <code>${answer_1}</code></i></li>
                                            <li><i>5. Khusus untuk pilihan <i>Multiple Answer</i> harus diapit dalam satu block dengan keyword <code>${value}</code> menjadi <code>${answer_1} ${value} ${/answer_1}</code>. Untuk lebih lengkap dapat dilihat pada file contoh.</i></li>
                                            <li><strong>Contoh Penggunaan dapat dilihat pada file berikut ini : <a style="font-size: 14px;" href="{{ url('eform_example_answer.docx') }}">Download Contoh</a></strong></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 paging_col" style="display: {{ $eform[0]->paging_form ? 'block' : 'none' }};">
                                    <label>Total Page</label>
                                    <input type="number" min="1" name="page" id="page" class="form-control" placeholder="Total Page" value="{{$eform[0]->paging_form_total}}">
                                </div>
                            </div>
                            <div class="row mb-5">
                                <div class="col-md-12">
                                    <input type="text" name="title" id="title" value="{{$eform[0]->title}}"  class="form-control" placeholder="Judul" required>
                                </div>
                            </div>
                            <div class="container-add-form">
                                @foreach ($eform as $key => $item)
                                    <input type="hidden" name="id[]" value="{{$item->id}}">
                                    <div class="row row-form mb-5">
                                        <div class="col-md-4">
                                            <label><b>Judul</b></label>
                                            <input type="text" name="question[]" id="question" value="{{$item->question}}" class="form-control" placeholder="Judul Pertanyaan" required>
                                        </div>
                                        <div class="col-md-3">
                                            <label><b>Description</b></label>
                                            <textarea name="short_desc[]" class="form-control" col="5" row="3">{{ $item->short_desc }}</textarea>
                                        </div>
                                        <div class="col-md-2">
                                            <label><b>Answer Mode</b></label>
                                            <select name="answer_type[]" id="answer_type{{$key}}" class="form-control" disabled>
                                                <option value="" disabled>Answer Mode</option>
                                                <option value="text" {{$item->answer_type == 'text' ? 'selected' : ''}}>Text</option>
                                                <option value="number" {{$item->answer_type == 'number' ? 'selected' : ''}}>Number</option>
                                                <option value="text_area" {{$item->answer_type == 'text_area' ? 'selected' : ''}}>Text Area</option>
                                                <option value="radio_button" {{$item->answer_type == 'radio_button' ? 'selected' : ''}}>Radio Button</option>
                                                <option value="check_box" {{$item->answer_type == 'check_box' ? 'selected' : ''}}>Check Box</option>
                                                <option value="select_box" {{$item->answer_type == 'select_box' ? 'selected' : ''}}>Select Box</option>
                                                <option value="url" {{$item->answer_type == 'url' ? 'selected' : ''}}>URL</option>
                                                <option value="date" {{$item->answer_type == 'date' ? 'selected' : ''}}>Date</option>
                                                <option value="time" {{$item->answer_type == 'time' ? 'selected' : ''}}>Time</option>
                                                <option value="file" {{$item->answer_type == 'file' ? 'selected' : ''}}>File</option>
                                                <option value="image" {{$item->answer_type == 'image' ? 'selected' : ''}}>Image</option>
                                                <option value="multi" {{$item->answer_type == 'multi' ? 'selected' : ''}}>Multiple Answer</option>
                                            </select>
                                        </div>
                                        <div class="col-md-1 page_num" style="display:{{ $item->paging_form ? 'block' : 'none' }};">
                                            <label><b>Page</b></label>
                                            <input type="number" name="page_num[]" class="form-control" min="1" value="{{ $item->paging_form_number ?? '1' }}" readonly>
                                        </div>
                                        <div class="col-md-1">
                                            <label>Mandatory</label>
                                            <select name="is_mandatory[]" class="form-control" disabled>
                                                <option value="1" {{ $item->is_mandatory ? 'selected' : '' }}>Yes</option>
                                                <option value="0" {{ $item->is_mandatory ? '' : 'selected' }}>No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-3" id="form-dynamic{{$key}}">
                                        @if ($item->answer_type == 'radio_button')
                                            <div class="row-rb">
                                                @foreach ($item->eform_detail as $value)
                                                    <input type="hidden" name="id_detail_rb[]" value="{{$value->id}}">
                                                    <div class="row-rb-append"> 
                                                        <input class="form-check-input" type="radio" id="radio" disabled>
                                                        <label class="form-check-label" for="radio">
                                                            <input type="text" name="answer_choice_rb[]" value="{{$value->answer_choice}}" class="form-control" maxlength="255"/>
                                                        </label>
                                                    </div>
                                                    <br>
                                                @endforeach
                                            </div>
                                        @elseif ($item->answer_type == 'check_box')
                                            <div class="row-cb">
                                                @foreach ($item->eform_detail as $value)
                                                    <input type="hidden" name="id_detail_cb[]" value="{{$value->id}}">
                                                    <div class="row-cb-append">
                                                        <input class="form-check-input" type="checkbox" id="checkbox" disabled>
                                                        <label class="form-check-label" for="checkbox">
                                                            <input type="text" name="answer_choice_cb[]" value="{{$value->answer_choice}}" class="form-control" maxlength="255"/>
                                                        </label>
                                                    </div>
                                                    <br>
                                                @endforeach
                                            </div>
                                        @elseif ($item->answer_type == 'select_box')
                                            <div class="row-sb">
                                                @foreach ($item->eform_detail as $i => $value)
                                                    <input type="hidden" name="id_detail_sb[]" value="{{$value->id}}">
                                                    <div class="row-sb-append">
                                                        {{$i+1}}
                                                        <label class="form-check-label">
                                                            <input type="text" name="answer_choice_sb[]" value="{{$value->answer_choice}}" class="form-control" maxlength="255"/>
                                                        </label>
                                                    </div>
                                                    <br>
                                                @endforeach
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                            </div>

                            <div class="row mt-5">
                                <div class="col-md-12">
                                    <label for=""><b>Select Target</b></label><br>
                                    <input class="form-check-input" type="radio" id="login" name="target" value="login" {{$eform[0]->target == 'login' ? 'checked' : 'disabled'}}>
                                    <label class="form-check-label mr-3" for="login">
                                        Private
                                    </label>
                                    <input class="form-check-input" type="radio" id="public" name="target" value="public" {{$eform[0]->target == 'public' ? 'checked' : 'disabled'}}>
                                    <label class="form-check-label mr-3" for="public">
                                        Public
                                    </label>
                                </div>
                            </div>

                            <div class="row mt-5">
                                <div class="col-md-12">
                                    <label for=""><b>After submitting form, take these action. </b></label><br>
                                    <input class="form-check-input action_sel" type="radio" id="thanks" name="action" value="thanks_page"  {{$eform[0]->action_submit == 'thanks_page' ? 'checked' : 'disabled'}}>
                                    <label class="form-check-label mr-3" for="thanks">
                                        Thankyou page
                                    </label>
                                    <input class="form-check-input action_sel" type="radio" id="redirect" name="action" value="redirect_to"  {{$eform[0]->action_submit == 'redirect_to' ? 'checked' : 'disabled'}}>
                                    <label class="form-check-label mr-3" for="redirect">
                                        Redirect to url
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="action_url" id="action_url" class="form-control"  style="display: {{$eform[0]->action_submit == 'redirect_to' ? 'block' : 'none'}};" placeholder="http://example.com/thankyou" value="{{ $eform[0]->action_submit_url }}">
                                </div>
                            </div>

                            @if ($eform[0]->action_submit == 'thanks_page')
                            <div class="row mt-2" id="thankyou-row">
                                <div class="col-md-6">
                                    <label><b>Header Page</b></label>
                                    <textarea id="header" name="header">{{ $eform[0]->header_page ?? '' }}</textarea>
                                </div>
                                <div class="col-md-6">
                                    <label><b>Description Page</b></label>
                                    <textarea id="description" name="description">{{ $eform[0]->description_page ?? '' }}</textarea>
                                </div>
                            </div>
                            @endif

                            <br>
                            <div align="right">
                                <button type="submit" class="btn btn-primary">Submit</a>
                                <button type="reset" class="btn btn-danger  ml-5">Reset</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{url('plugins/summernote/dist/summernote.min.js')}}"></script>
<script>
    $('#header').summernote('disable');

    $('#description').summernote('disable');

    $('.action_sel').change(function(){
        var sel = $('input[name="action"]:checked').val();

        if(sel == "redirect_to"){
            $('#action_url').show();
        }else{
            $('#action_url').hide();
            $('#action_url').val("");
        }
    });

    $("#generate_box").change(function() {
        if(this.checked) {
           $('.template_upload').show();
        }else{
            $('.template_upload').hide();
        }
    });

    $("#paging_form").change(function() {
        if(this.checked) {
           $('.paging_col').show();
           $('.page_num').show();
        }else{
            $('.paging_col').hide();
            $('.page_num').hide();
        }
    });
</script>
@endsection