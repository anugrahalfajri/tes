@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">Sub Organization</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{urlWithPrefix('/')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{urlWithPrefix('/sub_organization')}}">Sub Organization</a></li>
                    <li class="breadcrumb-item active">{{ $action == "store" ? "Create" : "Edit" }}</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="d-flex justify-content-between">
                            <h4 class="text-white card-title">Sub Organization - {{($action == 'store') ? 'Create' : 'Edit'}}</h4>          
                        </div>
                    </div>
                    <div class="card-body">
                        @if($action == 'store')
                        <form action="{{urlWithPrefix('/sub_organization/store')}}" method="post">
                        @elseif($action == 'update')
                        <form action="{{urlWithPrefix('/sub_organization/update', $sub_organization->id)}}" method="post">
                        @method('PUT')
                        @endif
                        @csrf
                            <div class="row">
                                <div class="col-md-3 mandatory">Name</div>
                                <div class="col-md-9">
                                  <input type="text" name="name" id="name" value="{{@$sub_organization->name}}" class="form-control" required>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-3">Description</div>
                                <div class="col-md-9">
                                    <textarea name="description" id="description" cols="30" rows="5" class="form-control">{{@$sub_organization->description}}</textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-3 mandatory">Status</div>
                                <div class="col-md-9">
                                    <select name="status" id="status" class="form-control" required>
                                        <option value="1" {{@$sub_organization->status == '1' ? 'selected' : ''}}>Active</option>
                                        <option value="0" {{@$sub_organization->status == '0' ? 'selected' : ''}}>Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-3 mandatory">Organization</div>
                                <div class="col-md-9">
                                    <select name="id_organization" id="id_organization" class="form-control select2" style="width: 100%;" required>
                                            @if(!empty($organizations))
                                                @foreach($organizations as $row)
                                                    <option value="{{$row->id}}" {{($row->id == @$sub_organization->admin)?'selected':''}}>{{$row->name}}</option>
                                                @endforeach
                                            @endif
                                    </select>
                                </div>
                            </div>
                            <br>
                            <div align="right">
                                <button type="submit" class="btn btn-primary">Submit</a>
                                <button type="reset" class="btn btn-danger  ml-5">Reset</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
