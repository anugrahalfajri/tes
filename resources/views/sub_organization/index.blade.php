@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">Sub Organization</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{urlWithPrefix('/')}}">Home</a></li>
                    <li class="breadcrumb-item active">Sub Organization</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="justify-content-between">
                            <h4 class="text-white card-title">Sub Organization</h4>
                            @if ($role->name == 'admin')
                                <a class="pull-right btn btn-primary" href="{{urlWithPrefix('/sub_organization/create')}}">Add Sub Organization</a>
                            @endif
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Status</th>
                                        <th>Organization</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($sub_organizations))
                                    @foreach($sub_organizations as $row)
                                    <tr>
                                        <td>{{$row->name}}</td>
                                        <td>{{$row->description}}</td>
                                        <td>{{($row->status == '1') ? 'Active' : 'Inactive'}}</td>
                                        <td>{{$row->organization->name}}</td>
                                        <td>
                                            <form action="{{urlWithPrefix('/sub_organization/delete', $row->id)}}" method="GET">
                                                <a href="{{urlWithPrefix('/sub_organization/edit', $row->id)}}" class="btn btn-warning btn-sm">Edit</a>&nbsp;
                                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>&nbsp;
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection