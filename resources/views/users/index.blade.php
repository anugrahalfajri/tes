@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">User Management</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">User Management</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="justify-content-between">
                            <h4 class="text-white card-title">User Management</h4>
                            @if ($role->name == 'admin')
                                <a class="pull-right btn btn-primary" href="{{urlWithPrefix('/users/create')}}">Add User</a>
                            @endif
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Level</th>
                                        <th>Created Date</th>
                                        <th>Mofified Date</th>
                                        <th>Active/Inactive</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($users))
                                    @foreach($users as $row)
                                    <tr>
                                        <td>{{$row->name}}</td>
                                        <td>{{$row->level}}</td>
                                        <td>{{ \Carbon\Carbon::parse($row->created_at)->format('d/m/Y H:i:s') }}</td>
                                        <td>{{\Carbon\Carbon::parse($row->updated_at)->format('d/m/Y H:i:s')}}</td>
                                        <td>
                                            <input type="checkbox" value="1" id="status_{{ $row->id }}" {{($row->status == '1') ? 'checked' : ''}} class="js-switch" data-color="#f62d51" data-size="small" onchange="updateStatus({{$row->id}})"/>
                                        </td>
                                        <td>
                                            <a href="{{urlWithPrefix('/users/edit', $row->id)}}" class="btn btn-warning btn-sm">Edit</a>&nbsp;
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-email" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Email</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            Perlu konfirmasi user, konfirmasi dikirim melalui email
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

<script src="{{url('plugins/jquery/jquery.min.js')}}"></script>
@if( Session::has('email'))
    <script>
        $(document).ready(function() {
            $('#modal-email').modal('show');
        });
    </script>
@endif

<script>
        function updateStatus(id){
            let status = $('#status_'+id).is(":checked");
            console.log(status)
            $.ajax({
                type: "POST",
                url: "{{ route('user.statuslog') }}",
                data: {user_id:id, status:status},
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: "json",
                success: function (response) {
                    alert(response.message)
                }
            });
        }
</script>
@endsection