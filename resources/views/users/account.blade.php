@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">My Profile</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">My Profile</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
        @if (\Session::has('success_msg'))
            <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <ul>
                    <li>{!! \Session::get('success_msg') !!}</li>
                </ul>
            </div>
        @endif
        @if (\Session::has('failed_msg'))
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <ul>
                    <li>{!! \Session::get('failed_msg') !!}</li>
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <h4 class="text-white card-title">My Profile</h4>
                        <h6 class="card-subtitle text-white m-b-0 op-5">User Profile</h6>
                    </div>
                    <div class="card-body">
                        <form action="{{ route(routePrefix().'users.setting.change', auth()->user()->id) }}" method="POST">
                            @csrf
                        <div class="table-responsive">
                            <ul class="navbar-nav">
                                <li class="nav-item dropdown">
                                    <a class="nav-link" data-toggle="collapse-nama" href="#" aria-controls="collapseExample">
                                        <div class="row">
                                            <div class="col-md-3">Organization : </div>
                                            <div class="col-md-6">
                                                <select name="organization" id="organization" class="form-control">
                                                    <option value="">Pilih Organization</option>   
                                                    @foreach ($organizations as $o)
                                                        <option value="{{ $o->id }}" {{ $user->id_organization == $o->id ? 'selected':'' }}>{{ $o->name }}</option>
                                                    @endforeach 
                                                </select>      
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" data-toggle="collapse-nama" href="#" aria-controls="collapseExample">
                                        <div class="row">
                                            <div class="col-md-3">Department</div>
                                            <div class="col-md-6">
                                                <select name="department" id="department" class="form-control">
                                                    <option value="">Pilih Department</option>   
                                                    @foreach ($departments as $d)
                                                        <option value="{{ $d->id }}" {{ $user->department_id == $d->id ? 'selected':'' }}>{{ $d->name }}</option>
                                                    @endforeach 
                                                </select>       
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" data-toggle="collapse-nama" href="#" aria-controls="collapseExample">
                                        <div class="row">
                                            <div class="col-md-3">Role</div>
                                            <div class="col-md-6">
                                                <select name="role" id="role" class="form-control">
                                                    <option value="">Pilih Role</option>   
                                                    @foreach ($roles as $r)
                                                        <option value="{{ $r->id }}" {{ $user->role_id == $r->id ? 'selected':'' }}>{{ $r->name }}</option>
                                                    @endforeach 
                                                </select>    
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link demo" data-toggle="collapse-nama" href="#" aria-controls="collapseExample">
                                        <div class="row">
                                            <div class="col-md-3">PIN</div>
                                            <div class="col-md-6"><input type="password" name="pin_account" id="" placeholder="******" class="form-control">
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-md-3">Confirm PIN</div>
                                            <div class="col-md-6"><input type="password" name="pin_account_confirmation" id="" placeholder="******" class="form-control"></div>
                                        </div>
                                    </a>
                                </li>
                            </ul>

                            <br>
                            <div align="left">
                              <button type="submit" class="btn btn-primary">Save & Change</a>
                            </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
