@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">My Profile</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ urlWithPrefix('/') }}">Home</a></li>
                    <li class="breadcrumb-item active">My Profile</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
        @if (\Session::has('success_msg'))
            <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <ul>
                    <li>{!! \Session::get('success_msg') !!}</li>
                </ul>
            </div>
        @endif
        @if (\Session::has('failed_msg'))
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <ul>
                    <li>{!! \Session::get('failed_msg') !!}</li>
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <h4 class="text-white card-title">My Profile</h4>
                        <h6 class="card-subtitle text-white m-b-0 op-5">User Profile</h6>
                    </div>
                    <div class="card-body">
                        <form action="{{ route(routePrefix().'users.profile.change', auth()->user()->id) }}" method="POST">
                            @csrf
                        <div class="table-responsive">
                            <div class="row mt-3">
                                <div class="col-md-3">Name</div>
                                <div class="col-md-6"><input type="text" name="name" id="" value="{{ auth()->user()->name }}" class="form-control" style="width: 100%;"></div>
                            </div>
                                <div class="row mt-3">
                                    <div class="col-md-3">Email</div>
                                    <div class="col-md-6"><input type="email" name="email" id="" value="{{ auth()->user()->email }}" class="form-control" style="width: 100%;"></div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-3">Password</div>
                                    <div class="col-md-6"><input type="password" name="password" id="" value="" class="form-control" style="width: 100%;"></div>
                                </div><br>
                                <div class="row mt-3">
                                    <div class="col-md-3">Konfirmasi Password</div>
                                    <div class="col-md-6"><input type="password" name="password_confirmation" id="" value="" class="form-control" style="width: 100%;"></div>
                                </div>
                            <br>
                            <div class="col-md-9" align="right">
                                <button type="submit" class="btn btn-primary">Submit</a>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Anda yakin akan mengedit data?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
