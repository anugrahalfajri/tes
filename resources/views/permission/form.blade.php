@extends('layouts.app')

@section('content')
<style>
    .swal2-container.swal2-center>.swal2-popup {
        width: 150px !important;
    }
    .swal2-confirm, .swal2-title {
        font-size: 10pt !important;
    }
</style>
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">Permission</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Permission</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="d-flex justify-content-between">
                            <h4 class="text-white card-title">Create Permission</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{urlWithPrefix('/add-on-setting/store')}}" method="post">
                            @csrf
                            <input type="hidden" name="id" value="{{$permission->id ?? ''}}">
                            <div class="row mb-5">
                                <div class="col-md-3">Menu Name</div>
                                <div class="col-md-9">
                                    <input type="text" name="menu_name" id="" class="form-control" value="{{$permission->menu_name ?? ''}}">
                                </div>
                            </div>
                            <div class="row mb-5">
                                <div class="col-md-3">Function</div>
                                <div class="col-md-9">
                                    <select name="function" id="function" class="form-control" required>
                                        <option value="" selected disabled>Select Function</option>
                                        <option value="view" {{($permission->function ?? '') == 'view' ? 'selected' : ''}}>View</option>
                                        <option value="create" {{($permission->function ?? '') == 'create' ? 'selected' : ''}}>Create</option>
                                        <option value="edit" {{($permission->function ?? '') == 'edit' ? 'selected' : ''}}>Edit</option>
                                        <option value="delete" {{($permission->function ?? '') == 'delete' ? 'selected' : ''}}>Delete</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-5">
                                <div class="col-md-3">System Name</div>
                                <div class="col-md-9">
                                    <input type="text" name="system_name" id="" class="form-control" value="{{$permission->system_name ?? ''}}">
                                </div>
                            </div>
                            <div align="right">
                                <button data-toggle="modal" data-target="#exampleModal" type="button" class="btn btn-primary">Submit</a>
                                <button type="reset" class="btn btn-danger  ml-5">Reset</a>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Anda yakin akan {{$act == 'tambah' ? 'menambahkan' : 'mengedit'}} data?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection