@extends('layouts.app')

@section('content')
<style>
    .swal2-container.swal2-center>.swal2-popup {
        width: 150px !important;
    }
    .swal2-confirm, .swal2-title {
        font-size: 10pt !important;
    }
</style>
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">Permission</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Permission</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="justify-content-between">
                            <h4 class="text-white card-title">Permission</h4>
                            <div class="btn-group" align="right">
                               <a class="btn btn-primary btn-sm" href="{{urlWithPrefix('/add-on-setting/create')}}">Add Permission</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Menu Name</th>
                                        <th>Function</th>
                                        <th>System Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($permission))
                                    @foreach($permission as $row)
                                    <tr>
                                        <td>{{$row->menu_name}}</td>
                                        <td>{{$row->function}}</td>
                                        <td>{{ $row->system_name }}</td>
                                        <td>
                                            <a href="{{urlWithPrefix('/add-on-setting/edit', $row->id)}}" class="btn btn-warning btn-sm">Edit</a>&nbsp;
                                            <a data-toggle="modal" data-target="#exampleModal{{$row->id}}" class="btn btn-danger btn-sm">Delete</a>&nbsp;
                                            <!-- Modal -->
                                            <div class="modal fade" id="exampleModal{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        Anda yakin akan menghapus data?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <a href="{{urlWithPrefix('/add-on-setting/delete', $row->id)}}" class="btn btn-danger">Delete</a>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection