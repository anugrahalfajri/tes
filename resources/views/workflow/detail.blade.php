@extends('layouts.app')

@section('content')
<style>
    .workflow-detail, .workflow-detail-status {
        cursor: pointer;
    }
    .workflow-detail {
        max-width: 15em;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .dropzone .dz-preview.dz-error:hover .dz-error-message {
        margin-top: 20px;
    }
</style>
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">Workflow Detail</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{urlWithPrefix('/')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{urlWithPrefix('workflow')}}">Workflow</a></li>
                    <li class="breadcrumb-item active">Workflow Detail</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="d-flex justify-content-between">
                            <h4 class="text-white card-title">{{$workflow->nama}}</h4>
                            <a align="right" href="#ModalUpload" class="btn btn-primary" type="button" data-toggle="modal">Upload Document</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-responsive-lg" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Nama File</th>
                                        <th>Directory File</th>
                                        <th>Created At</th>
                                        <th>Created By</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($workflow_detail))
                                    @foreach($workflow_detail as $row)
                                    <tr>
                                        <td>{{$row->title}}</td>
                                        <td class="workflow-detail" data-id="{{$row->id}}">{{$row->nama_file}}</td>
                                        <td>{{$row->folder}}</td>
                                        <td>{{$row->created_at}}</td>
                                        <td>{{$row->created_by}}</td>
                                        <td class="workflow-detail-status" data-id="{{$row->id_workflow_detail}}">{{$row->status}}</td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalUpload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Upload Document</h4>
            </div>
            <div class="modal-body">
                <div id="validate_error">

                </div><br>
                <form action="{{urlWithPrefix('/workflow/store_detail')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" id="id" value="{{$workflow->id}}">
                    <input type="hidden" name="id_directory" id="id_directory" value="{{$workflow->id_directory}}">
                    <div class="row mt-2">
                        <div class="col-md-2">Title *</div>
                        <div class="col-md-10"><input type="text" name="title" id="title" class="form-control" required></div>
                    </div>
                    <div class="row" style="margin-top:10px">
                        <div class="col-md-2">Description</div>
                        <div class="col-md-10"><textarea class="form-control" name="description" id="description" rows="5"></textarea></div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-md-2">Upload File *</div>
                        <div class="col-md-10">
                            <div class="dropzone" id="myDropzonePdf">
                                <div class="dz-message">
                                    <span>Drop files here or click to upload</span>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top:10px">
                        <div class="col-md-2">Due Date</div>
                        <div class="col-md-10"><input type="text" class="form-control datepicker" name="due_date" id="due_date" autocomplete="off"></div>
                    </div>
                    <div class="row" style="margin-top:10px">
                        <div class="col-md-2">Priority</div>
                        <div class="col-md-10">
                            <select name="priority" id="priority" class="form-control">
                                <option value="" selected disabled>Select Priority</option>
                                <option value="High">High</option>
                                <option value="Medium">Medium</option>
                                <option value="Low">Low</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div align="right">
                        <button type="button" class="btn btn-primary" id="submit-upload-pdf">Save</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Workflow Detail</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row mt-2">
                    <div class="col-md-2"><b>Title</b></div>
                    <div class="col-md-10"><span id="title"></span></div>
                </div>
                <div class="row" style="margin-top:10px">
                    <div class="col-md-2"><b>Directory</b></div>
                    <div class="col-md-10"><span id="directory"></span></div>
                </div>
                <div class="row" style="margin-top:10px">
                    <div class="col-md-2"><b>Description</b></div>
                    <div class="col-md-10"><span id="description"></span></div>
                </div>
                <div class="row" style="margin-top:10px">
                    <div class="col-md-2"><b>Created At</b></div>
                    <div class="col-md-10"><span id="created_at"></span></div>
                </div>
                <div class="row" style="margin-top:10px">
                    <div class="col-md-2"><b>Created By</b></div>
                    <div class="col-md-10"><span id="created_by"></span></div>
                </div>
                <div class="row" style="margin-top:10px">
                    <div class="col-md-2"><b>Due Date</b></div>
                    <div class="col-md-10"><span id="due_date"></span></div>
                </div>
                <div class="row" style="margin-top:10px">
                    <div class="col-md-2"><b>Priority</b></div>
                    <div class="col-md-10"><span id="priority"></span></div>
                </div>
                <div class="row" style="margin-top:10px">
                    <div class="col-md-2"><b>Document</b></div>
                    <div class="col-md-10"><span id="pdf-link"></span></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalDetailStatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <table id="table-workflow-detail-status" class="table" width="100%">
                    <thead>
                        <th>Level</th>
                        <th>Status</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ url('material/js/dropzone.js') }}"></script>
    <script>
        // NEW DROPZONE SETTING
        Dropzone.options.myDropzonePdf= {
            url: baseurl + "/workflow/store_detail",
            autoProcessQueue: false,
            uploadMultiple: false,
            maxFiles: 1,
            maxFilesize: 2,
            addRemoveLinks: true,
            paramName:"file",
            acceptedFiles: ".pdf",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            init: function() {
                dzClosure = this;

                document.getElementById("submit-upload-pdf").addEventListener("click", function(e) {
                    if (dzClosure.getQueuedFiles().length > 0) {                        
                        dzClosure.processQueue(); 
                    } else {                       
                        alert('Please fill the required field!')
                    }   
                    e.preventDefault();
                    e.stopPropagation();
                });

                this.on("sending", function(data, xhr, formData) {
                    formData.append("id", $("#id").val());
                    formData.append("id_directory", $("#id_directory").val());
                    formData.append("title", $("#title").val());
                    formData.append("description", $("#description").val());
                    formData.append("due_date", $("#due_date").val());
                    formData.append("priority", $("#priority").val());
                });

                this.on("success", function(file, response) {
                    $('#modal-upload').modal('hide');
                    $('.message').html(response.message);
                    $('.upload-success').show();
                    location.reload();
                });

                this.on('error', function(file, response) {
                    
                    if(file.xhr.status == 422){
                        // var response = JSON.parse(response);
                        var errorString = '<ul>';
                        $.each( response.errors, function( key, value) {
                            errorString += '<li style="color:red;">' + value + '</li>';
                        });
                        errorString += '</ul>';

                        console.log(errorString)
                        $('#validate_error').html(errorString);
                    }else{
                        alert(response);
                    }
                });
            }
        }
        // END DROPZONE

        var modalDetail = $('#modalDetail');
        $('.workflow-detail').on('click', function () {
            $.ajax({
                url: "{{route(routePrefix().'workflow-getWorkflowDetail')}}",
                method: "POST",
                data: {
                    file_upload_id : $(this).attr('data-id')
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    console.log(data)
                    modalDetail.modal('show');
                    modalDetail.find('#title').text(data.data.title);
                    modalDetail.find('#directory').text(data.data.folder);
                    modalDetail.find('#description').text(data.data.description);
                    modalDetail.find('#created_at').text(new Date(data.data.created_at));
                    modalDetail.find('#created_by').text(data.data.created_by);
                    modalDetail.find('#due_date').text(new Date(data.data.due_date));
                    modalDetail.find('#priority').text(data.data.priority);
                    PDFObject.embed(baseurl + '/workflow/' + data.data.nama_file, "#pdf-link", {height: "30rem"});
                }
            })
        })

        var modalDetailStatus = $('#modalDetailStatus');
        $('.workflow-detail-status').on('click', function () {
            $.ajax({
                url: "{{route(routePrefix().'workflow-getWorkflowDetailStatus')}}",
                method: "POST",
                data: {
                    id_workflow_detail : $(this).attr('data-id')
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    modalDetailStatus.modal('show');
                    $.each(data.data, function (index, value) {
                        $('#table-workflow-detail-status tbody').append(
                            '<tr>'
                            +   '<td>'+value.level+'</td>'
                            +   '<td>'+value.status+'</td>'
                            +'</tr>'
                        );
                    });
                }
            })
        })

        modalDetailStatus.on('hidden.bs.modal', function () {
            $('#table-workflow-detail-status tbody').empty();
        })
        
        $('#ModalUpload').on('hidden.bs.modal', function () {
            $('#myDropzonePdf .dz-preview').remove();
        })
    </script>
@endsection