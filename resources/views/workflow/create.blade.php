@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">Workflow</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{route('workflow')}}">Workflow</a></li>
                    <li class="breadcrumb-item active">Create</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')

        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="d-flex justify-content-between">
                            <h4 class="text-white card-title">Workflow Create</h4>
                            
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{urlWithPrefix('/workflow/store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                          <div class="row" style="margin-top:10px">
                            <div class="col-md-2">Name *</div>
                            <div class="col-md-10"><input type="text" name="nama" id="" class="form-control" required></div>
                          </div>
                          {{-- <div class="row" style="margin-top:10px">
                            <div class="col-md-2">Directory</div>
                            <div class="col-md-10">
                                <select name="id_directory" id="" class="form-control select2">
                                    @if(!empty($directory))
                                    @foreach($directory as $row)
                                    <option value="{{$row->id}}">{{$row->nama_folder}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                          </div> --}}
                          <div class="row" style="margin-top:10px">
                            <div class="col-md-2">Description</div>
                            <div class="col-md-10"><textarea class="form-control" name="description" rows="5"></textarea></div>
                          </div>
                          <div class="row-level">
                            <div class="row" style="margin-top:10px">
                              <div class="col-md-2">User Approval *</div>
                              <div class="col-md-9">
                                <select name="level[]" class="form-control select2" style="width: 100%;" required>
                                  <option value="" selected disabled>Select Level</option>
                                  @foreach ($users as $item)
                                      <option value="{{$item->name}}">{{$item->name}}</option>
                                  @endforeach
                                </select>
                              </div>
                              <div class="col-md-1">
                                <button type="button" class="btn btn-sm btn-success mt-1 add-level"><i class="mdi mdi-plus"></i></button>
                              </div>
                            </div>
                          </div>
                          <br>
                          <div align="right">
                            <button type="submit" class="btn btn-primary">Submit</a>
                            <button type="reset" class="btn btn-danger  ml-5">Reset</a>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
      $(document).on('click', '.add-level', function () {
        $('.row-level').append('<div class="row" style="margin-top:10px">'
                               +   '<div class="col-md-2"></div>'
                               +   '<div class="col-md-9">'
                               +     '<select name="level[]" class="form-control select2">'
                               +       '<option value="" selected disabled>Select Level</option>'
                               +       '@foreach ($users as $item)'
                               +           '<option value="{{$item->name}}">{{$item->name}}</option>'
                               +       '@endforeach'
                               +     '</select>'
                               +   '</div>'
                               +   '<div class="col-md-1">'
                               +     '<button type="button" class="btn btn-sm btn-success mt-1 add-level"><i class="mdi mdi-plus"></i></button>'
                               +     '<button type="button" class="btn btn-sm btn-danger mt-1 ml-2 remove-level"><i class="mdi mdi-minus"></i></button>'
                               +   '</div>'
                               + '</div>')

        $(".select2").select2();
      })

      $(document).on('click', '.remove-level', function () {
        $(this).parent().closest('.row').remove();
      })
    </script>
@endsection