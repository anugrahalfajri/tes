@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">Workflow</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{urlWithPrefix('/')}}">Home</a></li>
                    <li class="breadcrumb-item active">Workflow</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="justify-content-between">
                            <h4 class="text-white card-title">Workflow</h4>
                            <a class="pull-right btn btn-primary" href="{{urlWithPrefix('/workflow/create')}}">Buat Baru</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-responsive-lg" style="width:100%">
                                <thead>
                                    <tr>
                                        {{-- <th>Nama Dokumen</th> --}}
                                        <th>Nama Workflow</th>
                                        <th>Total Document</th>
                                        <th>Total Pending</th>
                                        <th>Created At</th>
                                        <th>Created By</th>
                                        {{-- <th>Date Create</th> --}}
                                        {{-- <th>Member</th> --}}
                                        <th>Aksi</th>
                                        <!-- <th>View</th>
                                        <th>Last Status</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($workflow))
                                    @foreach($workflow as $row)
                                    <tr>
                                        {{-- <td>{{$row->nama_file}}</td> --}}
                                        <td>{{$row->nama}}</td>
                                        <td>{{$row->total_document}}</td>
                                        <td>{{$row->total_pending}}</td>
                                        <td>{{$row->created_at}}</td>
                                        <td>{{$row->created_by}}</td>
                                        {{-- <td>{{$row->created_at}}</td> --}}
                                        {{-- <td>{{$row->member}}</td> --}}
                                        {{-- <td>
                                            <a href="#ModalApprove" data-id="{{$row->id}}" class="btn btn-primary" type="button" data-toggle="modal">Approve</a>
                                            <a href="#ModalDetail" data-id="{{$row->id}}" class="btn btn-success" type="button" data-toggle="modal">Detail</a>
                                        </td> --}}
                                        <td>
                                            <button class="btn btn-info workflow-info" data-id="{{$row->id}}">Info</button>
                                            <a href="{{urlWithPrefix('/workflow/detail/'.$row->id)}}" class="btn btn-success">Detail</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ModalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Workflow</h4>
            </div>
            <div class="modal-body">
                <table id="example2" class="table table-striped" style="width:100%">
                    <thead>
                        <tr>
                            <th>Nama File</th>
                            <th>Date Create</th>
                            <th>Member</th>
                            <!-- <th>Aksi</th> -->
                            <!-- <th>View</th>
                            <th>Last Status</th> -->
                        </tr>
                    </thead>
                    <tbody id="form_var_detail">
                        
                    </tbody>
                </table>
                <form action="{{urlWithPrefix('/workflow/store_detail')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" id="id">
                    <div class="row mt-4" style="margin-top:10px">
                        <div class="col-md-2">Select File</div>
                        <div class="col-md-10"><input type="file" name="file" id="" class="form-control"></div>
                    </div>
                    <div class="row" style="margin-top:10px">
                        <div class="col-md-2">Directory</div>
                        <div class="col-md-10">
                            <select name="id_directory" id="" class="form-control select2" style="width:100%;">
                                @if(!empty($directory))
                                    @foreach($directory as $row)
                                    <option value="{{$row->id}}">{{$row->nama_folder}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
                </form>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalDetailFile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Document</h4>
            </div>
            <div class="modal-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item"> 
                        <a class="nav-link active" data-toggle="tab" href="#docinfo" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Document Info</span></a> 
                    </li>
                    <li class="nav-item"> 
                        <a class="nav-link" data-toggle="tab" href="#home" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Properties</span></a> 
                    </li>
                    <li class="nav-item"> 
                        <a class="nav-link" data-toggle="tab" href="#profile" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Ext Prop</span></a> 
                    </li>
                    <li class="nav-item"> 
                        <a class="nav-link" data-toggle="tab" href="#version-log" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Version Log</span></a> 
                    </li>
                    <li class="nav-item"> 
                        <a class="nav-link" data-toggle="tab" href="#notelist" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Note List</span></a> 
                    </li>
                    <li class="nav-item"> 
                        <a class="nav-link" data-toggle="tab" href="#history" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">History</span></a> 
                    </li>
                </ul>
                <?php 
                    $day = date('D', strtotime(@$ext_properties->tgl_document));
                    $dayList = array(
                        'Sun' => 'Minggu',
                        'Mon' => 'Senin',
                        'Tue' => 'Selasa',
                        'Wed' => 'Rabu',
                        'Thu' => 'Kamis',
                        'Fri' => 'Jumat',
                        'Sat' => 'Sabtu'
                    );
                ?>
                <!-- Tab panes -->
                <div class="tab-content tabcontent-border">
                    <div class="tab-pane p-20 active" id="docinfo" role="tabpanel">
                        <table class="table table-bordered table-striped" width="100%">
                            <tbody>
                               <tr>
                                    <td width="20%">No. Document</td>
                                    <td width="5%" align="center">:</td>
                                    <td width="75%">
                                        <textarea name="no_document" id="info_no_document" class="form-control" cols="30" rows="2"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">Tgl. Document</td>
                                    <td width="5%" align="center">:</td>
                                    <td width="75%"><input type="text" class="form-control datepicker" name="tgl_document" id="info_tgl_document" value=""></td>
                                </tr>
                                <tr>
                                    <td width="20%">Relasi document</td><td width="5%" align="center">:</td>
                                    <td width="75%"><input type="text" class="form-control" name="relasi_document" id="info_relasi_document" value=""></td>
                                </tr>
                                <tr>
                                    <td>Tag</td><td align="center">:</td>
                                    <td>
                                        <select class="form-control select2" name="tag[]" multiple="multiple" data-placeholder="Select tag" style="width: 100%;" required>
                                        @if (!empty($tag))
                                            @foreach ($tag as $row)
                                                <option value="{{$row->id}}">
                                                    {{$row->tag}}
                                                </option>
                                            @endforeach
                                        @endif
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Document Expiration Date</td><td align="center">:</td>
                                    <td><input type="text" class="form-control datepicker" name="document_expiration_date" id="info_document_expiration_date" value=""></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane p-20" id="home" role="tabpanel">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <td width="20%">Name</td>
                                    <td width="5%" align="center">:</td>
                                    <td width="75%"><span class="fill-doc-name"></span></td>
                                </tr>
                                <tr>
                                    <td width="20%">Title</td>
                                    <td width="5%" align="center">:</td>
                                    <td width="75%"><span class="fill-doc-title"></span></td>
                                </tr>
                                <tr>
                                    <td width="20%">Folder</td>
                                    <td width="5%" align="center">:</td>
                                    <td width="75%"><span class="fill-doc-folder"></span></td>
                                </tr>
                                <tr>
                                    <td>Size</td>
                                    <td align="center">:</td>
                                    <td><span class="fill-doc-size"></span></td>
                                </tr>
                                <tr>
                                    <td>File Version</td>
                                    <td align="center">:</td>
                                    <td><span class="fill-doc-version"></span></td>
                                </tr>
                                <tr>
                                    <td>Workflow Status</td>
                                    <td align="center">:</td>
                                    <td>{{@$properties->status}}</td>
                                </tr>
                                <tr>
                                    <td>Created on</td>
                                    <td align="center">:</td>
                                    <td><span class="fill-doc-createdtime"></span> by <span class="fill-doc-createdby"></span></td>
                                </tr>
                                <tr>
                                    <td>Publish on</td>
                                    <td align="center">:</td>
                                    <td><span class="fill-doc-updatedtime"></span> by <span class="fill-doc-updatedby"></span></td>
                                </tr>
                                <tr>
                                    <td>Permalink</td>
                                    <td align="center">:</td>
                                    <td><a href="" class="fill-fileurl">Download</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane p-20" id="profile" role="tabpanel">
                        <form action="{{route(routePrefix().'ext_properties.store')}}" method="post">
                        @csrf
                        <table class="table table-bordered table-striped">
                            <input type="hidden" name="id_file" value="{{@$ext_properties->id_file}}">
                            <input type="hidden" name="relasi_document" value="{{@$ext_properties->file}}">
                            <tbody>
                                <tr>
                                    <td width="20%">No. Document</td>
                                    <td width="5%" align="center">:</td>
                                    <td width="75%">
                                        <textarea name="no_document" id="no_document" class="form-control" cols="30" rows="2"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">Tgl. Document</td>
                                    <td width="5%" align="center">:</td>
                                    <td width="75%"><input type="text" class="form-control datepicker" name="tgl_document" id="tgl_document" value=""></td>
                                </tr>
                                <tr>
                                    <td width="20%">Relasi document</td><td width="5%" align="center">:</td>
                                    <td width="75%"><input type="text" class="form-control" name="relasi_document" id="relasi_document" value=""></td>
                                </tr>
                                <tr>
                                    <td>Tag</td><td align="center">:</td>
                                    <td>
                                        <select class="form-control select2" name="tag[]" multiple="multiple" data-placeholder="Select tag" style="width: 100%;" required>
                                        @if (!empty($tag))
                                            @foreach ($tag as $row)
                                                <option value="{{$row->id}}">
                                                    {{$row->tag}}
                                                </option>
                                            @endforeach
                                        @endif
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Document Expiration Date</td><td align="center">:</td>
                                    <td><input type="text" class="form-control datepicker" name="document_expiration_date" id="document_expiration_date" value=""></td>
                                </tr>
                            </tbody>
                        </table>
                        <button type="submit" class="btn btn-sm btn-primary">Save</button>
                        </form>
                    </div>
                    <div class="tab-pane p-20" id="version-log" role="tabpanel">
                        <table class="table" width="90%">
                            <tbody>
                                <tr bgcolor="#f4f4f4">
                                    <td colspan="2">Last Version</td>
                                </tr>
                                <tr>
                                    <td width="10%">
                                        <label style="background-color:#f1f1f1;padding:5px;"><div id="version"></div></label>
                                    </td>
                                    <td width="80%"><div id="nama_file"></div>
                                        <div style="class:table-cell;float:left;margin-top:10px;">
                                            <img src="https://melkhior.co/new/demo/dms/assets/images/1.jpg" style="width:40px;height:40px">
                                        </div>
                                        <div style="class:table-cell;float:left;margin-left:10px;;margin-top:10px;">
                                            <div id="created_by"></div> - <div id="created_at"></div>(<div id="keterangan"></div>)
                                        </div>
                                    </td>
                                </tr>
                                <tr bgcolor="#f4f4f4">
                                    <td colspan="2">Older Version</td>
                                </tr>
                                @if(!empty($log_version))
                                    @foreach($log_version as $row)
                                    <tr>
                                        <td width="10%">
                                            <label style="background-color:#f1f1f1;padding:5px;">{{$row->version}}</label>
                                        </td>
                                        <td width="80%">{{$row->file}}<br>
                                            <div style="class:table-cell;float:left;margin-top:10px;">
                                                <img src="https://melkhior.co/new/demo/dms/assets/images/1.jpg" style="width:40px;height:40px">
                                            </div>
                                            <div style="class:table-cell;float:left;margin-left:10px;;margin-top:10px;">
                                                {{$row->created_by}} -  {{$row->created_at}}<br>
                                                ({{$row->keterangan}})
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane p-20" id="notelist" role="tabpanel">
                        <table class="table" width="90%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tgl</th>
                                    <th>Writer</th>
                                    <th>Notes</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($notelist))
                                @php $no=1; @endphp
                                @foreach($notelist as $row)
                                <tr>
                                    <td>{{$no}}</td>
                                    <td>{{$row->created_at}}</td>
                                    <td>{{$row->writer}}</td>
                                    <td>{{$row->notes}}</td>
                                </tr>
                                @php $no++; @endphp
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                        <form action="{{ route(routePrefix().'note_list.store') }}" method="post">
                            @csrf
                            <div class="form-group row">
                                <label for="notes" class="col-3 control-label">Notes</label>
                                <div class="col-9">
                                    <input type="text" class="form-control" name="notes" id="notes">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-sm btn-primary">Save</button>
                        </form>
                    </div>
                    <div class="tab-pane p-20" id="history" role="tabpanel">
                        <table class="table" width="90%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tgl</th>
                                    <th>Writer</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tbody>
                                    @if(!empty($history))
                                    @php $no=1; @endphp
                                    @foreach($history as $row)
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$row->created_at}}</td>
                                        <td>{{$row->user}}</td>
                                        <td>{{$row->description}}</td>
                                    </tr>
                                    @php $no++; @endphp
                                    @endforeach
                                    @endif
                                </tbody>
                            </tbody>    
                        </table>
                    </div>
                </div>
                
                <div class="preview-placeholder card-body image-viewer">
                    <img src="" class="image-link" style="max-width: 100%" alt="">
                </div>

                <div class="preview-placeholder card-body pdf-viewer">
                    <div class="pdf-link"></div>
                </div>

                <div class="preview-placeholder card-body txt-viewer">
                    <textarea class="txt-content" rows="30" style="width: 100%"></textarea>
                </div>
                    

                {{-- <div class="iframe"> --}}

                    <!-- <iframe class="doc" src="https://docs.google.com/gview?url=http://localhost/dms/public/documents/1616996140-MRENLIUV4W.pdf&embedded=true" height="500" width="100%" scrolling="auto"></iframe> -->

                    <!-- <embed src="http://localhost/dms/public/documents/1617161760-LI5XCYPXWM.docx" type="application/docx"   height="500px" width="100%"> -->

                        <!-- <iframe src='https://view.officeapps.live.com/op/embed.aspx?src=http://localhost/dms/public/documents/1617161760-LI5XCYPXWM.docx' width='1366px' height='623px' frameborder='0'>This is an embedded <a target='_blank' href='http://office.com'>Microsoft Office</a> document, powered by <a target='_blank' href='http://office.com/webapps'>Office Online</a>.</iframe> -->
                    {{-- <iframe src ="public/plugins/ViewerJS/#1616996140-MRENLIUV4W.pdf" width='400' height='300' allowfullscreen webkitallowfullscreen></iframe> --}}
                {{-- </div> --}}
            </div>
            <div class="modal-footer">
                <!-- <button onclick="preview()" type="button" class="btn btn-primary">Preview</button> -->
                <a href="" class="btn btn-success" id="sales-approve">Approved</a>
                <a href="" class="btn btn-danger" id="sales-reject">Reject</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalApprove" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Approve Workflow</h4>
            </div>
            <form action="{{urlWithPrefix('/workflow/approve')}}" method="post">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="id" id="id_workflow">
                    <div class="row">
                        <div class="col-md-6">Pin</div>
                        <div class="col-md-6"><input type="text" name="pin" id="" class="form-control"></div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-md-6">Status</div>
                        <div class="col-md-6">
                            <select name="status" id="" class="form-control select2me">
                                <option value="1">Approve</option>
                                <option value="2">Reject</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Workflow Info</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4"><b>Nama</b></div>
                    <div class="col-md-8" id="nama"></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Description</b></div>
                    <div class="col-md-8" id="description"></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Directory</b></div>
                    <div class="col-md-8" id="directory"></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Created At</b></div>
                    <div class="col-md-8" id="created_at"></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Created By</b></div>
                    <div class="col-md-8" id="created_by"></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>User Approval</b></div>
                    <div class="col-md-8" id="user_approval"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        var modalInfo = $('#modalInfo');
        var user_approval = $("#user_approval");
        $('.workflow-info').on('click', function () {
            $.ajax({
                url: "{{route(routePrefix().'workflow-getWorkflowInfo')}}",
                method: "POST",
                data: {
                    id_workflow : $(this).attr('data-id')
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    modalInfo.modal('show');
                    modalInfo.find('#nama').text(data.data.nama);
                    modalInfo.find('#description').text(data.data.description);
                    modalInfo.find('#directory').text(data.data.nama_folder);
                    modalInfo.find('#created_at').text(new Date(data.data.created_at));
                    modalInfo.find('#created_by').text(data.data.created_by);
                    $.each(data.data.workflow_approval, function (i, v) {
                        // console.log(v.member)
                        user_approval.append("- "+v.member+"<br>")
                    })
                }
            })
        })

        modalInfo.on('hidden.bs.modal', function () {
            user_approval.empty();
        })

        $('#example').DataTable({
            "bLengthChange": false,
            "bFilter": false,
        })
    </script>
@endsection
