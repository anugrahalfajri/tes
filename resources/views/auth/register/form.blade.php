<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon icon -->
    <!-- <link rel="icon" type="image/png" sizes="16x16" href="{{url('images/favicon.png')}}"> -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/images/apple-touch-icon.png.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/images/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/images/favicon-16x16.png')}}">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="{{asset('assets/images/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <title>MELKHIOR - DODI</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{url('plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('plugins/select2/dist/css/select2.min.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{url('material/css/style.css')}}" rel="stylesheet">
</head>

<style>
    .button{
        padding: 0;
        color: #007bff;
        border: 0;
        background-color: transparent;
        cursor: pointer;
        font-size: 13px;
        margin-left: 20px;
    }
    .button:focus {
        outline: 0;
    }
    .preview-placeholder {
        background: white;
        border: 1px solid #ddd;
        display: none;
    }
    .image-viewer {
        min-height: 30rem;
    }

    .ui-icon.ui-igtreegrid-expansion-indicator.ui-icon-minus {
        background: url(/images/samples/tree-grid/opened_folder.png) !important;
        background-repeat: no-repeat;
    }
    .ui-icon.ui-igtreegrid-expansion-indicator.ui-icon-plus {
        background: url(/images/samples/tree-grid/folder.png) !important;
        background-repeat: no-repeat;
    }
	.ui-icon-plus:before {
	    content: '' !important;
	}
	.ui-icon-minus:before{
	    content: '' !important;
	}

    .dropzone{
        border: 2px dashed rgba(0, 0, 0, 0.3) !important;
        background: white;
        border-radius: 5px;
        min-height: 145px;
        vertical-align: baseline;
    }
</style>

<body>
    <div class="container">
        <div class="container-fluid mt-5">
            <div class="row">
                <div class="col-lg-12 col-xlg-12 col-md-12">
                    <div class="card">
                        <div class="card-body bg-megna">
                            <div class="d-flex justify-content-between">
                                <h4 class="text-white card-title">Register User</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{url('/guest/register')}}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-md-3">Name</div>
                                    <div class="col-md-9"><input type="text" name="name" id="" value="{{@$users->name}}" class="form-control" placeholder="Enter Name"></div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3">Email</div>
                                    <div class="col-md-9"><input type="email" name="email" id="" class="form-control" value="{{@$users->email}}" placeholder="Enter Email"></div>
                                </div>
                                <br>
                                <div class="row">
                                  <div class="col-md-3">Phone</div>
                                  <div class="col-md-9"><input type="text" name="phone" id="phone" value="{{@$users->phone}}" class="form-control" placeholder="Enter Phone"></div>
                                </div>
                                <br>
                                <div class="row">
                                  <div class="col-md-3">Organization</div>
                                  <div class="col-md-9">
                                      <select name="id_organization" id="id_organization" class="form-control select2">
                                            @if(!empty($organizations))
                                                @foreach($organizations as $row)
                                                    <option value="{{$row->id}}" {{($row->id == @$users->id_organization)?'selected':''}}>{{$row->name}}</option>
                                                @endforeach
                                            @endif
                                      </select>
                                  </div>
                                </div>
                                <br>
                                <button type="submit" class="btn btn-primary">Confirm</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Email</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                Please confirm your email
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
        </div>
    </div>

    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{url('plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{url('plugins/bootstrap/js/popper.min.js')}}"></script>
    <script src="{{url('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    
    <script src="{{url('plugins/select2/dist/js/select2.min.js')}}"></script>
    <script>
        $(function () {
            $('.select2').select2({});
        });
    </script>

    @if( Session::has('confirm'))
    <script>
        $(document).ready(function() {
            $('#modal-confirm').modal('show');
        });
    </script>
    @endif
</body>
</html>