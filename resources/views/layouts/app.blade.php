<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon icon -->
    <!-- <link rel="icon" type="image/png" sizes="16x16" href="{{url('images/favicon.png')}}"> -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/images/apple-touch-icon.png.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/images/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/images/favicon-16x16.png')}}">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="{{asset('assets/images/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <title>XIMPLI Doc</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{url('plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('plugins/datatables-responsive/css/responsive.bootstrap.scss')}}" rel="stylesheet">
    <link href="{{url('plugins/datatables-responsive/css/responsive.dataTables.scss')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">

    <!-- Ignite UI for jQuery Required Combined CSS files -->
    <link href="https://cdn-na.infragistics.com/igniteui/2020.2/latest/css/themes/infragistics/infragistics.theme.css" rel="stylesheet" />
    <link href="https://cdn-na.infragistics.com/igniteui/2020.2/latest/css/structure/infragistics.css" rel="stylesheet" />
    
    <!-- chartist CSS -->
    <link href="{{url('plugins/chartist-js/dist/chartist.min.css')}}" rel="stylesheet">
    <link href="{{url('plugins/dropzone-master/dist/min/dropzone.min.css')}}" rel="stylesheet">
    <link href="{{url('plugins/select2/dist/css/select2.min.css')}}" rel="stylesheet">
    <link href="{{url('plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{url('plugins/chartist-js/dist/chartist-init.css')}}" rel="stylesheet">
    <link href="{{url('plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css')}}" rel="stylesheet">
    <!--This page css - Morris CSS -->
    <link href="{{url('plugins/c3-master/c3.min.css')}}" rel="stylesheet">
    <link href="{{url('plugins/switchery/dist/switchery.min.css')}}" rel="stylesheet" />

    <!-- Custom CSS -->
    @if(auth()->user()->role->name == 'user')
    <link href="{{url('material/pro/css/style.css')}}" rel="stylesheet">
    <link href="{{url('material/pro/css/checkbox.css')}}" rel="stylesheet">
    <link href="{{url('material/pro/css/w3/style.css')}}" rel="stylesheet">
    <link href="{{url('material/pro/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
    @elseif(auth()->user()->role->name == 'master admin')
    <link href="{{url('material/master-admin/css/style.css')}}" rel="stylesheet">
    @else
    <link href="{{url('material/css/style.css')}}" rel="stylesheet">
    @endif
    <link href="{{url('material/css/folders.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.9.0/viewer.css">
    <!-- You can change the theme colors from here -->
    <link href="{{url('material/css/colors/blue.css')}}" id="theme" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/7.1.96/css/materialdesignicons.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<style>
    .mandatory:after {
        color:red;
        content: ' *';
    }
    .button{
        padding: 0;
        color: #007bff;
        border: 0;
        background-color: transparent;
        cursor: pointer;
        font-size: 13px;
        margin-left: 20px;
    }
    .button:focus {
        outline: 0;
    }
    .preview-placeholder {
        background: white;
        border: 1px solid #ddd;
        display: none;
    }
    .image-viewer {
        min-height: 30rem;
    }

    .ui-icon.ui-igtreegrid-expansion-indicator.ui-icon-minus {
        background: url(/images/samples/tree-grid/opened_folder.png) !important;
        background-repeat: no-repeat;
    }
    .ui-icon.ui-igtreegrid-expansion-indicator.ui-icon-plus {
        background: url(/images/samples/tree-grid/folder.png) !important;
        background-repeat: no-repeat;
    }
	.ui-icon-plus:before {
	    content: '' !important;
	}
	.ui-icon-minus:before{
	    content: '' !important;
	}

    .dropzone{
        border: 2px dashed rgba(0, 0, 0, 0.3) !important;
        background: white;
        border-radius: 5px;
        min-height: 145px;
        vertical-align: baseline;
    }

    /**
    * CSS chat
    */
    .bubble-left,
    .bubble-right {
        line-height: 100%;
        display: block;
        position: relative;
        padding: .25em .5em;
        -webkit-border-radius: 11px;
        -moz-border-radius: 11px;
        border-radius: 11px;
        margin-bottom: 2em;
        clear: both;
    }
    .bubble-left {
        float: left;       
        margin-right:10%;
        text-align: left;
    }
    .bubble-right {
        float:right;
        margin-left:10%;
        text-align: right;
    }
</style>

{{-- horizontal menu --}}
<style>
@import url(https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,700,800);


article,aside,figure,footer,header,hgroup,nav,section {
    display: block;
}
/* 
a {
    color: #BA0707;
    text-decoration: none;
} */

a:hover {
    color: #BA0707;
}

nav {
    display: block;
    font: 14px "Open Sans",Helvetica,Arial,sans-serif;
    -webkit-font-smoothing: antialiased;
    line-height: 1;
    width: 100%;
    /* background: #374147; */
}

.menu {
    display: block;
}

.menu li {
    display: inline-block;
    position: relative;
    z-index: 100;
}

.menu li:first-child {
    margin-left: 0;
}

.menu li a {
    font-weight: 600;
    text-decoration: none;
    padding: 20px 15px;
    display: block;
    color: #fff;
    transition: all 0.2s ease-in-out 0s;
}

.menu li a:hover,.menu li:hover>a {
    color: #fff;
    background: #9ca3da;
}

.menu ul {
    visibility: hidden;
    opacity: 0;
    margin: 0;
    padding: 0;
    width: 170px;
    position: absolute;
    left: 0px;
    background: #fff;
    z-index: 99;
    transform: translate(0,20px);
    transition: all 0.2s ease-out;
}

.menu ul:after {
    bottom: 100%;
    left: 20%;
    border: solid transparent;
    content: " ";
    height: 0;
    width: 0;
    position: absolute;
    pointer-events: none;
    border-color: rgba(255, 255, 255, 0);
    border-bottom-color: #fff;
    border-width: 6px;
    margin-left: -6px;
}

.menu ul li {
    display: block;
    float: none;
    background: none;
    margin: 0;
    padding: 0;
}

.menu ul li a {
    font-size: 12px;
    font-weight: normal;
    display: block;
    color: #797979;
    background: #fff;
}

.menu ul li a:hover,.menu ul li:hover>a {
    background: #9ca3da;
    color: #fff;
}

.menu li:hover>ul {
    visibility: visible;
    opacity: 1;
    transform: translate(0,0);
}

.menu ul ul {
    left: 169px;
    top: 0px;
    visibility: hidden;
    opacity: 0;
    transform: translate(20px,20px);
    transition: all 0.2s ease-out;
}

.menu ul ul:after {
    left: -6px;
    top: 10%;
    border: solid transparent;
    content: " ";
    height: 0;
    width: 0;
    position: absolute;
    pointer-events: none;
    border-color: rgba(255, 255, 255, 0);
    border-right-color: #fff;
    border-width: 6px;
    margin-top: -6px;
}

.menu li>ul ul:hover {
    visibility: visible;
    opacity: 1;
    transform: translate(0,0);
    z-index: 9999;
}

.responsive-menu {
    display: none;
    width: 100%;
    padding: 20px 15px;
    /* background: #374147; */
    color: #fff;
    text-transform: uppercase;
    font-weight: 600;
}

.responsive-menu:hover {
    background: #374147;
    color: #fff;
    text-decoration: none;
}

a.homer {
    background: #9ca3da;
}

@media (min-width: 768px) and (max-width: 979px) {
    .mainWrap {
        width: 768px;
    }

    .menu ul {
        top: 37px;
    }

    .menu li a {
        font-size: 12px;
    }

    a.homer {
        background: #374147;
    }
}

@media (max-width: 767px) {
    .mainWrap {
        width: auto;
        padding: 50px 20px;
    }

    .menu {
        display: none;
    }

    .responsive-menu {
        display: block;
    }

    nav {
        margin: 0;
        background: none;
    }

    .menu li {
        display: block;
        margin: 0;
    }

    .menu li a {
        background: #fff;
        color: #797979;
    }

    .menu li a:hover,.menu li:hover>a {
        background: #9ca3da;
        color: #fff;
    }

    .menu ul {
        visibility: hidden;
        opacity: 0;
        top: 0;
        left: 0;
        width: 100%;
        transform: initial;
    }

    .menu li:hover>ul {
        visibility: visible;
        opacity: 1;
        position: relative;
        transform: initial;
    }

    .menu ul ul {
        left: 0;
        transform: initial;
    }

    .menu li>ul ul:hover {
        transform: initial;
    }
}

@media (max-width: 480px) {
}

@media (max-width: 320px) {
}
div.scrollmenu {
  overflow: auto;
  white-space: nowrap;
}

div.scrollmenu a {
  display: inline-block;
  color: white;
  text-align: center;
  padding: 14px;
  text-decoration: none;
}
#scrollGroup{
    overflow-y:scroll;
    max-height:500px; 
}
#rightClickFile {
  position: absolute;
  display: none;
}
#folder-container:hover{
  background:#c9e8f7;
  position:relative;
}
.text-themecolor {
    color: #67757c !important;
}
</style>

@if (auth()->user()->role->name == 'user')
<style>
    .nav-item a, .nav-item button {
        color: #607d8b !important;
        width: 125px !important;
        white-space: nowrap !important;
        overflow: hidden !important;
        text-overflow: ellipsis !important;
        font-size: 13px !important;
    }
    .w3-sidebar {
        margin-top: 125px;
        padding-bottom: 150px;
    }
    #close-sidebar-folder {
        float: right;
        cursor: pointer;
    }
    @media (max-width: 1000px) {
        .w3-sidebar {
            margin-top: 70px;
        }
    }
    .w3-opennav {
        cursor: pointer;
        color: #fff;
    }
    #toggleSidebar {
        float: right;
    }
    #toggleSidebar i {
        font-size: 30px;
        position: fixed;
        background-color: #fff;
    }
    .text-themecolor {
        color: #67757c !important;
    }
    .bg-megna {
        background-color: #00897b !important;
    }
    .popover {
        display: none;
    }
    @media screen and (min-width: 800px) {
        .navbar-brand {
            /*margin-left: -48px !important;*/
        }
    }
    .caret {
        cursor: pointer;
        -webkit-user-select: none; /* Safari 3.1+ */
        -moz-user-select: none; /* Firefox 2+ */
        -ms-user-select: none; /* IE 10+ */
        user-select: none;

    }
    .caret::before {
        content: "\25B6";
        font-size: 13px;
        position: relative;
        font-weight: 600;
        top: -.2em;
        margin-right: 3px;
    }
    .caret-down::before {
        content: "\25BC";
        font-size: 0.7rem;
        position: relative;
        top: -.2em;
        margin-right: 3px;
    }
    .mdi-folder:before {
        margin-right: 6px !important;
        font-size: 1.3rem !important;
        position: relative;
        bottom: -.1em;
    }
    .navbar-nav {
        padding-left: 12px !important;
    }
    .sidebar-nav > ul > li.active > a {
        background-color: #00897b !important;
    }
    .nav-pills .nav-link.active {
        color: #fff !important;
    }
</style>
@endif

<body class="fix-header fix-sidebar card-no-border">
        <input type="hidden" class="baseurl" value="{{ URL::to('/') }}">
        @include('include.material.header')

        @include('include.material.sidebar')

        @yield('content')

        {{-- @include('include.material.rightsidebar') --}}

        @include('include.material.footer')
        @include('include.material.modal')

        @include('include.material.js')

        @yield('scripts')
</body>
</html>