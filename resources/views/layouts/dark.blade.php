<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <!-- Favicon icon -->
        <link
            rel="icon"
            type="image/png"
            sizes="16x16"
            href="{{url('images/favicon.png')}}"
        />
        <title>
            MELKHIOR - DODI
        </title>
        <!-- Bootstrap Core CSS -->
        <link
            href="{{url('plugins/bootstrap/css/bootstrap.min.css')}}"
            rel="stylesheet"
        />
        <!-- chartist CSS -->
        <link
            href="{{url('plugins/chartist-js/dist/chartist.min.css')}}"
            rel="stylesheet"
        />
        <link
            href="{{url('plugins/chartist-js/dist/chartist-init.css')}}"
            rel="stylesheet"
        />
        <link
            href="{{url('plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css')}}"
            rel="stylesheet"
        />
        <!--This page css - Morris CSS -->
        <link href="{{url('plugins/c3-master/c3.min.css')}}" rel="stylesheet" />
        <!-- Custom CSS -->
        <link href="{{url('dark/css/style.css')}}" rel="stylesheet" />
        <!-- You can change the theme colors from here -->
        <link href="{{url('dark/css/colors/green-dark.css')}}" id="theme" rel="stylesheet" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="fix-header fix-sidebar card-no-border">
        @include('include.dark.header')

        @include('include.dark.sidebar')

        @yield('content')

        @include('include.dark.rightsidebar')

        @include('include.dark.footer')

        @include('include.dark.js')
    </body>
</html>