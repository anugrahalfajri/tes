@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">Pending User</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Pending User</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="d-flex justify-content-between">
                            <h4 class="text-white card-title">Pending User - Detail</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{urlWithPrefix('/pending_user/update', $users->id)}}" method="post">
                        @method('PUT')
                        @csrf
                          <div class="row">
                              <div class="col-md-3">Name</div>
                              <div class="col-md-9"><input type="text" name="name" id="" value="{{@$users->name}}" class="form-control" readonly></div>
                          </div>
                          <br>
                          <div class="row">
                              <div class="col-md-3">Email</div>
                              <div class="col-md-9"><input type="text" name="email" id="" class="form-control" value="{{@$users->email}}" readonly></div>
                          </div>
                          <br>
                          <div class="row">
                              <div class="col-md-3">Role</div>
                              <div class="col-md-9">
                                  <select name="role_id" id="role_id" class="form-control select2">
                                        @if(!empty($roles))
                                            @foreach($roles as $row)
                                                <option value="{{$row->id}}">{{$row->name}}</option>
                                            @endforeach
                                        @endif
                                  </select>
                              </div>
                          </div>
                          <br>
                          <div class="row">
                              <div class="col-md-3">Level</div>
                              <div class="col-md-9">
                                  <select name="level" id="level" class="form-control select2">
                                       <option value="1">1</option>
                                       <option value="2">2</option>
                                  </select>
                              </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-md-3">Phone</div>
                            <div class="col-md-9"><input type="text" name="phone" id="phone" value="{{@$users->phone}}" class="form-control" readonly></div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-md-3">Organization</div>
                            <div class="col-md-9">
                                <select name="id_organization" id="id_organization" class="form-control select2" disabled>
                                      @if(!empty($organizations))
                                          @foreach($organizations as $row)
                                              <option value="{{$row->id}}" {{($row->id == @$users->id_organization)?'selected':''}}>{{$row->name}}</option>
                                          @endforeach
                                      @endif
                                </select>
                            </div>
                          </div>
                          <br>
                          <div class="row">
                              <div class="col-md-3">Parent</div>
                              <div class="col-md-9">
                                  <select name="parent" id="parent" class="form-control select2">
                                        <option value="">Pilih Parent</option>
                                        @if(!empty($parents))
                                            @foreach($parents as $row)
                                                <option value="{{$row->id}}">{{$row->name}}</option>
                                            @endforeach
                                        @endif
                                  </select>
                              </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-md-3">Join Date</div>
                            <div class="col-md-9"><input type="text" name="join_date" id="join_date" value="{{@$users->join_date}}" class="form-control datepicker" readonly></div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-md-3">Approval</div>
                            <div class="col-md-9">
                                <select name="status" id="status" class="form-control select2">
                                     <option value="2">Approve</option>
                                     <option value="0">Reject</option>
                                </select>
                            </div>
                          </div>
                          <br>
                          <div align="right">
                            <button type="submit" class="btn btn-primary">Submit</a>
                            <button type="reset" class="btn btn-danger  ml-5" id="reset-user">Reset</a>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
