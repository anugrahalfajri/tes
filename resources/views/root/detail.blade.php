@extends('layouts.app')

@section('content')
<div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 col-8 align-self-center">
                    <h3 class="text-themecolor">Dashboard</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <div class="col-md-7 col-4 align-self-center">
                    <div class="d-flex m-t-10 justify-content-end">
                        <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                            <div class="chart-text m-r-10">
                                <h6 class="m-b-0"><small>THIS MONTH</small></h6>
                                <h4 class="m-t-0 text-info">$58,356</h4></div>
                            <div class="spark-chart">
                                <div id="monthchart"></div>
                            </div>
                        </div>
                        <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                            <div class="chart-text m-r-10">
                                <h6 class="m-b-0"><small>LAST MONTH</small></h6>
                                <h4 class="m-t-0 text-primary">$48,356</h4></div>
                            <div class="spark-chart">
                                <div id="lastmonthchart"></div>
                            </div>
                        </div>
                        <div class="">
                            <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                    <div class="col-md-12">
                        <div class="card detail">
                            <div class="card-body">
                                <h4 class="card-title">Detail - Lunar probe project</h4>
                                <h6 class="card-subtitle">Use default tab with class</h6>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Properties</span></a> </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Ext. Prop</span></a> </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Version Log</span></a> </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#notelist" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Note List</span></a> </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">History</span></a> </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Access Right</span></a> </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Work Flow</span></a> </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content tabcontent-border">
                                    <div class="tab-pane active" id="home" role="tabpanel">
                                        <table class="table table-bordered table-striped" style="margin-left:20px;margin-top:12px;width:90%"><tbody><tr><td width="20%">Name</td><td width="5%" align="center">:</td><td width="75%">Lunar probe project.pdf</td></tr><tr><td width="20%">Title</td><td width="5%" align="center">:</td><td width="75%">Lunar probe project.pdf</td></tr><tr><td width="20%">Folder</td><td width="5%" align="center">:</td><td width="75%"><a href="#">/Root</a></td></tr><tr><td>Size</td><td align="center">:</td><td>14.6 KB(14.973 bytes)</td></tr><tr><td>File Version</td><td align="center">:</td><td>1.2 (1.2)</td></tr><tr><td>Workflow Status</td><td align="center">:</td><td>The End</td></tr><tr><td>Created on</td><td align="center">:</td><td>10/04/2020 10.30 By Admin</td></tr><tr><td>Publish on</td><td align="center">:</td><td>20/04/2020 10.30 By Admin</td></tr><tr><td>Permanelink</td><td align="center">:</td><td><a href="#">Download</a> / <a href="#">Details</a></td></tr></tbody></table>
                                    </div>
                                    <div class="tab-pane  p-20" id="profile" role="tabpanel">
                                        <table class="table table-bordered table-striped" style="margin-left:20px;margin-top:12px;width:90%"><tbody><tr><td width="20%">No. Document</td><td width="5%" align="center">:</td><td width="75%">ADMIN/VII/2020/01</td></tr><tr><td width="20%">Tgl. Document</td><td width="5%" align="center">:</td><td width="75%">Mon, 20 Apr 2020</td></tr><tr><td width="20%">Relasi document</td><td width="5%" align="center">:</td><td width="75%">Lunar probe project.xlxs</td></tr><tr><td>Tag</td><td align="center">:</td><td>Katalog, Spesifikasi</td></tr><tr><td>Document Expiration Date</td><td align="center">:</td><td>31 Dec 2020</td></tr></tbody></table>
                                    </div>
                                    <div class="tab-pane p-20" id="messages" role="tabpanel">
                                        <table class="table" width="90%"><tbody><tr bgcolor="#f4f4f4"><td colspan="2">Last Version</td></tr><tr><td width="10%"><label style="background-color:#f1f1f1;padding:5px;">1.2</label></td><td width="80%">Lunar probe project.pdf<br><div style="class:table-cell;float:left;margin-top:10px;"><img src="https://melkhior.co/new/demo/dms/assets/images/1.jpg" style="width:40px;height:40px"></div><div style="class:table-cell;float:left;margin-left:10px;;margin-top:10px;">Admin -  20 Apr 2020 : 10:30<br>(Penambahan katalog)</div></td></tr><tr bgcolor="#f4f4f4"><td colspan="2">Older Version</td></tr><tr><td width="10%"><label style="background-color:#f1f1f1;padding:5px;">1.1</label></td><td width="80%">Lunar probe project.pdf<br><div style="class:table-cell;float:left;margin-top:10px;"><img src="https://melkhior.co/new/demo/dms/assets/images/1.jpg" style="width:40px;height:40px"></div><div style="class:table-cell;float:left;margin-left:10px;;margin-top:10px;">Admin -  20 Apr 2020 : 10:30<br>(Tidak ada komentar)</div></td></tr><tr><td width="10%"><label style="background-color:#f1f1f1;padding:5px;">1.0</label></td><td width="80%">Lunar probe project.pdf<br><div style="class:table-cell;float:left;margin-top:10px;"><img src="https://melkhior.co/new/demo/dms/assets/images/1.jpg" style="width:40px;height:40px"></div><div style="class:table-cell;float:left;margin-left:10px;;margin-top:10px;">Admin -  10 Apr 2020 : 10:30<br>(Tidak ada komentar)</div></td></tr></tbody></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
</div>
@endsection