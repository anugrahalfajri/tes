@foreach ($logs ?? [] as $row)
<tr class="version_log_list">
    <td width="10%">
        <label style="background-color:#f1f1f1;padding:5px;">{{$row->version}}</label>
    </td>
    <td width="80%"> <a href="javascript:;" data-toggle='popover' data-trigger='focus' data-title="Detail Versi" data-content="<b>Tanggal Upload : </b> {{ date('d/m/Y H:i:s', strtotime($row->created_at)) }} <br> <b>Uploader : </b> {{ $row->created_by ?? '-' }}"><i class="fa fa-solid fa-circle-exclamation"></i></a>
        <span data-toggle='popover' data-trigger='focus' data-content='{{ $row->uploaded_file->nama_file }}'>{{ \Illuminate\Support\Str::limit($row->uploaded_file->nama_file, $limit = 25, $end = ' ...') ?? '-'}}</span><br>
        <div style="class:table-cell;float:left;margin-top:10px;">
            @isset($row->uploaded_file->nama_file)
                @php
                    $path = str_replace("public/", "", $row->uploaded_file->path_file);
                @endphp

                <img src="{{ url('/') . $path . $row->uploaded_file->nama_file }}" style="width:40px;height:40px">
            @else
                <img src="https://melkhior.co/new/demo/dms/assets/images/1.jpg" style="width:40px;height:40px">
            @endisset
            
           
        </div>
        <div style="class:table-cell;float:left;margin-left:10px;;margin-top:10px;">
            {{$row->created_by}} -  {{$row->created_at}}<br>
            ({{$row->keterangan}})
        </div>
    </td>
</tr>
@endforeach
