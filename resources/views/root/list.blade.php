@extends('layouts.app')

@section('content')
<style>
    [type="checkbox"]:not(:checked), [type="checkbox"]:checked {
        position: unset !important;
        opacity: 1 !important;
        width: 20px !important;
    }
    [type="checkbox"] {
        display: inline-block !important;
        vertical-align: middle !important;
    }
    [type="checkbox"] + label {
        padding-left: 8px !important;
    }
    [type="checkbox"] + label::before {
        display: none !important;
    }
    #example_history_paginate {
        float: unset !important;
    }
    .rel-body {
        max-width: 100%;
        overflow-x: auto;
    }
    .scoll-tree {
        width:2000px;
    }
</style>
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">Document Explore</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Document Explore</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        @if (\Session::has('success_msg'))
            <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <ul>
                    <li>{!! \Session::get('success_msg') !!}</li>
                </ul>
            </div>
        @endif
        @if (\Session::has('failed_msg'))
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <ul>
                    <li>{!! \Session::get('failed_msg') !!}</li>
                </ul>
            </div>
        @endif
        <div class="row">
            @if(auth()->user()->role_id != 2)
            <div class="col-lg-3 col-xlg-3 col-md-3">
                <div class="card">
                    <div class="card-body bg-megna">
                        <h4 class="text-white card-title">Directory</h4>
                        <h6 class="card-subtitle text-white m-b-0 op-5">Directory Folder</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <h2 class="add-ct-btn"><button type="button" class="btn btn-circle btn-lg btn-success waves-effect waves-dark" data-toggle="modal" data-target="#directoryModal">+</button></h2>
                            <ul class="navbar-nav">
                                @if(!empty($parent_directory))
                                @foreach($parent_directory as $row)
                                <li class="nav-item dropdown">
                                    <a class="nav-link demo" data-toggle="collapse-{{$row->id}}" href="#demo" data-id="{{$row->id}}" data-folder="{{$row->nama_folder}}" aria-controls="collapseExample">
                                        <i class="mdi mdi-folder"></i> {{$row->nama_folder}}
                                    </a>
                                </li>
                                <div id="demo" class="collapse-{{$row->id}}">
                                   <div class="sub-list-{{$row->id}}"></div>
                                </div>
                                @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @else
            
                
            @endif

            @if (auth()->user()->role_id == 2)
                <div class="col-lg-8 col-xlg-8 col-md-8 col-lg-12 card-dynamic file">
            @else
                <div class="col-lg-5 col-xlg-3 col-md-5 file">
            @endif
                <div class="card">
                    <div class="card-body file-list">
                        
                    
                    </div>
                </div>
            </div>

            @include('root.detail_properties')
        </div>
    </div>
</div>

<div class="modal fade" id="modalAddAccess" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Access</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>User</th>
                            <th class="text-center">Read</th>
                            <th class="text-center">Create</th>
                            <th class="text-center">Edit</th>
                            <th class="text-center">Delete</th>
                            <th class="text-center">Download</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <input type="hidden" name="file_upload_id" id="file_upload_id">
                            <td>
                                <select name="user" id="user" class="form-control">
                                    <option value="">Pilih User</option>
                                    @foreach ($users as $user)
                                        <option value="{{$user->name}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger" id="user_error"></span>
                            </td>
                            <td class="text-center"><input type="checkbox" name="read" id="read" class="form-control"></td>
                            <td class="text-center"><input type="checkbox" name="create" id="create" class="form-control"></td>
                            <td class="text-center"><input type="checkbox" name="edit" id="edit" class="form-control"></td>
                            <td class="text-center"><input type="checkbox" name="delete" id="delete" class="form-control"></td>
                            <td class="text-center"><input type="checkbox" name="download" id="download" class="form-control"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="submit-add-access">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-sm" id="relateModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Relasi Dokumen</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <div class="alert alert-success alert-dismissible" id="msg-relation-green" style="display: none;">
                <p id="alert-relation-green"></p>
            </div>
            <div class="alert alert-danger alert-dismissible" id="msg-relation-red" style="display: none;">
                <p id="alert-relation-red"></p>
            </div>
            <br>

            <div id="relate-dokumen-body"></div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="relationTree" tabindex="-1" role="dialog" aria-labelledby="XlModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Struktur Relasi Dokumen</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body rel-body">

            <div class="scoll-tree">
                <div id="relationTree-body"></div>
            </div>
        </div>
        <div><br><br><h6 style="color:red;">*Scroll kursor untuk melihat content</h6></div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

@endsection

@section('scripts')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    var user = '{{auth()->user()->name}}';
    var tableAccessRight = $("#table-access-right tbody");
    var tableHistory = $("#example_history");
    
    tableHistory.DataTable({searching: false, info: false, lengthChange: false,  pagingType: "simple"});

    $(document).on('click', '.btn-info', function () {
        var status = $(this).attr('data-status');
        var id_file = $(this).attr('data-id');
        $('#file_upload_id').val(id_file);
        tableAccessRight.empty();
        $("#example_history tbody").remove();
        $.ajax({
            url: "{{route(routePrefix().'root.getAccessRight')}}",
            method: "POST",
            data: {
                file_upload_id : id_file
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $.each(data.access_right, function (i, v) {
                    tableAccessRight.append('<tr>'
                        +    '<td>'+(i+1)+'</td>'
                        +    '<td>'+v.user+'</td>'
                        +    '<td>'+(v.read == 't' ? '<i class="mdi mdi-check"></i>' : '<i class="fa fa-ban"></i>' )+'</td>'
                        +    '<td>'+(v.create == 't' ? '<i class="mdi mdi-check"></i>' : '<i class="fa fa-ban"></i>' )+'</td>'
                        +    '<td>'+(v.edit == 't' ? '<i class="mdi mdi-check"></i>' : '<i class="fa fa-ban"></i>' )+'</td>'
                        +    '<td>'+(v.delete == 't' ? '<i class="mdi mdi-check"></i>' : '<i class="fa fa-ban"></i>' )+'</td>'
                        +    '<td>'+(v.download == 't' ? '<i class="mdi mdi-check"></i>' : '<i class="fa fa-ban"></i>' )+'</td>'
                        +    '<td class="table-access-right-approval" style="display:none;"><i class="mdi mdi-check"></i></td>'
                        +'</tr>')
                })
                if (status == 'workflow') {
                    $('.table-access-right-approval').show();
                } else {
                    $('.table-access-right-approval').hide();
                }
            }
        })

        $.ajax({
            url: "{{route(routePrefix().'root.getAccessRightDownload')}}",
            method: "POST",
            data: {
                file_upload_id : id_file,
                user: user
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                if (data.access_right == 't') {
                    $('#permalink-download').show();
                } else {
                    $('#permalink-download').hide();
                }
            }
        })

        $.ajax({
            url: "{{route(routePrefix().'root.getDataFile')}}",
            method: "POST",
            data: {
                file_upload_id : id_file,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
                $('#permalink-download').attr('href', "{{url('documents')}}"+'/'+data.file_upload.nama_file);
            }
        })

        $.ajax({
            url: "{{route(routePrefix().'root.getHistory')}}",
            method: "POST",
            data: {
                file_upload_id : id_file
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $.each(data.history, function (i, v) {
                    tableHistory.append('<tbody><tr>'
                        +    '<td>'+v.history_date+'</td>'
                        +    '<td>'+v.name+'</td>'
                        +    '<td>'+v.description+'</td>'
                        +'</tr></tbody>')
                })
            }
        })
    })

    var modalAddAccess = $('#modalAddAccess');
    $('.add-access').on('click', function () {
        modalAddAccess.modal('show');
    })

    $('#submit-add-access').on('click', function () {
        if ($('#user').val() == '') {
            $('#user_error').text('User required');
        } else {
            $.ajax({
                url: "{{route(routePrefix().'root.storeAccessRight')}}",
                method: "POST",
                data: {
                    file_upload_id : $('#file_upload_id').val(),
                    user : $('#user').val(),
                    read : $('#read').is(':checked'),
                    create : $('#create').is(':checked'),
                    edit : $('#edit').is(':checked'),
                    delete : $('#delete').is(':checked'),
                    download : $('#download').is(':checked')
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    modalAddAccess.modal('hide');
                    if (data.code == 200) {
                        Swal.fire('Success!', data.message, 'success').then((result) => {
                            location.reload();
                        });
                    } else {
                        Swal.fire('Error!', data.message, 'error').then((result) => {
                            location.reload();
                        });
                    }
                }
            })
        }
    })

    $('#assignwf-submit').click(function (e) { 
        e.preventDefault();
       
        $.ajax({
            type: "POST",
            url: "{{ route('document.workflow') }}",
            data: {fileid:$('#file_id_workflow').val(),assign:$("#assign_wf").is(":checked"), workflows:$('#workflows_value').val()},
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            success: function (res) {
                Swal.fire('Data Saved!')
            },
            error: function(data){
                alert("Oops, something went wrong");
            } 
        });
    });

</script>
@endsection