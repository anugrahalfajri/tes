<h6>Dokumen : <br><b>{{ $primary->nama_file }}</b></h6>
<br>

<form action="{{ route(routePrefix().'documen-relation.store') }}" id="relation-form" method="POST">
    @csrf
    <input type="hidden" name="id" id="id" value="{{ $primary->id }}">
    <div class="input-group mb-3">
        <select name="dokumen" id="dokumen" class="form-control select2" style="width: 100%;" required>
            <option value="">Pilih Dokumen</option>
            @foreach($files->groupBy('directory.nama_folder') ?? [] as $folder => $file)
                <optgroup label="{{ $folder }}">
                    @foreach ($file as $f)
                        <option value="{{ $f->id }}">{{ $f->nama_file }}</option>
                    @endforeach
                </optgroup>
            @endforeach
        </select>
        <div class="input-group-append">
          <button class="btn btn-primary" type="submit">Save</button>
        </div>
    </div>
</form>

<hr><br>
<div class="list-group">
    @forelse ($document as $no => $dok)
        <a href="javascript:;" class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">{{ $dok->foreign->nama_file ?? '<span style="color:red;">Document Not Found</span>' }}</h5>
            <small>
                <span class="badge badge-info badge-pill">{{ $dok->foreign->lastVersion->version ?? '0.0' }}</span>
                <span class="badge badge-danger badge-pill" onclick="hapusRelasi({{$dok->id}})"><i class="fa fa-trash"></i>&nbsp;Hapus</span>
            </small>
            </div>
            <small>{{ $dok->foreign->properties->folder ?? '-' }}</small>
        </a>
    @empty
    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
        <div class="d-flex w-100 justify-content-between">
        <h5 class="mb-1"></h5>
        <small></small>
        </div>
        <p class="mb-1">No Relation found.</p>
        <small></small>
    </a>
    @endforelse
</div>

<script>
$(document).ready(function () {
    $('.select2').select2({});

    $("#relation-form").submit(function (event) {

        $.ajax({
            type: $( this ).attr('method'),
            url: $( this ).attr('action'),
            data: $( this ).serialize(),
        }).done(function (data) {
            $('#msg-relation-green').show();
            $('#msg-relation-red').hide();
            $('#alert-relation-green').html("Berhasil menambah relasi dokumen!");

            $('#relate-dokumen-body').html(data);
        }).fail(function(data) {
            $('#msg-relation-green').hide();
            $('#msg-relation-red').show();
            $('#alert-relation-red').html("<center><span style='color:red;'>Oops, Something went wrong!</span></center>");
        });

        event.preventDefault();
    });
});

function hapusRelasi(id){
    if (confirm("Relasi akan dihapus, yakin?") == true) {
        $.ajax({
            url: "{{ route(routePrefix().'documen-relation.delete') }}",
            type: "DELETE",
            data: {relation_id:id, id:"{{ $primary->id }}"},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr("content")
            },
        }).done(function (data) {
            $('#msg-relation-green').show();
            $('#msg-relation-red').hide();
            $('#alert-relation-green').html("Berhasil menghapus relasi!");

            $('#relate-dokumen-body').html(data);
        }).fail(function(data) {
            $('#msg-relation-green').hide();
            $('#msg-relation-red').show();
            $('#alert-relation-red').html("<center><span style='color:red;'>Oops, Something went wrong!</span></center>");
        });
    }
}
</script>
