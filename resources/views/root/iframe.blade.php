<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
.doc {
    width: 100%;
    height: 500px;
}
</style>
<body>
    @php $path = url('public/documents/'.$file->nama_file); @endphp
    @if(!empty($file))
         <iframe class="doc" src="https://docs.google.com/gview?url=http://writing.engr.psu.edu/workbooks/formal_report_template.doc&embedded=true"></iframe>
    @endif
</body>
</html>