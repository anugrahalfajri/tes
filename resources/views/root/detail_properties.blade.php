        @if(auth()->user()->role_id == 2)
            <div class="col-lg-4 col-xlg-3 col-md-4">
        @else
            <div class="col-lg-3 col-xlg-3 col-md-4">
        @endif
                <style>
                    #history .dataTables_wrapper .dataTables_length {
                        float: right;
                    }
                    #notelist .dataTables_wrapper .dataTables_length {
                        float: right;
                    }
                </style>
                <div class="card detail">
                    <div class="card-body">
                        <h4 class="card-title">Detail - <span class="fill-doc-name"></span></h4>
                        <h6 class="card-subtitle">Use default tab with class</h6>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Properties</span></a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Ext Prop</span></a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#signature" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Signature</span></a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#version-log" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Version Log</span></a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#notelist" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Note List</span></a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#history" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">History</span></a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#access" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Access Right</span></a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#workflow" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Workflow</span></a> </li>

                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content tabcontent-border">
                            <div class="tab-pane active" id="home" role="tabpanel">
                                @if(!empty($properties))
                                <table class="table table-bordered table-striped table-responsive">
                                    <tbody>
                                        <tr>
                                            <td width="20%">Name</td>
                                            <td width="5%" align="center">:</td>
                                            <td width="75%"><span class="fill-doc-name"></span></td>
                                        </tr>
                                        <tr>
                                            <td width="20%">Title</td>
                                            <td width="5%" align="center">:</td>
                                            <td width="75%"><span class="fill-doc-title"></span></td>
                                        </tr>
                                        <tr>
                                            <td width="20%">Folder</td>
                                            <td width="5%" align="center">:</td>
                                            <td width="75%"><span class="fill-doc-folder"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Size</td>
                                            <td align="center">:</td>
                                            <td><span class="fill-doc-size"></span></td>
                                        </tr>
                                        <tr>
                                            <td>File Version</td>
                                            <td align="center">:</td>
                                            <td><span class="fill-doc-version"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Workflow Status</td>
                                            <td align="center">:</td>
                                            <td>{{$properties->status}}</td>
                                        </tr>
                                        <tr>
                                            <td>Created on</td>
                                            <td align="center">:</td>
                                            <td><span class="fill-doc-createdtime"></span> by <span class="fill-doc-createdby"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Publish on</td>
                                            <td align="center">:</td>
                                            <td><span class="fill-doc-updatedtime"></span> by <span class="fill-doc-updatedby"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Permalink</td>
                                            <td align="center">:</td>
                                            <td><a href="" class="fill-fileurl" id="permalink-download" style="display: none;" target="_blank">Download</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                                @endif
                            </div>
                            <div class="tab-pane  p-20" id="profile" role="tabpanel">
                                <form action="{{route(routePrefix().'ext_properties.store')}}" method="post">
                                @csrf
                                <table class="table table-bordered table-striped table-responsive">
                                    <input type="hidden" name="id_file" value="">
                                    <input type="hidden" name="relasi_document" value="">
                                    <tbody>
                                        <tr>
                                            <td width="20%">No. Document</td>
                                            <td width="5%" align="center">:</td>
                                            <td width="75%">
                                                <textarea name="no_document" id="no_document" class="form-control" cols="30" rows="2"></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%">Tgl. Document</td>
                                            <td width="5%" align="center">:</td>
                                            <td width="75%"><input type="text" class="form-control datepicker" name="tgl_document" id="tgl_document" value=""></td>
                                        </tr>
                                        <tr>
                                            <td width="20%">Relasi document</td><td width="5%" align="center">:</td>
                                            <td width="75%"><input type="text" class="form-control" name="relasi_document" id="relasi_document" value=""></td>
                                        </tr>
                                        <tr>
                                            <td>Tag</td><td align="center">:</td>
                                            <td>
                                                <select class="form-control select2" name="tag[]" multiple="multiple" data-placeholder="Select tag" style="width: 100%;" required>
                                                @if (!empty($tag))
                                                    @foreach ($tag as $row)
                                                        <option value="{{$row->id}}">
                                                            {{$row->tag}}
                                                        </option>
                                                    @endforeach
                                                @endif
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Document Expiration Date</td><td align="center">:</td>
                                            <td><input type="text" class="form-control datepicker" name="document_expiration_date" id="document_expiration_date" value=""></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button type="submit" class="btn btn-sm btn-primary">Save</button>
                                </form>
                            </div>
                            <div class="tab-pane" id="signature" role="tabpanel">
                                <table class="table table-bordered table-striped table-responsive" width="100%">
                                    <tbody>
                                        <tr>
                                            <td width="20%">Signature Field</td>
                                            <td width="5%" align="center">:</td>
                                            <td width="75%">Enfocus Preflight</td>
                                        </tr>
                                        <tr>
                                            <td>Integrity</td>
                                            <td align="center">:</td>
                                            <td>Signature is valid</td>
                                        </tr>
                                        <tr>
                                            <td>Certificate</td><td align="center">:</td>
                                            <td>Untrusted</td>
                                        </tr>
                                        <tr>
                                            <td>Name</td><td align="center">:</td>
                                            <td>
                                                Preflight Ticket Signature
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Reason</td><td align="center">:</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Date</td><td align="center">:</td>
                                            <td>2021-04-28 13:27:00</td>
                                        </tr>
                                        <tr>
                                            <td>Validity</td><td align="center">:</td>
                                            <td>2010-04-28 13:27:00 <br>- 2021-04-28 13:27:00</td>
                                        </tr>
                                        <tr>
                                            <td>Subject</td><td align="center">:</td>
                                            <td>{"C":"BE", <br> "CN":"Preflight <br>Ticket Signature"}</td>
                                        </tr>
                                        <tr>
                                            <td>Issuer</td><td align="center">:</td>
                                            <td>{"C":"BE",<br>"CN":"Preflight <br>Ticket Signature"}</td>
                                        </tr>
                                        <tr>
                                            <td>Public Key</td><td align="center">:</td>
                                            <td>RSA-SHA1</td>
                                        </tr>
                                        <tr>
                                            <td>Algorithm</td><td align="center">:</td>
                                            <td>sha1With<br>RSAEncryption</td>
                                        </tr>
                                        <tr>
                                            <td>SHA-1 Fingerprint</td><td align="center">:</td>
                                            <td>5E:89:BA:CF:57:27:34:A1:<br>E8:A3:9A:9A:5B:F0:0E:BF:93<br>:C9:E5:30</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane p-20" id="version-log" role="tabpanel">
                                <table class="table table-responsive" width="90%">
                                    <tbody id="version_info">
                                        <tr bgcolor="#f4f4f4">
                                            <td colspan="2">Last Version</td>
                                        </tr>
                                        <tr>
                                            <td width="10%">
                                                <label style="background-color:#f1f1f1;padding:5px;"><div id="version"></div></label>
                                            </td>
                                            <td width="80%">
                                                <a href="javascript:;" id="popover-versi"><i class="fa fa-solid fa-circle-exclamation"></i></a>
                                                <span id="nama_file"></span>
                                                <div style="class:table-cell;float:left;margin-top:10px;">
                                                    <img src="https://melkhior.co/new/demo/dms/assets/images/1.jpg" style="width:40px;height:40px">
                                                </div>
                                                <div style="class:table-cell;float:left;margin-left:10px;;margin-top:10px;">
                                                    <div id="created_by"></div> - <div id="created_at"></div>(<span id="keterangan"></span>)
                                                </div>
                                            </td>
                                        </tr>
                                        <tr bgcolor="#f4f4f4">
                                            <td colspan="2">Older Version</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane p-20" id="notelist" role="tabpanel">
                                <table id="table-note" class="table table-responsive" width="90%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tgl</th>
                                            <th>Action By</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                                <form style="margin-top: 8px;" action="{{ route(routePrefix().'note_list.store') }}" method="post">
                                    @csrf
                                    <input type="hidden" name="id_file" id="id_file_dd" value="{{$row->id_file ?? ''}}">
                                    <div class="form-group row">
                                        <label for="notes" class="col-3 control-label">Notes</label>
                                        <div class="col-9">
                                            <input  type="text" class="form-control" name="notes" id="notes">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-sm btn-primary float-right mb-1">Save</button>
                                </form>
                            </div>
                            <div class="tab-pane p-20" id="history" role="tabpanel">
                                <table id="example_history" class="table table-responsive">
                                    <thead>
                                        <tr>
                                            <th>Tgl</th>
                                            <th>Action By</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="tab-pane p-20" id="access" role="tabpanel">
                                <table style="width:100%;" id="table-access-right" class="table table-responsive">
                                    <thead>
                                        <tr>
                                            <th style="font-size:12px;">No</th>
                                            <th style="font-size:12px;" width="20%">User</th>
                                            <th style="font-size:12px;">Lihat</th>
                                            <th style="font-size:12px;">Tambah</th>
                                            <th style="font-size:12px;">Ubah</th>
                                            <th style="font-size:12px;">Hapus</th>
                                            <th style="font-size:12px;">Unduh</th>
                                            <th style="font-size:12px; display:none;" class="table-access-right-approval">Approval</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                                <div align="right" class="mt-3">
                                    <button type="button" class="btn btn-primary btn-sm add-access">Add Access</button>
                                </div>
                            </div>
                            <div class="tab-pane p-20" id="workflow" role="tabpanel">
                                <div class="ml-3 mr-3">
                                    <form action="" method="post">
                                        <div class="form-group">
                                            <label>Workflow : </label>
                                            <select class="form-control" name="" id="">
                                                <option option="1">Review And Approve (single reviewer)</option>
                                            </select>
                                        </div>
                                        <h6>General</h6>
                                        <hr>
                                        <div class="form-group">
                                            <label for="">Message</label>
                                            <textarea  class="form-control" name="" id="" cols="30" rows="8"></textarea>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Due</label>
                                                    <input type="text" class="form-control datepicker" name="" id="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Priority</label>
                                                    <select  class="form-control" name="" id="">
                                                        <option value="">High</option>
                                                        <option value="">Medium</option>
                                                        <option value="">Low</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <h6 class="mt-3">Items</h6>
                                        <hr>
                                        <strong>items *</strong>
                                        <table style="font-size:12px;">
                                            <tbody>
                                                <tr>
                                                    <td><i class="fa fa-file"></i></td>
                                                    <td>
                                                        <strong>HW-SW-SPEC.pdf</strong> <br>
                                                        Description: (None) <br>
                                                        Modified on : Mon 20 Apr 2020 10:30
                                                    </td>
                                                    <td>
                                                        <i class="fa fa-arrow-circle-right"></i> View More Action<br>
                                                        <i class="far fa-times-circle"></i> Remove
                                                    </td>
                                                </tr>
                                                <tr class="mt-3">
                                                    <td><i class="fa fa-file"></i></td>
                                                    <td>
                                                        <strong>HW-SW-SPEC.pdf</strong> <br>
                                                        Description: (None) <br>
                                                        Modified on : Mon 20 Apr 2020 10:30
                                                    </td>
                                                    <td>
                                                        <i class="fa fa-arrow-circle-right"></i> View More Action<br>
                                                        <i class="far fa-times-circle"></i> Remove
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div class="mt-3">
                                        <button class="btn btn-primary">Add</button>
                                        <button class="btn btn-primary ml-3">Remove All</button>
                                        </div>
                                        <h6 class="mt-3">Other Options</h6>
                                        <hr>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                            <label class="form-check-label" for="defaultCheck1">
                                                Send Email Notification
                                            </label>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <strong>Approval User</strong><br>
                                            <div class="user_approval"></div>
                                        </div>
                                        <div class="mt-3 mb-3">
                                        <button class="btn btn-primary">Start Workflow</button>
                                        <button class="btn btn-light">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div> <!-- end detail -->


                    </div>
                </div>

                <div class="preview-placeholder card-body image-viewer">
                    <img src="" class="image-link" style="max-width: 100%" alt="">
                </div>

                <div class="preview-placeholder card-body pdf-viewer">
                    <div class="pdf-link"></div>
                </div>

                <div class="preview-placeholder card-body txt-viewer">
                    <textarea class="txt-content" rows="30" style="width: 100%"></textarea>
                </div>
                    

                {{-- <div class="iframe"> --}}

                    <!-- <iframe class="doc" src="https://docs.google.com/gview?url=http://localhost/dms/public/documents/1616996140-MRENLIUV4W.pdf&embedded=true" height="500" width="100%" scrolling="auto"></iframe> -->

                    <!-- <embed src="http://localhost/dms/public/documents/1617161760-LI5XCYPXWM.docx" type="application/docx"   height="500px" width="100%"> -->

                        <!-- <iframe src='https://view.officeapps.live.com/op/embed.aspx?src=http://localhost/dms/public/documents/1617161760-LI5XCYPXWM.docx' width='1366px' height='623px' frameborder='0'>This is an embedded <a target='_blank' href='http://office.com'>Microsoft Office</a> document, powered by <a target='_blank' href='http://office.com/webapps'>Office Online</a>.</iframe> -->
                    {{-- <iframe src ="public/plugins/ViewerJS/#1616996140-MRENLIUV4W.pdf" width='400' height='300' allowfullscreen webkitallowfullscreen></iframe> --}}
                {{-- </div> --}}


                <div class="card action">
                    <div class="card-body">
                        <h5>Nama Dokumen : <span class="fill-doc-name"></span></h5>
                        <hr>
                        <h4 class="m-b-20">Choose Action</h4>
                        <!-- Accordian -->
                        <div id="accordion" class="nav-accordion" role="tablist" aria-multiselectable="true">
                            <div class="card">
                                <div class="card-header" role="tab" id="headingrelated">
                                    <h5 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapserelated" aria-expanded="false" aria-controls="collapserelated">
                                            Document Relation
                                        </a>
                                    </h5> 
                                </div>
                                <div id="collapserelated" class="collapse" role="tabpanel" aria-labelledby="headingrelated">
                                    <div class="card-body">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li class="nav-item"> 
                                                <a class="nav-link active" data-toggle="tab" href="#relasi" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Relasi</span></a> 
                                            </li>
                                            <li class="nav-item"> 
                                                <a class="nav-link" data-toggle="tab" href="#pohon-relasi" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Struktur</span></a> 
                                            </li>
                                        </ul>

                                        <div class="tab-content tabcontent-border">
                                            <div class="tab-pane p-20 active" id="relasi" role="tabpanel">
                                                    <button type="button" onclick="relatedDokumen()" class="btn btn-primary">Relasi Dokumen</button>
                                            </div>
                                            <div class="tab-pane p-20" id="pohon-relasi" role="tabpanel">
                                                <button type="button" onclick="structureRelationDokumen()" class="btn btn-primary">Struktur Relasi Dokumen</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingZeroOne">
                                    <h5 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseZeroOne" aria-expanded="false" aria-controls="collapseZeroOne">
                                          Workflow
                                        </a>
                                    </h5> 
                                </div>
                                <div id="collapseZeroOne" class="collapse" role="tabpanel" aria-labelledby="headingZeroOne">
                                    <div class="card-body">
                                        <div class="alert alert-danger alert-dismissible" id="msg-alert-reupload" style="display: none;">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                <ul>
                                                    <li id="error-list-reupload"></li>
                                                </ul>
                                        </div><br>

                                        <form  method="post" id="workflows-form">
                                            <input type="hidden" name="fileid" id="file_id_workflow" value="" class="fill-fileid">

                                            <input type="checkbox" name="assign" id="assign_wf" class="form-control">
                                            <label for="assign">Assign Workflow</label>
                                            <br>
                                            <label for="keterangan">Select Workflow : </label><br>
                                            
                                            <select name="workflows[]" id="workflows_value" class="select2 m-b-10 select2-multiple" style="width: 100%" multiple="multiple" data-placeholder="Choose">
                                                <optgroup label="Workflow Name">
                                                    @foreach ($workflows ?? [] as $w)
                                                         <option value="{{ $w->id }}">{{$w->nama}}</option>
                                                    @endforeach
                                                </optgroup>
                                            </select>
                                            <br><br>
                                            <button type="button" id="assignwf-submit" class="btn btn-primary">Save</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingZeroTwo">
                                    <h5 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseZeroTwo" aria-expanded="false" aria-controls="collapseZeroTwo">
                                          Reupload Document
                                        </a>
                                    </h5> 
                                </div>
                                <div id="collapseZeroTwo" class="collapse" role="tabpanel" aria-labelledby="headingZeroTwo">
                                    <div class="card-body">
                                        <div class="alert alert-danger alert-dismissible" id="msg-alert-reupload" style="display: none;">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                <ul>
                                                    <li id="error-list-reupload"></li>
                                                </ul>
                                        </div><br>

                                        <form action="{{ route(routePrefix().'upload-file.reupload') }}" method="post" enctype="multipart/form-data" id="reupload-form">
                                        <label>File baru</label><br>
                                            @csrf
                                            <input type="hidden" name="file_id" id="file_id" value="" class="fill-fileid">
                                            <div class="dropzone" id="reupload">
                                                <div class="dz-message">
                                                    <span>Drop files here or click to upload</span>
                                                    <br>
                                                </div>
                                            </div><br>
                                            <label for="keterangan">Keterangan : </label>
                                            <textarea name="keterangan" id="keterangan-reupload" class="form-control" cols="30" rows="3"></textarea>
                                            <br><br>
                                            <button type="submit" id="reupload-submit" class="btn btn-primary">Upload File</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingOne">
                                    <h5 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                          Move Document
                                        </a>
                                    </h5> 
                                </div>
                                <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="card-body">
                                        <label>Move To</label><br>
                                        <form action="{{ urlWithPrefix('move-folder') }}" method="post">
                                            @csrf
                                            <input type="hidden" name="id_file" value="" class="fill-fileid">
                                            <select class="form-control select2" name="new_directory_id" style="width: 100%">
                                                {!! $directory_selection !!}
                                            </select>
                                            <input type="submit" value="Move File" class="btn btn-primary">
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingTwo">
                                    <h5 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                          Duplicate Document
                                        </a>
                                    </h5> 
                                </div>
                                <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="card-body">
                                        <label>Copy To</label><br>
                                        <form action="{{ urlWithPrefix('duplicate-file') }}" method="post">
                                            @csrf
                                            <input type="hidden" name="id_file" value="" class="fill-fileid">
                                            <select class="form-control select2" name="new_directory_id" style="width: 100%">
                                                {!! $directory_selection !!}
                                            </select>
                                            <input type="submit" value="Duplicate File" class="btn btn-primary">
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingThree">
                                    <h5 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                          Share Document
                                        </a>
                                    </h5> 
                                </div>
                                <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="card-body">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li class="nav-item"> 
                                                <a class="nav-link active" data-toggle="tab" href="#sharemail" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Share via Email</span></a> 
                                            </li>
                                            <li class="nav-item"> 
                                                <a class="nav-link" data-toggle="tab" href="#sharelink" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Share Link</span></a> 
                                            </li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content tabcontent-border">
                                            <div class="tab-pane p-20 active" id="sharemail" role="tabpanel">
                                                <form action="{{ urlWithPrefix('send-email') }}" method="post">
                                                    @csrf
                                                    <label>Email :</label>
                                                    <input type="email" required="required" name="email" class="form-control" placeholder="Email"><br>
                                                    <label>Subject :</label>
                                                    <input type="text" required="required" name="subject" class="form-control" placeholder="Subject"><br>
                                                    <label>Body :</label>
                                                    <textarea rows="8" class="form-control fill-email" name="message" required="required" placeholder="Body"></textarea><br><br>
                                                    <input type="submit" value="submit" class="btn btn-primary">
                                                </form>
                                            </div>
                                            <div class="tab-pane p-20" id="sharelink" role="tabpanel">
                                                <label>Copy Link</label>
                                                <input type="text" class="form-control fill-fileurl" style="cursor: pointer;" readonly="readonly" value=""><br>
                                                <span class="copied" style="display: none">Copied to Clipboard</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingFour">
                                    <h5 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                          Delete Document
                                        </a>
                                    </h5> 
                                </div>
                                <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingFour">
                                    <div class="card-body">
                                        Apakah Anda yakin ingin menghapus dokumen ?<br>
                                        <a href="#" class="btn btn-danger fill-trashid">Ya</a>
                                        <a href="#" class="btn btn-primary">Tidak</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- @section('scripts')

                <script>
                    function tes() {
                        console.log("tes111");
                        $('#table-note').DataTable().columns.adjust();
                    }

                    $(document).ready(function () {


                        // var url_string = window.location.href; //
                        // var url = new URL(url_string);
                        // var paramValue = url.searchParams.get("content");
                        var paramValue = '{!! \Session::get('content') !!}';
                        if(paramValue) {
                            detail(paramValue);

                            $('a[href="#notelist"]').trigger('click');
                            myTimeout = setTimeout(tes, 1000);
                        }

                        console.log(paramValue);


                    });


                    function pp(){



                    }

                </script>

                <script>
                    $('#table-note').DataTable({
                        recordsFiltered:10,
                        scrollX: true,
                    });
                    $('#example_history').DataTable({
                        recordsFiltered:10,
                        scrollX: true,
                    });

                    $("a[href='#notelist']").on('shown.bs.tab', function (e) {
                        $('#table-note').DataTable().columns.adjust().draw();

                    });

                    $("a[href='#history']").on('shown.bs.tab', function (e) {
                        $('#example_history').DataTable().columns.adjust().draw();

                    });
                </script>

            @endsection --}}