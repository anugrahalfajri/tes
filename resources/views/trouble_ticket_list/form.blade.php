@extends('layouts.app')

@section('content')
<style>
    /* .swal2-container.swal2-center>.swal2-popup {
        width: 150px !important;
    }
    .swal2-confirm, .swal2-title {
        font-size: 10pt !important;
    } */
</style>
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">Trouble Ticket List</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{urlWithPrefix('/')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{urlWithPrefix('/trouble_ticket_list')}}">Trouble Ticket List</a></li>
                    <li class="breadcrumb-item active">{{ $act == 'edit' ? 'Edit' : 'Create' }}</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="d-flex justify-content-between">
                            <h4 class="text-white card-title">Trouble Ticket List</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{urlWithPrefix('/trouble_ticket_list/store')}}" method="post">
                            @csrf
                            <input type="hidden" name="id" value="{{$trouble_ticket_list->id ?? ''}}">
                            <div class="row mb-5">
                                <div class="col-md-3">User</div>
                                <div class="col-md-9">
                                    <select name="id_user" id="id_user" class="form-control select2" style="width: 100%;">
                                        @if(!empty($users))
                                            @foreach($users as $row)
                                                <option value="{{$row->id}}" {{($row->id == @$trouble_ticket_list->id_user)?'selected':''}}>{{$row->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-5">
                                <div class="col-md-3">Date Time Delivery</div>
                                <div class="col-md-9">
                                    <input type="text" name="date_time_delivery" id="" class="form-control datepicker" value="{{$trouble_ticket_list->date_time_delivery ?? ''}}">
                                    @if($errors->has('date_time_delivery'))
                                        <span class="text-danger">{{ $errors->first('date_time_delivery') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="row mb-5">
                                <div class="col-md-3">Title</div>
                                <div class="col-md-9">
                                    <input type="text" name="title" id="" class="form-control" value="{{$trouble_ticket_list->title ?? ''}}">
                                    @if($errors->has('title'))
                                        <span class="text-danger">{{ $errors->first('title') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="row mb-5">
                                <div class="col-md-3">Description</div>
                                <div class="col-md-9">
                                    <textarea name="description" class="form-control" id="" cols="30" rows="10">{{$trouble_ticket_list->description ?? ''}}</textarea>
                                    @if($errors->has('description'))
                                        <span class="text-danger">{{ $errors->first('description') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="row mb-5">
                                <div class="col-md-3">Status</div>
                                <div class="col-md-9">
                                    <select name="status" id="status" class="form-control select2" style="width: 100%;">
                                        <option value="">Pilih</option>
                                        <option value="open" {{@$trouble_ticket_list->status == 'open'?'selected':''}}>Open</option>
                                        <option value="progress" {{@$trouble_ticket_list->status == 'progress'?'selected':''}}>Progress</option>
                                        <option value="hold" {{@$trouble_ticket_list->status == 'hold'?'selected':''}}>Hold</option>
                                        <option value="close" {{@$trouble_ticket_list->status == 'close'?'selected':''}}>Close</option>
                                    </select>
                                    @if($errors->has('status'))
                                        <span class="text-danger">{{ $errors->first('status') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="row mb-5">
                                <div class="col-md-3">Conversation</div>
                                <div class="col-md-9">
                                    <textarea name="conversation" class="form-control" id="" cols="30" rows="10">{{$trouble_ticket_list->conversation ?? ''}}</textarea>
                                    @if($errors->has('conversation'))
                                        <span class="text-danger">{{ $errors->first('conversation') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div align="right">
                                <button type="submit" class="btn btn-primary">Submit</a>
                                <button type="reset" class="btn btn-danger  ml-5">Reset</a>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Anda yakin akan {{$act == 'tambah' ? 'menambahkan' : 'mengedit'}} data?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection