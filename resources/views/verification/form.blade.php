<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon icon -->
    <!-- <link rel="icon" type="image/png" sizes="16x16" href="{{url('images/favicon.png')}}"> -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/images/apple-touch-icon.png.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/images/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/images/favicon-16x16.png')}}">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="{{asset('assets/images/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <title>MELKHIOR - DODI</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{url('plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{url('material/css/style.css')}}" rel="stylesheet">
</head>

<style>
    .button{
        padding: 0;
        color: #007bff;
        border: 0;
        background-color: transparent;
        cursor: pointer;
        font-size: 13px;
        margin-left: 20px;
    }
    .button:focus {
        outline: 0;
    }
    .preview-placeholder {
        background: white;
        border: 1px solid #ddd;
        display: none;
    }
    .image-viewer {
        min-height: 30rem;
    }

    .ui-icon.ui-igtreegrid-expansion-indicator.ui-icon-minus {
        background: url(/images/samples/tree-grid/opened_folder.png) !important;
        background-repeat: no-repeat;
    }
    .ui-icon.ui-igtreegrid-expansion-indicator.ui-icon-plus {
        background: url(/images/samples/tree-grid/folder.png) !important;
        background-repeat: no-repeat;
    }
	.ui-icon-plus:before {
	    content: '' !important;
	}
	.ui-icon-minus:before{
	    content: '' !important;
	}

    .dropzone{
        border: 2px dashed rgba(0, 0, 0, 0.3) !important;
        background: white;
        border-radius: 5px;
        min-height: 145px;
        vertical-align: baseline;
    }
</style>

<body>
    <div class="container">
        <div class="container-fluid mt-5">
            <div class="row">
                <div class="col-lg-12 col-xlg-12 col-md-12">
                    <div class="card">
                        <div class="card-body bg-megna">
                            <div class="d-flex justify-content-between">
                                <h4 class="text-white card-title">Form Verification</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{urlWithPrefix('/users/update_password', $user->id)}}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control {{$errors->has('password') ? 'is-invalid' : ''}}" name="password" id="password" placeholder="Enter password">
                                    @if ($errors->has('password'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password_confirmation">Confirmation Password</label>
                                    <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Enter password confirmation">
                                </div>
                                <div class="form-group">
                                    <label for="pin">PIN</label>
                                    <input type="password" class="form-control {{$errors->has('pin') ? 'is-invalid' : ''}}" name="pin" id="pin" placeholder="Enter PIN">
                                    @if ($errors->has('pin'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('pin') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="pin_confirmation">Confirmation PIN</label>
                                    <input type="password" class="form-control" id="pin_confirmation" name="pin_confirmation" placeholder="Enter PIN confirmation">
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>