 <!-- ============================================================== -->
        <!-- All Jquery -->
        <!-- ============================================================== -->
        <script src="{{url('plugins/jquery/jquery.min.js')}}"></script>
        <!-- Bootstrap tether Core JavaScript -->
        <script src="{{url('plugins/bootstrap/js/popper.min.js')}}"></script>
        <script src="{{url('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
        <!-- slimscrollbar scrollbar JavaScript -->
        <script src="{{url('dark/js/jquery.slimscroll.js')}}"></script>
        <!--Wave Effects -->
        <script src="{{url('dark/js/waves.js')}}"></script>
        <!--Menu sidebar -->
        <script src="{{url('dark/js/sidebarmenu.js')}}"></script>
        <!--stickey kit -->
        <script src="{{url('plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
        <script src="{{url('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
        <script src="{{url('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
        <!--Custom JavaScript -->
        <script src="{{url('dark/js/custom.min.js')}}"></script>
        <!-- ============================================================== -->
        <!-- This page plugins -->
        <!-- ============================================================== -->
        <!-- chartist chart -->
        <script src="{{url('plugins/chartist-js/dist/chartist.min.js')}}"></script>
        <script src="{{url('plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js')}}"></script>
        <!--c3 JavaScript -->
        <script src="{{url('plugins/d3/d3.min.js')}}"></script>
        <script src="{{url('plugins/c3-master/c3.min.js')}}"></script>
        <!-- Chart JS -->
        <script src="{{url('dark/js/dashboard1.js')}}"></script>
        <!-- ============================================================== -->
        <!-- Style switcher -->
        <!-- ============================================================== -->
        <script src="{{url('plugins/styleswitcher/jQuery.style.switcher.js')}}"></script>
        <script>
            $('.detail').hide();
            $('.root').click(function(){
                $('.detail').show();
            });
        </script>
    </body>
</html>