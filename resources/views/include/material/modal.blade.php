<div class="modal fade" id="modal-upload" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form action="{{ route(routePrefix().'file_upload.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id_directory" class="upload-folder-id" value="">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Upload File</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="alert alert-danger alert-dismissible" id="msg-alert-upload" style="display: none;">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <ul>
                                <li id="error-list"></li>
                            </ul>
                    </div><br>

                        <div class="dropzone" id="myDropzone">
                            <div class="dz-message">
                                <span>Drop files here or click to upload</span>
                                <br>
                            </div>
                        </div>
                    
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submit-upload" class="btn btn-primary mt-3">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="directoryModal" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <form action="{{ route(routePrefix().'directory.store') }}" method="post">
            @csrf
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Buat folder</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                
                    <div class="form-group row">
                        <label for="" class="col-4 control-label">Nama folder *</label>
                        <div class="col-8">
                            <input type="text" class="form-control" name="nama_folder" id="" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-4 control-label">Parent</label>
                        <div class="col-8">
                            <select class="form-control select2" name="parent_id" data-placeholder="Select parent" style="width: 100%;">
                                <option value="">Pilih Folder Utama</option>
                                @if (!empty($directory))
                                    @foreach ($directory as $row)
                                        <option value="{{$row->id}}">
                                            {{$row->nama_folder}}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    
                
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
        </form>
    </div>
</div>

<div class="modal fade" id="upload-eform-modal" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <form action="{{ route(routePrefix().'eform.answer.import') }}" method="post" enctype="multipart/form-data">
            @csrf

        <input type="hidden" name="form_number" value="{{ isset($eform) ? $eform[0]->form_id : '' }}">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Import Bulk Answer</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                
                    <div class="form-group row">
                        <label for="" class="col-4 control-label">File Import *</label>
                        <div class="col-8">
                            <input type="file" class="form-control" name="file_import" accept=".xlsx" id="" required>
                        </div>
                    </div>
                
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Import</button>
            </div>
        </div>
        </form>
    </div>
</div>

<div id="rightClickFile" class="dropdown clearfix">
<div class="dropdown-menu" style="display:block;position:static;margin-bottom:5px;">
    <a class="dropdown-item" href="javascript:;" onclick="triggerAction('preview')">Preview</a>
    <a class="dropdown-item" href="javascript:;" onclick="triggerAction('action')">Action</a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="javascript:;" onclick="triggerAction('info')">Info</a>
    </div>
</div>

