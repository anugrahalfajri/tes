@if(auth()->user()->role->name == 'user')
    <!--Custom JavaScript -->
    <!-- ============================================================== -->
    <script src="{{url('material/pro/js/jquery.min.js')}}"></script>
    <script src="{{url('material/pro/js/jquery-ui.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{url('material/pro/js/popper.min.js')}}"></script>
    <script src="{{url('material/pro/js/bootstrap.min.js')}}"></script>
    <script src="{{url('plugins/datatables-responsive/js/dataTables.responsive.js')}}"></script>
    <script src="{{url('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <!-- select2 -->
    <script src="{{url('plugins/select2/dist/js/select2.min.js')}}"></script>
    <!-- apps -->
    <script src="{{url('material/pro/js/app.min.js')}}"></script>
    <script src="{{url('material/pro/js/app.init.horizontal.js')}}"></script>
    <script src="{{url('material/pro/js/app-style-switcher.horizontal.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{url('material/pro/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{url('material/pro/js/sparkline.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{url('material/pro/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{url('material/pro/js/sidebarmenu.js')}}"></script>
    <script src="{{url('material/pro/js/custom.min.js')}}"></script>
    <!--This page JavaScript -->
    <script src="{{url('material/pro/js/moment.min.js')}}"></script>
    <script>
        $(function () {
            let isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;
            if (isMobile) {
                close_folder_sidebar();
            } else {
                open_folder_sidebar();
            }
        })
    </script>
@elseif(auth()->user()->role->name == 'master admin')
    <script>
        console.log(1);
    </script>
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{url('plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{url('plugins/bootstrap/js/popper.min.js')}}"></script>
    <script src="{{url('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{url('plugins/datatables-responsive/js/dataTables.responsive.js')}}"></script>

    <script src="{{url('plugins/select2/dist/js/select2.min.js')}}"></script>

    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{url('material/master-admin/js/jquery.slimscroll.js')}}"></script>

    <!--Wave Effects -->
    <script src="{{url('material/master-admin/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{url('material/master-admin/js/sidebarmenu.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{url('material/master-admin/js/custom.js')}}"></script>

    <script src="{{url('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <!--stickey kit -->
    <script src="{{url('plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
    <script src="{{url('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
    {{-- <script src="{{url('plugins/dropzone-master/dist/min/dropzone.min.js')}}"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.9.0/viewer.min.js"></script>

    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!-- chartist chart -->
    <!-- <script src="{{url('plugins/chartist-js/dist/chartist.min.js')}}"></script> -->
    <!-- <script src="{{url('plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js')}}"></script> -->
    <!--c3 JavaScript -->
    <script src="{{url('plugins/d3/d3.min.js')}}"></script>
    <script src="{{url('plugins/c3-master/c3.min.js')}}"></script>
    <!-- Chart JS -->
    <script src="{{url('material/master-admin/js/dashboard1.js')}}"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="{{url('plugins/styleswitcher/jQuery.style.switcher.js')}}"></script>

    <script src="https://ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.8.3.js"></script>
    <script src="https://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
@elseif(auth()->user()->role->name == 'admin organisasi')
    <script>
        $("body").removeClass("mini-sidebar");
        if (width < 1000) {
            $("body").addClass("mini-sidebar");
            $('.navbar-brand span').hide();
            $(".scroll-sidebar, .slimScrollDiv").css("overflow-x", "visible").parent().css("overflow", "visible");
            $(".sidebartoggler i").addClass("ti-menu");
        }
    </script>
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{url('plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{url('plugins/bootstrap/js/popper.min.js')}}"></script>
    <script src="{{url('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{url('plugins/datatables-responsive/js/dataTables.responsive.js')}}"></script>

    <script src="{{url('plugins/select2/dist/js/select2.min.js')}}"></script>

    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{url('material/master-admin/js/jquery.slimscroll.js')}}"></script>

    <!--Wave Effects -->
    <script src="{{url('material/master-admin/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{url('material/master-admin/js/sidebarmenu.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{url('material/master-admin/js/custom.js')}}"></script>

    <script src="{{url('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <!--stickey kit -->
    <script src="{{url('plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
    <script src="{{url('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
    {{-- <script src="{{url('plugins/dropzone-master/dist/min/dropzone.min.js')}}"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.9.0/viewer.min.js"></script>

    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!-- chartist chart -->
    <!-- <script src="{{url('plugins/chartist-js/dist/chartist.min.js')}}"></script> -->
    <!-- <script src="{{url('plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js')}}"></script> -->
    <!--c3 JavaScript -->
    <script src="{{url('plugins/d3/d3.min.js')}}"></script>
    <script src="{{url('plugins/c3-master/c3.min.js')}}"></script>
    <!-- Chart JS -->
    <script src="{{url('material/master-admin/js/dashboard1.js')}}"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="{{url('plugins/styleswitcher/jQuery.style.switcher.js')}}"></script>

    <script src="https://ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.8.3.js"></script>
    <script src="https://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
@else
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{url('plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{url('plugins/bootstrap/js/popper.min.js')}}"></script>
    <script src="{{url('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{url('plugins/datatables-responsive/js/dataTables.responsive.js')}}"></script>

    <script src="{{url('plugins/select2/dist/js/select2.min.js')}}"></script>

    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{url('material/js/jquery.slimscroll.js')}}"></script>

    <!--Wave Effects -->
    <script src="{{url('material/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{url('material/js/sidebarmenu.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{url('material/js/custom.js')}}"></script>

    <script src="{{url('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <!--stickey kit -->
    <script src="{{url('plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
    <script src="{{url('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
    {{-- <script src="{{url('plugins/dropzone-master/dist/min/dropzone.min.js')}}"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.9.0/viewer.min.js"></script>

    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!-- chartist chart -->
    <!-- <script src="{{url('plugins/chartist-js/dist/chartist.min.js')}}"></script> -->
    <!-- <script src="{{url('plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js')}}"></script> -->
    <!--c3 JavaScript -->
    <script src="{{url('plugins/d3/d3.min.js')}}"></script>
    <script src="{{url('plugins/c3-master/c3.min.js')}}"></script>
    <!-- Chart JS -->
    <script src="{{url('material/js/dashboard1.js')}}"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="{{url('plugins/styleswitcher/jQuery.style.switcher.js')}}"></script>

    <script src="https://ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.8.3.js"></script>
    <script src="https://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
@endif

<script src="{{ url('material/js/dropzone.js') }}"></script>
<!-- Ignite UI for jQuery Required Combined JavaScript files -->
<script src="https://cdn-na.infragistics.com/igniteui/2020.2/latest/js/infragistics.core.js"></script>
<script src="https://cdn-na.infragistics.com/igniteui/2020.2/latest/js/infragistics.lob.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfobject/2.2.5/pdfobject.min.js"></script>

<script src="{{url('plugins/switchery/dist/switchery.min.js')}}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
    $(document).ready(function () {
        // Optimalisation: Store the references outside the event handler:
        var $window = $(window);


        function checkWidth() {
            var windowsize = $window.width();
            if (windowsize > 976) {
                //if the window is greater than 440px wide then turn on jScrollPane..
                document.getElementById("left-sidebar-dd").style.left = "0";

            } else if (windowsize < 976) {
                //if the window is greater than 440px wide then turn on jScrollPane..
                document.getElementById("left-sidebar-dd").style.left = "-240px";


            }
        }

        // Execute on load
        checkWidth();
        // Bind event listener
        $(window).resize(checkWidth);
    });

    function togglerPengganti() {

        if ($("#togglerstats").val() == "dd" || $("#togglerstats").val() == "") {
            document.getElementById("left-sidebar-dd").style.left = "-240px";
            $("#togglerstats").val("aa")
        } else {
            document.getElementById("left-sidebar-dd").style.left = "0px";
            $("#togglerstats").val("dd")
        }


        console.log("Tes111");
    }
</script>

<script>
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function () {
        new Switchery($(this)[0], $(this).data());
    });

    let isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;

    function open_folder_sidebar() {
        $('.mdi-chevron-right').attr('hidden', true);
        $('.mdi-chevron-left').removeAttr('hidden');
        if (isMobile) {
            $(".w3-sidebar").animate({width: '90%'}, 500);
        } else {
            $('body .page-wrapper').css({marginLeft: '15%'});
            $(".w3-sidebar").animate({width: '15%'}, 500);
        }
    }

    function close_folder_sidebar() {
        $('.mdi-chevron-left').attr('hidden', true);
        $('.mdi-chevron-right').removeAttr('hidden');
        $(".w3-sidebar").animate({width: '0%'}, 500);
        $('body .page-wrapper').css({marginLeft: '0%'});
    }

    var baseurl = $('.baseurl').val();

    $(function () {
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today = now.getFullYear() + "-" + (month) + "-" + (day);
        $('#date').val(today);
        $("#example").DataTable();
        $("#example2").DataTable();
        $('.select2').select2({
            closeOnSelect: false,
            allowClear: true
        });
        $('.preview-placeholder').hide();
        $('tbody tr td button.button-action').hide();
        $('.datepicker').datepicker({
            format: 'yyyy/mm/dd',
            autoclose: true
        });
    });
    // $(function () {
    //     $('[data-toggle="popover"]').popover({
    //         trigger: 'focus hover'
    //     });
    // });
    $(document).ready(function () {
        $('body').popover({
            selector: '[data-toggle="popover"]',
            html: true,
            placement: 'bottom',
            trigger: 'focus hover'
        });
        // $('your selector').bind("click",function(){
        //        // your statements;
        // });

        // you can use the above or the one shown below

        $('input[type="search"]').attr("autocomplete", "off");
        $('input[type="search"]').attr("readonly", true);
        $('input[type="search"]').attr("onfocus", "this.removeAttribute('readonly');");

        $('.info-root').click(function (e) {
            console.log('test');
            $('.detail').show();
            // your statements;
        });

        $('.fill-fileurl').on('click', function () {
            $('.fill-fileurl').select();
            document.execCommand("copy");
            $('.copied').show();
            setTimeout(function () {
                $('.copied').hide()
            }, 1000);
        });


    });

    $('.opsi').hide();
    $('.directory-select').hide();
    $('.opsi-textbox').hide();
    $('.select-opsi').on('change', function () {
        var value = $(this).val();
        $('.opsi').show();
        if (value == 'id_directory') {
            $('.directory-select').show();
            $('.opsi-textbox').hide();
        } else {
            $('.directory-select').hide();
            $('.opsi-textbox').show();
            // $('#label').empty();
            switch (value) {
                case 'properties.name':
                    $('#label').html('Name');
                    // $('.textbox').attr('name','name');
                    break;
                case 'properties.title':
                    $('#label').html('Title');
                    // $('.textbox').attr('name','title');
                    break;
                case 'extented_properties.no_document':
                    $('#label').html('No Document');
                    // $('.textbox').attr('name','no_document');
                    break;
            }
        }
    }).trigger('change');

    function ubahFormat(tgl) {
        var tahun = tgl.slice(0, 4);
        var bulan = tgl.slice(5, 7);

        var tanggal = tgl.slice(8, 10);
        var jam = tgl.slice(11, 19);

        return tahun + "-" + bulan + "-" + tanggal + " " + jam;

    }

    function detail(id_file) {

        $("#table-note").DataTable().clear().draw();


        $.ajax({
            type: 'POST',
            url: '/ximplidoc/getIdFileNt',
            data: {id_file: id_file, _token: '{{csrf_token()}}'},
            success: function (data) {
                console.log(data);

                $('#id_file_dd').val(data.msg);
                console.log("Sukes get id file for notes list")

            }


        });


        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '/ximplidoc/getSeluruNoteById',
            data: {id_file: id_file, _token: '{{csrf_token()}}'},
            success: function (result) {
                console.log(result);
                $.each(result, function (i, item) {
                    cc = i + 1;
                    // $("#table-note > tbody").append("<tr><td>"+ cc +"</td><td>"+ ubahFormat(item.created_at)  +"</td><td>"+ item.writer +"</td><td>"+ item.notes +"</td></tr>");
                    $("#table-note").DataTable().row.add([cc, ubahFormat(item.created_at), item.writer, item.notes]).draw();

                    console.log("Sukes get data notes for notes list")
                });
            },

        });

        $.ajax({
            type: "get",
            url: baseurl + "/file-information/" + id_file,
            cache: false,
            dataType: "json",
            success: function (data) {

                var document_link = encodeURI(baseurl + '/documents/' + data['nama_file']);
                var tgl_upload = data['created_at'] ? data['created_at'] : data['file_created'];
                var uploader = data['created_by'] ? data['created_by'] : data['member'];
                var fullname = `<b>Tanggal Upload : </b>${tgl_upload} <br> <b>Uploader : </b> ${uploader}`;
                console.log(data);
                $('.fill-doc-name').text(data['nama_file']);
                $('.fill-doc-title').text(data['title']);
                $('.fill-doc-folder').text(data['folder']);
                $('.fill-doc-version').text(data['version']);
                $('.fill-doc-createdtime').text(data['created_at']);
                $('.fill-doc-createdby').text(data['created_by']);
                $('.fill-doc-updatedtime').text(data['updated_at']);
                $('.fill-doc-updatedby').text(data['updated_by']);
                $('.fill-fileurl').val(document_link);
                $('.fill-fileid').val(id_file);
                $('#no_document').val(data['no_document']);
                $('#tgl_document').val(data['tgl_document']);
                $('#relasi_document').val(data['relasi_document']);
                $('#document_expiration_date').val(data['document_expiration_date']);
                $('#nama_file').html(data['nama_file']);
                $('#version').html(data['version']);
                $('#created_by').html(data['created_by']);
                $('#keterangan').html(data['keterangan']);
                $('#created_at').html(data['created_at']);
                $('#popover-versi').attr("data-content", fullname);
                $('#popover-versi').popover({
                    html: true,
                    placement: 'bottom',
                    trigger: 'focus hover',
                    content: function () {
                        return fullname.html();
                    },
                    title: function () {
                        return "Detail Versi";
                    }
                });
                if (data['tag']) {
                    $('#tag_doc').val(data['tag'].split(',')).change();
                }

                if (data['size'] > 1024 * 1024) {
                    $('.fill-doc-size').text((data['size'] / (1024 * 1024)).toFixed(2) + " MB");
                } else if (data['size'] > 1024) {
                    $('.fill-doc-size').text((data['size'] / 1024).toFixed(2) + " KB");
                } else {
                    $('.fill-doc-size').text(data['size'] + " bytes");
                }

                if (data['title_file_upload'] == null) {
                    $('#workflow').attr('hidden', true);
                    $('a[href="#workflow"]').attr('hidden', true);
                    $('.add-access').removeAttr('hidden', true);
                } else {
                    $('#workflow').removeAttr('hidden');
                    $('a[href="#workflow"]').removeAttr('hidden');
                    $('.add-access').attr('hidden', true);
                }

                $('.card-dynamic').removeClass('col-lg-12');
                $('.detail').show();
                $('.preview-placeholder').hide();
                $('.action').hide();
                $('body, html').animate({
                    scrollTop: $(".detail").offset().top
                }, 600);
            }
        });

        showVersionLog(id_file);

    }

    function showVersionLog(id_file) {
        $.ajax({
            type: "GET",
            url: "{{ route(routePrefix().'upload-file.versionlog') }}",
            data: {id_file: id_file},
            success: function (response) {
                $('.version_log_list').remove();
                $('#version_info').append(response);
                $('.error_version').remove();
            },
            error: function (request, status, error) {
                $('.version_log_list').remove();
                $('#version_info').append('<td colspan="2" class="error_version" style="color:red;">Oops, Something went wrong!</td>');
            }
        });
    }

    function detail_file(id) {
        $.ajax({
            type: "get",
            url: baseurl + "/workflow-information/" + id,
            cache: false,
            dataType: "json",
            success: function (data) {
                console.log(data);
                var document_link = encodeURI(baseurl + '/sales_uploads/' + data['nama_file']);
                $('.fill-doc-name').html(data['nama_file']);
                $('.fill-doc-title').html(data['title']);
                $('.fill-doc-folder').html(data['folder']);
                $('.fill-doc-version').html(data['version']);
                $('.fill-doc-createdtime').html(data['created_at']);
                $('.fill-doc-createdby').html(data['created_by']);
                $('.fill-doc-updatedtime').html(data['updated_at']);
                $('.fill-doc-updatedby').html(data['updated_by']);
                $('.fill-fileurl').html(document_link);
                $('.fill-fileid').html(id);
                $('#info_no_document').val(data['no_document']);
                $('#info_tgl_document').val(data['tgl_document']);
                $('#info_relasi_document').val(data['relasi_document']);
                $('#info_document_expiration_date').val(data['document_expiration_date']);
                $('#no_document').val(data['no_document']);
                $('#tgl_document').val(data['tgl_document']);
                $('#relasi_document').val(data['relasi_document']);
                $('#document_expiration_date').val(data['document_expiration_date']);
                $('#nama_file').html(data['nama_file']);
                $('#version').html(data['version']);
                $('#created_by').html(data['created_by']);
                $('#keterangan').html(data['keterangan']);
                $('#created_at').html(data['created_at']);

                if (data['size'] > 1024 * 1024) {
                    $('.fill-doc-size').text((data['size'] / (1024 * 1024)).toFixed(2) + " MB");
                } else if (data['size'] > 1024) {
                    $('.fill-doc-size').text((data['size'] / 1024).toFixed(2) + " KB");
                } else {
                    $('.fill-doc-size').text(data['size'] + " bytes");
                }

                // $('.detail').show();
                // $('.preview-placeholder').hide();
                // $('.action').hide();
                // $('body, html').animate({
                //   scrollTop: $(".detail").offset().top
                // }, 600);

                var extension = data['nama_file'].substr((data['nama_file'].lastIndexOf('.') + 1));
                var file_url = encodeURI(baseurl + '/documents/' + data['nama_file']);
                switch (extension) {
                    case 'jpg':
                    case 'png':
                    case 'bmp':
                    case 'jpeg':
                    case 'webp':
                    case 'gif':
                        $('.image-link').attr('src', file_url);
                        $('.image-viewer').show();
                        break;
                    case 'pdf':
                        PDFObject.embed(file_url, ".pdf-link", {height: "30rem"});
                        $('.pdf-viewer').show();
                        break;
                    case 'txt':
                        $('.txt-content').load(file_url);
                        $('.txt-viewer').show();
                        break;
                    default:
                        alert('Preview for This File Currently Not Supported');
                }
            }
        });
    };


    $('#ModalDetail').on('show.bs.modal', function (e) {
        var id = $(e.relatedTarget).data('id');
        console.log(id);
        $.ajax({
            type: "get",
            url: baseurl + "/workflow/get_detail/" + id,
            success: function (data) {
                $('#form_var_detail').html(data);
            },
            timeout: 3000 // sets timeout to 3 seconds
        });
        $('#id').val(id);
    });
    $('#ModalApprove').on('show.bs.modal', function (e) {
        var id = $(e.relatedTarget).data('id');
        console.log(id);
        $('#id_workflow').val(id);
    });
    $('#ModalDetailMessage').on('show.bs.modal', function (e) {
        var id = $(e.relatedTarget).data('id');
        console.log(id);
        $('#form_var_message').html('');
        $.ajax({
            type: "get",
            url: baseurl + "/private-message/detail/" + id,
            success: function (data) {
                $('#form_var_message').html(data);
            },
            timeout: 3000 // sets timeout to 3 seconds
        });
    });
    $('#ModalDetailMessageBroadcast').on('show.bs.modal', function (e) {
        var id = $(e.relatedTarget).data('id');
        console.log(id);
        $('#form_var_message').html('');
        $.ajax({
            type: "get",
            url: baseurl + "/broadcast-message/detail/" + id,
            success: function (data) {
                $('#form_var_message').html(data);
            },
            timeout: 3000 // sets timeout to 3 seconds
        });
    });
    $('#ModalPurchaseApprove').on('show.bs.modal', function (e) {
        var id = $(e.relatedTarget).data('id');
        console.log(id);
        $('#sales-approve').attr('href', baseurl + '/sales-invoice/approve/' + id);
        $('#sales-reject').attr('href', baseurl + '/sales-invoice/reject/' + id);
        $.ajax({
            type: "get",
            url: baseurl + "/purchasing-information/" + id,
            cache: false,
            dataType: "json",
            success: function (data) {
                console.log(data);
                var document_link = encodeURI(baseurl + '/sales_uploads/' + data['nama_file']);
                $('.fill-doc-name').html(data['nama_file']);
                $('.fill-doc-title').html(data['title']);
                $('.fill-doc-folder').html(data['folder']);
                $('.fill-doc-version').html(data['version']);
                $('.fill-doc-createdtime').html(data['created_at']);
                $('.fill-doc-createdby').html(data['created_by']);
                $('.fill-doc-updatedtime').html(data['updated_at']);
                $('.fill-doc-updatedby').html(data['updated_by']);
                $('.fill-fileurl').html(document_link);
                $('.fill-fileid').html(id);
                $('#info_no_document').val(data['no_document']);
                $('#info_tgl_document').val(data['tgl_document']);
                $('#info_relasi_document').val(data['relasi_document']);
                $('#info_document_expiration_date').val(data['document_expiration_date']);
                $('#no_document').val(data['no_document']);
                $('#tgl_document').val(data['tgl_document']);
                $('#relasi_document').val(data['relasi_document']);
                $('#document_expiration_date').val(data['document_expiration_date']);
                $('#nama_file').html(data['nama_file']);
                $('#version').html(data['version']);
                $('#created_by').html(data['created_by']);
                $('#keterangan').html(data['keterangan']);
                $('#created_at').html(data['created_at']);

                if (data['size'] > 1024 * 1024) {
                    $('.fill-doc-size').text((data['size'] / (1024 * 1024)).toFixed(2) + " MB");
                } else if (data['size'] > 1024) {
                    $('.fill-doc-size').text((data['size'] / 1024).toFixed(2) + " KB");
                } else {
                    $('.fill-doc-size').text(data['size'] + " bytes");
                }

                // $('.detail').show();
                // $('.preview-placeholder').hide();
                // $('.action').hide();
                // $('body, html').animate({
                //   scrollTop: $(".detail").offset().top
                // }, 600);

                var extension = data['nama_file'].substr((data['nama_file'].lastIndexOf('.') + 1));
                var file_url = encodeURI(baseurl + '/documents/' + data['nama_file']);
                switch (extension) {
                    case 'jpg':
                    case 'png':
                    case 'bmp':
                    case 'jpeg':
                    case 'webp':
                    case 'gif':
                        $('.image-link').attr('src', file_url);
                        $('.image-viewer').show();
                        break;
                    case 'pdf':
                        PDFObject.embed(file_url, ".pdf-link", {height: "30rem"});
                        $('.pdf-viewer').show();
                        break;
                    case 'txt':
                        $('.txt-content').load(file_url);
                        $('.txt-viewer').show();
                        break;
                    default:
                        alert('Preview for This File Currently Not Supported');
                }
            }
        });
    });
    $('#ModalPurchaseStatus').on('show.bs.modal', function (e) {
        var id = $(e.relatedTarget).data('id');
        console.log(id);
        $.ajax({
            type: "get",
            url: baseurl + "/purchasing-invoice/status/" + id,
            success: function (data) {
                $('#form_var_purchase_status').html(data);
            }
        });
    });

    function preview(id_file) {
        $('.detail').hide();
        $('.action').hide();
        $('.preview-placeholder').hide();
        $.ajax({
            type: "get",
            url: baseurl + "/file-information/" + id_file,
            cache: false,
            dataType: "json",
            success: function (data) {
                var extension = data['nama_file'].substr((data['nama_file'].lastIndexOf('.') + 1));
                var file_url = encodeURI(baseurl + '/documents/' + data['nama_file']);
                switch (extension) {
                    case 'jpg':
                    case 'png':
                    case 'bmp':
                    case 'jpeg':
                    case 'webp':
                    case 'gif':
                        $('.image-link').attr('src', file_url);
                        $('.image-viewer').show();
                        break;
                    case 'pdf':
                        PDFObject.embed(file_url, ".pdf-link", {height: "30rem"});
                        $('.pdf-viewer').show();
                        break;
                    case 'txt':
                        $('.txt-content').load(file_url);
                        $('.txt-viewer').show();
                        break;
                    default:
                        alert('Preview for This File Currently Not Supported');
                }
                $('body, html').animate({
                    scrollTop: $(".detail").offset().top
                }, 600);
            }
        });
    }

    function openaction(id_file) {
        $.ajax({
            type: "get",
            url: baseurl + "/file-information/" + id_file,
            cache: false,
            dataType: "json",
            success: function (data) {
                var document_link = encodeURI(baseurl + '/documents/' + data['nama_file']);
                $('.fill-doc-name').text(data['nama_file']);
                $('.fill-fileurl').val(document_link);
                $('.fill-email').text('\n\nDownload Link:\n' + document_link);
                $('.fill-fileid').val(id_file);
                $('.fill-trashid').attr('href', baseurl + '/move-trash/' + id_file);
                $('.card-dynamic').removeClass('col-lg-12');
                $('.detail').hide();
                $('.preview-placeholder').hide();
                $('.action').show();
                $('body, html').animate({
                    scrollTop: $(".detail").offset().top
                }, 600);
            }
        });
    }

    function action() {
        $('tbody tr td button.button-action').show();
    }

    function root(id, path) {
        // var root = document.getElementById('root');
        // var rowid = root.getAttribute('data-id');
        $.ajax({
            type: "post",
            url: baseurl + "/root/get_file",
            cache: false,
            data: {rowid: id, folder: path},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $('.file').show();
                $('.file-list').html(data);
                $('.upload-folder-id').val(id);
            }
        });
        // $('.root-folder').click(function(){
        //     var rowid = $(this).data('id');
        // });
    }

    $('.detail').hide();
    $('.action').hide();
    $('.file').hide();
    $('.upload-success').hide();
    $('.upload-document').hide();
    $('.info-root').click(function () {
        $('.detail').show();
    });
    $('.preview').click(function () {
        $('.iframe').show();
        $('.detail').hide();
    });

    $(document).on('click', '.demo, .caret', function () {
        var rowid = $(this).data('id');
        var folder = $(this).data('folder');
        $(".mdi-folder-open").addClass("mdi-folder");
        $(".mdi-folder-open").removeClass("mdi-folder-open");
        $(".left-active").removeClass("left-active");

        $(this).addClass("left-active");
        $(".left-active .mdi").removeClass("mdi-folder");
        $(".left-active .mdi").addClass("mdi-folder-open");



        $(this).toggleClass('caret-down');

        if ($(this).closest('li').next('#demo').children().html().length > 0) {
            $(this).closest('li').next('#demo').children('div').empty();
        } else {
            $.ajax({
                type: "POST",
                url: baseurl + "/root/get_sub",
                cache: false,
                data: {rowid: rowid, folder: folder},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    $('.sub-list-' + rowid).html(data);
                }
            });

            root(rowid, folder)
        }
    });
    $('.upload').click(function () {
        $('.upload-document').show();
    });
    $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
        if (!$(this).next().hasClass('show')) {
            $(this).parents('.dropdown-menu').first().find('.show').removeClass('show');
        }
        var $subMenu = $(this).next('.dropdown-menu');
        $subMenu.toggleClass('show');


        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
            $('.dropdown-submenu .show').removeClass('show');
        });


        return false;
    });


    // NEW DROPZONE SETTING
    Dropzone.options.myDropzone = {
        url: baseurl + "/file_upload",
        autoProcessQueue: false,
        uploadMultiple: false,
        maxFiles: 1,
        maxFilesize: 2,
        addRemoveLinks: true,
        paramName: "file",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        init: function () {
            dzClosure = this;

            document.getElementById("submit-upload").addEventListener("click", function (e) {
                e.preventDefault();
                e.stopPropagation();
                handlingCrudAction(dzClosure, true);
            });

            this.on("sending", function (data, xhr, formData) {
                formData.append("id_directory", $(".upload-folder-id").val());
            });

            this.on("success", function (file, response) {
                $('#modal-upload').modal('hide');
                $('.message').html(response.message);
                $('.upload-success').show();
                $('#msg-alert-upload').hide();
                location.reload();
            });

            this.on("error", function (file, response) {
                $('#msg-alert-upload').show();
                $('#error-list').html(response.errors.file[0])
            });
        }
    }
    // END DROPZONE

    Dropzone.options.reupload = {
        url: "{{ route(routePrefix().'upload-file.reupload') }}",
        autoProcessQueue: false,
        uploadMultiple: false,
        maxFiles: 1,
        maxFilesize: 2,
        addRemoveLinks: true,
        paramName: "file",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        init: function () {
            dzClosure2 = this;

            document.getElementById("reupload-submit").addEventListener("click", function (e) {
                e.preventDefault();
                e.stopPropagation();
                dzClosure2.processQueue();
            });

            this.on("sending", function (data, xhr, formData) {
                formData.append("file_id", $("#file_id").val());
                formData.append("keterangan", $("#keterangan-reupload").val());
            });

            this.on("success", function (file, response) {
                $('.message').html(response.message);
                $('.upload-success').show();
                $('#msg-alert-reupload').hide();
                location.reload();
            });

            this.on("error", function (file, response) {
                $('#msg-alert-reupload').show();
                $('#error-list-reupload').html(response.message)
            });
        }
    }

</script>

<script>
    $(function () {

        var files = [
            {
                "id": 1,
                "name": "Documents",
                "dateModified": "9/12/2013",
                "type": "File Folder",
                "size": 4480,
                "files": [
                    {"id": 11, "name": "To do list.txt", "dateModified": "11/5/2013", "type": "TXT File", "size": 4448},
                    {"id": 12, "name": "Work.txt", "dateModified": "9/12/2013", "type": "TXT File", "size": 32}
                ]
            },
            {
                "id": 2, "name": "Music", "dateModified": "6/10/2014", "type": "File Folder", "size": 5594, "files": [
                    {
                        "id": 21,
                        "name": "AC/DC",
                        "dateModified": "6/10/2014",
                        "type": "File Folder",
                        "size": 2726,
                        "files": [
                            {
                                "id": 211,
                                "name": "Stand Up.mp3",
                                "dateModified": "6/10/2014",
                                "type": "MP3 File",
                                "size": 456
                            },
                            {
                                "id": 212,
                                "name": "T.N.T.mp3",
                                "dateModified": "6/10/2014",
                                "type": "MP3 File",
                                "size": 1155
                            },
                            {
                                "id": 213,
                                "name": "The Jack.mp3",
                                "dateModified": "6/10/2014",
                                "type": "MP3 File",
                                "size": 1115
                            }
                        ]
                    },
                    {
                        "id": 22,
                        "name": "WhiteSnake",
                        "dateModified": "6/11/2014",
                        "type": "File Folder",
                        "size": 2868,
                        "files": [
                            {
                                "id": 221,
                                "name": "Trouble.mp3",
                                "dateModified": "6/11/2014",
                                "type": "MP3 File",
                                "size": 1234
                            },
                            {
                                "id": 222,
                                "name": "Bad Boys.mp3",
                                "dateModified": "6/11/2014",
                                "type": "MP3 File",
                                "size": 522
                            },
                            {
                                "id": 223,
                                "name": "Is This Love.mp3",
                                "dateModified": "6/11/2014",
                                "type": "MP3 File",
                                "size": 1112
                            },
                        ]
                    }
                ]
            },
            {
                "id": 3, "name": "Pictures", "dateModified": "1/9/2014", "type": "File Folder", "size": 1825, "files": [
                    {
                        "id": 31,
                        "name": "Jacks Birthday",
                        "dateModified": "6/9/2014",
                        "type": "File Folder",
                        "size": 631,
                        "files": [
                            {
                                "id": 311,
                                "name": "Picture1.png",
                                "dateModified": "6/9/2014",
                                "type": "PNG image",
                                "size": 493
                            },
                            {
                                "id": 312,
                                "name": "Picture2.png",
                                "dateModified": "6/9/2014",
                                "type": "PNG image",
                                "size": 88
                            },
                            {
                                "id": 313,
                                "name": "Picture3.gif",
                                "dateModified": "6/9/2014",
                                "type": "GIF File",
                                "size": 50
                            },
                        ]
                    },
                    {
                        "id": 32,
                        "name": "Trip to London",
                        "dateModified": "3/10/2014",
                        "type": "File Folder",
                        "size": 1194,
                        "files": [
                            {
                                "id": 321,
                                "name": "Picture1.png",
                                "dateModified": "3/10/2014",
                                "type": "PNG image",
                                "size": 974
                            },
                            {
                                "id": 322,
                                "name": "Picture2.png",
                                "dateModified": "3/10/2014",
                                "type": "PNG image",
                                "size": 142
                            },
                            {
                                "id": 323,
                                "name": "Picture3.png",
                                "dateModified": "3/10/2014",
                                "type": "PNG image",
                                "size": 41
                            },
                            {
                                "id": 324,
                                "name": "Picture4.png",
                                "dateModified": "3/10/2014",
                                "type": "PNG image",
                                "size": 25
                            },
                            {
                                "id": 325,
                                "name": "Picture5.png",
                                "dateModified": "3/10/2014",
                                "type": "PNG image",
                                "size": 12
                            },
                        ]
                    }
                ]
            },
            {
                "id": 4, "name": "Videos", "dateModified": "1/4/2014", "type": "File Folder", "size": 0

            },
            {
                "id": 5, "name": "Books", "dateModified": "1/4/2014", "type": "File Folder", "size": 99376, "files": [
                    {
                        "id": 51,
                        "name": "James Rollins",
                        "dateModified": "6/2/2014",
                        "type": "File Folder",
                        "size": 34228,
                        "files": [
                            {
                                "id": 511,
                                "name": "Alter of Eden.pdf",
                                "dateModified": "6/5/2014",
                                "type": "Adobe Acrobat Document",
                                "size": 8894
                            },
                            {
                                "id": 512,
                                "name": "Amazonia.pdf",
                                "dateModified": "3/2/2014",
                                "type": "Adobe Acrobat Document",
                                "size": 6212
                            },
                            {
                                "id": 513,
                                "name": "Subterranean.pdf",
                                "dateModified": "1/4/2014",
                                "type": "Adobe Acrobat Document",
                                "size": 4820
                            },
                            {
                                "id": 514,
                                "name": "Sandstorm.pdf",
                                "dateModified": "2/2/2014",
                                "type": "Adobe Acrobat Document",
                                "size": 14302
                            }
                        ]
                    },
                    {
                        "id": 52,
                        "name": "Stephen King",
                        "dateModified": "3/10/2014",
                        "type": "File Folder",
                        "size": 65148,
                        "files": [
                            {
                                "id": 521,
                                "name": "It.pdf",
                                "dateModified": "3/10/2014",
                                "type": "Adobe Acrobat Document",
                                "size": 9987
                            },
                            {
                                "id": 522,
                                "name": "Misery.pdf",
                                "dateModified": "3/10/2014",
                                "type": "Adobe Acrobat Document",
                                "size": 32313
                            },
                            {
                                "id": 523,
                                "name": "Pet Sematary.pdf",
                                "dateModified": "3/10/2014",
                                "type": "Adobe Acrobat Document",
                                "size": 22848
                            }
                        ]
                    }
                ]
            },
            {"id": 6, "name": "Games", "dateModified": "8/8/2014", "type": "File Folder", "size": 0},
            {
                "id": 7, "name": "Projects", "dateModified": "7/4/2014", "type": "File Folder", "size": 4, "files": [
                    {
                        "id": 71,
                        "name": "Visual Studio 2012",
                        "dateModified": "7/4/2014",
                        "type": "File Folder",
                        "size": 1,
                        "files": [
                            {
                                "id": 711,
                                "name": "Project10.sln",
                                "dateModified": "7/4/2014",
                                "type": "Microsoft Visual Studio Solution",
                                "size": 1
                            }
                        ]
                    },
                    {
                        "id": 72,
                        "name": "Visual Studio 2013",
                        "dateModified": "9/6/2014",
                        "type": "File Folder",
                        "size": 3,
                        "files": [
                            {
                                "id": 721,
                                "name": "Project1.sln",
                                "dateModified": "9/6/2014",
                                "type": "Microsoft Visual Studio Solution",
                                "size": 1
                            },
                            {
                                "id": 722,
                                "name": "Project2.sln",
                                "dateModified": "9/6/2014",
                                "type": "Microsoft Visual Studio Solution",
                                "size": 1
                            },
                            {
                                "id": 723,
                                "name": "Project3.sln",
                                "dateModified": "9/6/2014",
                                "type": "Microsoft Visual Studio Solution",
                                "size": 1
                            }
                        ]
                    }
                ]
            }
        ];

        $("#treegrid").igTreeGrid({
            width: "1000px",
            height: "400px",
            dataSource: files,
            autoGenerateColumns: false,
            primaryKey: "id",
            columns: [
                {headerText: "ID", key: "id", width: "250px", dataType: "number", hidden: true},
                {headerText: "Name", key: "name", width: "250px", dataType: "string"},
                {headerText: "Date Modified", key: "dateModified", width: "130px", dataType: "date"},
                {headerText: "Type", key: "type", width: "230px", dataType: "string"},
                {headerText: "Size in KB", key: "size", width: "130px", dataType: "number"}
            ],
            childDataKey: "files",
            initialExpandDepth: 2,
            features: [
                {
                    name: "Selection",
                    multipleSelection: false
                },
                {
                    name: "RowSelectors",
                    enableCheckBoxes: true,
                    checkBoxMode: "biState",
                    enableSelectAllForPaging: true,
                    enableRowNumbering: false
                },
                {
                    name: "Sorting"
                },
                {
                    name: "Filtering",
                    columnSettings: [
                        {
                            columnKey: "dateModified",
                            condition: "after"
                        },
                        {
                            columnKey: "size",
                            condition: "greaterThan"
                        }
                    ]
                },
                {
                    name: "Paging",
                    pageSize: 4
                }]
        });
    });

    $("#phone").on("input", function () {
        this.value = this.value.replace(/\D/g, '');
    });

    function openParentFolder(dirId, folder) {
        var pathname = window.location.pathname.split("/").pop();

        if (pathname == 'root') {
            // $.ajax({
            //     type: "GET",
            //     url: "{{ route(routePrefix() . 'getFileFolder') }}",
            //     data: {
            //         rowid: dirId,
            //         folder: folder + ";"
            //     },
            //     success: function(response) {
            //         $('#foldersGroup').html(response);
            //         $('#main-folders').addClass('flex-column');
            //         $('#main-files').addClass('flex-column');
            //         $('#btn-grid').removeClass('active')
            //         $('#btn-list').addClass('active')
            //         root(dirId, folder);
            //     }
            // });

            // $('#parent-folder-' + dirId).next('ul').remove();

            // $.ajax({
            //     type: "GET",
            //     url: "{{ route(routePrefix() . 'getFolderSidebar') }}",
            //     data: {
            //         dirId: dirId
            //     },
            //     success: function(response) {
            //         $('#parent-folder-' + dirId).after(response);
            //     }
            // });

            // $.ajax({
            //     type: "GET",
            //     url: "{{ route(routePrefix() . 'getFileDocumentExplorer') }}",
            //     data: {
            //         dirId: dirId
            //     },
            //     success: function(response) {
            //         $('#list-document-explorer').html(response);
            //     }
            // });
        } else {
            window.location.href = "{{ url('/root') }}";
        }
    }

    function rightClickFile(fileid) {
        var $contextMenu = $("#rightClickFile");

        $contextMenu.css({
            display: "block",
            // left: e.pageX,
            // top: e.pageY
        });
    }

    $(function () {

        var $contextMenu = $("#rightClickFile");


        $("body").on("contextmenu", ".file-div", function (e) {
            let fileid = $(this).data("fileid");
            $contextMenu.css({
                display: "block",
                left: e.pageX,
                top: e.pageY
            });

            $("[data-toggle='popover']").popover('hide');
            $('.fill-fileid').val(fileid);

            console.log(fileid);

            return false;
        });

        $('html').click(function () {
            $contextMenu.hide();
        });

        $("#rightClickFile li a").click(function (e) {
            var f = $(this);
        });

    });

    function triggerAction(eventname) {
        let fileid = $('.fill-fileid').val();

        if (eventname == 'preview') {
            preview(fileid);
        } else if (eventname == 'action') {
            openaction(fileid);
        } else {
            detail(fileid);
        }
    }

    function relatedDokumen() {
        let file_id = $('.fill-fileid').val();
        $.ajax({
            type: "GET",
            url: "{{ route(routePrefix().'getRelatedDokumen') }}",
            data: {id: file_id},
            success: function (response) {
                $('#msg-relation-red').hide();
                $('#msg-relation-green').hide();
                $('#relate-dokumen-body').html(response);
                $('#relateModal').modal('show');
            },
            error: function (request, status, error) {
                $('#relate-dokumen-body').html('<center><b>Oops, Something went wrong!</b></center>');
                $('#relateModal').modal('show');
            }
        });
    }

    function structureRelationDokumen() {
        let file_id = $('.fill-fileid').val();
        // window.open("{{ urlWithPrefix('/') }}/root/structure/"+file_id+"/relation", '_blank');
        $.ajax({
            type: "GET",
            url: "{{ urlWithPrefix('/') }}/root/structure/" + file_id + "/relation",
            success: function (response) {
                $('#relationTree-body').html(response);
                $('#relationTree').modal('show');
            },
            error: function (request, status, error) {
                $('#relationTree-body').html('<center><b>Oops, Something went wrong!</b></center>');
                $('#relationTree').modal('show');
            }
        });
    }

</script>
<script type="text/javascript">
    $('#ModalPin').on('show.bs.modal', function () {
        // Load up a new modal...
        $('#ModalApprove').modal('hide')
    });

    $("form").submit(function (event) {
        event.preventDefault();
        handlingCrudAction($(this));
    });

    function handlingCrudAction(form, dropzone = false) {

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, please!'
        }).then((result) => {
            if (result.isConfirmed) {
                if (dropzone) {
                    form.processQueue();
                } else {
                    form.unbind('submit').submit();
                }
            } else {
                return;
            }
        })
    }

    (function ($) {
        $.fn.inputFilter = function (callback, errMsg) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop focusout", function (e) {
                if (callback(this.value)) {
                    // Accepted value
                    if (["keydown", "mousedown", "focusout"].indexOf(e.type) >= 0) {
                        $(this).removeClass("input-error");
                        this.setCustomValidity("");
                    }
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    // Rejected value - restore the previous one
                    $(this).addClass("input-error");
                    this.setCustomValidity(errMsg);
                    this.reportValidity();
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    // Rejected value - nothing to restore
                    this.value = "";
                }
            });
        };
    }(jQuery));
</script>