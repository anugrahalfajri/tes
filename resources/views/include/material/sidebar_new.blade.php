      <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
      <style>
          #togglerMenu2 {
              display: none;
          }

          @media(max-width: 751.99px)  {
              #oldToggler {
                  display: inline !important;

              }
              #newToggler {
                  display: none !important;
              }
          }
        @media(min-width: 752px) and (max-width: 975.99px) {
            #oldToggler {

                display: none !important;

            }
            #newToggler {
                display: inline !important;
            }

            #left-sidebar-dd {
                left: -240px
            }
        }

          @media(min-width: 976px) {
              #oldToggler {
                  display: none !important;

              }
              #newToggler {
                  display: none !important;
              }
          }






      </style>

        @if(auth()->user()->role->name == 'user')
        <style>
            .sidebar-custom {
                width: inherit !important;
            }
            .sidebar-custom .hide-menu {
                width: 125px !important;
                white-space: nowrap !important;
                overflow: hidden !important;
                text-overflow: ellipsis !important;
            }
        </style>
        <aside id="left-sidebar-dd" class="left-sidebar">
            <!-- Sidebar scroll-->
            <input type="hidden" id="togglerstats" value="ax">
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav d-flex justify-content-center" style="background: none !important;">
                    <ul id="sidebar-nav d-flex justify-content-center col-md-12" style="padding:0; background: none !important; ">
                        <!-- User Profile-->
                        {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Personal</span></li> --}}
                        {{-- @if (App\Models\OrganizationAccess::checkPermission("document-explore-view", Auth::user()->id))
                            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="{{ urlWithPrefix('/root') }}" aria-expanded="false"><i class="mdi mdi-folder-multiple"></i><span class="hide-menu">Document Explore</span></a></li>
                        @endif --}}
                        @if (App\Models\OrganizationAccess::checkPermission("document-search-view", Auth::user()->id))
                            <li class="sidebar-item  d-flex justify-content-center" style="margin-top: 5px; margin-left: 8px; margin-right: 8px"> <a class="sidebar-link waves-effect waves-dark" href="{{ urlWithPrefix('/document-search') }}" aria-expanded="false"><i class="mdi mdi-search-web"></i><span class="hide-menu">Document Search</span></a></li>
                        @endif
                        @if (App\Models\OrganizationAccess::checkPermission("private-message-view", Auth::user()->id))
                            <li class="sidebar-item  d-flex justify-content-center" style="margin-top: 5px; margin-left: 8px; margin-right: 8px"> <a class="sidebar-link waves-effect waves-dark" href="{{ urlWithPrefix('/private-message') }}" aria-expanded="false"><i class="mdi mdi-email"></i><span class="hide-menu">Private Message</span></a></li>
                        @endif
                        @if (App\Models\OrganizationAccess::checkPermission("workflow-view", Auth::user()->id))
                            <li class="sidebar-item  d-flex justify-content-center" style="margin-top: 5px; margin-left: 8px; margin-right: 8px"> <a class="sidebar-link waves-effect waves-dark" href="{{ urlWithPrefix('/workflow') }}" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">Workflow</span></a></li>
                        @endif
                        @if (App\Models\OrganizationAccess::checkPermission("approval-workflow-view", Auth::user()->id))
                            <li class="sidebar-item  d-flex justify-content-center" style="margin-top: 5px; margin-left: 8px; margin-right: 8px"> <a class="sidebar-link waves-effect waves-dark" href="{{ urlWithPrefix('/approval-workflow') }}" aria-expanded="false"><i class="mdi mdi-checkbox-multiple-marked"></i><span class="hide-menu">Approval Document</span></a></li>
                        @endif
                        @if (App\Models\OrganizationAccess::checkPermission("eform-view", Auth::user()->id))
                            <li class="sidebar-item  d-flex justify-content-center" style="margin-top: 5px; margin-left: 8px; margin-right: 8px"> <a class="sidebar-link waves-effect waves-dark" href="{{ urlWithPrefix('/eform') }}" aria-expanded="false"><i class="mdi mdi-file-document"></i><span class="hide-menu">E-form</span></a></li>
                        @endif
                        @if (App\Models\OrganizationAccess::checkPermission("trash-view", Auth::user()->id))
                            <li class="sidebar-item  d-flex justify-content-center" style="margin-top: 5px; margin-left: 8px; margin-right: 8px"> <a class="sidebar-link waves-effect waves-dark" href="{{ urlWithPrefix('/trash') }}" aria-expanded="false"><i class="mdi mdi-delete"></i><span class="hide-menu">Trash</span></a></li>
                        @endif
                        {{-- <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="{{ urlWithPrefix('/logout') }}" aria-expanded="false"><i class="mdi mdi-power"></i><span class="hide-menu">Logout</span></a></li> --}}
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <div class="w3-sidebar w3-white w3-bar-block">
            <a id="toggleSidebar" href="#"><i class="mdi mdi-chevron-right" onclick="open_folder_sidebar();" hidden></i></a>
            <a id="toggleSidebar" href="#"><i class="mdi mdi-chevron-left" onclick="close_folder_sidebar();"></i></a>
            <h3 class="w3-bar-item">Folder List</h3>
            <nav class="sidebar-nav">
                <ul id="sidebarnav">
                    @if(!empty($root_dir))
                        @foreach($root_dir as $row)
                            <li class="sidebar-item sidebar-custom" id="parent-folder-{{$row->id}}">
                                <a class="sidebar-link waves-effect waves-dark" href="javascript:;" title="{{ $row->nama_folder }}" aria-expanded="false" onclick="openParentFolder({{$row->id}}, '{{$row->nama_folder}}')"><i class="mdi mdi-folder"></i><span class="hide-menu">{{$row->nama_folder}} </span></a>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </nav>
        </div>
        @else
        <aside class="left-sidebar" data-scroll-to-active="true">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                {{-- <div class="user-profile" style="background: url({{url('images/background/user-info.jpg')}}) no-repeat;">
                    <!-- User profile image -->
                    <div class="profile-img"> <img src="{{url('images/users/profile.png')}}" alt="user" /> </div>
                    <!-- User profile text-->
                </div> --}}
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <!-- <li><a href="{{ URL::to('/dashboard/dark') }}">Dark Version</a></li> -->
            @if(auth()->user()->role->name == 'admin')
                        <li>
                            <a class="waves-effect waves-dark" href="{{ urlWithPrefix('/') }}" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard </span></a>
                        </li>
                        
                        <li>
                            <a class="has-arrow waves-effect waves-dark" href="{{ urlWithPrefix('/c') }}" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">Message</span></a>
                            <ul aria-expanded="false" class="collapse">
                                @if (App\Models\OrganizationAccess::checkPermission("broadcast-message-view", Auth::user()->id))
                                <li><a href="{{ urlWithPrefix('/broadcast-message') }}"><i class="ti-user"></i>&nbsp; Broadcast Message</a></li>
                                @endif
                                {{-- @if (App\Models\OrganizationAccess::checkPermission("private-message-view", Auth::user()->id))
                                <li><a href="{{ urlWithPrefix('/private-message') }}">Private Message</a></li>
                                @endif --}}
                            </ul>
                        </li>

                        @if (App\Models\OrganizationAccess::checkPermission("workflow-view", Auth::user()->id))
                        <li> 
                            <a class="waves-effect waves-dark" href="{{ urlWithPrefix('/workflow') }}" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">Workflow</span></a>
                        </li>
                        @endif

                        @if (App\Models\OrganizationAccess::checkPermission("approval-workflow-view", Auth::user()->id))
                        <li> 
                            <a class="waves-effect waves-dark" href="{{ urlWithPrefix('/approval-workflow') }}" aria-expanded="false"><i class="mdi mdi-checkbox-multiple-marked"></i><span class="hide-menu">Approval Document</span></a>
                        </li>
                        @endif
                        <!-- <li><i class=""></i>Organization</li>
                        <li><i class=""></i>Directory Management</li>
                        <li><i class=""></i>Workflow Setting</li>
                        <li><i class=""></i>Access Rule</li>
                        <li><i class=""></i>System Report</li>
                        <li><i class=""></i>Document Settings</li>
                        <li><i class=""></i>Messages</li>
                        <li><i class=""></i>Account Info</li> -->

                        {{-- @if (App\Models\OrganizationAccess::checkPermission("document-explore-view", Auth::user()->id))
                        <li class="">
                            <a class="waves-effect waves-dark" href="{{ urlWithPrefix('/root') }}" aria-expanded="false"><i class="mdi mdi-folder-multiple"></i><span class="hide-menu">Document Explore</span></a>
                        </li>
                        @endif --}}
                        
                        @if (App\Models\OrganizationAccess::checkPermission("document-search-view", Auth::user()->id))
                        <li class="">
                            <a class="waves-effect waves-dark" href="{{ urlWithPrefix('/document-search') }}" aria-expanded="false"><i class="mdi mdi-search-web"></i><span class="hide-menu">Document Search</span></a>
                        </li>
                        @endif

                        @if (App\Models\OrganizationAccess::checkPermission("trash-view", Auth::user()->id))
                        <li class="">
                            <a class="waves-effect waves-dark" href="{{ urlWithPrefix('/trash') }}" aria-expanded="false"><i class="mdi mdi-delete"></i><span class="hide-menu">Trash</span></a>
                        </li>
                        @endif
                        
                        @if (App\Models\OrganizationAccess::checkPermission("organization-view", Auth::user()->id))
                        <li>
                           <a class="waves-effect waves-dark" href="{{ urlWithPrefix('/organization') }}" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">{{ auth()->user()->role->name == 'master admin' ? 'Client Organization' : 'Organization' }}</span></a>
                        </li>
                        @endif

                        @if (App\Models\OrganizationAccess::checkPermission("sub-organization-view", Auth::user()->id))
                        <li>
                            <a class="waves-effect waves-dark" href="{{ urlWithPrefix('/sub_organization') }}" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Sub Organization</span></a>
                        </li>
                        @endif

                        {{-- <li>
                            <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Structure Organization</span></a>
                        </li> --}}

                        {{-- @if (App\Models\OrganizationAccess::checkPermission("user-management-view", Auth::user()->id))
                        <li>
                           <a class="waves-effect waves-dark" href="{{ urlWithPrefix('/users') }}" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">User Management</span></a>
                        </li>
                        @endif --}}

                        @if (App\Models\OrganizationAccess::checkPermission("eform-view", Auth::user()->id))
                        <li>
                            <a class="waves-effect waves-dark" href="{{ urlWithPrefix('/eform') }}" aria-expanded="false"><i class="mdi mdi-file-document"></i><span class="hide-menu">E-form</span></a>
                        </li>
                        @endif

                        <li class=""><a class="waves-effect waves-dark" href="{{ urlWithPrefix('/config') }}" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Konfigurasi</span></a>
                        </li>

                        <li><a href="{{ urlWithPrefix('/logout') }}"><i class="mdi mdi-power"></i><span class="hide-menu">Logout</span></a></li>
                        {{-- @elseif(auth()->user()->role->name == 'user')
                        
                            @if(!empty($root_dir))
                                @foreach($root_dir as $row)
                                    <li>
                                        <a class="waves-effect waves-dark" href="javascript:;" aria-expanded="false" onclick="openParentFolder({{$row->id}}, '{{$row->nama_folder}}')"><i class="mdi mdi-folder"></i><span class="hide-menu">{{$row->nama_folder}} </span></a>
                                    </li>
                                @endforeach
                            @endif --}}

            @elseif(auth()->user()->role->name == 'admin organisasi')

                        @if (App\Models\OrganizationAccess::checkPermission("document-explore-view", Auth::user()->id))
                        <li class="">
                            <a class="waves-effect waves-dark" href="{{ urlWithPrefix('/root') }}" aria-expanded="false"><i class="mdi mdi-folder-multiple"></i><span class="hide-menu">Document Explore</span></a>
                        </li>
                        @endif
                        
                        @if (App\Models\OrganizationAccess::checkPermission("document-search-view", Auth::user()->id))
                        <li class="">
                            <a class="waves-effect waves-dark" href="{{ urlWithPrefix('/document-search') }}" aria-expanded="false"><i class="mdi mdi-search-web"></i><span class="hide-menu">Document Search</span></a>
                        </li>
                        @endif
                        
                        @if (App\Models\OrganizationAccess::checkPermission("workflow-view", Auth::user()->id))
                        <li> 
                            <a class="waves-effect waves-dark" href="{{ urlWithPrefix('/workflow') }}" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">Pengaturan Workflow</span></a>
                        </li>
                        @endif

                        @if (App\Models\OrganizationAccess::checkPermission("approval-workflow-view", Auth::user()->id))
                        <li> 
                            <a class="waves-effect waves-dark" href="{{ urlWithPrefix('/approval-workflow') }}" aria-expanded="false"><i class="mdi mdi-checkbox-multiple-marked"></i><span class="hide-menu">Approval Document</span></a>
                        </li>
                        @endif

                        @if (App\Models\OrganizationAccess::checkPermission("eform-view", Auth::user()->id))
                        <li>
                            <a class="waves-effect waves-dark" href="{{ urlWithPrefix('/eform') }}" aria-expanded="false"><i class="mdi mdi-file-document"></i><span class="hide-menu">E-form</span></a>
                        </li>
                        @endif
                        
                        {{-- @if (App\Models\Level::checkLevel("user-management-view", Auth::user()->level)) --}}
                        <li>
                            <a class="waves-effect waves-dark" href="{{ urlWithPrefix('/users') }}" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">User Management</span></a>
                        </li>
                        {{-- @endif --}}


                        <li><a class="waves-effect waves-dark" href="{{ urlWithPrefix('/add-on-setting/organization-access') }}" aria-expanded="false"><i class="ti-settings"></i><span class="hide-menu">Organization Acess</span></a></li>
    
                        <li class=""><a class="waves-effect waves-dark" href="{{ urlWithPrefix('/config') }}" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Konfigurasi</span></a>
                        </li>

                        <li><a class="waves-effect waves-dark" href="{{ urlWithPrefix('/trouble_ticket_list') }}"><i class="ti-user"></i>&nbsp; Trouble Ticket List</a></li>

                        <li class=""><a class="waves-effect waves-dark" href="{{ urlWithPrefix('/logout') }}" aria-expanded="false"><i class="mdi mdi-power"></i><span class="hide-menu">Logout</span></a>
                        </li>

            @elseif(auth()->user()->role->name == 'master admin')
                        <li>
                            <a class="waves-effect waves-dark" href="{{ urlWithPrefix('/') }}" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard </span></a>
                        </li>
                        
                        @if (App\Models\Level::checkLevel("organization-view", Auth::user()->level))
                        <li>
                            <a class="waves-effect waves-dark" href="{{ urlWithPrefix('/organization') }}" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">{{ auth()->user()->role->name == 'master admin' ? 'Client Organization' : 'Organization' }}</span></a>
                        </li>
                        @endif

                        @if (App\Models\Level::checkLevel("add-on-setting-view", Auth::user()->level))
                        <li>
                            <a class="has-arrow waves-effect waves-dark" href="{{ urlWithPrefix('/c') }}" aria-expanded="false"><i class="ti-settings"></i><span class="hide-menu">Add On Setting</span></a>
                            <ul aria-expanded="false" class="collapse">
                                
                                <li>
                                    <a class="has-arrow waves-effect waves-dark" href="{{ urlWithPrefix('/c') }}" aria-expanded="false"><span class="hide-menu">Permission</span></a>
                                    <ul aria-expanded="false" class="collapse">
                                        <li><a href="{{ urlWithPrefix('/add-on-setting') }}">List Permission</a></li>
                                    </ul>
                                </li>
                            </a>
                        </li>
                        <li><a href="{{ urlWithPrefix('/add-on-setting/organization-access') }}">Organization Acess</a></li>

                            </ul>
                        </li>
                        @endif
 
                        {{-- <li>
                            <a class="waves-effect waves-dark" href="{{ urlWithPrefix('/sub_organization') }}" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Sub Organization</span></a>
                        </li> --}}
                        
                        @if (App\Models\Level::checkLevel("user-management-view", Auth::user()->level))
                        <li>
                            <a class="waves-effect waves-dark" href="{{ urlWithPrefix('/master/users') }}" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">User Management</span></a>
                        </li>
                        @endif

                        @if (App\Models\Level::checkLevel("broadcast-message-view", Auth::user()->level))
                        <li><a class="waves-effect waves-dark" href="{{ urlWithPrefix('/broadcast-message') }}"><i class="ti-user"></i>&nbsp; Broadcast Message</a></li>
                        @endif
                        
                        <li><a class="waves-effect waves-dark" href="{{ urlWithPrefix('/trouble_ticket_list') }}"><i class="ti-user"></i>&nbsp; Trouble Ticket List</a></li>
                        
                        <li>
                            <a class="waves-effect waves-dark" href="{{ route(routePrefix().'users.profile', auth()->user()->id) }}" aria-expanded="false"><i class="ti-user"></i><span class="hide-menu">My Profile</span></a>
                        </li>

                        {{-- @if (App\Models\OrganizationAccess::checkPermission("user-management-view", Auth::user()->id)) --}}
                        <li>
                            <a class="waves-effect waves-dark" href="{{ urlWithPrefix('/level') }}" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Level Management</span></a>
                        </li>
                        {{-- @endif --}}
                        
                        
                        <li class=""><a class="waves-effect waves-dark" href="{{ urlWithPrefix('/logout') }}" aria-expanded="false"><i class="mdi mdi-power"></i><span class="hide-menu">Logout</span></a>
                        </li>
                        @endif
                        <!-- <li class="nav-small-cap">PERSONAL</li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="index.html">Dashboard 1</a></li>
                                <li><a href="index2.html">Dashboard 2</a></li>
                                <li><a href="index3.html">Dashboard 3</a></li>
                                <li><a href="index4.html">Dashboard 4</a></li>
                                <li><a href="index5.html">Dashboard 5</a></li>
                                <li><a href="index6.html">Dashboard 6</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-laptop-windows"></i><span class="hide-menu">Template Demos</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="ebar/index.html">Minisidebar</a></li>
                                <li><a href="tal/index2.html">Horizontal</a></li>
                                <li><a href="{{ URL::to('/dashboard/dark') }}">Dark Version</a></li>
                                <li><a href="l-rtl/index4.html">RTL Version</a></li>
                                <li><a href="javascript:angular">Anuglar-CLI Starter kit</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Apps</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="app-calendar.html">Calendar</a></li>
                                <li><a href="app-chat.html">Chat app</a></li>
                                <li><a href="app-ticket.html">Support Ticket</a></li>
                                <li><a href="app-contact.html">Contact / Employee</a></li>
                                <li><a href="app-contact2.html">Contact Grid</a></li>
                                <li><a href="app-contact-detail.html">Contact Detail</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-email"></i><span class="hide-menu">Inbox</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="app-email.html">Mailbox</a></li>
                                <li><a href="app-email-detail.html">Mailbox Detail</a></li>
                                <li><a href="app-compose.html">Compose Mail</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-chart-bubble"></i><span class="hide-menu">Ui Elements</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="ui-cards.html">Cards</a></li>
                                <li><a href="ui-user-card.html">User Cards</a></li>
                                <li><a href="ui-buttons.html">Buttons</a></li>
                                <li><a href="ui-modals.html">Modals</a></li>
                                <li><a href="ui-tab.html">Tab</a></li>
                                <li><a href="ui-tooltip-popover.html">Tooltip &amp; Popover</a></li>
                                <li><a href="ui-tooltip-stylish.html">Tooltip stylish</a></li>
                                <li><a href="ui-sweetalert.html">Sweet Alert</a></li>
                                <li><a href="ui-notification.html">Notification</a></li>
                                <li><a href="ui-progressbar.html">Progressbar</a></li>
                                <li><a href="ui-nestable.html">Nestable</a></li>
                                <li><a href="ui-range-slider.html">Range slider</a></li>
                                <li><a href="ui-timeline.html">Timeline</a></li>
                                <li><a href="ui-typography.html">Typography</a></li>
                                <li><a href="ui-horizontal-timeline.html">Horizontal Timeline</a></li>
                                <li><a href="ui-session-timeout.html">Session Timeout</a></li>
                                <li><a href="ui-session-ideal-timeout.html">Session Ideal Timeout</a></li>
                                <li><a href="ui-bootstrap.html">Bootstrap Ui</a></li>
                                <li><a href="ui-breadcrumb.html">Breadcrumb</a></li>
                                <li><a href="ui-bootstrap-switch.html">Bootstrap Switch</a></li>
                                <li><a href="ui-list-media.html">List Media</a></li>
                                <li><a href="ui-ribbons.html">Ribbons</a></li>
                                <li><a href="ui-grid.html">Grid</a></li>
                                <li><a href="ui-carousel.html">Carousel</a></li>
                                <li><a href="ui-date-paginator.html">Date-paginator</a></li>
                                <li><a href="ui-dragable-portlet.html">Dragable Portlet</a></li>
                            </ul>
                        </li>
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">FORMS, TABLE &amp; WIDGETS</li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">Forms</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="form-basic.html">Basic Forms</a></li>
                                <li><a href="form-layout.html">Form Layouts</a></li>
                                <li><a href="form-addons.html">Form Addons</a></li>
                                <li><a href="form-material.html">Form Material</a></li>
                                <li><a href="form-float-input.html">Floating Lable</a></li>
                                <li><a href="form-pickers.html">Form Pickers</a></li>
                                <li><a href="form-upload.html">File Upload</a></li>
                                <li><a href="form-mask.html">Form Mask</a></li>
                                <li><a href="form-validation.html">Form Validation</a></li>
                                <li><a href="form-dropzone.html">File Dropzone</a></li>
                                <li><a href="form-icheck.html">Icheck control</a></li>
                                <li><a href="form-img-cropper.html">Image Cropper</a></li>
                                <li><a href="form-bootstrapwysihtml5.html">HTML5 Editor</a></li>
                                <li><a href="form-typehead.html">Form Typehead</a></li>
                                <li><a href="form-wizard.html">Form Wizard</a></li>
                                <li><a href="form-xeditable.html">Xeditable Editor</a></li>
                                <li><a href="form-summernote.html">Summernote Editor</a></li>
                                <li><a href="form-tinymce.html">Tinymce Editor</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-table"></i><span class="hide-menu">Tables</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="table-basic.html">Basic Tables</a></li>
                                <li><a href="table-layout.html">Table Layouts</a></li>
                                <li><a href="table-data-table.html">Data Tables</a></li>
                                <li><a href="table-footable.html">Footable</a></li>
                                <li><a href="table-jsgrid.html">Js Grid Table</a></li>
                                <li><a href="table-responsive.html">Responsive Table</a></li>
                                <li><a href="table-bootstrap.html">Bootstrap Tables</a></li>
                                <li><a href="table-editable-table.html">Editable Table</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">Widgets</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="widget-apps.html">Widget Apps</a></li>
                                <li><a href="widget-data.html">Widget Data</a></li>
                                <li><a href="widget-charts.html">Widget Charts</a></li>
                            </ul>
                        </li>
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">EXTRA COMPONENTS</li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-book-multiple"></i><span class="hide-menu">Page Layout</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="layout-single-column.html">1 Column</a></li>
                                <li><a href="layout-fix-header.html">Fix header</a></li>
                                <li><a href="layout-fix-sidebar.html">Fix sidebar</a></li>
                                <li><a href="layout-fix-header-sidebar.html">Fixe header &amp; Sidebar</a></li>
                                <li><a href="layout-boxed.html">Boxed Layout</a></li>
                                <li><a href="layout-logo-center.html">Logo in Center</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-book-open-variant"></i><span class="hide-menu">Sample Pages</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="starter-kit.html">Starter Kit</a></li>
                                <li><a href="pages-blank.html">Blank page</a></li>
                                <li><a href="#" class="has-arrow">Authentication <span class="label label-rounded label-success">6</span></a>
                                    <ul aria-expanded="false" class="collapse">
                                        <li><a href="pages-login.html">Login 1</a></li>
                                        <li><a href="pages-login-2.html">Login 2</a></li>
                                        <li><a href="pages-register.html">Register</a></li>
                                        <li><a href="pages-register2.html">Register 2</a></li>
                                        <li><a href="pages-lockscreen.html">Lockscreen</a></li>
                                        <li><a href="pages-recover-password.html">Recover password</a></li>
                                    </ul>
                                </li>
                                <li><a href="pages-profile.html">Profile page</a></li>
                                <li><a href="pages-animation.html">Animation</a></li>
                                <li><a href="pages-fix-innersidebar.html">Sticky Left sidebar</a></li>
                                <li><a href="pages-fix-inner-right-sidebar.html">Sticky Right sidebar</a></li>
                                <li><a href="pages-invoice.html">Invoice</a></li>
                                <li><a href="pages-treeview.html">Treeview</a></li>
                                <li><a href="pages-utility-classes.html">Helper Classes</a></li>
                                <li><a href="pages-search-result.html">Search result</a></li>
                                <li><a href="pages-scroll.html">Scrollbar</a></li>
                                <li><a href="pages-pricing.html">Pricing</a></li>
                                <li><a href="pages-lightbox-popup.html">Lighbox popup</a></li>
                                <li><a href="pages-gallery.html">Gallery</a></li>
                                <li><a href="pages-faq.html">Faqs</a></li>
                                <li><a href="#" class="has-arrow">Error Pages</a>
                                    <ul aria-expanded="false" class="collapse">
                                        <li><a href="pages-error-400.html">400</a></li>
                                        <li><a href="pages-error-403.html">403</a></li>
                                        <li><a href="pages-error-404.html">404</a></li>
                                        <li><a href="pages-error-500.html">500</a></li>
                                        <li><a href="pages-error-503.html">503</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">Charts</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="chart-morris.html">Morris Chart</a></li>
                                <li><a href="chart-chartist.html">Chartis Chart</a></li>
                                <li><a href="chart-echart.html">Echarts</a></li>
                                <li><a href="chart-flot.html">Flot Chart</a></li>
                                <li><a href="chart-knob.html">Knob Chart</a></li>
                                <li><a href="chart-chart-js.html">Chartjs</a></li>
                                <li><a href="chart-sparkline.html">Sparkline Chart</a></li>
                                <li><a href="chart-extra-chart.html">Extra chart</a></li>
                                <li><a href="chart-peity.html">Peity Charts</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-brush"></i><span class="hide-menu">Icons</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="icon-material.html">Material Icons</a></li>
                                <li><a href="icon-fontawesome.html">Fontawesome Icons</a></li>
                                <li><a href="icon-themify.html">Themify Icons</a></li>
                                <li><a href="icon-linea.html">Linea Icons</a></li>
                                <li><a href="icon-weather.html">Weather Icons</a></li>
                                <li><a href="icon-simple-lineicon.html">Simple Lineicons</a></li>
                                <li><a href="icon-flag.html">Flag Icons</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-map-marker"></i><span class="hide-menu">Maps</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="map-google.html">Google Maps</a></li>
                                <li><a href="map-vector.html">Vector Maps</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-arrange-send-backward"></i><span class="hide-menu">Multi level dd</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="#">item 1.1</a></li>
                                <li><a href="#">item 1.2</a></li>
                                <li> <a class="has-arrow" href="#" aria-expanded="false">Menu 1.3</a>
                                    <ul aria-expanded="false" class="collapse">
                                        <li><a href="#">item 1.3.1</a></li>
                                        <li><a href="#">item 1.3.2</a></li>
                                        <li><a href="#">item 1.3.3</a></li>
                                        <li><a href="#">item 1.3.4</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">item 1.4</a></li>
                            </ul>
                        </li> -->
                        
                         
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->

        </aside>
        @endif
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        
