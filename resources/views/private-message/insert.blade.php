@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">Private Message</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Private Message</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="d-flex justify-content-between">
                            <h4 class="text-white card-title">Private Message - Create</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        @if($action == 'sent')
                        <form action="{{urlWithPrefix('/private-message/store')}}" method="post">
                        @elseif($action == 'reply')  
                        <form action="{{urlWithPrefix('/private-message/reply', $message->id)}}" method="post">
                        @endif
                        @csrf
                            <div class="row">
                                <div class="col-md-3">To</div>
                                <div class="col-md-9">
                                    @if($action == 'sent')
                                    <select name="to" id="" class="form-control select2 {{$errors->has('to') ? 'is-invalid' : ''}}">
                                        @if(!empty($user))
                                            @foreach($user as $row)
                                                <option value="{{$row->id}}">{{$row->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if ($errors->has('to'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('to') }}</strong>
                                        </span>
                                    @endif
                                    @elseif($action == 'reply')
                                    <input type="text" name="to" id="" class="form-control" value="{{$user->name}}" disabled>
                                    <input type="hidden" name="to" value="{{$user->id}}">
                                    @endif
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-3">Message</div>
                                <div class="col-md-9">
                                    <textarea name="text" id="" cols="30" rows="10" class="form-control {{$errors->has('text') ? 'is-invalid' : ''}}"></textarea>
                                    @if ($errors->has('text'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('text') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <br>
                            <div align="right">
                                <button type="submit" class="btn btn-primary">Submit</a>
                                <button type="reset" class="btn btn-danger  ml-5">Reset</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
