@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">Private Message</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Private Message</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="justify-content-between">
                            <h4 class="text-white card-title">Private Message</h4>
                            <a class="pull-right btn btn-primary" href="{{urlWithPrefix('/private-message/create')}}">Add Message</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Inbox</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Outbox</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                <div class="table-responsive">
                                    <table id="example" class="table table-striped table-responsive-lg" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>From</th>
                                                <th>Message</th>
                                                <th>Sent At</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(!empty($inbox))
                                            @foreach($inbox as $row)
                                            <tr>
                                                <td>{{$row->users}}</td>
                                                <td>{{$row->text}}</td>
                                                <td>{{$row->updated_at}}</td>
                                                <td>
                                                    <a href="#ModalDetailMessage" data-id="{{$row->id}}" class="btn btn-light btn-sm" type="button" data-toggle="modal">Detail</a>
                                                    <a href="{{urlWithPrefix('/private-message/reply', $row->id)}}" class="btn btn-info btn-sm">Balas</a>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                <div class="table-responsive">
                                    <table id="example2" class="table table-striped" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>To</th>
                                                <th>Message</th>
                                                <th>Sent At</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(!empty($outbox))
                                            @foreach($outbox as $row)
                                            <tr>
                                                <td>{{$row->users}}</td>
                                                <td>{{$row->text}}</td>
                                                <td>{{$row->updated_at}}</td>
                                                 <td>
                                                    <a href="#ModalDetailMessage" data-id="{{$row->id}}" class="btn btn-light btn-sm" type="button" data-toggle="modal">Detail</a>
                                                    <a href="{{urlWithPrefix('/private-message/reply', $row->id)}}" class="btn btn-info btn-sm">Balas</a>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<div class="modal fade" id="ModalDetailMessage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Message</h4>
            </div>
            <div class="modal-body" id="form_var_message">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@section('scripts')
<script>
    $('#example').DataTable({
        "bLengthChange": false,
        "bFilter": false,
    })
</script>
@endsection