@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">Broadcast Message</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('/broadcast-message') }}">Broadcast Message</a></li>
                    <li class="breadcrumb-item active">Create</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="d-flex justify-content-between">
                            <h4 class="text-white card-title">Broadcast Message - Create</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        @if($action == 'sent')
                        <form action="{{urlWithPrefix('/broadcast-message/store')}}" method="post">
                        @elseif($action == 'reply')  
                        <form action="{{urlWithPrefix('/broadcast-message/reply', $message->id)}}" method="post">
                        @endif
                        @csrf
                            <div class="row">
                                <div class="col-md-3">To</div>
                                <div class="col-md-9">
                                    @if($action == 'sent')
                                    <select name="type" id="type" class="form-control {{$errors->has('type') ? 'is-invalid' : ''}}">
                                        <option value="">~~ Select One ~~</option>
                                        <option value="user_admin">ALL CLIENT - User & Admin</option>
                                        <option value="admin">ALL CLIENT - Admin Only</option>
                                        <option value="selected_admin">Selected Admin</option>
                                    </select>
                                    @if ($errors->has('to'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('to') }}</strong>
                                        </span>
                                    @endif
                                    @elseif($action == 'reply')
                                    <input type="text" name="to" id="" class="form-control" value="{{$user->name}}" disabled>
                                    <input type="hidden" name="to" value="{{$user->id}}">
                                    @endif
                                </div>
                            </div>
                            <br>
                            <div class="row" id="row_to"  style="display:none;">
                                <div class="col-md-3"></div>
                                <div class="col-md-9">
                                    <select name="to[]" id="" class="form-control select2 {{$errors->has('to') ? 'is-invalid' : ''}}" multiple="multiple" style="width: 100%;">
                                        @if(!empty($user))
                                            @foreach($user as $row)
                                                <option value="{{$row->id}}">{{$row->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-3">Message</div>
                                <div class="col-md-9">
                                    <textarea name="text" id="editor" cols="30" rows="10" class="form-control {{$errors->has('text') ? 'is-invalid' : ''}}"></textarea>
                                    @if ($errors->has('text'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('text') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <br><br><hr>
                            <div align="right">
                                <button type="submit" class="btn btn-primary">Submit</a>
                                <button type="reset" class="btn btn-danger  ml-5">Reset</a>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Anda yakin akan menambahkan data?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<link rel="stylesheet" href="{{ url('plugins/quill/quill.snow.css') }}">
<script src="{{ url('plugins/quill/quill.js') }}"></script>
<script>
var toolbarOptions = [
  ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
  ['blockquote', 'code-block'],

  [{ 'header': 1 }, { 'header': 2 }],               // custom button values
  [{ 'list': 'ordered'}, { 'list': 'bullet' }],
  [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
  [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
  [{ 'direction': 'rtl' }],                         // text direction

  [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
  [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

  [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
  [{ 'font': [] }],
  [{ 'align': [] }],

  ['clean']                                         // remove formatting button
];

var quill = new Quill('#editor', {
    modules: {
        toolbar: toolbarOptions
    },
    theme: 'snow'
});

$('#type').change(function(){
    let value = $(this).val();

    if(value === "selected_admin"){
        $('#row_to').show();
    }else{
        $('#row_to').hide();
    }
});
</script>
@endsection