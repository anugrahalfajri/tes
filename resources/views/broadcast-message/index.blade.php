@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">Broadcast Message</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                    <li class="breadcrumb-item active">Broadcast Message</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="d-flex justify-content-between">
                            <h4 class="text-white card-title">Broadcast Message</h4>
                            <a align="right" class="btn btn-primary" href="{{urlWithPrefix('/broadcast-message/create')}}">Add Message</a>
                        </div>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            
                            <table id="example" class="table table-striped" style="width:100%">
                                <thead>
                                <tr>
                                    <th>To</th>
                                    <th>Message</th>
                                    <th>Sent At</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($message))
                                @foreach($message as $row)
                                <tr>
                                    <td>{{$row->name}}</td>
                                    <td>{{$row->text}}</td>
                                    <td>{{$row->updated_at}}</td>
                                        <td>
                                        <a href="#ModalDetailMessageBroadcast" data-id="{{$row->id}}" class="btn btn-light btn-sm" type="button" data-toggle="modal">Detail</a>
                                        {{-- <a href="{{urlWithPrefix('/broadcast-message/reply', $row->id)}}" class="btn btn-info btn-sm">Balas</a> --}}
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<div class="modal fade" id="ModalDetailMessageBroadcast" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Message</h4>
            </div>
            <div class="modal-body" id="form_var_message" style="height: 70vh;overflow-y: auto;">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>