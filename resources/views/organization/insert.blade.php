@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">Organization</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{urlWithPrefix('/')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{urlWithPrefix('/organization')}}">Organization</a></li>
                    <li class="breadcrumb-item active">{{ $action == "store" ? "Create" : "Edit" }}</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="d-flex justify-content-between">
                            <h4 class="text-white card-title">Organization - {{($action == 'store') ? 'Create' : 'Edit'}}</h4>          
                        </div>
                    </div>
                    <div class="card-body">
                        @if($action == 'store')
                        <form action="{{urlWithPrefix('/organization/store')}}" method="post">
                        @elseif($action == 'update')
                        <form action="{{urlWithPrefix('/organization/update', $organization->id)}}" method="post">
                        @method('PUT')
                        @endif
                        @csrf
                            <div class="row">
                                <div class="col-md-3 mandatory">Name</div>
                                <div class="col-md-9">
                                  <input type="text" name="name" id="name" value="{{@$organization->name}}" class="form-control" required>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-3">Description</div>
                                <div class="col-md-9">
                                    <textarea name="description" id="description" cols="30" rows="5" class="form-control">{{@$organization->description}}</textarea>
                                </div>
                            </div>
                            <br>
                            @if ($action == 'store')
                            <div class="row">
                                <div class="col-md-3 mandatory">Status</div>
                                <div class="col-md-9">
                                    <select name="status" id="status" class="form-control" required>
                                        <option value="1" {{@$organization->status == '1' ? 'selected' : ''}}>Active</option>
                                        <option value="0" {{@$organization->status == '0' ? 'selected' : ''}}>Inactive</option>
                                    </select>
                                </div>
                            </div>
                            @endif
                            <br>
                            <div class="row">
                                <div class="col-md-3 mandatory">Admin</div>
                                <div class="col-md-9">
                                    <select name="admin" id="admin" class="form-control select2" style="width: 100%;" required>
                                            @if(!empty($users))
                                                @foreach($users as $row)
                                                    <option value="{{$row->id}}" {{($row->id == @$organization->admin)?'selected':''}}>{{$row->name}}</option>
                                                @endforeach
                                            @endif
                                    </select>
                                </div>
                            </div>
                            <br><br>


                            @if ($action !== 'store')
                            <br>
                            <h5>Detail History Status</h5>
                            <hr>
                            <div class="message-box">
                                <div class="message-widget">
                                    @forelse ($logs ?? [] as $l)
                                        <a href="#">
                                            <div class="user-img">
                                                <i class="fa fa-bell-o text-white"></i>
                                            </div>
                                            <div class="mail-contnet">
                                                <h5>{{$l->user->name ?? 'Unknown'}}</h5> <span class="mail-desc">{{$l->description}}.</span> <span class="time">{{\Carbon\Carbon::parse($l->created_at)->format('D, d M Y H:i:s')}}</span> </div>
                                        </a>
                                    @empty
                                        <a href="#">
                                            <div class="user-img">
                                                <i class="fa fa-bell-o text-white"></i>
                                            </div>
                                            <div class="mail-contnet">
                                                <h5>Belum ada history</h5></div>
                                        </a>
                                    @endforelse
                                </div>
                            </div>
                            <br>
                            @endif

                            @if ($action !== 'store')

                            <br>
                            <h5>Detail History Modification</h5>
                            <hr>
                            <div class="message-box">
                                <div class="message-widget">
                                    @forelse ($modif ?? [] as $m)
                                        <a href="#">
                                            <div class="user-img">
                                                <i class="fa fa-bell-o text-white"></i>
                                            </div>
                                            <div class="mail-contnet">
                                                <h5>{{$m->user->name ?? 'Unknown'}}</h5> <span class="mail-desc">{!! $m->description !!}.</span> <span class="time">{{\Carbon\Carbon::parse($m->created_at)->format('D, d M Y H:i:s')}}</span> </div>
                                        </a>
                                    @empty
                                        <a href="#">
                                            <div class="user-img">
                                                <i class="fa fa-bell-o text-white"></i>
                                            </div>
                                            <div class="mail-contnet">
                                                <h5>Belum ada Modifikasi</h5></div>
                                        </a>
                                    @endforelse
                                </div>
                            </div>
                            <br>
                            @endif

                            <div align="right">
                                <button type="submit" class="btn btn-primary">Submit</a>
                                <button type="reset" class="btn btn-danger  ml-5">Reset</a>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Anda yakin akan {{$action == 'store' ? 'menambahkan' : 'mengedit'}} data?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
