@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">Konfigurasi</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ urlWithPrefix('/') }}">Home</a></li>
                    <li class="breadcrumb-item active">Konfigurasi</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="d-flex justify-content-between">
                            <h4 class="text-white card-title">Konfigurasi</h4>
                            <a class="pull-right btn btn-primary" href="{{urlWithPrefix('/config/create')}}">Add New</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Modul</th>
                                        <th>Allowed type</th>
                                        <th>File size</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($configs))
                                    @foreach($configs as $config)
                                    <tr>
                                        <td>{{ $moduls[$config->modul_name] ?? '-' }}</td>
                                        <td>{{str_replace(";", ", ", $config->allowed_mime_types)}}</td>
                                        <td>{{($config->max_file_size) }}</td>
                                        <td>
                                            <a href="{{urlWithPrefix('/config/edit', $config->id)}}" class="btn btn-warning btn-sm">Edit</a>&nbsp;
                                            <a href="{{urlWithPrefix('/config/delete', $config->id)}}" class="btn btn-danger btn-sm">Delete</a>&nbsp;
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
