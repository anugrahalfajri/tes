@extends('layouts.app')

@section('content')
<style>
    [type="checkbox"]:not(:checked):disabled + label:before {
        border: 1px solid rgba(0, 0, 0, 0.26);
        background-color: white;
    }
</style>
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">Konfigurasi</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ urlWithPrefix('/') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ urlWithPrefix('/config') }}">Konfigurasi</a></li>
                    <li class="breadcrumb-item active">{{ $label }}</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="d-flex justify-content-between">
                            <h4 class="text-white card-title">Konfigurasi - {{ $label }}</h4>          
                        </div>
                    </div>
                    <div class="card-body">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li><span class="text-danger">{{ $error }}</span></li>
                            @endforeach
                        </ul>

                        @php
                        $file_type = [
                            'pdf'   => 'Pdf File',
                            'docx'  => 'Docx',
                            'csv'   => 'Csv',
                            'ppt'   => 'PPT',
                            'pptx'  => 'PPTX',
                            'ods'   => 'Ods Document Spreadsheet',
                            'odt'   => 'Odt Text Document',
                            'txt'   => 'Txt File',
                            'xlsx'  => 'Xlsx Spreadsheet',
                            'zip'   => 'Zip Archive',
                            'rar'   => 'Rar Archive',
                            'gz'    => 'Gz Archive',
                            'tar'   => 'Tar Archive',
                            'svg'   => 'Svg Image',
                            'gif'   => 'Gif Image',
                            'jpg'   => 'Jpg Image',
                            'jpeg'  => 'Jpeg Image',
                            'png'   => 'Png Image',
                            'mp3'   => 'Mp3 File',
                            'mp4'   => 'Mp4 File',
                            '3gp'   => '3gp File',
                            'mpeg'  => 'Mpeg File',
                            'wav'   => 'wav Audio',
                        ];
                        $config_type = isset($config) ? explode(";", $config->allowed_mime_types) : []; 
                    @endphp

                        <form id="blbl" action="{{urlWithPrefix('/config/store')}}" method="post">
                            @csrf

                            <input type="hidden" name="config_id" value="{{ $config->id ?? '' }}">
                            <div class="row mb-2">
                                <div class="col-md-3">Modul</div>
                                <div class="col-md-9">
                                    <select name="tipe_modul" class="select2 form-control" style="width: 100%;" required>
                                        <option value="">Pilih modul</option>
                                        @foreach ($moduls as $value => $mod)
                                            <option value="{{ $value }}" {{ isset($config) ? $config->modul_name == $value ? 'selected' : '' : '' }}>{{ $mod }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-3">File Size (kb)</div>
                                <div class="col-md-9">
                                    <input type="number" name="file_size" id="file_size" class="form-control" placeholder="File Size dalam Kb, eg: 1024" value="{{ $config->max_file_size ?? old('file_size') }}" required>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-3">File Type</div>
                                <div class="col-md-9">
                                    <select name="tipe_file[]" id="tipe_file" class="select2 form-control" style="width: 100%;" multiple required>
                                        @foreach ($file_type as $type => $name)
                                            <option value="{{ $type }}" {{ isset($config) ? in_array($type, $config_type) ? 'selected' : '' : '' }}>{{ $name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <br>
                            <div align="right">
                                <button type="submit" class="btn btn-primary">Submit</a>
                                <button type="reset" class="btn btn-danger  ml-5">Reset</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script>
</script>
@endsection