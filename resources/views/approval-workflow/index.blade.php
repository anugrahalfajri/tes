@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">Approval Document</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Approval Document</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="d-flex justify-content-between">
                            <h4 class="text-white card-title">Approval Document</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{route(routePrefix().'approval_workflow.show')}}">
                            <div class="form-group row">
                                <label for="status" class="col-sm-2 col-form-label">Filter</label>
                                <div class="col-sm-4">
                                    <select name="status" id="status" class="form-control">
                                        <option value="" selected disabled>Select Status</option>
                                        <option value="0" {{ isset($status) ? ($status == '0') ? 'selected' : '' : '' }}>Pending</option>
                                        <option value="1" {{ isset($status) ? ($status == '1') ? 'selected' : '' : '' }}>Approve</option>
                                        <option value="2" {{ isset($status) ? ($status == '2') ? 'selected' : '' : '' }}>Reject</option>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <button class="btn btn-success" id="filter">Filter</button>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table id="example" class="table table-striped" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Created At</th>
                                        <th>Created By</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($workflow_detail))
                                        @foreach($workflow_detail as $row)
                                        <tr>
                                            <td>{{$row->title}}</td>
                                            <td>{{$row->created_at}}</td>
                                            <td>{{$row->created_by}}</td>
                                            <td>{{$row->status_name}}</td>
                                            <td>
                                                {{-- <button type="button" class="btn btn-warning btn-action ml-2" data-id="{{$row->id_workflow_detail_approval}}">Action</a> --}}
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                          Action
                                                        </button>
                                                        <div class="dropdown-menu">
                                                          <a class="dropdown-item btn-action"  data-id="{{$row->id_workflow_detail_approval}}" href="javascript:;">Approval</a>
                                                          <a class="dropdown-item" href="javascript:;" onclick="signApproval({{$row->id_workflow_detail_approval}})">Signature</a>
                                                        </div>
                                                      </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalAction" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Approval Document</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row mt-2">
                    <div class="col-md-2"><b>Title</b></div>
                    <div class="col-md-10"><span id="title"></span></div>
                </div>
                <div class="row" style="margin-top:10px">
                    <div class="col-md-2"><b>Directory</b></div>
                    <div class="col-md-10"><span id="directory"></span></div>
                </div>
                <div class="row" style="margin-top:10px">
                    <div class="col-md-2"><b>Description</b></div>
                    <div class="col-md-10"><span id="description"></span></div>
                </div>
                <div class="row" style="margin-top:10px">
                    <div class="col-md-2"><b>Created At</b></div>
                    <div class="col-md-10"><span id="created_at"></span></div>
                </div>
                <div class="row" style="margin-top:10px">
                    <div class="col-md-2"><b>Created By</b></div>
                    <div class="col-md-10"><span id="created_by"></span></div>
                </div>
                <div class="row" style="margin-top:10px">
                    <div class="col-md-2"><b>Due Date</b></div>
                    <div class="col-md-10"><span id="due_date"></span></div>
                </div>
                <div class="row" style="margin-top:10px">
                    <div class="col-md-2"><b>Priority</b></div>
                    <div class="col-md-10"><span id="priority"></span></div>
                </div>
                <div class="row" style="margin-top:10px">
                    <div class="col-md-2"><b>Approval</b></div>
                    <div class="col-md-10" id="user_approval"></div>
                </div>
                <div class="row" style="margin-top:10px">
                    <div class="col-md-2"><b>Document</b></div>
                    <div class="col-md-10"><span id="pdf-link"></span></div>
                </div>
                <div class="row row-approve" style="margin-top:10px">
                    <div class="col-md-4">
                        <button type="button" class="btn btn-success btn-block signature ml-2" data-file="">Signature</a>
                    </div>
                    <div class="col-md-4">
                        <button type="submit" class="btn btn-primary btn-block update-status ml-2" id="approve" data-status="1" data-id="">Approve</a>
                    </div>
                    <div class="col-md-4">
                        <button type="submit" class="btn btn-danger btn-block update-status ml-2" id="reject" data-status="2" data-id="">Reject</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="signModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Sign Document</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="signBody">
                <div id="pspdfkit" style="height: 100vh"></div>
                <input type="hidden" id="signed_file" value="">

                <div class="row row-approve" style="margin-top:10px">
                    <div class="col-md-4">
                        <button type="button" id="signatureSave" class="btn btn-success btn-block" data-file="">Save File</a>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{url('plugins/pspdfkit/pspdfkit.js')}}"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@sweetalert2/theme-borderless/borderless.css">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>

    <script>
        var modalAction = $('#modalAction');
        var user_approval = $("#user_approval");
        $('.btn-action').on('click', function () {
            $.ajax({
                url: "{{route(routePrefix().'approval_workflow.getWorkflowApprovalDetail')}}",
                method: "POST",
                data: {
                    id_workflow_detail_approval : $(this).attr('data-id')
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    console.log(data.data.status)
                    if (data.data.status !== '0') {
                        $('.row-approve').attr('hidden', true)
                        $('.row-pin').attr('hidden', true)
                    }
                    modalAction.modal('show');
                    modalAction.find('#title').text(data.data.title);
                    modalAction.find('#directory').text(data.data.folder);
                    modalAction.find('#description').text(data.data.description);
                    modalAction.find('#created_at').text(new Date(data.data.created_at));
                    modalAction.find('#created_by').text(data.data.created_by);
                    modalAction.find('#due_date').text(new Date(data.data.due_date));
                    modalAction.find('#priority').text(data.data.priority);
                    PDFObject.embed(baseurl + '/documents/' + data.data.nama_file, "#pdf-link", {height: "30rem"});
                    modalAction.find('#approve').attr('data-id', data.data.id);
                    modalAction.find('#reject').attr('data-id', data.data.id);
                    $.each(data.data.workflow_detail_approval, function (i, v) {
                        // console.log(v.member)
                        user_approval.append("- "+v.level+" ("+textStatus(v.status)+")<br>")
                    })
                    modalAction.find('.signature').attr('data-file', 'documents/' + data.data.nama_file);
                }
            })
        })

        $(document).on('click', '.update-status', function () {
            var status = $(this).attr('data-status');
            var id_workflow_detail = $(this).attr('data-id');

            $('#modalAction').modal('hide');
            var formData = new FormData();

            Swal.fire({
            title: 'Masukkan PIN',
            input: 'password',
            inputAttributes: {
                autocapitalize: 'off',
                autocorrect: 'off',
                autocomplete: 'off',
            },
            inputPlaceholder: 'PIN',
            showCancelButton: true,
            confirmButtonText: 'Continue',
            showLoaderOnConfirm: true,
            allowOutsideClick: false,
            preConfirm: (login) => {
                formData.append('pin', login);

                return fetch(`{{ route("approval_workflow.getPinUser") }}`, {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    'method': 'POST',
                    'body': formData
                })
                .then(response => {
                    if (!response.ok) {
                        throw new Error(response.statusText)
                    }
                    return response.json()
                })
                .catch(error => {
                    Swal.showValidationMessage(
                        `Request failed: ${error}`
                    )
                })
            },
            }).then((result) => {
                if (result.isConfirmed) {
                    approveOrReject(status, id_workflow_detail);
                }else{
                    $('#modalAction').modal('show');
                }
            })
        });


        $('.signature').on('click', function () {
            $.ajax({
                url: "{{ route(routePrefix().'signature') }}",
                method: "POST",
                data: {
                    file : $(this).attr('data-file')
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    console.log(data.status)
                    if (data.status == 200) {
                        modalAction.modal('hide');
                        Swal.fire('Success!', data.message, 'success').then((result) => {
                            location.reload();
                        });
                    } else {
                        modalAction.modal('hide');
                        Swal.fire('Error!', data.message, 'error').then((result) => {
                            location.reload();
                        });
                    }
                }
            })
        })

        function approveOrReject(status, id_workflow_detail){

            $.ajax({
                url: "{{route(routePrefix().'approval_workflow.approval')}}",
                method: "POST",
                data: {
                    status: status,
                    id_workflow_detail: id_workflow_detail
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    $('.message').html(data.data.message);
                    $('.upload-success').show();
                    location.reload();
                }
            });
        }
        
        modalAction.on('hidden.bs.modal', function () {
            $('.row-approve').removeAttr('hidden');
            $('.row-pin').removeAttr('hidden');
            user_approval.empty();
        })

        function textStatus(status) {
            if (status == '1') {
                return 'Approve';
            } else if (status == '2') {
                return 'Reject';
            } else {
                return 'Pending';
            }
        }

        function signApproval(id){
            $.ajax({
                url: "{{route(routePrefix().'approval_workflow.getWorkflowApprovalDetail')}}",
                method: "POST",
                data: {
                    id_workflow_detail_approval : id
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    $('#signModal').modal('show');
                    $('#pspdfkit').show();
                    $('#signed_file').val(data.data.nama_file);

                    PSPDFKit.load({
                        container: "#pspdfkit",
                        document: baseurl + '/documents/' + data.data.nama_file
                    })
                    .then(function(instance) {
                        $('#signatureSave').click(function(){

                        instance.exportPDF().then(function (buffer) {
                            const oldFileName = $('#signed_file').val();
                            const blob = new Blob([buffer], { type: 'application/pdf' });
                            console.log(blob);

                            const formData = new FormData();
                            formData.append("file", blob);
                            formData.append("oldFile", oldFileName);

                            $.ajax({
                                type: "POST",
                                url: "{{ route(routePrefix().'approval_workflow.signature.save') }}",
                                async: false,
                                cache: false,
                                contentType: false,
                                enctype: 'multipart/form-data',
                                processData: false,
                                data: formData,
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                success: function (response) {
                                    // let data = JSON.parse(response);
                                    // alert(data.message);
                                    // location.reload();
                                    console.log(oldFileName)
                                },                
                                error: function(data) {
                                    alert("Oops, something went wrong!");
                                }
                            });

                        });
                    });
                        // $('#signatureSave').click(function(){
                        //     // async function getExport() {
                        //     //     return await instance.exportPDF();
                        //     // }
                        //     const aRb = async() => {
                        //         const arrayBuffer = await instance.exportPDF();
                        //         console.log(arrayBuffer);
                        //     }
                            
                        //     // let arrayBuffer = await instance.exportPDF();
                        //     const oldFileName = $('#signed_file').val();
                        //     const blob = new Blob([aRb()], { type: 'application/pdf' });

                        //     const objectUrl = window.URL.createObjectURL(blob);
                        //     aRb();
                        //     const formData = new FormData();
                        //     formData.append("file", blob);
                        //     formData.append("oldFile", oldFileName);

                        //     $.ajax({
                        //         type: "POST",
                        //         url: "{{ route(routePrefix().'approval_workflow.signature.save') }}",
                        //         async: false,
                        //         cache: false,
                        //         contentType: false,
                        //         enctype: 'multipart/form-data',
                        //         processData: false,
                        //         data: formData,
                        //         headers: {
                        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        //         },
                        //         success: function (response) {
                        //             // let data = JSON.parse(response);
                        //             // alert(data.message);
                        //             // location.reload();
                        //             console.log(oldFileName)
                        //         },                
                        //         error: function(data) {
                        //             alert("Oops, something went wrong!");
                        //         }
                        //     });
                        // });
                    })
                    .catch(function(error) {
                        
                    });
                },
                error: function(data) {
                    $('#signModal').modal('show');
                    $('#pspdfkit').hide();
                    $('#signBody').html('<center>Oop, Something went wrong!</center>');
                }
            });
        }

        $('#example').DataTable({
            "bLengthChange": false,
            "bFilter": false,
        })
    </script>
@endsection