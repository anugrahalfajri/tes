@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">Organization</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Organization</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="justify-content-between">
                            <h4 class="text-white card-title">Organization</h4>
                            @if ($role->name == 'admin')
                                <a class="pull-right btn btn-primary" href="{{urlWithPrefix('/organization/create')}}">Add Organization</a>
                            @endif
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Status</th>
                                        <th>Admin</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($organizations))
                                    @foreach($organizations as $row)
                                    <tr>
                                        <td>{{$row->name}}</td>
                                        <td>{{$row->description}}</td>
                                        <td>{{($row->status == '1') ? 'Active' : 'Inactive'}}</td>
                                        <td>{{$row->user_name}}</td>
                                        <td>
                                            <a href="{{urlWithPrefix('/add-on-setting/set-organization-access', $row->id)}}" class="btn btn-warning btn-sm">Access</a>&nbsp;
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection