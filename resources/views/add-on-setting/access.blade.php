@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">Organization</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Organization</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="d-flex justify-content-between">
                            <h4 class="text-white card-title">Organization Access</h4>
                            
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{urlWithPrefix('/add-on-setting/store-organization-access')}}" method="post">
                        @csrf
                            <input type="hidden" name="organization_id" value="{{$organization->id}}">
                            <div class="row mb-5" style="margin-top:10px">
                                <div class="col-md-2">Name</div>
                                <div class="col-md-10"><input type="text" id="" value="{{$organization->name}}" class="form-control" disabled></div>
                            </div>
                            <div align="right">
                                <input type="checkbox" name="" id="check_all">
                                <label class="form-check-label" for="check_all">
                                    Check All
                                </label>
                            </div>
                          <table class="table table-striped" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Menu Name</th>
                                        <th>View</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($permission))
                                    @foreach($permission as $key => $row)
                                    @php
                                        $checked = '';
                                    @endphp
                                    @if(!empty($organization_access))
                                    @foreach ($organization_access as $item)
                                        @if ($item->id_permission == $row->id)
                                            @php
                                                $checked = 'checked';
                                            @endphp
                                        @endif
                                    @endforeach
                                    @endif
                                    <tr>
                                        <td>{{$row->menu_name}}</td>
                                        <td>
                                                <input class="form-check-input permis" type="checkbox" name="permissions[]" value="{{ $row->id }}" id="flexRadioDefault{{ $key . $key+1 }}" {{$checked}}>
                                                <label class="form-check-label" for="flexRadioDefault{{ $key . $key+1 }}">
                                                    
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <br>
                            <div align="right">
                                <button data-toggle="modal" data-target="#exampleModal" type="button" class="btn btn-primary">Submit</a>
                                <button type="reset" class="btn btn-danger  ml-5">Reset</a>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Anda yakin akan mengedit data?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
    $("#check_all").click(function() {
        $('.permis').not(this).prop('checked', this.checked);
    });
    </script>
@endsection
