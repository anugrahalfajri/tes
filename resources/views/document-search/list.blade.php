@extends('layouts.app')

@section('content')
    <style>
        @media (max-width: 575.98px) {
            #tombol-reset-search {
                margin-left: 5px;
            }
        }

        @media (min-width: 576px) and (max-width: 969px) {
            #tombol-reset-search {
                margin-left: 0px;
                margin-top: 8px;
            }
        }


        @media (min-width: 970px) {
            #tombol-reset-search {
                margin-left: 5px;
            }
        }

    </style>
    <div class="page-wrapper" style="max-width: none;">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 col-8 align-self-center">
                    <h3 class="text-themecolor">Document Search</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Document Search</li>
                    </ol>
                </div>
            </div>
            @include('include/material/alert')
            @if ($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fa fa-ban"></i> incomplete submission</h5>
                    <ul>
                        <li><span class="text-danger">{{ $errors->first() }}</span></li>
                    </ul>
                </div>
            @endif
            <div class="alert alert-success alert-dismissible upload-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5>Success</h5>
                <p class="message"></p>
            </div>
            <div class="row">
                <div class="col-lg-8 col-xlg-8 col-md-8 col-lg-12 card-dynamic">
                    <div class="card">
                        <div class="card-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link  @if(!isset($tab_detail))

                                            active


@endif" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
                                       aria-selected="true">Pencarian Cepat</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " id="detail-tab" data-toggle="tab" href="#detail" role="tab"
                                       aria-controls="detail" aria-selected="false">Pencarian Detail</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content mt-3">
                                <div class="tab-pane active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <form action="{{ route(routePrefix().'document-search.store') }}" method="post">
                                        @csrf
                                        <div class="row mb-4">
                                            <div class="col-sm-3">
                                                <input type="text" value="{{@$nama_file}}" name="nama_file"
                                                       class="form-control" placeholder="Masukan kata pencarian">
                                            </div>
                                            <div class="col-sm-3">
                                                <button type="submit" class="btn btn-primary">CARI</button>
                                                <a href="{{ route(routePrefix().'document-search') }}"
                                                   class="btn btn-primary">Reset</a>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="card">
                                        <div class="card-body bg-megna">
                                            <h4 class="text-white card-title">Pencarian</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table id="docs" class="table table-striped" style="width:100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Nama Dokumen</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if(!empty($file_upload)){
                                                    foreach($file_upload as $row){
                                                    $path = explode('/', $row->path_file, 3)[2];
                                                    if(file_exists(public_path() . '/' . $path . $row->nama_file)){ ?>
                                                    <tr>
                                                        <td>{{$row->nama_file}}<br>
                                                            <button onclick="detail('{{$row->id}}')"
                                                                    class="btn btn-xs btn-info">Info
                                                            </button>&nbsp;
                                                            <button onclick="preview('{{$row->id}}')"
                                                                    class="btn btn-xs btn-secondary preview">Preview
                                                            </button>&nbsp;
                                                            <button onclick="openaction('{{$row->id}}')"
                                                                    class="btn btn-xs btn-success">Action
                                                            </button>&nbsp;
                                                        </td>
                                                    </tr>
                                                    <?php  }
                                                    }
                                                    } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="detail" role="tabpanel" aria-labelledby="detail-tab">
                                    <form action="{{ route(routePrefix().'document-search.search_detail') }}"
                                          method="post">
                                        @method('PUT')
                                        @csrf

                                        <div class="form-group row">
                                            <div class="col-sm-6 col-md-6 col-lg-4">
                                                <label for="">Opsi</label>
                                                <select name="opsi" id="" class="form-control select2 select-opsi"
                                                        style="width:100%;">
                                                    <option value="properties.name">Name</option>
                                                    <option value="properties.title">Title</option>
                                                    <option value="extented_properties.no_document">No Document</option>
                                                    <option value="id_directory">Directory</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-4 opsi">
                                                <div class="opsi-textbox">
                                                    <label id="label"></label>
                                                    <input type="text" name="value" class="form-control">
                                                </div>
                                                <div class="directory-select">
                                                    <label for="">Direktori</label>
                                                    <select name="id_directory" id="" class="form-control select2"
                                                            style="width:100%;">
                                                        @if(!empty($directory))
                                                            @foreach($directory as $row)
                                                                <option value="{{$row->id}}" <?php echo ($row->id == @$id_directory) ? 'selected' : ''; ?>>{{$row->nama_folder}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-md-4 col-lg-4">
                                                <label id="label" style="color: rgba(0, 0, 0, 0);">Action</label>
                                                <div class="row" style="white-space: nowrap; padding-left: 15px">
                                                    <button type="submit" class="btn btn-primary">Search Document
                                                    </button>
                                                    <a href="{{ route(routePrefix().'document-search') }}"
                                                       class="btn btn-primary" id="tombol-reset-search">Reset</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="card">
                                        <div class="card-body bg-megna">
                                            <h4 class="text-white card-title">Pencarian</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table id="example2" class="table table-striped" style="width:100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Nama Dokumen</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if(!empty($file_upload)){
                                                    foreach($file_upload as $row){
                                                    $path = explode('/', $row->path_file, 3)[2];
                                                    if(file_exists(public_path() . '/' . $path . $row->nama_file)){ ?>
                                                    <tr>
                                                        <td>{{$row->nama_file}}<br>
                                                            <button onclick="detail('{{$row->id}}')"
                                                                    class="btn btn-xs btn-info" data-id="$row->id">Info
                                                            </button>&nbsp;
                                                            <button onclick="preview('{{$row->id}}')"
                                                                    class="btn btn-xs btn-secondary preview">Preview
                                                            </button>&nbsp;
                                                            <button onclick="openaction('{{$row->id}}')"
                                                                    class="btn btn-xs btn-success">Action
                                                            </button>&nbsp;
                                                        </td>
                                                    </tr>
                                                    <?php  }
                                                    }
                                                    } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-xlg-3 col-md-4">
                    <div class="card detail">
                        <div class="card-body">
                            <h4 class="card-title">Detail - <span class="fill-doc-name"></span></h4>
                            <h6 class="card-subtitle">Use default tab with class</h6>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home1"
                                                        role="tab"><span class="hidden-sm-up"><i
                                                    class="ti-home"></i></span> <span
                                                class="hidden-xs-down">Properties</span></a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#profile"
                                                        role="tab"><span class="hidden-sm-up"><i
                                                    class="ti-user"></i></span> <span
                                                class="hidden-xs-down">Ext Prop</span></a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#signature" role="tab"><span
                                                class="hidden-sm-up"><i class="ti-user"></i></span> <span
                                                class="hidden-xs-down">Signature</span></a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#version-log"
                                                        role="tab"><span class="hidden-sm-up"><i
                                                    class="ti-email"></i></span> <span class="hidden-xs-down">Version Log</span></a>
                                </li>
                                <li class="nav-item"><a class="nav-link" id="note_list_tab" data-toggle="tab" href="#notelist"
                                                        role="tab"><span class="hidden-sm-up"><i
                                                    class="ti-email"></i></span> <span
                                                class="hidden-xs-down">Note List</span></a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#history"
                                                        role="tab"><span class="hidden-sm-up"><i
                                                    class="ti-email"></i></span> <span
                                                class="hidden-xs-down">History</span></a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#access"
                                                        role="tab"><span class="hidden-sm-up"><i
                                                    class="ti-email"></i></span> <span class="hidden-xs-down">Access Right</span></a>
                                </li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#workflow"
                                                        role="tab"><span class="hidden-sm-up"><i
                                                    class="ti-email"></i></span> <span
                                                class="hidden-xs-down">Workflow</span></a></li>

                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content tabcontent-border" style="overflow: auto;">
                                <div class="tab-pane active" id="home1" role="tabpanel">
                                    @if(!empty($properties))
                                        <table class="table table-bordered table-striped">
                                            <tbody>
                                            <tr>
                                                <td width="20%">Name</td>
                                                <td width="5%" align="center">:</td>
                                                <td width="75%"><span class="fill-doc-name"></span></td>
                                            </tr>
                                            <tr>
                                                <td width="20%">Title</td>
                                                <td width="5%" align="center">:</td>
                                                <td width="75%"><span class="fill-doc-title"></span></td>
                                            </tr>
                                            <tr>
                                                <td width="20%">Folder</td>
                                                <td width="5%" align="center">:</td>
                                                <td width="75%"><span class="fill-doc-folder"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Size</td>
                                                <td align="center">:</td>
                                                <td><span class="fill-doc-size"></span></td>
                                            </tr>
                                            <tr>
                                                <td>File Version</td>
                                                <td align="center">:</td>
                                                <td><span class="fill-doc-version"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Workflow Status</td>
                                                <td align="center">:</td>
                                                <td>{{$properties->status}}</td>
                                            </tr>
                                            <tr>
                                                <td>Created on</td>
                                                <td align="center">:</td>
                                                <td><span class="fill-doc-createdtime"></span> by <span
                                                            class="fill-doc-createdby"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Publish on</td>
                                                <td align="center">:</td>
                                                <td><span class="fill-doc-updatedtime"></span> by <span
                                                            class="fill-doc-updatedby"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Permalink</td>
                                                <td align="center">:</td>
                                                <td><a href="" class="fill-fileurl">Download</a></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                                <div class="tab-pane  p-20" id="profile" role="tabpanel">
                                    <form action="{{route(routePrefix().'ext_properties.store')}}" method="post">
                                        @csrf
                                        <table class="table table-bordered table-striped">
                                            <input type="hidden" name="id_file" value="" class="fill-fileid">
                                            <input type="hidden" name="relasi_document" value="">
                                            <tbody>
                                            <tr>
                                                <td width="20%">No. Document</td>
                                                <td width="5%" align="center">:</td>
                                                <td width="75%">
                                                    <textarea name="no_document" id="no_document" class="form-control"
                                                              cols="30" rows="2"></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">Tgl. Document</td>
                                                <td width="5%" align="center">:</td>
                                                <td width="75%"><input type="text" class="form-control datepicker"
                                                                       name="tgl_document" id="tgl_document" value="">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">Relasi document</td>
                                                <td width="5%" align="center">:</td>
                                                <td width="75%"><input type="text" class="form-control"
                                                                       name="relasi_document" id="relasi_document"
                                                                       value=""></td>
                                            </tr>
                                            <tr>
                                                <td>Tag</td>
                                                <td align="center">:</td>
                                                <td>
                                                    <select class="form-control select2" name="tag[]"
                                                            multiple="multiple" data-placeholder="Select tag"
                                                            style="width: 100%;" id="tag_doc" required>
                                                        @if (!empty($tag))
                                                            @foreach ($tag as $row)
                                                                <option value="{{$row->id}}">
                                                                    {{$row->tag}}
                                                                </option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Document Expiration Date</td>
                                                <td align="center">:</td>
                                                <td><input type="text" class="form-control datepicker"
                                                           name="document_expiration_date" id="document_expiration_date"
                                                           value=""></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <button type="submit" class="btn btn-sm btn-primary">Save</button>
                                    </form>
                                </div>
                                <div class="tab-pane" id="signature" role="tabpanel">
                                    <table class="table table-bordered table-striped" width="100%">
                                        <tbody>
                                        <tr>
                                            <td width="20%">Signature Field</td>
                                            <td width="5%" align="center">:</td>
                                            <td width="75%">Enfocus Preflight</td>
                                        </tr>
                                        <tr>
                                            <td>Integrity</td>
                                            <td align="center">:</td>
                                            <td>Signature is valid</td>
                                        </tr>
                                        <tr>
                                            <td>Certificate</td>
                                            <td align="center">:</td>
                                            <td>Untrusted</td>
                                        </tr>
                                        <tr>
                                            <td>Name</td>
                                            <td align="center">:</td>
                                            <td>
                                                Preflight Ticket Signature
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Reason</td>
                                            <td align="center">:</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Date</td>
                                            <td align="center">:</td>
                                            <td>2021-04-28 13:27:00</td>
                                        </tr>
                                        <tr>
                                            <td>Validity</td>
                                            <td align="center">:</td>
                                            <td>2010-04-28 13:27:00 <br>- 2021-04-28 13:27:00</td>
                                        </tr>
                                        <tr>
                                            <td>Subject</td>
                                            <td align="center">:</td>
                                            <td>{"C":"BE", <br> "CN":"Preflight <br>Ticket Signature"}</td>
                                        </tr>
                                        <tr>
                                            <td>Issuer</td>
                                            <td align="center">:</td>
                                            <td>{"C":"BE",<br>"CN":"Preflight <br>Ticket Signature"}</td>
                                        </tr>
                                        <tr>
                                            <td>Public Key</td>
                                            <td align="center">:</td>
                                            <td>RSA-SHA1</td>
                                        </tr>
                                        <tr>
                                            <td>Algorithm</td>
                                            <td align="center">:</td>
                                            <td>sha1With<br>RSAEncryption</td>
                                        </tr>
                                        <tr>
                                            <td>SHA-1 Fingerprint</td>
                                            <td align="center">:</td>
                                            <td>5E:89:BA:CF:57:27:34:A1:<br>E8:A3:9A:9A:5B:F0:0E:BF:93<br>:C9:E5:30</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane p-20" id="version-log" role="tabpanel">
                                    <table class="table" width="90%">
                                        <tbody>
                                        <tr bgcolor="#f4f4f4">
                                            <td colspan="2">Last Version</td>
                                        </tr>
                                        <tr>
                                            <td width="10%">
                                                <label style="background-color:#f1f1f1;padding:5px;">
                                                    <div id="version"></div>
                                                </label>
                                            </td>
                                            <td width="80%">
                                                <div id="nama_file"></div>
                                                <div style="class:table-cell;float:left;margin-top:10px;">
                                                    <img src="https://melkhior.co/new/demo/dms/assets/images/1.jpg"
                                                         style="width:40px;height:40px">
                                                </div>
                                                <div style="class:table-cell;float:left;margin-left:10px;;margin-top:10px;">
                                                    <div id="created_by"></div>
                                                    -
                                                    <div id="created_at"></div>
                                                    (
                                                    <div id="keterangan"></div>
                                                    )
                                                </div>
                                            </td>
                                        </tr>
                                        <tr bgcolor="#f4f4f4">
                                            <td colspan="2">Older Version</td>
                                        </tr>
                                        @if(!empty($log_version))
                                            @foreach($log_version as $row)
                                                <tr>
                                                    <td width="10%">
                                                        <label style="background-color:#f1f1f1;padding:5px;">{{$row->version}}</label>
                                                    </td>
                                                    <td width="80%">{{$row->file}}<br>
                                                        <div style="class:table-cell;float:left;margin-top:10px;">
                                                            <img src="https://melkhior.co/new/demo/dms/assets/images/1.jpg"
                                                                 style="width:40px;height:40px">
                                                        </div>
                                                        <div style="class:table-cell;float:left;margin-left:10px;;margin-top:10px;">
                                                            {{$row->created_by}} - {{$row->created_at}}<br>
                                                            ({{$row->keterangan}})
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>

                                    <style>
                                        #history .dataTables_wrapper .dataTables_length {
                                            float: right;
                                        }

                                        #notelist .dataTables_wrapper .dataTables_length {
                                            float: right;
                                        }
                                    </style>
                                </div>
                                <div class="tab-pane p-20" id="notelist" role="tabpanel">
                                    <table id="table-note" class="table">
                                        <thead style="width: 100% !important">
                                        <tr>
                                            <th id="nomor_tabel">No</th>
                                            <th>Tgl</th>
                                            <th>Writer</th>
                                            <th>Notes</th>

                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                    <form style="margin-top: 8px;" action="{{ route(routePrefix().'note_list.store') }}"
                                          method="post">
                                        @csrf
                                        <input type="hidden" name="id_file" id="id_file_dd"
                                               value="{{$row->id_file ?? ''}}">

                                        <div class="form-group row">
                                            <label for="notes" class="col-3 control-label">Notes</label>
                                            <div class="col-9">
                                                <input type="text" class="form-control" name="notes" id="notes">
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-sm btn-primary float-right mb-1">Save
                                        </button>
                                    </form>
                                </div>
                                <div class="tab-pane p-20" id="history" role="tabpanel">
                                    <table id="example_history" class="table" width="100%">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tgl</th>
                                            <th>Writer</th>
                                            <th>Description</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($history))
                                            @php $no=1; @endphp
                                            @foreach($history as $row)
                                                <tr>
                                                    <td>{{$no}}</td>
                                                    <td>{{$row->created_at}}</td>
                                                    <td>{{$row->user}}</td>
                                                    <td>{{$row->description}}</td>
                                                </tr>
                                                @php $no++; @endphp
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane p-20" id="access" role="tabpanel">
                                    <table style="width:100%;">
                                        <thead>
                                        <tr>
                                            <th style="font-size:12px;">No</th>
                                            <th style="font-size:12px;" width="20%">User</th>
                                            <th style="font-size:12px;">Lihat</th>
                                            <th style="font-size:12px;">Tambah</th>
                                            <th style="font-size:12px;">Ubah</th>
                                            <th style="font-size:12px;">Hapus</th>
                                            <th style="font-size:12px;">Unduh</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td style="font-size:12px;">Admin</td>
                                            <td><i class="mdi mdi-check"></i></td>
                                            <td><i class="mdi mdi-check"></i></td>
                                            <td><i class="mdi mdi-check"></i></td>
                                            <td><i class="mdi mdi-check"></i></td>
                                            <td><i class="mdi mdi-check"></i></td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td style="font-size:12px;">Rangga</td>
                                            <td><i class="mdi mdi-check"></i></td>
                                            <td><i class="fa fa-ban"></i></td>
                                            <td><i class="fa fa-ban"></i></td>
                                            <td><i class="fa fa-ban"></i></td>
                                            <td><i class="mdi mdi-check"></i></td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td style="font-size:12px;">Sandy</td>
                                            <td><i class="mdi mdi-check"></i></td>
                                            <td><i class="fa fa-ban"></i></td>
                                            <td><i class="fa fa-ban"></i></td>
                                            <td><i class="fa fa-ban"></i></td>
                                            <td><i class="mdi mdi-check"></i></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane p-20" id="workflow" role="tabpanel">
                                    <div class="ml-3 mr-3">
                                        <form action="" method="post">
                                            <div class="form-group">
                                                <label>Workflow : </label>
                                                <select class="form-control" name="" id="">
                                                    <option option="1">Review And Approve (single reviewer)</option>
                                                </select>
                                            </div>
                                            <h6>General</h6>
                                            <hr>
                                            <div class="form-group">
                                                <label for="">Message</label>
                                                <textarea class="form-control" name="" id="" cols="30"
                                                          rows="8"></textarea>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="">Due</label>
                                                        <input type="text" class="form-control datepicker" name=""
                                                               id="">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="">Priority</label>
                                                        <select class="form-control" name="" id="">
                                                            <option value="">High</option>
                                                            <option value="">Medium</option>
                                                            <option value="">Low</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <h6 class="mt-3">Items</h6>
                                            <hr>
                                            <strong>items *</strong>
                                            <table style="font-size:12px;">
                                                <tbody>
                                                <tr>
                                                    <td><i class="fa fa-file"></i></td>
                                                    <td>
                                                        <strong>HW-SW-SPEC.pdf</strong> <br>
                                                        Description: (None) <br>
                                                        Modified on : Mon 20 Apr 2020 10:30
                                                    </td>
                                                    <td>
                                                        <i class="fa fa-arrow-circle-right"></i> View More Action<br>
                                                        <i class="far fa-times-circle"></i> Remove
                                                    </td>
                                                </tr>
                                                <tr class="mt-3">
                                                    <td><i class="fa fa-file"></i></td>
                                                    <td>
                                                        <strong>HW-SW-SPEC.pdf</strong> <br>
                                                        Description: (None) <br>
                                                        Modified on : Mon 20 Apr 2020 10:30
                                                    </td>
                                                    <td>
                                                        <i class="fa fa-arrow-circle-right"></i> View More Action<br>
                                                        <i class="far fa-times-circle"></i> Remove
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div class="mt-3">
                                                <button class="btn btn-primary">Add</button>
                                                <button class="btn btn-primary ml-3">Remove All</button>
                                            </div>
                                            <h6 class="mt-3">Other Options</h6>
                                            <hr>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value=""
                                                       id="defaultCheck1">
                                                <label class="form-check-label" for="defaultCheck1">
                                                    Send Email Notification
                                                </label>
                                            </div>
                                            <div class="mt-3 mb-3">
                                                <button class="btn btn-primary">Start Workflow</button>
                                                <button class="btn btn-light">Cancel</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div> <!-- end detail -->


                        </div>
                    </div>

                    <div class="preview-placeholder card-body image-viewer">
                        <img src="" class="image-link" style="max-width: 100%" alt="">
                    </div>

                    <div class="preview-placeholder card-body pdf-viewer">
                        <div class="pdf-link"></div>
                    </div>

                    <div class="preview-placeholder card-body txt-viewer">
                        <textarea class="txt-content" rows="30" style="width: 100%"></textarea>
                    </div>


                {{-- <div class="iframe"> --}}

                <!-- <iframe class="doc" src="https://docs.google.com/gview?url=http://localhost/dms/public/documents/1616996140-MRENLIUV4W.pdf&embedded=true" height="500" width="100%" scrolling="auto"></iframe> -->

                    <!-- <embed src="http://localhost/dms/public/documents/1617161760-LI5XCYPXWM.docx" type="application/docx"   height="500px" width="100%"> -->

                    <!-- <iframe src='https://view.officeapps.live.com/op/embed.aspx?src=http://localhost/dms/public/documents/1617161760-LI5XCYPXWM.docx' width='1366px' height='623px' frameborder='0'>This is an embedded <a target='_blank' href='http://office.com'>Microsoft Office</a> document, powered by <a target='_blank' href='http://office.com/webapps'>Office Online</a>.</iframe> -->
                    {{-- <iframe src ="public/plugins/ViewerJS/#1616996140-MRENLIUV4W.pdf" width='400' height='300' allowfullscreen webkitallowfullscreen></iframe> --}}
                    {{-- </div> --}}


                    <div class="card action">
                        <div class="card-body">
                            <h5>Nama Dokumen : <span class="fill-doc-name"></span></h5>
                            <hr>
                            <h4 class="m-b-20">Choose Action</h4>
                            <!-- Accordian -->
                            <div id="accordion" class="nav-accordion" role="tablist" aria-multiselectable="true">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                               href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                Move Document
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="card-body">
                                            <label>Move To</label><br>
                                            <form action="{{ urlWithPrefix('move-folder') }}" method="post">
                                                @csrf
                                                <input type="hidden" name="id_file" value="" class="fill-fileid">
                                                <select class="form-control select2" name="new_directory_id"
                                                        style="width: 100%">
                                                    {!! $directory_selection !!}
                                                </select>
                                                <input type="submit" value="Move File" class="btn btn-primary">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingTwo">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                               href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                Duplicate Document
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="card-body">
                                            <label>Copy To</label><br>
                                            <form action="{{ urlWithPrefix('duplicate-file') }}" method="post">
                                                @csrf
                                                <input type="hidden" name="id_file" value="" class="fill-fileid">
                                                <select class="form-control select2" name="new_directory_id"
                                                        style="width: 100%">
                                                    {!! $directory_selection !!}
                                                </select>
                                                <input type="submit" value="Duplicate File" class="btn btn-primary">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                               href="#collapseThree" aria-expanded="false"
                                               aria-controls="collapseThree">
                                                Share Document
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" role="tabpanel"
                                         aria-labelledby="headingThree">
                                        <div class="card-body">
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#sharemail"
                                                       role="tab"><span class="hidden-sm-up"><i
                                                                    class="ti-home"></i></span> <span
                                                                class="hidden-xs-down">Share via Email</span></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#sharelink"
                                                       role="tab"><span class="hidden-sm-up"><i
                                                                    class="ti-user"></i></span> <span
                                                                class="hidden-xs-down">Share Link</span></a>
                                                </li>
                                            </ul>
                                            <!-- Tab panes -->
                                            <div class="tab-content tabcontent-border" style="overflow-x:auto;">
                                                <div class="tab-pane p-20 active" id="sharemail" role="tabpanel">
                                                    <form action="{{ urlWithPrefix('send-email') }}" method="post">
                                                        @csrf
                                                        <label>Email :</label>
                                                        <input type="email" required="required" name="email"
                                                               class="form-control" placeholder="Email"><br>
                                                        <label>Subject :</label>
                                                        <input type="text" required="required" name="subject"
                                                               class="form-control" placeholder="Subject"><br>
                                                        <label>Body :</label>
                                                        <textarea rows="8" class="form-control fill-email"
                                                                  name="message" required="required"
                                                                  placeholder="Body"></textarea><br><br>
                                                        <input type="submit" value="submit" class="btn btn-primary">
                                                    </form>
                                                </div>
                                                <div class="tab-pane p-20" id="sharelink" role="tabpanel">
                                                    <label>Copy Link</label>
                                                    <input type="text" class="form-control fill-fileurl"
                                                           style="cursor: pointer;" readonly="readonly" value=""><br>
                                                    <span class="copied"
                                                          style="display: none">Copied to Clipboard</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingFour">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                               href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                Delete Document
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseFour" class="collapse" role="tabpanel"
                                         aria-labelledby="headingFour">
                                        <div class="card-body">
                                            Apakah Anda yakin ingin menghapus dokumen ?<br>
                                            <a href="#" class="btn btn-danger fill-trashid">Ya</a>
                                            <a href="#" class="btn btn-primary">Tidak</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('scripts')
    <script>
        function tes() {
            console.log("tes111");
            $('#table-note').DataTable().columns.adjust();
        }

        $(document).ready(function () {

            var tt = "<?php if(isset($tab_detail)){echo $tab_detail;}  ?>";

            if(tt){
                console.log("Tab aktif");

                $('#myTab a[href="#detail"]').trigger('click');
            }


            // var url_string = window.location.href; //
            // var url = new URL(url_string);
            // var paramValue = url.searchParams.get("content");
            var paramValue = '{!! \Session::get('content') !!}';
            if(paramValue) {
                detail(paramValue);

                $('a[href="#notelist"]').trigger('click');
                myTimeout = setTimeout(tes, 1000);
            }

            console.log(paramValue);


        });

        $('#docs').DataTable({
            "searching": false
        });
        var lol = $('#table-note').DataTable({
            recordsFiltered: 10,
            scrollX: true,
            sScrollXInner: "100%",
            autoWidth: false,

        });

        $('#example_history').DataTable({
            recordsFiltered: 10,
            scrollX: true,
        });

        $("a[href='#notelist']").on('shown.bs.tab', function (e) {
            $('#table-note').DataTable().columns.adjust().draw();

        });

        $("a[href='#history']").on('shown.bs.tab', function (e) {
            $('#example_history').DataTable().columns.adjust().draw();

        });
    </script>
@endsection