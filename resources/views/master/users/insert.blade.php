@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">User Management</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">User Management</li>
                </ol>
            </div>
        </div>
        @include('include/material/alert')
        <div class="alert alert-success alert-dismissible upload-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>Success</h5>
            <p class="message"></p>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body bg-megna">
                        <div class="d-flex justify-content-between">
                            <h4 class="text-white card-title">User Management - Create</h4>
                            
                        </div>
                    </div>
                    <div class="card-body">
                        @if($action == 'store')
                        <form action="{{urlWithPrefix('master/users/store')}}" method="post" id="form-user">
                        @elseif($action == 'update')
                        <form action="{{urlWithPrefix('master/users/update', $users->id)}}" method="post">
                        @method('PUT')
                        @endif
                        @csrf
                            <input type="hidden" name="id" value="{{@$users->id}}">
                          <div class="row">
                              <div class="col-md-3">Name</div>
                              <div class="col-md-9"><input type="text" name="name" id="" value="{{@$users->name}}" class="form-control"></div>
                          </div>
                          <br>
                          <div class="row">
                                <div class="col-md-3">Level</div>
                                <div class="col-md-9">
                                  <select name="level" id="" class="form-control select2">
                                       <option value="">Pilih</option>
                                        @if (!empty($level))
                                            @foreach ($level as $row)
                                            <option value="{{$row->id}}" {{@$users->level == $row->id ?'selected':''}}>{{$row->name}}</option>
                                            @endforeach
                                        @endif
                                  </select>
                                </div>
                          </div>
                          <br>
                          <div class="row">
                              <div class="col-md-3">Email</div>
                              <div class="col-md-9"><input type="text" name="email" id="" class="form-control" value="{{@$users->email}}" autocomplete="off"></div>
                          </div>
                          <br>
                          <div class="row">
                              <div class="col-md-3">Password</div>
                              <div class="col-md-9"><input type="password" name="password" id="" class="form-control" value="" autocomplete="new-password"></div>
                          </div>
                          <br>
                          <div class="row">
                              <div class="col-md-3">Phone</div>
                              <div class="col-md-9"><input type="text" name="phone" id="" value="{{@$users->phone}}" class="form-control"></div>
                          </div>
                          <br>
                          <div class="row">
                              <div class="col-md-3">Join Date</div>
                              <div class="col-md-9"><input type="text" name="join_date" id="" value="{{@$users->join_date}}" class="form-control datepicker"></div>
                          </div>
                          <br>
                          <div class="row">
                              <div class="col-md-3">Created At</div>
                              <div class="col-md-9"><input type="text" name="created_at" id="" value="{{@$users->created_at}}" class="form-control datepicker"></div>
                          </div>
                          <br>
                          <div class="row">
                              <div class="col-md-3">Updated At</div>
                              <div class="col-md-9"><input type="text" name="updated_at" id="" value="{{@$users->updated_at}}" class="form-control datepicker"></div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-md-3">Status</div>
                            <div class="col-md-9">
                                <select name="status" id="" class="form-control select2">
                                    <option value="1" {{(@$users->status == '1')?'selected':''}}>Active</option>
                                    <option value="2" {{(@$users->status == '2')?'selected':''}}>Not Active</option>
                                </select>
                            </div>
                          </div>
                          <br>
                          <div align="right">
                                <button data-toggle="modal" data-target="#exampleModal" type="button" class="btn btn-primary">Submit</a>
                                <button type="reset" class="btn btn-danger  ml-5">Reset</a>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Anda yakin akan {{$action == 'store' ? 'menambahkan' : 'mengedit'}} data?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
