<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ResetController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/getIdFileNt', 'NoteListController@getId');
Route::post('/getSeluruNoteById', 'NoteListController@getNoteListById');
//Route::get("/reset-acp", [ResetController::class, 'reset']);
$globalRoutes = function(){


    Route::get('/', 'DashboardController@index');
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/dashboard/dark', 'DashboardController@dark')->name('dark');
    Route::get('/dashboard/folder', 'RootController@get_file_home')->name('getFileFolder');
    Route::get('/dashboard/folder/sidebar', 'RootController@get_folder_sidebar')->name('getFolderSidebar');
    Route::get('/dashboard/folder/document_explorer', 'RootController@get_file_document_explorer')->name('getFileDocumentExplorer');

    Route::resource('/ext_properties', 'ExtentedPropertiesController')->except('show');
    Route::resource('/note_list', 'NoteListController')->except('show');
    Route::resource('/directory', 'DirectoryController')->except('show');
    Route::post('/directory/workflow', 'DirectoryController@workflow')->name('document.workflow');

    
    Route::get('/notifications', 'NotificationController@index')->name('notifications.index');

    Route::get('/users', 'UsersController@index')->name('users.index');
    Route::get('/users/create', 'UsersController@create')->name('users.create');
    Route::post('/users/store', 'UsersController@store')->name('users.store');
    Route::get('/users/edit/{id}', 'UsersController@edit')->name('users.edit');
    Route::put('/users/update/{id}', 'UsersController@update')->name('users.update');
    Route::get('/users/delete/{id}', 'UsersController@delete')->name('users.delete');
    Route::get('/users/verify/{id}', 'UsersController@verify')->name('users.verify');
    Route::put('/users/update_password/{id}', 'UsersController@updatePassword')->name('users.update.password');
    Route::get('/users/{id}/profile', 'UsersController@profile')->name('users.profile');
    Route::post('/users/changeprofile/{id}', 'UsersController@changeProfile')->name('users.profile.change');
    Route::get('/users/{id}/setting', 'UsersController@setting')->name('users.setting');
    Route::post('/users/accountsetting/{id}', 'UsersController@accountSetting')->name('users.setting.change');
    Route::post('/users/save-signature', 'UsersController@saveSignature')->name('users.signature.save');

    Route::get('/broadcast-message', 'BroadcastMessageController@index')->name('broadcast-message.index');
    Route::get('/broadcast-message/create', 'BroadcastMessageController@create')->name('broadcast-message.create');
    Route::post('/broadcast-message/store', 'BroadcastMessageController@store')->name('broadcast-message.store');
    Route::get('/broadcast-message/detail/{id}', 'BroadcastMessageController@show')->name('broadcast-message.detail_message');
    Route::get('/broadcast-message/reply/{id}', 'BroadcastMessageController@edit')->name('broadcast-message.reply');
    Route::post('/broadcast-message/reply/{id}', 'BroadcastMessageController@update')->name('broadcast-message.reply_message');

    Route::get('/private-message', 'PrivateMessageController@index')->name('private-message.index');
    Route::get('/private-message/create', 'PrivateMessageController@create')->name('private-message.create');
    Route::post('/private-message/store', 'PrivateMessageController@store')->name('private-message.store');
    Route::get('/private-message/detail/{id}', 'PrivateMessageController@show')->name('private-message.detail_message');
    Route::get('/private-message/reply/{id}', 'PrivateMessageController@edit')->name('private-message.reply');
    Route::post('/private-message/reply/{id}', 'PrivateMessageController@update')->name('private-message.reply_message');

    Route::get('/root', 'RootController@index')->name('root');
    Route::get('/trash', 'TrashController@index')->name('trash');
    Route::get('/document-search', 'DocumentSearchController@index')->name('document-search');
    Route::post('/document-search/store', 'DocumentSearchController@store')->name('document-search.store');
    Route::put('/document-search/search_detail', 'DocumentSearchController@search_detail')->name('document-search.search_detail');
    Route::get('/workflow', 'WorkflowController@index')->name('workflow');
    Route::get('/workflow/create', 'WorkflowController@create')->name('workflow-create');
    Route::get('/workflow/get_detail/{id}', 'WorkflowController@show')->name('workflow-show');
    Route::get('/workflow/detail/{id}', 'WorkflowController@detail')->name('workflow-detail');
    Route::post('/workflow/store', 'WorkflowController@store')->name('workflow-store');
    Route::post('/workflow/store_detail', 'WorkflowController@store_detail')->name('workflow-store_detail');
    Route::post('/workflow/approve', 'WorkflowController@approve')->name('workflow-approve');
    Route::post('/workflow/getWorkflowInfo', 'WorkflowController@getWorkflowInfo')->name('workflow-getWorkflowInfo');
    Route::post('/workflow/getWorkflowDetail', 'WorkflowController@getWorkflowDetail')->name('workflow-getWorkflowDetail');
    Route::post('/workflow/getWorkflowDetailStatus', 'WorkflowController@getWorkflowDetailStatus')->name('workflow-getWorkflowDetailStatus');
    Route::get('/root/iframe', 'RootController@iframe')->name('root.iframe');
    Route::post('/root/get_file', 'RootController@get_file')->name('root.get_file');
    Route::post('/root/get_sub', 'RootController@get_sub')->name('root.get_sub');
    Route::get('/root/detail/{id}', 'RootController@detail')->name('root.detail');
    Route::get('/root/delete/{id}', 'RootController@delete')->name('root.delete');
    Route::post('/root/storeAccessRight', 'RootController@storeAccessRight')->name('root.storeAccessRight');
    Route::post('/root/getAccessRight', 'RootController@getAccessRight')->name('root.getAccessRight');
    Route::post('/root/getAccessRightDownload', 'RootController@getAccessRightDownload')->name('root.getAccessRightDownload');
    Route::post('/root/getDataFile', 'RootController@getDataFile')->name('root.getDataFile');
    Route::post('/root/getHistory', 'RootController@getHistory')->name('root.getHistory');
    Route::get('/root/structure/{id}/relation', 'RootController@showRelationTree')->name('root.structure.relation');

    Route::get('/logout', 'AuthController@logout');
    Route::get('/guest/register', 'AuthController@showRegisterForm');
    Route::post('/guest/register', 'AuthController@registerUser');
    Route::get('/guest/verify/{id}', 'AuthController@verify')->name('guest.verify');
    Route::put('/guest/update_password/{id}', 'AuthController@updatePassword')->name('guest.update.password');

    Route::post('/file_upload', 'FileUploadController@store')->name('file_upload.store');


    // FROM NIKKO
    Route::get('/move-trash/{id}', 'TrashController@move_to_trash');
    Route::get('/restore-trash/{id}', 'TrashController@restore_from_trash');
    Route::get('/remove-trash/{id}', 'TrashController@permanent_delete_from_trash');

    Route::get('/file-information/{id}', 'FileUploadController@show_file_info');
    Route::get('/workflow-information/{id}', 'FileUploadController@show_workflow_upload_info');
    Route::get('/purchasing-information/{id}', 'FileUploadController@show_purchasing_upload_info');
    Route::post('/move-folder', 'FileUploadController@change_file_directory');
    Route::post('/duplicate-file', 'FileUploadController@duplicate_file');
    Route::post('/reupload-file', 'FileUploadController@reUploadDocument')->name('upload-file.reupload');
    Route::get('/version/log', 'FileUploadController@getVersionLog')->name('upload-file.versionlog');
    Route::get('/dokumen/relation', 'FileUploadController@getRelatedDokumen')->name('getRelatedDokumen');
    Route::post('/dokumen/relation/store', 'FileUploadController@relationStore')->name('documen-relation.store');
    Route::delete('/relation/delete', 'FileUploadController@relationDestroy')->name('documen-relation.delete');

    Route::post('/send-email', 'EmailController@sendEmail');

    Route::get('/test', 'RootController@test');

    /**
     * Route organization
     */
    Route::get('/organization', 'OrganizationController@index')->name('organization.index');
    Route::get('/organization/create', 'OrganizationController@create')->name('organization.create');
    Route::post('/organization/store', 'OrganizationController@store')->name('organization.store');
    Route::get('/organization/edit/{id}', 'OrganizationController@edit')->name('organization.edit');
    Route::put('/organization/update/{id}', 'OrganizationController@update')->name('organization.update');
    Route::get('/organization/delete/{id}', 'OrganizationController@delete')->name('organization.delete');
    Route::get('/organization/user-list/{id}', 'OrganizationController@userList')->name('organization.userlist');
    Route::post('organization/logs', 'Master\OrganizationController@statusLog')->name('organization.statuslog');    

    /**
     * Route sub organization
     */
    Route::get('/sub_organization', 'SubOrganizationController@index')->name('sub_organization.index');
    Route::get('/sub_organization/create', 'SubOrganizationController@create')->name('sub_organization.create');
    Route::post('/sub_organization/store', 'SubOrganizationController@store')->name('sub_organization.store');
    Route::get('/sub_organization/edit/{id}', 'SubOrganizationController@edit')->name('sub_organization.edit');
    Route::put('/sub_organization/update/{id}', 'SubOrganizationController@update')->name('sub_organization.update');
    Route::get('/sub_organization/delete/{id}', 'SubOrganizationController@delete')->name('sub_organization.delete');

    /**
     * Route pending user
     */
    Route::get('/pending_user', 'PendingUserController@index')->name('pending_user.index');
    Route::get('/pending_user/show/{id}', 'PendingUserController@show')->name('pending_user.show');
    Route::put('/pending_user/update/{id}', 'PendingUserController@update')->name('pending_user.update');

    /**
     * Route eform
     */
    Route::get('/eform', 'EformController@index')->name('eform.index');
    Route::get('/eform/create', 'EformController@create')->name('eform.create');
    Route::post('/eform/store', 'EformController@store')->name('eform.store');
    Route::post('/eform/import', 'EformController@importToDB')->name('eform.import');
    Route::get('/eform/show/{slug}', 'EformController@show')->name('eform.show');
    Route::get('/eform/edit/{id}', 'EformController@edit')->name('eform.edit');
    Route::put('/eform/update/{id}', 'EformController@update')->name('eform.update');
    Route::get('/eform/delete/{id}', 'EformController@delete')->name('eform.delete');
    Route::post('/eform/submit', 'EformController@submit')->name('eform.submit')->middleware('eform.submission');
    Route::get('/eform/answer/{id}', 'EformController@answer')->name('eform.answer');
    Route::get('/eform/generate/{type}/{answer_id}', 'EformController@answerToDocx')->name('eform.generate');
    Route::get('/eform/dashboard/{id}', 'EformController@dashboard')->name('eform.dashboard');
    Route::post('/eform/softdelete', 'EformController@inactive')->name('eform.inactive');
    Route::post('/eform/answer/import', 'EformController@import')->name('eform.answer.import');
    Route::post('/eform/answer/ots-ip/{form_id}', 'EformController@otsIp')->name('eform.answer.ots-ip');
    Route::post('/eform/editor/image', 'EformController@fileUpload')->name('eform.editor.image');
    Route::get('/eform/duplicate/{slug}', 'EformController@duplicate')->name('eform.duplicate');

    /**
     * Route approval workflow
     */
    Route::get('/approval-workflow', 'ApprovalWorkflowController@index')->name('approval_workflow.index');
    Route::get('/approval-workflow/show', 'ApprovalWorkflowController@show')->name('approval_workflow.show');
    Route::post('/approval-workflow/approval', 'ApprovalWorkflowController@approval')->name('approval_workflow.approval');
    Route::post('/approval-workflow/getWorkflowApprovalDetail', 'ApprovalWorkflowController@getWorkflowApprovalDetail')->name('approval_workflow.getWorkflowApprovalDetail');
    Route::post('/approval-workflow/getPinUser', 'ApprovalWorkflowController@getPinUser')->name('approval_workflow.getPinUser');
    Route::post('/approval-workflow/signature/save', 'ApprovalWorkflowController@saveSignature')->name('approval_workflow.signature.save');

    /**
     * Route add on setting
     */
    Route::get('/add-on-setting', 'AddOnSettingController@index')->name('add_on_setting.index');
    Route::get('/add-on-setting/organization-access', 'AddOnSettingController@organization_access')->name('add_on_setting.organization_access');
    Route::get('/add-on-setting/set-organization-access/{id}', 'AddOnSettingController@set_organization_access')->name('add_on_setting.set_organization_access');
    Route::post('/add-on-setting/store-organization-access', 'AddOnSettingController@store_organization_access')->name('add_on_setting.store_organization_access');
    Route::get('/add-on-setting/create', 'AddOnSettingController@create')->name('add_on_setting.create');
    Route::post('/add-on-setting/store', 'AddOnSettingController@store')->name('add_on_setting.store');
    Route::get('/add-on-setting/edit/{id}', 'AddOnSettingController@edit')->name('add_on_setting.edit');
    Route::get('/add-on-setting/delete/{id}', 'AddOnSettingController@delete')->name('add_on_setting.delete');

     /**
     * Route list ip
     */
    Route::get('/list_ip', 'Master\ListIpController@index')->name('list_ip.index');
    Route::get('/list_ip/create', 'Master\ListIpController@create')->name('list_ip.create');
    Route::post('/list_ip/store', 'Master\ListIpController@store')->name('list_ip.store');
    Route::get('/list_ip/edit/{id}', 'Master\ListIpController@edit')->name('list_ip.edit');
    Route::get('/list_ip/delete/{id}', 'Master\ListIpController@delete')->name('list_ip.delete');

     /**
     * Route users
     */
    Route::get('master/users', 'Master\UsersController@index')->name('master_users.index');
    Route::get('master/users/create', 'Master\UsersController@create')->name('master_users.create');
    Route::post('master/users/store', 'Master\UsersController@store')->name('master_users.store');
    Route::get('master/users/edit/{id}', 'Master\UsersController@edit')->name('master_users.edit');
    Route::put('master/users/update/{id}', 'Master\UsersController@update')->name('master_users.update');
    Route::get('master/users/active_inactive/{id}', 'Master\UsersController@active_inactive')->name('master_users.active_inactive');
    Route::get('master/users/delete/{id}', 'Master\UsersController@delete')->name('master_users.delete');


     /**
     * Route trouble ticket list
     */
    Route::get('/trouble_ticket_list', 'TroubleTicketListController@index')->name('trouble_ticket_list.index');
    Route::get('/trouble_ticket_list/create', 'TroubleTicketListController@create')->name('trouble_ticket_list.create');
    Route::post('/trouble_ticket_list/store', 'TroubleTicketListController@store')->name('trouble_ticket_list.store');
    Route::get('/trouble_ticket_list/edit/{id}', 'TroubleTicketListController@edit')->name('trouble_ticket_list.edit');
    Route::get('/trouble_ticket_list/delete/{id}', 'TroubleTicketListController@delete')->name('trouble_ticket_list.delete');

    /**
     * Route add on setting
     */
    Route::get('/level', 'LevelController@index')->name('level.index');
    Route::get('/level/create', 'LevelController@create')->name('level.create');
    Route::post('/level/store', 'LevelController@store')->name('level.store');
    Route::get('/level/edit/{id}', 'LevelController@edit')->name('level.edit');
    Route::post('/level/update/{id}', 'LevelController@update')->name('level.update');

    /**
     * Route API
     */
    Route::get('/api/getAccessRight', 'ApiController@getAccessRight')->name('api.getAccessRight');
    Route::get('/api/getAccessRightById/{id}', 'ApiController@getAccessRightById')->name('api.getAccessRightById');
    Route::get('/api/getDirectory', 'ApiController@getDirectory')->name('api.getDirectory');
    Route::get('/api/getDirectoryById/{id}', 'ApiController@getDirectoryById')->name('api.getDirectoryById');
    Route::get('/api/getEform', 'ApiController@getEform')->name('api.getEform');
    Route::get('/api/getEformById/{id}', 'ApiController@getEformById')->name('api.getEformById');
    Route::get('/api/getExtendProperties', 'ApiController@getExtendProperties')->name('api.getExtendProperties');
    Route::get('/api/getExtendPropertiesById/{id}', 'ApiController@getExtendPropertiesById')->name('api.getExtendPropertiesById');
    Route::get('/api/getFileUploads', 'ApiController@getFileUploads')->name('api.getFileUploads');
    Route::get('/api/getFileUploadsById/{id}', 'ApiController@getFileUploadsById')->name('api.getFileUploadsById');
    Route::get('/api/getHistory', 'ApiController@getHistory')->name('api.getHistory');
    Route::get('/api/getHistoryById/{id}', 'ApiController@getHistoryById')->name('api.getHistoryById');
    Route::get('/api/getMessage', 'ApiController@getMessage')->name('api.getMessage');
    Route::get('/api/getMessageById/{id}', 'ApiController@getMessageById')->name('api.getMessageById');
    Route::get('/api/getNoteList', 'ApiController@getNoteList')->name('api.getNoteList');
    Route::get('/api/getNoteListById/{id}', 'ApiController@getNoteListById')->name('api.getNoteListById');
    Route::get('/api/getOrganization', 'ApiController@getOrganization')->name('api.getOrganization');
    Route::get('/api/getOrganizationById/{id}', 'ApiController@getOrganizationById')->name('api.getOrganizationById');
    Route::get('/api/getRole', 'ApiController@getRole')->name('api.getRole');
    Route::get('/api/getRoleById/{id}', 'ApiController@getRoleById')->name('api.getRoleById');
    Route::get('/api/getUser', 'ApiController@getUser')->name('api.getUser');
    Route::get('/api/getUserById/{id}', 'ApiController@getUserById')->name('api.getUserById');
    Route::get('/api/getWorkflow', 'ApiController@getWorkflow')->name('api.getWorkflow');
    Route::get('/api/getWorkflowById/{id}', 'ApiController@getWorkflowById')->name('api.getWorkflowById');
    Route::get('/api/getWorkflowDetail', 'ApiController@getWorkflowDetail')->name('api.getWorkflowDetail');
    Route::get('/api/getWorkflowDetailById/{id}', 'ApiController@getWorkflowDetailById')->name('api.getWorkflowDetailById');

    /** signature */
    Route::post('/signature', 'SignatureController@signature')->name('signature')->withoutMiddleware(['auth']);

    /** eform public */
    Route::get('/eform/show/public/{slug}', 'Publics\EformController@show_public')->name('eform.show.public')->withoutMiddleware(['auth']);
    Route::post('/eform/submit/public', 'Publics\EformController@submit_public')->name('eform.submit.public')->middleware('eform.submission')->withoutMiddleware(['auth']);
    Route::get('/eform/submit/thankspage', 'Publics\EformController@thankspage')->name('eform.submit.thanks')->withoutMiddleware(['auth']);

    /**
     * config file
     */
    Route::get('/config', 'ConfigUploadController@index')->name('config.index');
    Route::get('/config/create', 'ConfigUploadController@create')->name('config.create');
    Route::get('/config/edit/{id}', 'ConfigUploadController@edit')->name('config.edit');
    Route::post('/config/store', 'ConfigUploadController@store')->name('config.store');
    Route::get('/config/delete/{id}', 'ConfigUploadController@destroy')->name('config.delete');

    //Master Admin Routes
    Route::post('users/logs', 'Master\UsersController@statusLog')->name('user.statuslog');
    Route::middleware(['masteradmin.only'])->group(function(){
        Route::get('/dashboard', 'DashboardController@index');
    });
};

Auth::routes();

Route::group( ['middleware' => 'auth'], $globalRoutes);
Route::group(['middleware' => ['auth:master', 'masteradmin.access'], 'prefix' => 'ma-mlk', 'as' => 'ma-mlk.'], $globalRoutes);

Route::prefix('ma-mlk')->group(function(){
    Route::get('login', function(){
        return view('auth.login');
    })->name('ma-mlk.login')->withoutMiddleware(['auth:master']);
    Route::post('login/attempt', 'Auth\LoginController@login')->name('ma-mlk.login.master')->withoutMiddleware(['auth:master']); 
});

Route::get('forget-password', 'Auth\ForgotPasswordController@showForgetPasswordForm')->name('forget.password.get');
Route::post('forget-password', 'Auth\ForgotPasswordController@submitForgetPasswordForm')->name('forget.password.post'); 
Route::get('reset-password/{token}', 'Auth\ForgotPasswordController@showResetPasswordForm')->name('reset.password.get');
Route::post('reset-password', 'Auth\ForgotPasswordController@submitResetPasswordForm')->name('reset.password.post');